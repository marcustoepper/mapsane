package de.aoemedia.ane.maps.overlays
{
	import de.aoemedia.ane.maps.LatLng;
	import de.aoemedia.ane.maps.Map;

	public class Marker
	{
		public var _myId:int;
		public var latLng:LatLng;
		public var title:String="";
		public var subtitle:String="";
		public var fillColor:uint=MarkerStyles.MARKER_COLOR_RED;
		
		public var annotationTitle:String="default_anno_title";
		public var annotationSubtitle:String="default_anno_subtitle";
		
		private static var nextId:int=0;
		
		public function Marker(latLng:LatLng)
		{
			this.latLng=latLng;
			
			// this is reset after instantiation in Map-addMarker. (AOE extension)
			this._myId=getNextMarkerId();
		}
		private static function getNextMarkerId():int
		{
			return nextId++;	
		}
		public function get myId():int
		{
			return _myId;
		}
		public function openInfoWindow():void
		{
			Map.getContext().call("openMarker",myId);
		}
		public function closeInfoWindow():void
		{
			Map.getContext().call("closeMarker",myId);
		}
	}
}