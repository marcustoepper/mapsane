package de.aoemedia.ane.maps {
	/**
	 * An Map API to communicate with the Map
	 */
	public interface IMap {
		/**
		 * Sets the Center of the Map
		 * @param latitude
		 * @param longitude
		 */
		function resetCenter(latitude : Number, longitude : Number) :void;

		/**
		 * Removes all Markers and Overlays from the map
		 */
		function clear() :void;

		/**
		 *
		 * @param id 				An Id
		 * @param latitude
		 * @param longitude
		 * @param annotationContent	{title:"some title", subtitle:"some subtitle"}
		 * @param calloutContent	{title:"some title", subtitle:"some subtitle"}
		 */
		function addMarker(id : int, latitude : Number, longitude : Number, annotationContent : Object, calloutContent : Object) :void;
	}
}
