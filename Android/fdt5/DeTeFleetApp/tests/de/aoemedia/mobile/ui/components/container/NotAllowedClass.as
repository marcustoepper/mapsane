package de.aoemedia.mobile.ui.components.container {
import de.aoemedia.mobile.ui.components.ILayoutComponent;

public class NotAllowedClass implements ILayoutComponent {

    public function get paddingLeft():Number {
        return 0;
    }

    public function set paddingLeft(paddingLeft:Number):void {
    }

    public function get paddingRight():Number {
        return 0;
    }

    public function set paddingRight(paddingRight:Number):void {
    }

    public function get paddingTop():Number {
        return 0;
    }

    public function set paddingTop(paddingTop:Number):void {
    }

    public function get paddingBottom():Number {
        return 0;
    }

    public function set paddingBottom(paddingBottom:Number):void {
    }

    public function get y():Number {
        return 0;
    }

    public function set y(y:Number):void {
    }

    public function get x():Number {
        return 0;
    }

    public function set x(x:Number):void {
    }

    public function get width():Number {
        return 0;
    }

    public function get height():Number {
        return 0;
    }

    public function get left():Number {
        return 0;
    }

    public function set left(left:Number):void {
    }

    public function get right():Number {
        return 0;
    }

    public function set right(right:Number):void {
    }

    public function get marginLeft():Number {
        return 0;
    }

    public function set marginLeft(marginLeft:Number):void {
    }

    public function get marginRight():Number {
        return 0;
    }

    public function set marginRight(marginRight:Number):void {
    }

    public function get marginTop():Number {
        return 0;
    }

    public function set marginTop(marginTop:Number):void {
    }

    public function get marginBottom():Number {
        return 0;
    }

    public function set marginBottom(marginBottom:Number):void {
    }

    public function get scaleFactor():Number {
        return 0;
    }

    public function set scaleFactor(scaleFactor:Number):void {
    }
}

}
