package de.aoemedia.mobile.ui.components.container {
import flash.display.Sprite;

import org.flexunit.asserts.assertEquals;
import org.flexunit.asserts.assertFalse;
import org.flexunit.asserts.assertTrue;

public class LayoutContainerTest {

    private var container:LayoutContainer;
    private var sprite:Sprite;
    private const scaleFactor:Number = 2;

    [Before]
    public function setUp():void {
        sprite = new Sprite();
        LayoutContainer.scaleFactor = scaleFactor;
        container = new LayoutContainer(sprite);
    }

    [After]
    public function tearDown():void {
    }

    [Test]
    public function itemsAddedAtTheEndOfDisplayList():void {
        var item:MockLayoutItem = new MockLayoutItem(10, 10);
        container.addItem(new MockLayoutItem(10, 10));
        container.addItem(new MockLayoutItem(20, 20));
        container.addItem(item);

        assertEquals(container.getItemAt(2), item);
    }

    [Test]
    public function allItemsAdded():void {
        var item:MockLayoutItem = new MockLayoutItem(10, 10);
        container.addItem(new MockLayoutItem(10, 10));
        container.addItem(new MockLayoutItem(20, 20));
        container.addItem(item);

        assertEquals(container.numberItems, 3);
    }

    [Test]
    public function addItemAtThirdPlace():void {
        var item:MockLayoutItem = new MockLayoutItem(10, 10);
        container.addItem(new MockLayoutItem(10, 10));
        container.addItem(new MockLayoutItem(20, 20));
        container.addItem(new MockLayoutItem(20, 20));
        container.addItem(new MockLayoutItem(20, 20));
        container.addItem(new MockLayoutItem(20, 20));
        container.addItemAt(item, 2);

        assertEquals(container.getItemAt(2), item);
    }

    [Test(expects="Error")]
    public function addNotAllowedItem():void {
        container.addItem(new NotAllowedClass());
    }

    [Test]
    public function removeItemAtThirdPlace():void {
        var item:MockLayoutItem = new MockLayoutItem(10, 10);
        container.addItem(new MockLayoutItem(10, 10));
        container.addItem(new MockLayoutItem(20, 20));
        container.addItem(new MockLayoutItem(20, 20));
        container.addItem(new MockLayoutItem(20, 20));
        container.addItemAt(item, 2);

        assertEquals(container.numberItems, 5);
        container.removeItem(item);

        assertFalse(container.getItemAt(2) == item);
        assertEquals(container.numberItems, 4);

    }

    [Test]
    public function noItemsAfterClear():void {
        var item:MockLayoutItem = new MockLayoutItem(10, 10);
        container.addItem(new MockLayoutItem(10, 10));
        container.addItem(new MockLayoutItem(20, 20));
        container.addItem(new MockLayoutItem(20, 20));
        container.addItem(new MockLayoutItem(20, 20));
        container.addItemAt(item, 2);

        assertEquals(container.numberItems, 5);
        container.clear();
        assertEquals(container.numberItems, 0);
    }

    [Test]
    public function noItemsInViewAfterClear():void {
        var item:MockLayoutItem = new MockLayoutItem(10, 10);
        container.addItem(new MockLayoutItem(10, 10));
        container.addItem(new MockLayoutItem(20, 20));
        container.addItem(new MockLayoutItem(20, 20));
        container.addItem(new MockLayoutItem(20, 20));
        container.addItemAt(item, 2);
        container.addItemsToView();

        assertEquals(container.view.numChildren, 5);
        container.clear();
        assertEquals(container.view.numChildren, 0);
    }

    [Test]
    public function keepChildInChildContainerAfterClear():void {
        var secondContainer:LayoutContainer = addItemsAndContainerWithItemsToContainer();

        container.clear();
        assertEquals(container.numberItems, 0);
        assertEquals(secondContainer.numberItems, 2);
    }

    [Test]
    public function removeChildsInChildContainerAfterrecursiveClear():void {
        var secondContainer:LayoutContainer = addItemsAndContainerWithItemsToContainer();

        container.clear(true);
        assertEquals(container.numberItems, 0);
        assertEquals(secondContainer.numberItems, 0);
    }

    private function addItemsAndContainerWithItemsToContainer():LayoutContainer {
        var secondContainer:LayoutContainer = new LayoutContainer(sprite);
        secondContainer.addItem(new MockLayoutItem(10, 10));
        secondContainer.addItem(new MockLayoutItem(10, 10));
        container.addItem(secondContainer);

        assertEquals(secondContainer.numberItems, 2);

        container.addItem(new MockLayoutItem(10, 10));
        container.addItem(new MockLayoutItem(20, 20));
        container.addItem(new MockLayoutItem(20, 20));
        container.addItem(new MockLayoutItem(20, 20));

        container.addItemsToView();

        assertEquals(container.numberItems, 5);

        return secondContainer;
    }

    [Test]
    public function checkIfItemsAddedToView():void {
        container.addItem(new MockLayoutItem(10, 10));
        container.addItem(new MockLayoutItem(20, 20));
        container.addItem(new MockLayoutItem(20, 20));
        container.addItem(new MockLayoutItem(20, 20));

        container.addItemsToView();

        assertEquals(container.view.numChildren, 4);
    }

    [Test]
    public function checkIfItemsOfChildLayoutContainerAddedToView():void {
        var secondContainer:LayoutContainer = new LayoutContainer(sprite);
        secondContainer.addItem(new MockLayoutItem(10, 10));
        secondContainer.addItem(new MockLayoutItem(10, 10));
        container.addItem(secondContainer);

        container.addItem(new MockLayoutItem(10, 10));
        container.addItem(new MockLayoutItem(20, 20));
        container.addItem(new MockLayoutItem(20, 20));
        container.addItem(new MockLayoutItem(20, 20));

        container.addItemsToView();

        assertEquals(6, container.view.numChildren);
    }

    [Test]
    public function checkIfLayoutContainerContainsItem():void {
        var item:MockLayoutItem = new MockLayoutItem(10, 10);
        container.addItem(item);
        assertTrue(container.contains(item));
    }

    [Test]
    public function checkIfLayoutContainerDoesntContainsItem():void {
        var item:MockLayoutItem = new MockLayoutItem(10, 10);
        assertFalse(container.contains(item));
    }

    [Test]
    public function isGapUsedVertical():void {
        var gap:Number = 20;
        var item1:MockLayoutItem = new MockLayoutItem(10, 10);
        var item2:MockLayoutItem = new MockLayoutItem(10, 10);

        container.gap = gap;
        container.addItem(item1);
        container.addItem(item2);

        assertEquals(item1.height + gap * scaleFactor, item2.y);
    }

    [Test]
    public function isGapUsedHorizontal():void {
        var gap:Number = 20;
        var item1:MockLayoutItem = new MockLayoutItem(10, 10);
        var item2:MockLayoutItem = new MockLayoutItem(10, 10);

        container.gap = gap;
        container.orientation = LayoutOrientation.HORIZONTAL;
        container.addItem(item1);
        container.addItem(item2);

        assertEquals(item1.width + gap * scaleFactor, item2.x);
    }

    [Test]
    public function isLayoutContainerPaddingLeftUsedVertical():void {
        var item1:MockLayoutItem = new MockLayoutItem(10, 10);
        var item2:MockLayoutItem = new MockLayoutItem(10, 10);
        var padding:Number = 20;

        container.paddingLeft = padding;
        container.addItem(item1);
        container.addItem(item2);

        assertEquals(padding * scaleFactor, item1.x);
        assertEquals(padding * scaleFactor, item2.x);
    }

    [Test]
    public function isLayoutContainerPaddingLeftUsedHorizontal():void {
        var item1:MockLayoutItem = new MockLayoutItem(10, 10);
        var item2:MockLayoutItem = new MockLayoutItem(10, 10);
        var padding:Number = 20;

        container.paddingLeft = padding;
        container.orientation = LayoutOrientation.HORIZONTAL;
        container.addItem(item1);
        container.addItem(item2);

        assertEquals(padding * scaleFactor, item1.x);
        assertTrue(padding * scaleFactor != item2.x);
    }

    [Test]
    public function isLayoutContainerPaddingRightUsedVertical():void {
        var item1:MockLayoutItem = new MockLayoutItem(10, 10);
        var item2:MockLayoutItem = new MockLayoutItem(10, 10);
        var padding:Number = 20;

        container.paddingRight = padding;
        container.addItem(item1);
        container.addItem(item2);

        assertEquals(padding * scaleFactor + item1.width, container.width);
    }

    [Test]
    public function isLayoutContainerPaddingRightUsedHorizontal():void {
        var item1:MockLayoutItem = new MockLayoutItem(10, 10);
        var item2:MockLayoutItem = new MockLayoutItem(10, 10);
        var padding:Number = 20;

        container.gap = 0;
        container.paddingLeft = padding;
        container.orientation = LayoutOrientation.HORIZONTAL;
        container.addItem(item1);
        container.addItem(item2);

        assertEquals(padding * scaleFactor + item1.width + item2.width, container.width);
    }

    [Test]
    public function isLayoutContainerPaddingTopUsedVertical():void {
        var item1:MockLayoutItem = new MockLayoutItem(10, 10);
        var item2:MockLayoutItem = new MockLayoutItem(10, 10);
        var padding:Number = 20;

        container.paddingTop = padding;
        container.addItem(item1);
        container.addItem(item2);

        assertEquals(padding * scaleFactor, item1.y);
        assertTrue(padding * scaleFactor != item2.y);

    }

    [Test]
    public function isLayoutContainerPaddingTopUsedHorizontal():void {
        var item1:MockLayoutItem = new MockLayoutItem(10, 10);
        var item2:MockLayoutItem = new MockLayoutItem(10, 10);
        var padding:Number = 20;

        container.paddingTop = padding;
        container.orientation = LayoutOrientation.HORIZONTAL;
        container.addItem(item1);
        container.addItem(item2);

        assertEquals(padding * scaleFactor, item1.y);
        assertEquals(padding * scaleFactor, item2.y);
    }

    [Test]
    public function isLayoutContainerPaddingBottomUsedVertical():void {
        var item1:MockLayoutItem = new MockLayoutItem(10, 10);
        var item2:MockLayoutItem = new MockLayoutItem(10, 10);
        var padding:Number = 20;

        container.gap = 0;
        container.paddingBottom = padding;
        container.addItem(item1);
        container.addItem(item2);

        assertEquals(padding * scaleFactor + item1.height + item2.height, container.height);
    }

    [Test]
    public function isLayoutContainerPaddingBottomUsedHorizontal():void {
        var item1:MockLayoutItem = new MockLayoutItem(10, 10);
        var item2:MockLayoutItem = new MockLayoutItem(10, 10);
        var padding:Number = 20;

        container.paddingBottom = padding;
        container.orientation = LayoutOrientation.HORIZONTAL;
        container.addItem(item1);
        container.addItem(item2);

        assertEquals(padding * scaleFactor + item1.height, container.height);
    }

    [Test]
    public function testInnerHeight():void {
        var item1:MockLayoutItem = new MockLayoutItem(40, 40);
        var item2:MockLayoutItem = new MockLayoutItem(10, 10);

        container.gap = 0;
        container.paddingTop = 10;
        container.paddingBottom = 10;
        container.addItem(item1);
        container.addItem(item2);

        var innerHeight:Number = item1.height + item2.height;
        assertEquals(innerHeight, container.innerHeight);
    }

    [Test]
    public function testInnerWidthHorizontal():void {
        var item1:MockLayoutItem = new MockLayoutItem(40, 40);
        var item2:MockLayoutItem = new MockLayoutItem(10, 10);

        container.orientation = LayoutOrientation.HORIZONTAL;
        container.gap = 0;
        container.paddingLeft = 10;
        container.paddingRight = 10;
        container.addItem(item1);
        container.addItem(item2);

        var innerWidth:Number = item1.width + item2.width;
        assertEquals(innerWidth, container.innerWidth);
    }

    [Test]
    public function testInnerWidthVertical():void {
        var item1:MockLayoutItem = new MockLayoutItem(40, 40);
        var item2:MockLayoutItem = new MockLayoutItem(10, 10);

        container.gap = 0;
        container.paddingLeft = 10;
        container.paddingRight = 10;
        container.addItem(item1);
        assertEquals(40, container.innerWidth);

        container.addItem(item2);
        assertEquals(40, container.innerWidth);
    }

    [Test]
    public function testInnerWidthOfChildContainerHorizontal():void {
        var item1:MockLayoutItem = new MockLayoutItem(40, 40);
        var item2:MockLayoutItem = new MockLayoutItem(10, 10);
        var secondContainer:LayoutContainer = new LayoutContainer(sprite);
        secondContainer.orientation = LayoutOrientation.HORIZONTAL;
        secondContainer.paddingLeft = 10;
        secondContainer.paddingRight = 10;
        secondContainer.gap = 0;
        secondContainer.addItem(item1);
        secondContainer.addItem(item2);

        container.orientation = LayoutOrientation.HORIZONTAL;
        container.gap = 0;
        container.paddingLeft = 10;
        container.paddingRight = 10;

        container.addItem(secondContainer);

        var innerWidth:Number = item1.width + item2.width;
        assertEquals(innerWidth, secondContainer.innerWidth);
    }

    [Test]
    public function checkExplicitHeight():void {
        container.explicitHeight = 100;
        assertEquals(100, container.height);

        var item1:MockLayoutItem = new MockLayoutItem(120, 120);

        container.gap = 0;
        container.paddingTop = 10;
        container.paddingBottom = 10;
        container.addItem(item1);

        assertEquals(100, container.height);
    }

    [Test]
    public function checkExplicitWidth():void {
        container.explicitWidth = 100;
        assertEquals(100, container.width);

        var item1:MockLayoutItem = new MockLayoutItem(120, 120);

        container.gap = 0;
        container.paddingTop = 10;
        container.paddingBottom = 10;
        container.addItem(item1);

        assertEquals(100, container.width);
    }

    [Test]
    public function checkPercentalWidth():void {
        sprite.graphics.drawRect(0, 0, 100, 100);
        assertEquals(100, sprite.width);

        container.percentalWidth = 100;
        assertEquals(100, container.width);

        container.percentalWidth = 50;
        assertEquals(50, container.width);

        var item1:MockLayoutItem = new MockLayoutItem(120, 120);

        container.gap = 0;
        container.paddingTop = 10;
        container.paddingBottom = 10;
        container.addItem(item1);

        assertEquals(50, container.width);
        assertEquals(160, container.height);
    }

    [Test]
    public function checkPercentalHeight():void {
        sprite.graphics.drawRect(0, 0, 100, 100);
        assertEquals(100, sprite.height);

        container.percentalHeight = 100;
        assertEquals(100, container.height);

        container.percentalHeight = 50;
        assertEquals(50, container.height);

        var item1:MockLayoutItem = new MockLayoutItem(120, 120);

        container.gap = 0;
        container.paddingTop = 10;
        container.paddingBottom = 10;
        container.addItem(item1);

        assertEquals(50, container.height);
        assertEquals(120, container.width);
    }

    [Test]
    public function addItem50PxFromLeftVertical():void {
        var item1:MockLayoutItem = new MockLayoutItem(120, 120);
        item1.left = 50;

        container.gap = 0;
        container.paddingLeft = 10;
        container.addItem(item1);

        assertEquals(item1.left * scaleFactor, item1.x);
        assertEquals(item1.width + item1.left * scaleFactor, container.width);
    }

    [Test]
    public function addItem50PxFromLeftHorizontal():void {
        var item1:MockLayoutItem = new MockLayoutItem(120, 120);
        item1.left = 50;
        var item2:MockLayoutItem = new MockLayoutItem(120, 120);

        container.orientation = LayoutOrientation.HORIZONTAL;
        container.gap = 0;
        container.paddingLeft = 10;
        container.addItem(item1);
        container.addItem(item2);

        assertEquals(item1.left * scaleFactor, item1.x);
        assertEquals(item1.left * scaleFactor + item1.width, item2.x);
        assertEquals(item1.width + item2.width + item1.left * scaleFactor, container.width);
    }

    [Test]
    public function checkStopOfInvalidation():void {
        var item1:MockLayoutItem = new MockLayoutItem(120, 120);
        item1.left = 50;

        container.stopInvalidation = true;
        container.orientation = LayoutOrientation.HORIZONTAL;
        container.gap = 0;
        container.paddingLeft = 10;
        container.addItem(item1);

        assertEquals(0, item1.x);
        assertEquals(0, container.width);
    }

    [Test]
    public function centerItems():void {
        var item1:MockLayoutItem = new MockLayoutItem(100, 100);
        var item2:MockLayoutItem = new MockLayoutItem(50, 50);
        var item3:MockLayoutItem = new MockLayoutItem(30, 30);
        item3.marginLeft = 5;
        item3.marginRight = 5;

        container.explicitWidth = 200;
        container.horizontalAlign = LayoutHorizontalAlign.CENTER;
        container.gap = 0;
        container.addItem(item1);
        container.addItem(item2);
        container.addItem(item3);

        assertEquals(50, item1.x);
        assertEquals(75, item2.x);
        assertEquals(75, item3.x);
    }

    [Test]
    public function alignRightItems():void {
        var item1:MockLayoutItem = new MockLayoutItem(100, 100);
        var item2:MockLayoutItem = new MockLayoutItem(50, 50);
        var item3:MockLayoutItem = new MockLayoutItem(30, 30);
        item3.marginLeft = 5;
        item3.marginRight = 5;

        container.explicitWidth = 200;
        container.horizontalAlign = LayoutHorizontalAlign.RIGHT;
        container.gap = 0;
        container.addItem(item1);
        container.addItem(item2);
        container.addItem(item3);

        assertEquals(100, item1.x);
        assertEquals(150, item2.x);
        assertEquals(160, item3.x);

        container.paddingRight = 5;

        assertEquals(90, item1.x);
        assertEquals(140, item2.x);
        assertEquals(150, item3.x);

    }

    [Test]
    public function middleItems():void {
        var item1:MockLayoutItem = new MockLayoutItem(100, 100);
        var item2:MockLayoutItem = new MockLayoutItem(50, 50);

        container.explicitHeight = 200;
        container.orientation = LayoutOrientation.HORIZONTAL;
        container.verticalAlign = LayoutVerticalAlign.MIDDLE;
        container.gap = 0;
        container.addItem(item1);
        container.addItem(item2);

        assertEquals(50, item1.y);
        assertEquals(75, item2.y);
    }

    [Test]
    public function alignBottomItems():void {
        var item1:MockLayoutItem = new MockLayoutItem(100, 100);
        var item2:MockLayoutItem = new MockLayoutItem(50, 50);
        var item3:MockLayoutItem = new MockLayoutItem(30, 30);
        item3.marginTop = 5;
        item3.marginBottom = 5;

        container.explicitHeight = 200;
        container.orientation = LayoutOrientation.HORIZONTAL;
        container.verticalAlign = LayoutVerticalAlign.BOTTOM;
        container.gap = 0;
        container.addItem(item1);
        container.addItem(item2);
        container.addItem(item3);

        assertEquals(100, item1.y);
        assertEquals(150, item2.y);
        assertEquals(160, item3.y);

        container.paddingBottom = 5;

        assertEquals(90, item1.y);
        assertEquals(140, item2.y);
        assertEquals(150, item3.y);

    }

    [Test]
    public function stackMultipleContainer():void {
        container.gap = 10;
        container.paddingLeft = 10;
        container.paddingRight = 10;
        container.paddingTop = 10;
        container.paddingBottom = 10;

        var secondContainer:LayoutContainer = new LayoutContainer(sprite);
        var secondContainerItem:MockLayoutItem = new MockLayoutItem(120, 120);
        secondContainer.percentalWidth = 100;
        secondContainer.verticalAlign = LayoutVerticalAlign.MIDDLE;
        secondContainer.addItem(secondContainerItem);
        container.addItem(secondContainer);

        var thirdContainer:LayoutContainer = new LayoutContainer(sprite);
        var thirdContainerItem:MockLayoutItem = new MockLayoutItem(120, 120);
        thirdContainer.percentalWidth = 100;
        thirdContainer.verticalAlign = LayoutVerticalAlign.MIDDLE;
        thirdContainer.addItem(thirdContainerItem);
        container.addItem(thirdContainer);

        container.addItem(new MockLayoutItem(20, 20));

        container.addItemsToView();

        assertEquals(20, secondContainer.y);
        assertEquals(160, thirdContainer.y);
        assertEquals(20, secondContainerItem.y);
        assertEquals(160, thirdContainerItem.y);
    }

}

}

