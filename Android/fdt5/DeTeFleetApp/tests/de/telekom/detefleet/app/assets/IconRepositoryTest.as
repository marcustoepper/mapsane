package de.telekom.detefleet.app.assets {
import com.kaizimmer.ui.Screen;

import flash.display.Sprite;

import org.flexunit.asserts.assertEquals;

import org.fluint.uiImpersonation.UIImpersonator;

public class IconRepositoryTest {
    private var view:Sprite;

    [Before]
    public function setUp():void {


    }

    [Ignore("Need Screen initialized (stage is missing)")]
    [Test]
    public function checkCorrectScaledSize():void {

        assertEquals(58, IconRepository.iconGarage.height);
        assertEquals(58, IconRepository.iconGarage.width);
    }
}
}
