package de.telekom.detefleet.app.services.remote {

import org.flexunit.Assert;

public class JSONServiceConfigurationTest {
    public function JSONServiceConfigurationTest() {
    }

    [Test]
    public function checkEndpointUrl():void {
        Assert.assertEquals(JSONServiceConfiguration.endpointUrl, 'http://www.deploy.detefleet.aoe-works.de/detefleet/webservice');
    }

    [Test]
    public function checkDomain():void {
        Assert.assertEquals(JSONServiceConfiguration.domain, 'http://www.deploy.detefleet.aoe-works.de');
    }
}

}
