package de.telekom.detefleet.app.services.remote {
import de.telekom.detefleet.app.events.login.LoginFault;
import de.telekom.detefleet.app.events.login.LoginSuccess;
import de.telekom.detefleet.app.models.vo.PersonVO;
import de.telekom.detefleet.app.models.vo.ServiceAuthVO;

import org.flexunit.Assert;
import org.flexunit.async.Async;

public class MockLoginServiceTest {

    public var service:MockLoginService;
    private var loginFault:LoginFault;
    private var loginSuccess:LoginSuccess;

    [Before]
    public function setUp():void {
        loginFault = new LoginFault();
        loginSuccess = new LoginSuccess();
        service = new MockLoginService();
    }

    [After]
    public function tearDown():void {
        this.service = null;
    }

    [Ignore("Not Ready to Run")]
    [Test(async)]
    public function setWrongUser():void {
        service.loginFault.add(Async.asyncHandler(this, handleFailure, 3000, null, handleServiceTimeout));
        service.login('wrong', 'test', 'key');
    }

    [Ignore("Not Ready to Run")]
    [Test(async)]
    public function setWrongPassword():void {
        service.loginFault.add(Async.asyncHandler(this, handleFailure, 3000, null, handleServiceTimeout));
        service.login('test', 'wrong', 'key');
    }

    private function handleFailure(message:String):void {
        Assert.assertEquals(message, "Zu diesem User und Passwort wurden keine Daten gefunden");
    }

    [Ignore("Not Ready to Run")]
    [Test(async)]
    public function successfulLogin():void {
        service.loginSuccess.add(Async.asyncHandler(this, handleSuccessfulLogin, 3000, null, handleServiceTimeout));
        this.service.login('test', 'test', 'key');
    }

    private function handleServiceTimeout(object:Object):void {
        Assert.fail('Pending Event Never Occurred');

    }

    private function handleSuccessfulLogin(person:PersonVO, auth:ServiceAuthVO):void {
        Assert.assertEquals(PersonVO(person).firstName, "Hans");

    }

}
}
