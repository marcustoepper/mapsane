package de.telekom.detefleet.app.services.local {

import org.flexunit.Assert;

public class RandomDeviceKeyTest {
    private var deviceKeyService:RandomDeviceKey;

    [Before]
    public function setUp():void {
        deviceKeyService = new RandomDeviceKey();
    }

    [Test]
    public function isHex():void {
        var pattern:RegExp = /^[0-9A-F]+$/i;
        Assert.assertTrue(pattern.exec(deviceKeyService.getKey()));
    }

    [Test]
    public function lengthIs256bit():void {
        Assert.assertEquals(64, deviceKeyService.getKey().length);

    }

    [Test]
    public function keyIsUnique():void {
        var last:Array = [];
        var current:String = deviceKeyService.getKey();

        for (var i:int = 0; i < 10; i++) {
            Assert.assertTrue(!last.some(exist, current));
            last[i] = current;
            current = deviceKeyService.getKey();
        }

        function exist(element:*, index:int, arr:Array):Boolean {
            return (element == this);
        }
    }
}
}
