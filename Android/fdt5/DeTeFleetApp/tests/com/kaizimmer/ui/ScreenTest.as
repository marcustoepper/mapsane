package com.kaizimmer.ui {
import org.flexunit.asserts.assertEquals;
import org.flexunit.asserts.assertFalse;
import org.flexunit.asserts.assertTrue;

public class ScreenTest {

    [Test]
    public function getIPhoneDpiClass():void {

    }

    [Test]
    public function testCalcRealPixels():void {

    }

    [Test]
    public function testCalcScaleFactor():void {
        assertEquals(2, Screen.calcScaleFactor(320, 480, 640, 960));
        assertFalse(Screen.needsLetterBox);
    }

    [Test]
    public function needsLetterBoxing():void {
        Screen.calcScaleFactor(320, 480, 640, 640);
        assertTrue(Screen.needsLetterBox);
    }

    [Test]
    public function testInit():void {

    }

    [Test]
    public function testCalcDiagonalInInches():void {

    }

    [Test]
    public function testCalcVirtualPixels():void {

    }
}
}
