package {
import de.telekom.detefleet.app.LocalContext;

import flash.display.Sprite;
import flash.display.StageAlign;
import flash.display.StageQuality;
import flash.display.StageScaleMode;
import flash.events.Event;

import org.as3commons.logging.api.LOGGER_FACTORY;
import org.as3commons.logging.setup.SimpleTargetSetup;
import org.as3commons.logging.setup.target.TraceTarget;

[SWF(frameRate="60", backgroundColor="0xffffff", width="320", height="460")]
public class Local extends Sprite {
    protected var mainContext:LocalContext;
    //protected var contextView:Sprite;

    public function Local() {
        super();

        // support autoOrients
        stage.align = StageAlign.TOP_LEFT;
        stage.scaleMode = StageScaleMode.NO_SCALE;
        stage.frameRate = 60;
        stage.quality = StageQuality.LOW;

        this.stage.addEventListener(Event.RESIZE, onResize);

    }

    private function onResize(event:Event):void {
        LOGGER_FACTORY.setup = new SimpleTargetSetup(new TraceTarget);

        mainContext = new LocalContext(this);

        this.stage.removeEventListener(Event.RESIZE, onResize);
    }
}
}
