package
{
import com.kaizimmer.ui.Screen;

import de.telekom.detefleet.app.MainContext;

import flash.display.Sprite;
import flash.display.StageAlign;
import flash.display.StageQuality;
import flash.display.StageScaleMode;
import flash.events.Event;
import flash.system.Capabilities;

import org.as3commons.logging.api.LOGGER_FACTORY;
import org.as3commons.logging.setup.SimpleTargetSetup;
import org.as3commons.logging.setup.target.TraceTarget;

[SWF(frameRate="60", backgroundColor="0xffffff", width="320", height="460")]
public class Main extends Sprite
{
	protected var mainContext:MainContext;

	public function Main() {
		super();

		// support autoOrients
		stage.align = StageAlign.TOP_LEFT;
		stage.scaleMode = StageScaleMode.NO_SCALE;
		stage.frameRate = 60;
		stage.quality = StageQuality.LOW;

		this.stage.addEventListener(Event.RESIZE, onResize);
	}

	private function onResize(event:Event):void {
		if(Capabilities.isDebugger) {
			LOGGER_FACTORY.setup = new SimpleTargetSetup(new TraceTarget);
		}

		mainContext = new MainContext(this);

		this.stage.removeEventListener(Event.RESIZE, onResize);
	}
}
}
