package de.aoemedia.utils
{
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;

	public class ClassUtils
	{
		/**
		 * ...
		 * 
		 * @author kai zimmer <kai.zimmer@aoemedia.de>
		 */
		public function ClassUtils()
		{
			
		}
		
		public static function getClass ( instance:Object ):Class
		{
			return getDefinitionByName(getQualifiedClassName(instance)) as Class;
		}
	}
}