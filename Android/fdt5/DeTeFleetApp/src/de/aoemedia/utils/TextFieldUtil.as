package de.aoemedia.utils
{
import flash.text.TextField;
/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class TextFieldUtil
{
	public static function fillText(_textfieldRef:TextField, _str:String, _breakWords:Boolean = true, _dots:String = "..."):Boolean
	{
		var buildStr:String = _str;

		_textfieldRef.text = trim(buildStr);

		var endWordIndex:Number;
		var multiline:Boolean = _textfieldRef.multiline;

		if ((!multiline && _textfieldRef.maxScrollH > 0) || (multiline && _textfieldRef.maxScrollV > 1)) {
			while (buildStr.length > 0) {

				if (_breakWords) {
					buildStr = buildStr.substr(0, buildStr.length - 1);
				} else {
					endWordIndex = Math.max(buildStr.lastIndexOf(" "), 0);
					buildStr = buildStr.substr(0, endWordIndex);
				}

				_textfieldRef.text = trim(buildStr) + _dots;

				if ((!multiline && _textfieldRef.maxScrollH <= 0) || (multiline && _textfieldRef.maxScrollV <= 1)) {
					break;
				}
			}
		}

		if (buildStr != _str) {
			return(true);
		}
		return(false);
	}

	public static function trim(s:String):String
	{
		if (s == null) {
			return ("");
		}
		return (s.replace(/^\s+|\s+$/g, ''));
	}
}
}
