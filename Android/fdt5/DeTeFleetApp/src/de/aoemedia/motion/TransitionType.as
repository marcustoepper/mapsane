package de.aoemedia.motion
{
	public class TransitionType
	{
		public static const SLIDE_LEFT:String = "SLIDE_LEFT";
		public static const SLIDE_RIGHT:String = "SLIDE_RIGHT";
		public static const SLIDE_UP:String = "SLIDE_UP";
		public static const SLIDE_DOWN:String = "SLIDE_DOWN";
		public static const FLIP_RIGHT:String = "FLIP_RIGHT";
		public static const FLIP_LEFT:String = "FLIP_LEFT";
		
		public function TransitionType ( )
		{
			
		}
	}
}