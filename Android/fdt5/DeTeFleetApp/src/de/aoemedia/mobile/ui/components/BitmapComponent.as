package de.aoemedia.mobile.ui.components {
import de.aoemedia.mobile.ui.MultiDpiHelper;

import flash.display.Bitmap;
import flash.display.BitmapData;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class BitmapComponent extends Bitmap implements ILayoutComponent {
    protected var _paddingLeft:Number = 0;
    protected var _paddingRight:Number = 0;
    protected var _paddingTop:Number = 0;
    protected var _paddingBottom:Number = 0;
    protected var _left:Number;
    protected var _right:Number;
    private var _scaleFactor:Number = 1;
    private var _marginBottom:Number = 0;
    private var _marginTop:Number = 0;
    private var _marginLeft:Number = 0;
    private var _marginRight:Number = 0;

    public function BitmapComponent(bitmapData:BitmapData = null, pixelSnapping:String = "auto", smoothing:Boolean = false) {
        super(bitmapData, pixelSnapping, smoothing);
    }

    public function get paddingLeft():Number {
        return _paddingLeft;
    }

    public function set paddingLeft(paddingLeft:Number):void {

        _paddingLeft = MultiDpiHelper.scale(paddingLeft);
    }

    public function get paddingRight():Number {
        return _paddingRight;
    }

    public function set paddingRight(paddingRight:Number):void {

        _paddingRight = MultiDpiHelper.scale(paddingRight);
    }

    public function get paddingTop():Number {
        return _paddingTop;
    }

    public function set paddingTop(paddingTop:Number):void {

        _paddingTop = MultiDpiHelper.scale(paddingTop);
    }

    public function get paddingBottom():Number {
        return _paddingBottom;
    }

    public function set paddingBottom(paddingBottom:Number):void {

        _paddingBottom = MultiDpiHelper.scale(paddingBottom);
    }

    public function get left():Number {
        return _left;
    }

    public function set left(left:Number):void {
        _left = left;
    }

    public function get right():Number {
        return _right;
    }

    public function set right(right:Number):void {
        _right = right;
    }

    override public function get height():Number {
        return super.height + _paddingBottom * scaleFactor;
    }

    override public function get width():Number {
        return super.width + _paddingRight * scaleFactor;
    }

    public function get marginLeft():Number {
        return _marginLeft;
    }

    public function set marginLeft(marginLeft:Number):void {
        _marginLeft = marginLeft;
    }

    public function get marginRight():Number {
        return _marginRight;
    }

    public function set marginRight(marginRight:Number):void {
        _marginRight = marginRight
    }

    public function get marginTop():Number {
        return _marginTop;
    }

    public function set marginTop(marginTop:Number):void {
        _marginTop = marginTop;
    }

    public function get marginBottom():Number {
        return _marginBottom;
    }

    public function set marginBottom(marginBottom:Number):void {
        _marginBottom = marginBottom;
    }

    override public function set x(value:Number):void {
        super.x = value + paddingLeft * scaleFactor;
    }

    override public function set y(value:Number):void {
        super.y = value + paddingTop * scaleFactor;
    }

    public function get scaleFactor():Number {
        return _scaleFactor;
    }

    public function set scaleFactor(scaleFactor:Number):void {
        _scaleFactor = scaleFactor;
    }

    public function set baseWidth(width:Number):void{
        this.width = width*scaleFactor;
    }

    public function get baseWidth():Number{
        return width/scaleFactor;
    }

    public function set baseHeight(height:Number):void{
        this.height = height*scaleFactor;
    }

    public function get baseHeight():Number{
        return height/scaleFactor;
    }
}
}
