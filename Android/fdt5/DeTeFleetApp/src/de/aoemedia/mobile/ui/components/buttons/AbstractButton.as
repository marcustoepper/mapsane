package de.aoemedia.mobile.ui.components.buttons {

import de.aoemedia.mobile.ui.components.base.AbstractUIComponent;
import de.aoemedia.mobile.ui.components.skins.ButtonBitmaps;

import flash.display.Bitmap;
import flash.events.MouseEvent;

import org.bytearray.display.ScaleBitmap;
import org.osflash.signals.Signal;

/**
 * ...
 * @author dev.mode
 */
public class AbstractButton extends AbstractUIComponent {
    public var signalDown:Signal;
    public var signalClicked:Signal;

    protected const BUTTON_STATE_DEFAULT:int = 0;
    protected const BUTTON_STATE_DISABLED:int = 1;
    protected const BUTTON_STATE_DOWN:int = 2;
    protected const BUTTON_STATE_DOWN_DISABLED:int = 3;

    protected var buttonState:int = BUTTON_STATE_DEFAULT;
    protected var isDown:Boolean = false;

    protected var buttonBmp:Bitmap;
    protected var buttonBmpDown:Bitmap;
    protected var buttonBmpDisabled:Bitmap;
    protected var buttonBmpDownDisabled:Bitmap;
    protected var renderedBmp:Bitmap;

    protected var _enabled:Boolean = true;

    public function AbstractButton(bmps:ButtonBitmaps) {
        signalDown = new Signal();
        signalClicked = new Signal(AbstractButton);

        if (bmps != null) {
            buttonBmp = bmps.up;
            buttonBmpDown = bmps.down;
            buttonBmpDisabled = bmps.upDisabled;
            buttonBmpDownDisabled = bmps.downDisabled;
        }
        super();
    }

    public function get enabled():Boolean {
        return _enabled;
    }

    public function set enabled(value:Boolean):void {
        if (value == _enabled) return;
        _enabled = value;
        switch (buttonState) {
            case BUTTON_STATE_DOWN:
                if (_enabled) buttonState = BUTTON_STATE_DOWN;
                else buttonState = BUTTON_STATE_DOWN_DISABLED;
                break;
            default:
                if (_enabled) buttonState = BUTTON_STATE_DEFAULT;
                else buttonState = BUTTON_STATE_DISABLED;
        }
        if (_enabled) startListening();
        else stopListening();
        invalidate();
    }

    /**
     *
     */
    override public function setSize(w:Number = -1, h:Number = -1):void {
        super.setSize(w, h);

        if (renderedBmp is ScaleBitmap) {
            ScaleBitmap(renderedBmp).setSize(componentWidth, componentHeight);
        }
        else {
            renderedBmp.width = componentWidth;
            renderedBmp.height = componentHeight;
        }
    }

    /**
     *
     */
    override protected function init():void {
        super.init();
        startListening();
    }

    /**
     *
     */
    override protected function createChildren():void {
//        if (buttonBmp == null ||
//                buttonBmpDisabled == null ||
//                buttonBmpDown == null ||
//                buttonBmpDownDisabled == null) {
//            throw new Error("ERROR: AbstractButton .createChildren1x | Make sure all button bitmaps are defined!");
//        }
        super.createChildren();
    }

    /**
     *
     */
    override protected function draw():void {
        if (renderedBmp != null) {
            removeChild(renderedBmp);
        }
        switch (buttonState) {
            case BUTTON_STATE_DEFAULT:
                renderedBmp = buttonBmp;
                break;
            case BUTTON_STATE_DISABLED:
                renderedBmp = buttonBmpDisabled;
                break;
            case BUTTON_STATE_DOWN:
                renderedBmp = buttonBmpDown;
                break;
            case BUTTON_STATE_DOWN_DISABLED:
                renderedBmp = buttonBmpDownDisabled;
                break;
        }
        addChildAt(renderedBmp, 0);
        super.draw();
    }

    protected function updateRenderState(e:MouseEvent):void {
        switch (e.type) {
            case MouseEvent.MOUSE_DOWN:
                if (_enabled) buttonState = BUTTON_STATE_DOWN;
                else buttonState = BUTTON_STATE_DOWN_DISABLED;
                break;
            case MouseEvent.MOUSE_OUT:
            case MouseEvent.MOUSE_UP:
                if (_enabled) buttonState = BUTTON_STATE_DEFAULT;
                else buttonState = BUTTON_STATE_DISABLED;
                break;
        }
        invalidate();
    }

    protected function startListening():void {
        addEventListener(MouseEvent.MOUSE_DOWN, updateRenderState);
        addEventListener(MouseEvent.MOUSE_OUT, updateRenderState);
        addEventListener(MouseEvent.MOUSE_UP, updateRenderState);
        addEventListener(MouseEvent.RELEASE_OUTSIDE, updateRenderState);

        addEventListener(MouseEvent.MOUSE_DOWN, dispatchInteraction);
        addEventListener(MouseEvent.MOUSE_OUT, dispatchInteraction);
        addEventListener(MouseEvent.MOUSE_UP, dispatchInteraction);
        addEventListener(MouseEvent.RELEASE_OUTSIDE, dispatchInteraction);
    }

    protected function stopListening():void {
        removeEventListener(MouseEvent.MOUSE_DOWN, updateRenderState);
        removeEventListener(MouseEvent.MOUSE_OUT, updateRenderState);
        removeEventListener(MouseEvent.MOUSE_UP, updateRenderState);
        removeEventListener(MouseEvent.RELEASE_OUTSIDE, updateRenderState);

        removeEventListener(MouseEvent.MOUSE_DOWN, dispatchInteraction);
        removeEventListener(MouseEvent.MOUSE_OUT, dispatchInteraction);
        removeEventListener(MouseEvent.MOUSE_UP, dispatchInteraction);
        removeEventListener(MouseEvent.RELEASE_OUTSIDE, dispatchInteraction);
    }

    protected function dispatchInteraction(e:MouseEvent):void {
        switch (e.type) {
            case MouseEvent.MOUSE_DOWN:
                isDown = true;
                signalDown.dispatch();
                break;
            case MouseEvent.RELEASE_OUTSIDE:
            case MouseEvent.MOUSE_UP:
                if (isDown) signalClicked.dispatch(this);
            case MouseEvent.MOUSE_OUT:
                isDown = false;
                break;
        }
    }

}

}