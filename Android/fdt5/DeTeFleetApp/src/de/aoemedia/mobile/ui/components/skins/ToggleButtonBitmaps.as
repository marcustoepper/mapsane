package de.aoemedia.mobile.ui.components.skins
{
	import flash.display.Bitmap;
	
	public class ToggleButtonBitmaps extends ButtonBitmaps
	{
		public var upActive:Bitmap;
		public var downActive:Bitmap;
		public var upActiveDisabled:Bitmap;
		public var downActiveDisabled:Bitmap;
		
		public function ToggleButtonBitmaps(up:Bitmap,
											down:Bitmap,
											upDisabled:Bitmap,
											downDisabled:Bitmap,
											upActive:Bitmap,
											downActive:Bitmap,
											upActiveDisabled:Bitmap,
											downActiveDisabled:Bitmap)
		{
			this.upActive = upActive;
			this.downActive = downActive;
			this.upActiveDisabled = upActiveDisabled;
			this.downActiveDisabled = downActiveDisabled;
			super(up, down, upDisabled, downDisabled);
		}
	}
}