package de.aoemedia.mobile.ui.components.forms
{
import flash.display.Bitmap;
import flash.events.FocusEvent;
import flash.text.TextField;
import flash.text.TextFieldType;
import flash.text.TextFormat;

import de.aoemedia.mobile.ui.components.base.AbstractUIComponent;
import de.aoemedia.mobile.ui.components.skins.InputFieldBitmaps;

import org.bytearray.display.ScaleBitmap;

//TODO: consider adding text setter and getter
public class InputField extends AbstractUIComponent
{
	protected const FIELD_STATE_UNFOCUSED:Number = 0;
	protected const FIELD_STATE_FOCUSED:Number = 1;
	protected const FIELD_STATE_VALID:Number = 2;
	protected const FIELD_STATE_INVALID:Number = 3;
	protected const FIELD_STATE_DISABLED:Number = 4;

	protected var fieldState:Number = 0;

	private var _isDebug:Boolean = false;

	protected var _isFocused:Boolean = false;
	protected var _isHighlighted:Boolean = false;
	// true if field is marked as success or error
	protected var _enabled:Boolean = true;

	protected var _paddingHorizontal:uint = 5;
	protected var _paddingVertical:uint = 7;

	protected var _colorUnFocused:Number = 0;
	protected var _colorFocused:Number = 0;
	protected var _colorValid:Number = 0;
	protected var _colorInvalid:Number = 0;
	protected var _colorDisabled:Number = 0;

	protected var unfocused:Bitmap;
	protected var focused:Bitmap;
	protected var disabled:Bitmap;
	protected var invalid:Bitmap;
	protected var valid:Bitmap;

	protected var renderedBmp:Bitmap;


	protected var _field:ITextField;
	protected var fieldTF:TextFormat;

	public function InputField(bmps:InputFieldBitmaps, tF:TextFormat = null, width:Number = 200, height:Number = 58)
	{
		unfocused = bmps.unfocused;
		focused = bmps.focused;
		disabled = bmps.disabled;
		invalid = bmps.invalid;
		valid = bmps.valid;
		// fieldTF = tF;
		super();
		stopInvalidation = true;
		if ( tF != null ) {
			textFormat = tF;
			var col:Number = tF.color as Number;
			_colorUnFocused = col;
			_colorFocused = col;
			_colorValid = col;
			_colorInvalid = col;
			_colorDisabled = col;
		}
		setSize(width, height);
		stopInvalidation = false;
		startListening();
	}

	public function get paddingHorizontal():uint {
		return _paddingHorizontal;
	}

	public function set paddingHorizontal(value:uint):void {
		if ( _paddingHorizontal == value ) return;
		_paddingHorizontal = value;
		invalidate();
	}

	public function get paddingVertical():uint {
		return _paddingVertical;
	}

	public function set paddingVertical(value:uint):void {
		if ( _paddingVertical == value ) return;
		_paddingVertical = value;
		invalidate();
	}

	/**
	 * Indicates whether red debugging border is rendered arround the textfield
	 */
	public function get isDebug():Boolean {
		return _isDebug;
	}

	/**
	 * Adds red border arround textfield if set to true
	 */
	public function set isDebug(value:Boolean):void {
		if ( _isDebug == value ) return;
		_isDebug = value;
		invalidate();
	}

	public function get textField():TextField {
		return _field as TextField;
	}

	public function get field():ITextField {
		return _field;
	}

	public function get isValid():Boolean {
		return fieldState == FIELD_STATE_INVALID;
	}

	public function get colorUnFocused():Number {
		return _colorUnFocused;
	}

	public function set colorUnFocused(value:Number):void {
		if ( _colorUnFocused == value ) return;
		_colorUnFocused = value;
		if ( !_isFocused ) invalidate();
	}

	public function get colorFocused():Number {
		return _colorFocused;
	}

	public function set colorFocused(value:Number):void {
		if ( _colorFocused == value ) return;
		_colorFocused = value;
		if ( _isFocused ) invalidate();
	}

	public function get colorValid():Number {
		return _colorValid;
	}

	public function set colorValid(value:Number):void {
		if ( _colorValid == value ) return;
		_colorValid = value;
		if ( isValid ) invalidate();
	}

	public function get colorInvalid():Number {
		return _colorInvalid;
	}

	public function set colorInvalid(value:Number):void {
		if ( _colorInvalid == value ) return;
		_colorInvalid = value;
		if ( !_enabled ) invalidate();
	}

	public function get colorDisabled():Number {
		return _colorDisabled;
	}

	public function set colorDisabled(value:Number):void {
		if ( _colorDisabled == value ) return;
		_colorDisabled = value;
		if ( !isValid ) invalidate();
	}

	public function set enabled(isEnabled:Boolean):void {
		if ( _enabled == isEnabled ) return;
		_enabled = isEnabled;
		mouseEnabled = isEnabled;
		mouseChildren = isEnabled;
		if ( !_enabled ) fieldState = FIELD_STATE_DISABLED;
		invalidate();
	}

	public function get enabled():Boolean {
		return _enabled;
	}

	public function set textFormat(tF:TextFormat):void {
		// if ( fieldTF == tF ) return;
		fieldTF = tF;
		if ( _field is TextField ) {
			TextField(_field).setTextFormat(fieldTF);
		}
		_field.defaultTextFormat = fieldTF;
		_field.embedFonts = true;
		invalidate();
	}

	public function get textFormat():TextFormat {
		return fieldTF;
	}

	public function showError():void
	{
		if ( fieldState == FIELD_STATE_INVALID ) return;
		_isHighlighted = true;
		fieldState = FIELD_STATE_INVALID;
		invalidate();
	}

	public function showSuccess():void
	{
		if ( fieldState == FIELD_STATE_VALID ) return;
		_isHighlighted = true;
		fieldState = FIELD_STATE_VALID;
		invalidate();
	}

	public function normalize():void
	{
		switch ( fieldState ) {
			case FIELD_STATE_FOCUSED:
			case FIELD_STATE_UNFOCUSED:
				return;
		}
		_isHighlighted = false;
		if ( _isFocused ) fieldState = FIELD_STATE_FOCUSED;
		else fieldState = FIELD_STATE_UNFOCUSED;
		invalidate();
	}

	override protected function createChildren():void
	{
		_field = new FlashTextField();
		_field.type = TextFieldType.INPUT;
		addChild(_field as FlashTextField);
	}

	/**
	 * 
	 */
	override public function setSize(w:Number = -1, h:Number = -1):void
	{
		if(w < -1 || h < -1){
			throw new Error("the size of an InputField can't be negativ.");
		}
		super.setSize(w, h);
		
		if ( renderedBmp is ScaleBitmap ) {
			ScaleBitmap(renderedBmp).setSize(componentWidth, componentHeight);
		} else {
			renderedBmp.width = componentWidth;
			renderedBmp.height = componentHeight;
		}
		
		if ( !(_field is StageTextField ) )
		{
			_field.x = paddingHorizontal;
			_field.width = componentWidth - 2 * paddingHorizontal;
			if(_field.multiline){
					_field.height = componentHeight - 2 * paddingVertical;
			} else {
				if ( _field is TextField )
				{
					_field.height = TextField(_field).textHeight+scale(2);
				}
			}
			_field.y = renderedBmp.height/2 - _field.height/2;
		}
		//TODO: IN CASE OF STAGETEXTFIELD WE NEED TO ADD this x AND y

		
		/*/
		_field.x = paddingHorizontal;
		_field.width = componentWidth - 2 * paddingHorizontal;
		if(_field.multiline){
			_field.height = componentHeight - 2 * paddingVertical;
		} else {
			if ( _field is TextField )
			{
				_field.height = TextField(_field).textHeight+scale(2);
			}
		}

		if ( renderedBmp is ScaleBitmap ) {
			ScaleBitmap(renderedBmp).setSize(componentWidth, componentHeight);
		} else {
			renderedBmp.width = componentWidth;
			renderedBmp.height = componentHeight;
		}
		_field.y = renderedBmp.height/2 - _field.height/2;
		//*/
	}

	/**
	 * 
	 */
	override protected function draw():void
	{
		if ( renderedBmp != null ) removeChild(renderedBmp);
		switch ( fieldState ) {
			case FIELD_STATE_UNFOCUSED:
				renderedBmp = unfocused;
				_field.textColor = _colorUnFocused;
				break;
			case FIELD_STATE_FOCUSED:
				renderedBmp = focused;
				_field.textColor = _colorFocused;
				break;
			case FIELD_STATE_INVALID:
				renderedBmp = invalid;
				_field.textColor = _colorInvalid;
				break;
			case FIELD_STATE_VALID:
				renderedBmp = valid;
				_field.textColor = _colorValid;
				break;
			case FIELD_STATE_DISABLED:
				renderedBmp = disabled;
				_field.textColor = _colorDisabled;
				break;
		}
		addChildAt(renderedBmp, 0);
		if ( _isDebug ) {
			_field.border = true;
			_field.borderColor = 0xff0000;
		}
		super.draw();
	}

	protected function startListening():void
	{
		_field.addEventListener(FocusEvent.FOCUS_IN, updateRenderState);
		_field.addEventListener(FocusEvent.FOCUS_OUT, updateRenderState);
	}

	protected function stopListening():void
	{
		_field.removeEventListener(FocusEvent.FOCUS_IN, updateRenderState);
		_field.removeEventListener(FocusEvent.FOCUS_OUT, updateRenderState);
	}

	protected function updateRenderState(e:FocusEvent):void
	{
		switch ( e.type ) {
			case FocusEvent.FOCUS_IN:
				if ( !_isHighlighted ) fieldState = FIELD_STATE_FOCUSED;
				_isFocused = true;
				break;
			case FocusEvent.FOCUS_OUT:
				if ( !_isHighlighted ) fieldState = FIELD_STATE_UNFOCUSED;
				_isFocused = false;
				break;
		}
		invalidate();
	}

	public function get multiline():Boolean {
		return textField.multiline;
	}

	public function set multiline(multiline:Boolean):void {
		textField.multiline = multiline;
	}
}
}
