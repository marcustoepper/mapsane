package de.aoemedia.mobile.ui.components.forms
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.StageText;
	import flash.text.StageTextInitOptions;
	import flash.text.TextFormat;
	
	public class StageTextField implements ITextField
	{
		protected var _maxChars:int;
		protected var _restrict:String;
		protected var _textFormat:TextFormat;
		
		protected var _localViewport:Rectangle;
		protected var _modifierRect:Rectangle;
		protected var _stageText:StageText;
		
		protected var _parent:DisplayObjectContainer;

		private var _type:String;


		private var _hasChanged:Boolean;

		private var _snapShotText:String;

		private var _snapshot:Bitmap;
		private var _canvas:BitmapData;

		private var _isVisible:Boolean;

		
		public function StageTextField ( initOptions:StageTextInitOptions = null )
		{
			_localViewport = new Rectangle();
			_modifierRect = new Rectangle();
			_stageText = new StageText(initOptions);
		}
		
		public function get stageText():StageText
		{
			return _stageText;
		}

		public function get text():String
		{
			return _stageText.text;
		}

		public function set text(s:String):void
		{
			if ( _stageText.text == s ) return;
			_stageText.text = s;
			_hasChanged = true;
		}
		
		protected function forceDisplayAsPassword(event:Event):void
		{
			//trace("forceDisplayAsPassword");
			//trace("_stageText.displayAsPassword: " + _stageText.displayAsPassword);
			_stageText.displayAsPassword = true;
		}
		
		/*/
		protected function onFocusIn(event:Event):void
		{
			trace("onFocusIn");
			trace("_stageText.displayAsPassword: " + _stageText.displayAsPassword);
			_stageText.displayAsPassword = true;
		}
		
		protected function onFocusOut(event:Event):void
		{
			trace("onFocusOut");
			trace("_stageText.displayAsPassword: " + _stageText.displayAsPassword);
			_stageText.displayAsPassword = true;
		}
		//*/
		
		public function addEventListener(type:String, listener:Function, useCapture:Boolean=false, priority:int=0, useWeakReference:Boolean=false):void
		{
			_stageText.addEventListener(type, listener, useCapture, priority, useWeakReference);
		}

		public function dispatchEvent(event:Event):Boolean
		{
			return _stageText.dispatchEvent(event);
		}

		public function hasEventListener(type:String):Boolean
		{
			return _stageText.hasEventListener(type);
		}

		public function removeEventListener(type:String, listener:Function, useCapture:Boolean=false):void
		{
			_stageText.removeEventListener(type, listener, useCapture);
		}

		public function willTrigger(type:String):Boolean
		{
			return _stageText.willTrigger(type);
		}

		public function set defaultTextFormat(f:TextFormat):void
		{
			// do nothing
		}
		
		public function get defaultTextFormat():TextFormat
		{
			return null;
		}
		
		public function set embedFonts(b:Boolean):void
		{
			// do nothing
		}
		
		public function get embedFonts():Boolean
		{
			return false;
		}
		
		public function set type(t:String):void
		{
			// do nothing
			_type = t;
		}
		
		public function get type():String
		{
			return _type;
		}
		
		public function set x(n:Number):void
		{
			updateViewportX(n);
			updateSnapshotCoords();
		}
		
		public function get x():Number
		{
			return _localViewport.x;
		}
		
		public function set y(n:Number):void
		{
			updateViewportY(n);
			updateSnapshotCoords();
		}
		
		public function get y():Number
		{
			return _localViewport.y;
		}
		
		protected function updateViewportX ( newX:Number ):void
		{
			updateViewport(newX, _localViewport.y, _localViewport.width, _localViewport.height);
		}
		
		protected function updateViewportY ( newY:Number ):void
		{
			updateViewport(_localViewport.x, newY, _localViewport.width, _localViewport.height);
		}
		
		protected function updateViewportWidth ( newW:Number ):void
		{
			updateViewport(_localViewport.x, _localViewport.y, newW, _localViewport.height);
		}
		
		protected function updateViewportHeight ( newH:Number ):void
		{
			updateViewport(_localViewport.x, _localViewport.y, _localViewport.width, newH);
		}
		
		protected function updateViewport ( newX:Number, newY:Number, newW:Number, newH:Number ):void
		{
			_localViewport.x = newX;
			_localViewport.y = newY;
			_localViewport.width = newW;
			_localViewport.height = newH;
			
			var origoInGlobCoords:Point = new Point();
			if ( _parent != null )
			{
				origoInGlobCoords = _parent.localToGlobal(origoInGlobCoords);
			}

			_modifierRect.x = origoInGlobCoords.x + _localViewport.x;
			_modifierRect.y = origoInGlobCoords.y + _localViewport.y;
			_modifierRect.width = _localViewport.width;
			_modifierRect.height = _localViewport.height;
			/*/
			trace("#####>>> globCoords.x: " + origoInGlobCoords.x + " | set x (n): " + newX );
			trace("#####>>> globCoords.y: " + origoInGlobCoords.y + " | set y (n): " + newY );
			//*/
			_stageText.viewPort = _modifierRect;

        }
		
		/**
		 * From time to time it may happen that the coordinates of the parent DisplayObjectContainer change.
		 * (For example via an drag and drop operation.)
		 * This method updates the StageText coordinates according to the current/new parent position.
		 */
		public function invalidateCoordinates ( ):void
		{
			updateViewportX(_localViewport.x);
		}
		
		protected function updateSnapshotCoords ( ):void
		{
			if (_snapshot != null )
			{
				_snapshot.x = _localViewport.x;
				_snapshot.x = _localViewport.y;
			}
		}
		
		public function set width(n:Number):void
		{
			updateViewportWidth(n);
			_hasChanged = true;
		}
		
		public function get width():Number
		{
			return _stageText.viewPort.width;
		}
		
		public function set height(n:Number):void
		{
			updateViewportHeight(n);
			_hasChanged = true;
		}
		
		public function get height():Number
		{
			return _stageText.viewPort.height;
		}
		
		public function set border(b:Boolean):void
		{
			// do nothing
		}
		
		public function get border():Boolean
		{
			return false;
		}
		
		public function set borderColor(n:uint):void
		{
			// do nothing
		}
		
		public function get borderColor():uint
		{
			return 0;
		}
		
		public function set textColor(c:uint):void
		{
			_stageText.color = c;
		}
		
		public function get textColor():uint
		{
			return _stageText.color;
		}
		
		public function set multiline(c:Boolean):void
		{
			//
		}
		
		public function get multiline():Boolean
		{
			return false;
		}
		
		public function set selectable(c:Boolean):void
		{
			//
		}
		
		public function get selectable():Boolean
		{
			return false;
		}
		
		
		public function get displayAsPassword ( ):Boolean
		{
			return _stageText.displayAsPassword;
		}
		
		public function set displayAsPassword ( b:Boolean ):void
		{
			/**
			 * BUG:
			 * StageText has focus; then clicking outside of StageText and immediately clicking inside again
			 * makes the StageText displayAsPassword turn to false!
			 * 
			 * (Debug code in the Event.CHANGE handler says: displayAsPassword is false.)
			 * 
			 * Bugfix/Workaround:
			 * set displayAsPassword = true in FocusEvent.FOCUS_OUT (and Event.CHANGE) handler(s)
			 */
			trace("StageTextField displayAsPassword: " + b);
			if ( b ) 
			{
				_stageText.addEventListener(Event.CHANGE, forceDisplayAsPassword);
				//_stageText.addEventListener(FocusEvent.FOCUS_IN, onFocusIn);
				//_stageText.addEventListener(FocusEvent.FOCUS_OUT, onFocusOut);
				_stageText.addEventListener(FocusEvent.FOCUS_OUT, forceDisplayAsPassword);
			}
			else
			{
				_stageText.removeEventListener(Event.CHANGE, forceDisplayAsPassword);
				//_stageText.removeEventListener(FocusEvent.FOCUS_IN, onFocusIn);
				//_stageText.removeEventListener(FocusEvent.FOCUS_OUT, onFocusOut);
				_stageText.removeEventListener(FocusEvent.FOCUS_OUT, forceDisplayAsPassword);
			}
			_stageText.displayAsPassword = b;
		}
		
		public function get maxChars ( ):int
		{
			return _maxChars;
		}
		public function set maxChars ( n:int ):void
		{
			_maxChars = n;
		}
		
		public function set restrict ( s:String ):void
		{
			_restrict = s;
			_stageText.restrict = s;
		}
		
		public function get length ( ):int
		{
			return _stageText.text.length;
		}
		
		public function getTextFormat(beginIndex:int = -1, endIndex:int = -1):TextFormat
		{
			return null;
		}
		
		public function setTextFormat(format:TextFormat, beginIndex:int = -1, endIndex:int = -1):void
		{
			_hasChanged = true;
		}

		public function get parent():DisplayObjectContainer
		{
			return _parent;
		}

		public function set parent(value:DisplayObjectContainer):void
		{
			_parent = value;
		}
		
		public function set fontSize ( s:Number ):void
		{
			if ( _stageText.fontSize == s ) return;
			_stageText.fontSize = s;
			_hasChanged = true;
		}
		
		public function get fontSize ( ):Number
		{
			return _stageText.fontSize;
		}
		
		public function set softKeyboardType ( s:String ):void
		{
			_stageText.softKeyboardType = s;
		}
		
		public function get softKeyboardType ( ):String
		{
			return _stageText.softKeyboardType;
		}
		
		/**
		 * Removes StageText component "from stage" and adds StageText snapshot to displayobject defined as parent 
		 */
		public function freeze ( ):void
		{
			removeSnapshot();
			if ( _snapShotText != text )
			{
				_hasChanged = true;
				_snapShotText = text;
			}
			if ( _hasChanged )
			{
				if ( _canvas is BitmapData ) _canvas.dispose();
				_canvas = new BitmapData(width, height, true, 0x00000000);
				_stageText.drawViewPortToBitmapData(_canvas);
				_stageText.stage = null;
				_snapshot = new Bitmap(_canvas);
			}
			_snapshot.x = _localViewport.x;
			_snapshot.y = _localViewport.y;
			_parent.addChild(_snapshot);
		}
		
		/**
		 * Removes StageText snapshot (bitmap instance) from displayobject defined as parent and adds StageText component again
		 */
		public function unfreeze ( ):void
		{
			removeSnapshot();
			if ( _isVisible ) _stageText.stage = _parent.stage;
		}
		
		protected function removeSnapshot ( ):void
		{
			if (_snapshot != null && _parent.contains(_snapshot) )
			{
				_parent.removeChild(_snapshot);
			}
		}
		
		/*/
		public function set visible ( v:Boolean ):void
		{
			_isVisible = v;
			if (_snapshot != null && _parent.contains(_snapshot) )
			{
				_snapshot.visible = v;
			}
			if ( v )
			{
				_stageText.stage = _parent.stage;
			}
			else {
				_stageText.stage = null;
			}
		}
		//*/
		
		public function set visible ( v:Boolean ):void
		{
			_isVisible = v;
			if ( v )
			{
				if (_snapshot != null && _parent.contains(_snapshot) )
				{
					_snapshot.visible = true;
				}
				else {
					_stageText.stage = _parent.stage;
				}
			}
			else {
				if (_snapshot != null && _parent.contains(_snapshot) )
				{
					_snapshot.visible = false;
				}
				else {
					_stageText.stage = null;
				}
			}
		}
		
		public function get visible ( ):Boolean
		{
			return _isVisible;
		}
		
		public function destroy ( ):void
		{
			_stageText.removeEventListener(Event.CHANGE, forceDisplayAsPassword);
			//_stageText.removeEventListener(FocusEvent.FOCUS_IN, onFocusIn);
			//_stageText.removeEventListener(FocusEvent.FOCUS_OUT, onFocusOut);
			_stageText.removeEventListener(FocusEvent.FOCUS_OUT, forceDisplayAsPassword);
			removeSnapshot();
			if ( _canvas is BitmapData ) _canvas.dispose();
			_snapshot = null;
			_stageText.stage = null;
		}
		
		
	}
}