package de.aoemedia.mobile.ui.components.skins
{
	import flash.display.Bitmap;

	public class InputFieldBitmaps
	{
		public var unfocused:Bitmap;
		public var focused:Bitmap;
		public var disabled:Bitmap;
		public var invalid:Bitmap;
		public var valid:Bitmap;
		
		public function InputFieldBitmaps ( unfocused:Bitmap,
											focused:Bitmap,
											disabled:Bitmap,
											invalid:Bitmap,
											valid:Bitmap )
		{
			this.unfocused = unfocused;
			this.focused = focused;
			this.disabled = disabled;
			this.invalid = invalid;
			this.valid = valid;
		}
	}
}