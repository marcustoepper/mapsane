package de.aoemedia.mobile.ui.components.base {
import com.kaizimmer.ui.Screen;

import de.aoemedia.animation.Juggler;
import de.aoemedia.mobile.ui.components.ILayoutComponent;

import flash.display.Sprite;
import flash.display.Stage;

public class AbstractUIComponent extends Sprite implements IComponent, ILayoutComponent {
    public static var scaleFactor:Number = 1;

    protected var _juggler:Juggler;

    protected var screenWidth:uint = 0;
    protected var screenHeight:uint = 0;
    protected var mainStage:Stage;

    protected var _paddingLeft:Number = 0;
    protected var _paddingRight:Number = 0;
    protected var _paddingTop:Number = 0;
    protected var _paddingBottom:Number = 0;

    private var _marginBottom:Number = 0;
    private var _marginTop:Number = 0;
    private var _marginRight:Number = 0;
    private var _marginLeft:Number = 0;

    protected var componentWidth:uint = 0;
    protected var componentHeight:uint = 0;

    protected var _stopInvalidation:Boolean = false;
    protected var _needsInvalidation:Boolean = false;
    protected var _left:Number;
    protected var _right:Number;

    private static var __staticsInitialized:Boolean = false;
    private static var __screenWidth:uint = 0;
    private static var __screenHeight:uint = 0;
    private static var __mainStage:Stage;
    private static var __juggler:Juggler;

    public function AbstractUIComponent() {
        if (!__staticsInitialized) _initStatics();
        _init();
        init();
        createChildren();
        draw();
    }

    public function get componentSprite():Sprite {
        return this;
    }

    public function scale(designPixels:Number):int {
        return int(designPixels * scaleFactor);
    }

    public function setSize(width:Number = -1, height:Number = -1):void {
        if (width == -1) {
            if (componentWidth == 0) {
                componentWidth = this.width * scaleFactor;
            }
        } else {
            componentWidth = width * scaleFactor;
        }

        if (height == -1) {
            if (componentHeight == 0) {
                componentHeight = this.height * scaleFactor;
            }
        } else {
            componentHeight = height * scaleFactor;
        }
    }

    public function get juggler():Juggler {
        return _juggler;
    }

    private function _initStatics():void {
        __staticsInitialized = true;
        __screenWidth = Screen.stage.fullScreenWidth;
        __screenHeight = Screen.stage.fullScreenHeight;
        __mainStage = Screen.stage;
        __juggler = new Juggler(__mainStage);
    }

    private function _init():void {
        scaleFactor = Screen.scaleFactor;
        screenWidth = __screenWidth;
        screenHeight = __screenHeight;
        mainStage = __mainStage;
        _juggler = __juggler;
    }

    protected function init():void {

    }

    protected function createChildren():void {

    }

    protected function draw():void {

    }

    protected function invalidate():void {
        if (_stopInvalidation) {
            _needsInvalidation = true;
            return;
        }
        draw();
        setSize();
        _needsInvalidation = false;
    }

    protected function set stopInvalidation(isStopped:Boolean):void {
        _stopInvalidation = isStopped;
        if (!isStopped && _needsInvalidation) invalidate();
    }

    protected function get stopInvalidation():Boolean {
        return _stopInvalidation;
    }

    public function get paddingLeft():Number {
        return _paddingLeft;
    }

    public function set paddingLeft(paddingLeft:Number):void {
        _paddingLeft = paddingLeft;
    }

    public function get paddingRight():Number {
        return _paddingRight;
    }

    public function set paddingRight(paddingRight:Number):void {
        _paddingRight = paddingRight;
    }

    public function get paddingTop():Number {
        return _paddingTop;
    }

    public function set paddingTop(paddingTop:Number):void {
        _paddingTop = paddingTop;
    }

    public function get paddingBottom():Number {
        return _paddingBottom;
    }

    public function set paddingBottom(paddingBottom:Number):void {
        _paddingBottom = paddingBottom;
    }

    public function get left():Number {
        return _left;
    }

    public function set left(left:Number):void {
        _left = left;
    }

    public function get right():Number {
        return _right;
    }

    public function set right(right:Number):void {
        _right = right;
    }

    override public function get height():Number {
        return super.height + _paddingBottom * scaleFactor;
    }

    override public function get width():Number {
        return super.width + _paddingRight * scaleFactor;
    }

    public function get marginLeft():Number {
        return _marginLeft;
    }

    public function set marginLeft(marginLeft:Number):void {
        _marginLeft = marginLeft;
    }

    public function get marginRight():Number {
        return _marginRight;
    }

    public function set marginRight(marginRight:Number):void {
        _marginRight = marginRight
    }

    public function get marginTop():Number {
        return _marginTop;
    }

    public function set marginTop(marginTop:Number):void {
        _marginTop = marginTop;
    }

    public function get marginBottom():Number {
        return _marginBottom;
    }

    public function set marginBottom(marginBottom:Number):void {
        _marginBottom = marginBottom;
    }

    override public function set x(value:Number):void {
        super.x = value + paddingLeft * scaleFactor;
    }

    override public function set y(value:Number):void {
        super.y = value + paddingTop * scaleFactor;
    }

    public function get scaleFactor():Number {
        return AbstractUIComponent.scaleFactor;
    }

    public function set scaleFactor(scaleFactor:Number):void {
        AbstractUIComponent.scaleFactor = scaleFactor;
    }
}
}