package de.aoemedia.mobile.ui.components
{
import de.aoemedia.mobile.ui.components.base.AbstractUIComponent;
import de.aoemedia.utils.TextFieldUtil;

import flash.text.AntiAliasType;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import flash.text.TextFormat;

public class LabelComponent extends AbstractUIComponent implements ILabelComponent
{
	protected var _textField:TextField;

	protected var isDown:Boolean = false;

	protected var _shortenText:Boolean = false;

	public function LabelComponent(label:String = "", tF:TextFormat = null) {
		super();
		stopInvalidation = true;
		if(label != "") {
			this.label = label;
		}
		if(tF != null) {
			this.textFormat = tF;
		}
		stopInvalidation = false;

	}

	override public function setSize(w:Number = -1, h:Number = -1):void {
		componentWidth = _textField.width;
		componentHeight = _textField.height;
	}

	public function get textField():TextField {
		return _textField;
	}

	public function get label():String {
		return _textField.text;
	}

	public function set label(value:String):void {
		if(_textField.text == value) return;

//		_textField.cacheAsBitmap = false;
		if(_shortenText) {
			TextFieldUtil.fillText(_textField, value);
			_textField.height = _textField.textHeight + 4; // TODO Move to invalidateSize()
			_textField.width = _textField.textWidth + 4;
		} else {
			_textField.text = value;
		}

		//_textField.cacheAsBitmap = true;
		invalidate();
	}

	public function set color(col:Number):void {
		_textField.textColor = col;
	}

	public function get color():Number {
		return _textField.textColor;
	}

	// */
	public function set textFormat(tFormat:TextFormat):void {
		_textField.defaultTextFormat = tFormat;
		// _label.defaultTextFormat.size = Number(_label.defaultTextFormat.size) * scaleFactor;
		_textField.setTextFormat(tFormat);
		_textField.embedFonts = true;
		invalidate();
	}

	public function get textFormat():TextFormat {
		return _textField.getTextFormat();
	}

	// */

	override protected function createChildren():void {
		_textField = new TextField();
		_textField.selectable = false;
		_textField.autoSize = TextFieldAutoSize.LEFT;
		_textField.antiAliasType = AntiAliasType.ADVANCED;

		addChild(_textField);
		super.createChildren();
	}

	/**
	 * Disable Scrolling of Text
	 */
	public function disableScrolling():void {
		_textField.mouseWheelEnabled = false;
		_textField.mouseEnabled = false;
	}

	/*/
	 protected function createLabel ( tFormat:TextFormat ):TextField
	 {
	 _label = new TextField();
	 _label.selectable = false;
	 _label.autoSize = TextFieldAutoSize.LEFT;
	 _label.antiAliasType = AntiAliasType.ADVANCED;
	 _label.defaultTextFormat = tFormat;
	 _label.setTextFormat(tFormat);
	 _label.embedFonts = true;
	 addChild(_label);
	 return _label;
	 }
	 // */

	public function get shortenText():Boolean {
		return _shortenText;
	}

	public function set shortenText(shortenText:Boolean):void {
		_shortenText = shortenText;
		_textField.autoSize = TextFieldAutoSize.NONE;
	}

}
}
