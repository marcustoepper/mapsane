package de.aoemedia.mobile.ui.components.forms
{
	import flash.text.TextFormat;
	
	import de.aoemedia.mobile.ui.components.skins.InputFieldBitmaps;
	
	public class NativeInputField extends InputField
	{
		private var _nativeTextField:NativeTextField;
		public function NativeInputField(bmps:InputFieldBitmaps, tF:TextFormat=null, width:Number=200, height:Number=58)
		{
			super(bmps, tF, width, height);
		}
		
		override protected function createChildren ( ):void
		{
			_nativeTextField = new NativeTextField();
			_field = _nativeTextField;
			
			var f:NativeTextField = _field as NativeTextField;
			
			f.fontSize = 40;
			f.borderThickness = 1;
			f.fontFamily = "Arial";
			f.text = "This is native text.";
			
			//_nativeTextField.stageText.viewPort = new Rectangle(0,0, 200,100);
			//addChild(field as FlashTextField);
		}
	}
}