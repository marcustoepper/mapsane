package de.aoemedia.mobile.ui.components.container {
public class LayoutOrientation {
    public static const VERTICAL:String = "vertical";
    public static const HORIZONTAL:String = "horizontal";
}
}
