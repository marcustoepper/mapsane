package de.aoemedia.mobile.ui.components.forms {
import de.aoemedia.mobile.ui.components.skins.InputFieldBitmaps;

import flash.display.Stage;
import flash.events.Event;
import flash.text.StageTextInitOptions;
import flash.text.TextFormat;

public class StageInputField extends InputField {
    protected var _stageTextField:StageTextField;

    private var _isMultiline:Boolean;

    public function StageInputField(bmps:InputFieldBitmaps, tF:TextFormat = null, width:Number = 200, height:Number = 58, isMultiline:Boolean = false) {
        super(bmps, tF, width, height);
        _isMultiline = isMultiline;
    }

    override protected function init():void {
        //TODO: REMOVE EVENT HANDLER ON DESTROY
        //addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
    }

    protected function onAddedToStage(event:Event):void {
        //TODO: REMOVE showText invokation! should be done explicitly by a managing instance!
        //showText(stage);

        //_stageTextField.text = "foobar";

        /*/
         ////////////////////////////////////////////////////
         // DEBUGGING CODE - NEEDS TO BE REMOVED WHEN READY
         ////////////////////////////////////////////////////
         //_stageTextField.text = "foobar";
         //_stageTextField.stageText.color = 0xff0000;
         //_stageTextField.stageText.fontSize = 40;
         //_stageTextField.stageText.editable = true;
         //_stageTextField.stageText.displayAsPassword = true;

         _stageTextField.stageText.addEventListener(Event.CHANGE, onChange);
         _stageTextField.stageText.addEventListener(FocusEvent.FOCUS_IN, onFocusIn);
         _stageTextField.stageText.addEventListener(FocusEvent.FOCUS_OUT, onFocusOut);

         _debug("onAddedToStage");

         function onChange(event:Event):void
         {
         _debug("onChange");
         //_stageTextField.stageText.viewPort = new Rectangle(200,200, 200,100);

         }

         function onFocusIn(event:Event):void
         {
         _debug("onFocusIn");
         //_stageTextField.stageText.viewPort = new Rectangle(200,200, 200,100);

         }

         function onFocusOut(event:Event):void
         {
         _debug("onFocusOut");
         //_stageTextField.stageText.viewPort = new Rectangle(200,200, 200,100);
         }
         //*/

    }

    /*/
     protected function _debug ( msg:String ):void
     {
     var t:StageText = _stageTextField.stageText;
     trace(msg + ": " + t.text + " | x: " + t.viewPort.x + " | y: " + t.viewPort.y + " | w: " + t.viewPort.width + " | h: " + t.viewPort.height + " | t.stage: " + t.stage + " | visible: " + t.visible);
     }
     //*/

    public function get stageTextField():StageTextField {
        return _stageTextField;
    }

    public function showText(stage:Stage):void {
        trace("StageInputField showText");
        _stageTextField.visible = true;
        //_stageTextField.stageText.stage = stage;
    }

    public function hideText():void {
        trace("StageInputField hideText");
        _stageTextField.visible = false;
        //_stageTextField.stageText.stage = null;
    }

    override protected function createChildren():void {
        _stageTextField = new StageTextField(new StageTextInitOptions(_isMultiline));
        _stageTextField.parent = this;
        _field = _stageTextField;
    }

    override public function set x(n:Number):void {
        super.x = n;
        _stageTextField.x = paddingHorizontal;
        //_stageTextField.x = n + paddingHorizontal;
        //_stageTextField.stageText.viewPort = new Rectangle(n,_stageTextField.stageText.viewPort.y, 200,100);
    }

    override public function set y(n:Number):void {
        super.y = n;
        _stageTextField.y = paddingVertical;
        //_stageTextField.y = n + paddingVertical;
        //_stageTextField.stageText.viewPort = new Rectangle(_stageTextField.stageText.viewPort.x,n, 200,100);
    }

    override public function setSize(w:Number = -1, h:Number = -1):void {
        //trace("StageInputField setSize | w: " + w + " | h: " + h);

        if (w < -1 || h < -1) {
            throw new Error("the size of an InputField can't be negativ.");
        }
        super.setSize(w, h);

        if (w >= 0) {
            var newWidth:Number = componentWidth - 2 * paddingHorizontal;
            if (newWidth < 0) {
                newWidth = 0;
            }
            _stageTextField.width = newWidth;
        }
        if (h >= 0) {
            var newHeight:Number = componentHeight - 2 * paddingVertical;
            if (newHeight < 0) {
                newHeight = 0;
            }
            _stageTextField.height = newHeight;
            //_stageTextField.y = paddingVertical;
        }

        /*/
         _stageTextField.x = paddingHorizontal;
         _stageTextField.y = (renderedBmp.height - _field.height) >> 1;
         _stageTextField.width = componentWidth - 2 * paddingHorizontal;
         //*/

        /*/
         _stageTextField.x = x + paddingHorizontal;
         _stageTextField.width = componentWidth - 2 * paddingHorizontal;
         if ( renderedBmp is ScaleBitmap ) {
         ScaleBitmap(renderedBmp).setSize(componentWidth, componentHeight);
         } else {
         renderedBmp.width = componentWidth;
         renderedBmp.height = componentHeight;
         }
         _stageTextField.y = y + (renderedBmp.height - _field.height) >> 1;
         //*/
    }

    public function set displayAsPassword(b:Boolean):void {
        _stageTextField.displayAsPassword = b;
    }

    public function get displayAsPassword():Boolean {
        return _stageTextField.displayAsPassword;
    }

    public function set fontSize(s:Number):void {
        _stageTextField.fontSize = s;
    }

    public function get fontSize():Number {
        return _stageTextField.fontSize;
    }

    public function set softKeyboardType(s:String):void {
        _stageTextField.softKeyboardType = s;
    }

    public function get softKeyboardType():String {
        return _stageTextField.softKeyboardType;
    }

    public function freeze():void {
        _stageTextField.freeze();
    }

    public function unfreeze():void {
        _stageTextField.unfreeze();
    }

    /**
     * From time to time it may happen that the coordinates of the StageInputField are changing.
     * (For example via an drag and drop operation.)
     * This method updates the corresponding StageText coordinates according to the current StageInputField position.
     */
    public function invalidateStageTextCoords():void {
        _stageTextField.invalidateCoordinates();
    }

}
}