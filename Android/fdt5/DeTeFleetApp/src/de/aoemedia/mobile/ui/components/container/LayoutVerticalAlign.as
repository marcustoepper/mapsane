package de.aoemedia.mobile.ui.components.container {
public class LayoutVerticalAlign {
    public static const TOP:String = "top";
    public static const MIDDLE:String = "middle";
    public static const BOTTOM:String = "bottom";
}
}
