package de.aoemedia.mobile.ui.components {
/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public interface ILayoutComponent {
    function get scaleFactor():Number;

    function set scaleFactor(scaleFactor:Number):void;

    function get paddingLeft():Number;

    function set paddingLeft(paddingLeft:Number):void;

    function get paddingRight():Number;

    function set paddingRight(paddingRight:Number):void;

    function get paddingTop():Number;

    function set paddingTop(paddingTop:Number):void;

    function get paddingBottom():Number;

    function set paddingBottom(paddingBottom:Number):void;

    function get marginLeft():Number;

    function set marginLeft(marginLeft:Number):void;

    function get marginRight():Number;

    function set marginRight(marginRight:Number):void;

    function get marginTop():Number;

    function set marginTop(marginTop:Number):void;

    function get marginBottom():Number;

    function set marginBottom(marginBottom:Number):void;

    function get y():Number;

    function set y(y:Number):void;

    function get x():Number;

    function set x(x:Number):void;

    function get width():Number;

    function get height():Number;

    /**
     * Describe the space on left left side ignoring existing elements.
     */
    function get left():Number;

    function set left(left:Number):void;

    function get right():Number;

    function set right(right:Number):void;
}
}
