package de.aoemedia.mobile.ui.components
{
	public interface ILabelComponent
	{
				function get label():String;

		function set label(value:String):void;

	}
}