package de.aoemedia.mobile.ui {
	import com.kaizimmer.ui.Screen;

	public class MultiDpiHelper
	{
		/**
		 * Scale the Pixels to Device Factor
		 */		
		public static function scale( designPixels:Number):int{
			return int( designPixels * Screen.scaleFactor );
		}
		
		/**
		 * Unsclale the Pixels
		 */
		public static function unscale( designPixels:Number):int{
			return int( designPixels / Screen.scaleFactor );
		}
	}
}