package de.aoemedia.mobile.ui.components.skins
{
	import flash.display.Bitmap;

	public class ButtonBitmaps
	{
		public var up:Bitmap;
		public var down:Bitmap;
		public var upDisabled:Bitmap;
		public var downDisabled:Bitmap;
		
		public function ButtonBitmaps ( up:Bitmap, 
										down:Bitmap, 
										upDisabled:Bitmap, 
										downDisabled:Bitmap )
		{
			this.up = up;
			this.down = down;
			this.upDisabled = upDisabled;
			this.downDisabled = downDisabled;
		}
	}
}