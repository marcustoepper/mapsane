package de.aoemedia.mobile.ui.components.forms
{
	import flash.events.IEventDispatcher;
	import flash.text.TextFormat;
	
	public interface ITextField extends IEventDispatcher
	{
		function set text ( s:String ):void;
		function get text ( ):String;
		
		function set textColor ( s:uint ):void;
		function get textColor ( ):uint;
		
		function set defaultTextFormat ( f:TextFormat ):void;
		function get defaultTextFormat ( ):TextFormat;
		
		function set embedFonts ( b:Boolean ):void;
		function get embedFonts ( ):Boolean;
		
		function set type ( t:String ):void;
		function get type ( ):String;
		
		function set x ( n:Number ):void;
		function get x ( ):Number;
		
		function set y ( n:Number ):void;
		function get y ( ):Number;
		
		function set width ( n:Number ):void;
		function get width ( ):Number;
		
		function set height ( n:Number ):void;
		function get height ( ):Number;
		
		function set border ( b:Boolean ):void;
		function get border ( ):Boolean;
		
		function set borderColor ( n:uint ):void;
		function get borderColor ( ):uint;
		
		function get multiline ( ):Boolean;
		function set multiline ( b:Boolean ):void;
		
		function get selectable ( ):Boolean;
		function set selectable ( b:Boolean ):void;
		
		function get displayAsPassword ( ):Boolean;
		function set displayAsPassword ( b:Boolean ):void;
		
		function get maxChars ( ):int;
		function set maxChars ( n:int ):void;
		
		function set restrict ( s:String ):void;
		
		function get length ( ):int;
		
		function getTextFormat(beginIndex:int = -1, endIndex:int = -1):TextFormat
		function setTextFormat(format:TextFormat, beginIndex:int = -1, endIndex:int = -1):void
		
	}
}