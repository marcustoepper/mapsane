package de.aoemedia.mobile.ui.components
{
import de.aoemedia.mobile.ui.MultiDpiHelper;
import flash.display.Sprite;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class RuleComponent extends Sprite implements ILayoutComponent
{
	protected var _paddingLeft:Number = 0;
	protected var _paddingRight:Number = 0;
	protected var _paddingTop:Number = 0;
	protected var _paddingBottom:Number = 0;

	protected var _left:Number;
	protected var _color:uint = 0x000000;
	protected var _height:Number;
	protected var _width:Number;
	protected var _right:Number;
    private var _scaleFactor:Number = 1;
    private var _marginBottom:Number = 0;
    private var _marginLeft:Number = 0;
    private var _marginTop:Number = 0;
    private var _marginRight:Number = 0;

	public function RuleComponent(height:Number = 0, width:Number = 0, color:uint = 0x000000):void
	{
		//cacheAsBitmap = true;

		_height = height;
		_width = width;
		_color = color;

		draw();
	}

	public function draw():void
	{
		graphics.clear();

		graphics.beginFill(_color);
		graphics.drawRect(0, 0, MultiDpiHelper.scale(_width), MultiDpiHelper.scale(_height));
		graphics.endFill();
	}

	override public function set height(height:Number):void {
		if (_height == height) {
			return;
		}
		_height = height;

		draw();
	}


	override public function set width(width:Number):void {
		if (_width == width) {
			return;
		}
		_width = width;

		draw();
	}

	public function get paddingLeft():Number {
		return _paddingLeft;
	}

	public function set paddingLeft(paddingLeft:Number):void {
		if (_paddingLeft == paddingLeft) {
			return;
		}
		_paddingLeft = paddingLeft;
	}

	public function get paddingTop():Number {
		return _paddingTop;
	}

	public function set paddingTop(paddingTop:Number):void {
		if (_paddingTop == paddingTop) {
			return;
		}
		_paddingTop = paddingTop;
	}

	public function get paddingRight():Number {
		return _paddingRight;
	}

	public function set paddingRight(paddingRight:Number):void {
		if (_paddingRight == paddingRight) {
			return;
		}
		_paddingRight = paddingRight;
	}

	public function get paddingBottom():Number {
		return _paddingBottom;
	}

	public function set paddingBottom(paddingBottom:Number):void {
		if (_paddingBottom == paddingBottom) {
			return;
		}
		_paddingBottom = paddingBottom;
	}


	public function get left():Number {
		return _left;
	}

	public function set left(left:Number):void {
		if (_left == left) {
			return;
		}

		_left = left;
	}

	public function get right():Number {
		return _right;
	}

	public function set right(right:Number):void {
		if (_right == right) {
			return;
		}
		_right = right;
	}
	
	public function get color():uint {
		return _color;
	}

	public function set color(color:uint):void {
		if (color == _color) {
			return;
		}

		_color = color;

		draw();
	}


    override public function get height():Number {
        return super.height + _paddingBottom*scaleFactor;
    }

    override public function get width():Number {
        return super.width + _paddingRight*scaleFactor;
    }

    public function get marginLeft():Number {
        return _marginLeft;
    }

    public function set marginLeft(marginLeft:Number):void {
        _marginLeft = marginLeft;
    }

    public function get marginRight():Number {
        return _marginRight;
    }

    public function set marginRight(marginRight:Number):void {
        _marginRight = marginRight
    }

    public function get marginTop():Number {
        return _marginTop;
    }

    public function set marginTop(marginTop:Number):void {
        _marginTop = marginTop;
    }

    public function get marginBottom():Number {
        return _marginBottom;
    }

    public function set marginBottom(marginBottom:Number):void {
        _marginBottom = marginBottom;
    }

    override public function set x(value:Number):void{
        super.x = value + paddingLeft*scaleFactor;
    }

    override public function set y(value:Number):void{
        super.y = value + paddingTop*scaleFactor;
    }

    public function get scaleFactor():Number {
        return _scaleFactor;
    }

    public function set scaleFactor(scaleFactor:Number):void {
        _scaleFactor = scaleFactor;
    }
}
}
