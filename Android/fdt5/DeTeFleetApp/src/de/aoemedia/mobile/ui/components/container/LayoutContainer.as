package de.aoemedia.mobile.ui.components.container {
import de.aoemedia.mobile.ui.components.ILayoutComponent;

import flash.display.DisplayObject;
import flash.display.Sprite;

import org.osflash.signals.Signal;

/**
 * With the LayoutContainer you can easily create Layouts without move every element
 *
 * TODO add top
 * TODO add bottom
 * TODO add right
 *
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class LayoutContainer implements ILayoutComponent {
    public static var scaleFactor:Number = 1;

    protected var _orientation:String = LayoutOrientation.VERTICAL;

    protected var _items:Vector.<ILayoutComponent>;

    protected var _gap:Number = 5;
    protected var _gapScaled:Number = 5;

    protected var _paddingLeft:Number = 0;
    protected var _paddingRight:Number = 0;
    protected var _paddingTop:Number = 0;
    protected var _paddingBottom:Number = 0;

    protected var _marginLeft:Number = 0;
    protected var _marginRight:Number = 0;
    protected var _marginTop:Number = 0;
    protected var _marginBottom:Number = 0;

    protected var _left:Number;
    protected var _right:Number;

    protected var _horizontalAlign:String;
    protected var _verticalAlign:String;

    protected var _x:Number = 0;
    protected var _y:Number = 0;

    private var _view:Sprite;

    protected var _width:Number = 0;
    protected var _height:Number = 0;

    public var invalidateSignal:Signal;
    protected var _invalidationNeeded:Boolean = true;
    /**
     * Manually stop Invalidation. Use this for performance issues
     */
    public var stopInvalidation:Boolean = false;

    protected var _percentalWidth:Number;
    protected var _percentalHeight:Number;

    protected var _explicitWidth:Number;
    protected var _explicitHeight:Number;

    protected var _parent:LayoutContainer;

    public var showBorders:Boolean = false;

    public function LayoutContainer(view:Sprite):void {
        _view = view;
        _items = new <ILayoutComponent>[];
        invalidateSignal = new Signal();

        _gapScaled = _gap * scaleFactor;
    }

    /**
     * Check if item exist in Layout Container
     * @param item
     * @return
     */
    public function contains(item:ILayoutComponent):Boolean {
        for (var i:int = _items.length - 1; i >= 0; i--) {
            if (_items[i] == item) {
                return true;
            }
        }
        return false;
    }

    /**
     * Add Item to layoutContainer
     */
    public function addItem(item:ILayoutComponent):void {
        if (item is DisplayObject) {
            item.scaleFactor = scaleFactor;
            _items[_items.length] = item;
        } else if (item is LayoutContainer) {
            item.scaleFactor = scaleFactor;
            LayoutContainer(item).invalidateSignal.add(invalidate);
            LayoutContainer(item).parent = this;
            _items[_items.length] = item;
        } else {
            throw new Error("Only add DisplayObject or Layout objects");
        }

        invalidate();
    }

    /**
     * Add Item to layoutContainer at Index
     */
    public function addItemAt(item:ILayoutComponent, index:int):void {
        if (item is DisplayObject) {
            _items.splice(index, 0, item);
        } else if (item is LayoutContainer) {
            LayoutContainer(item).invalidateSignal.add(invalidate);
            LayoutContainer(item).parent = this;
            _items.splice(index, 0, item);
        } else {
            throw new Error("Only add DisplayObject or Layout objects");
        }

        invalidate();
    }

    /**
     * Get Item at given Index
     *
     * @return
     */
    public function getItemAt(index:int):ILayoutComponent {
        return _items[index];
    }

    /**
     * Returns the number of added items
     */
    public function get numberItems():int {
        return _items.length;
    }

    /**
     * Remove Item from container
     */
    public function removeItem(item:ILayoutComponent):void {
        for (var i:int = 0; i < _items.length; i++) {
            if (_items[i] == item) {
                if (_items[i] is DisplayObject && _view.contains(DisplayObject(item))) {
                    _view.removeChild(DisplayObject(item));
                }

                _items.splice(i, 1);
                break;
            }
        }

        invalidate();
    }

    /**
     * Clear Items
     */
    public function clear(recursive:Boolean = false):void {
        for (var i:int = 0; i < _items.length; i++) {
            if (_items[i] is LayoutContainer) {
                if (recursive) {
                    LayoutContainer(_items[i]).clear();
                }
            } else {
                if (_view.contains(DisplayObject(_items[i]))) {
                    _view.removeChild(DisplayObject(_items[i]));
                }
            }
        }

        _items = new Vector.<ILayoutComponent>();

        invalidate();
    }

    /**
     * Recalculate Item Properties
     */
    public function invalidate(force:Boolean = false):void {
        if (!force && (!_invalidationNeeded || stopInvalidation)) {
            return;
        }
        _invalidationNeeded = false;
        measureHeightAndWidth();

        var currentX:Number = _x + (_marginRight + _paddingLeft) * scaleFactor;
        var currentY:Number = _y + (_marginTop + _paddingTop) * scaleFactor;

        var absoluteVerticalPositoning:Boolean;
        var absoluteHorizontalPositoning:Boolean;

        for each (var item:ILayoutComponent in _items) {
            absoluteHorizontalPositoning = false;
            absoluteVerticalPositoning = false;

            // Calc items x
            if (!isNaN(item.left)) {
                absoluteHorizontalPositoning = true;
                item.x = _x + (item.left + item.marginLeft) * scaleFactor;
            } else if (!isNaN(item.right)) {
                absoluteHorizontalPositoning = true;
                if (item is LayoutContainer) {
                    item.x = _x + _width - (_marginRight + _paddingRight) * scaleFactor - item.right - item.width;
                } else {
                    // Only if DisplayObject use scale factor
                    var scaled:Number = scaleFactor * (_marginRight + _paddingRight + item.right + item.paddingRight);
                    item.x = _x + _width - item.width - scaled;
                }
            }
            // Basic positoning
            if (!absoluteHorizontalPositoning) {
                item.x = currentX + item.marginLeft * scaleFactor;
            }
            if (!absoluteVerticalPositoning) {
                item.y = currentY + item.marginTop * scaleFactor;
            }

            if (_orientation == LayoutOrientation.VERTICAL) {
                switch (_horizontalAlign) {
                    // We don't need LayoutHorizontalAlign.LEFT here, because it's default
                    case LayoutHorizontalAlign.CENTER:
                        item.x = _x + _marginLeft * scaleFactor + (_width - (_marginRight + _marginLeft) * scaleFactor) / 2 - (item.width + (item.marginLeft + item.marginRight) * scaleFactor) / 2;
                        break;
                    case LayoutHorizontalAlign.RIGHT:
                        item.x = _x + _width - item.width - (item.marginRight + _paddingRight + _marginRight) * scaleFactor;
                        break;
                }

                // Where is the next y to find
                currentY = item.y + item.height + (_gap + item.marginBottom) * scaleFactor;
            } else {
                switch (_verticalAlign) {
                    // we don't need LayoutVerticalAlign.TOP here, because it's default
                    case LayoutVerticalAlign.MIDDLE:
                        item.y = _y + _marginTop * scaleFactor + (_height - (_marginTop + _marginBottom) * scaleFactor) / 2 - (item.height + (item.marginTop + item.marginBottom) * scaleFactor) / 2;
                        break;
                    case LayoutVerticalAlign.BOTTOM:
                        item.y = _y + _height - item.height - (item.marginBottom + _paddingBottom + _marginBottom) * scaleFactor;
                        break;
                }

                // Where is the next x to find
                currentX = item.x + item.width + (_gap + item.marginRight) * scaleFactor;
            }

            if (item is LayoutContainer) {
                LayoutContainer(item).invalidate(true);
            }
        }

        invalidateSignal.dispatch();
        _invalidationNeeded = true;
    }

    /**
     * Calc Height and Width of this LayoutContainer
     */
    public function measureHeightAndWidth():void {
        // if explicitHeight or Width is set ignore the others
        if (_explicitHeight) {
            _height = _explicitHeight;
        }
        if (_explicitWidth) {
            _width = _explicitWidth;
        }
        if (_explicitHeight && _explicitWidth) {
            return;
        }

        // if percentalHeight and Width is set ignore the rest
        if (_percentalHeight) {
            if (_parent) {
                _height = parent.innerHeight * (_percentalHeight / 100);
            } else {
                _height = _view.height * (_percentalHeight / 100);
            }
        }
        if (_percentalWidth) {
            if (_parent) {
                _width = parent.innerWidth * (_percentalWidth / 100);
            } else {
                _width = _view.width * (_percentalWidth / 100);
            }
        }
        if (_percentalHeight && _percentalWidth) {
            return;
        }

        var width:Number = _paddingLeft * scaleFactor;
        var height:Number = _paddingTop * scaleFactor;

        for each (var item:ILayoutComponent in _items) {
            var itemHeight:Number;
            var itemWidth:Number;

            if (item is LayoutContainer) {
                LayoutContainer(item).measureHeightAndWidth();
            }

            itemHeight = item.height + item.marginTop * scaleFactor + item.marginBottom * scaleFactor;
            itemWidth = item.width + item.marginLeft * scaleFactor + item.marginRight * scaleFactor;

            if (_orientation == LayoutOrientation.VERTICAL) {
                height += itemHeight;

                if (item.left && width < item.left * scaleFactor + itemWidth) { // we have to check if item + left space is bigger than current biggest item
                    width = item.left * scaleFactor + itemWidth;
                } else if (width < itemWidth) {
                    width = _paddingLeft * scaleFactor + itemWidth;
                }
            } else {
                // Where is the next x to find
                if (item.left) {
                    width = item.left * scaleFactor + item.width;
                } else {
                    width += itemWidth;
                }

                if (height < itemHeight) {
                    height = _paddingTop * scaleFactor + itemHeight;
                }
            }

        }

        if (_orientation == LayoutOrientation.VERTICAL) {
            height += (_items.length - 1) * _gapScaled; // -1 because we only use gaps between items not at the end
        } else {
            width += (_items.length - 1) * _gapScaled;
        }

        // Only set if not explicit or percental set
        if (isNaN(_explicitHeight) && isNaN(_percentalHeight)) {
            _height = height + _paddingBottom * scaleFactor;
        }
        if (!_explicitWidth && !_percentalWidth) {
            _width = width + _paddingRight * scaleFactor;
        }
    }

    /**
     * Add Childs to view
     */
    public function addItemsToView():void {
        for (var i:int = 0; i < _items.length; i++) {
            if (_items[i] is LayoutContainer) {
                LayoutContainer(_items[i]).addItemsToView();
            } else {
                if (!_view.contains(DisplayObject(_items[i]))) {
                    _view.addChild(DisplayObject(_items[i]));
                }

            }
        }

        if (showBorders) {
            // Only for debugging
            drawBorders();
        }
    }

    /**
     * Set Paddings at ones
     *
     * Paddings are the inner space to the childs
     *
     * @param left
     * @param top
     * @param right
     * @param bottom
     */
    public function setPadding(left:Number = 0, top:Number = 0, right:Number = 0, bottom:Number = 0):void {
        paddingLeft = left;
        paddingTop = top;
        paddingRight = right;
        paddingBottom = bottom;
    }

    /**
     * Set Margins at ones
     *
     * Margin are the outer space to the neighbour items
     *
     * @param left
     * @param top
     * @param right
     * @param bottom
     */
    public function setMargin(left:Number = 0, top:Number = 0, right:Number = 0, bottom:Number = 0):void {
        marginLeft = left;
        marginTop = top;
        marginRight = right;
        marginBottom = bottom;
    }

    /**
     * draws border on every item. Just for debbuging
     */
    private function drawBorders():void {
        var border:Sprite = new Sprite();
        border.graphics.clear();
        border.graphics.lineStyle(1, 0x00ff00);
        border.graphics.drawRect(scaleFactor * (x), scaleFactor * (y), width - 1, height);

        var paddingBorder:Sprite = new Sprite();
        paddingBorder.graphics.clear();
        paddingBorder.graphics.lineStyle(1, 0x00ffff);
        paddingBorder.graphics.drawRect(scaleFactor * (x + paddingLeft), scaleFactor * (y + paddingTop), width - scaleFactor * (paddingLeft - paddingRight) - 1, height - scaleFactor * (paddingTop - paddingBottom));
        // border.graphics.lineStyle(1, Math.random() * 0xFFFFFF);
        // border.graphics.drawRect(0, 0, this.width, this.height);
        // border.graphics.endFill();

        _view.addChild(border);
        _view.addChild(paddingBorder);

        for (var i:int = 0; i < _items.length; i++) {
            if (!(_items[i] is LayoutContainer)) {
                var itemborder:Sprite = new Sprite();
                itemborder.graphics.clear();
                itemborder.graphics.lineStyle(1, 0xff0000);
                itemborder.graphics.drawRect(_items[i].x, _items[i].y, _items[i].width - 1, _items[i].height);

                _view.addChild(itemborder);
            }
        }

    }

    /**
     * The innerHeight is the Height of all Child Items without margin and padding, but including gaps
     * the Margins and Padding of the Childs will be included
     */
    public function get innerHeight():Number {
        if (parent) {
            return parent.innerHeight - (_paddingTop + _paddingBottom) * scaleFactor;
        }
        return _height - (_paddingTop + _paddingBottom) * scaleFactor;
    }

    /**
     * The innerWidth is the Width of all Child Items without margin and padding, but including gaps
     * the Margins and Padding of the Childs will be included
     */
    public function get innerWidth():Number {
        if (parent) {
            return parent.innerWidth - (_paddingLeft + _paddingRight) * scaleFactor;
        }
        return _width - (_paddingLeft + _paddingRight) * scaleFactor;
    }

    /*****************************************
     * PROPERTY SETTER AND GETTER
     *****************************************/

    public function get y():Number {
        return _y;
    }

    public function set y(y:Number):void {
        if (_y == y) {
            return;
        }
        _y = y;

        invalidate();
    }

    public function get x():Number {
        return _x;
    }

    public function set x(x:Number):void {
        if (_x == x) {
            return;
        }
        _x = x;

        invalidate();
    }

    /**
     * Unscaled
     */
    public function get gap():Number {
        return _gap;
    }

    /**
     * Unscaled
     * @param gap
     */
    public function set gap(gap:Number):void {
        if (_gap == gap) {
            return;
        }
        _gap = gap;
        _gapScaled = _gap * scaleFactor;

        invalidate();
    }

    public function get orientation():String {
        return _orientation;
    }

    /**
     * Use Consts of LayoutOrientation for setting
     * @param orientation
     */
    public function set orientation(orientation:String):void {
        if (_orientation == orientation) {
            return;
        }
        _orientation = orientation;

        invalidate();
    }

    /**
     * Width includes padding, but not margins
     */
    public function get width():Number {
        return _width;
    }

    /**
     * Height includes paddings, but nit margins
     */
    public function get height():Number {
        return _height;
    }

    public function get align():String {
        return _horizontalAlign;
    }

    public function set align(align:String):void {
        _horizontalAlign = align;

        invalidate();
    }

    public function get verticalAlign():String {
        return _verticalAlign;
    }

    /**
     * Use Constant in LayoutVerticalAlign for setting
     * Will be ignored at  LayoutOrientation.VERTICAL
     *
     * @param verticalAlign
     * @see LayoutVerticalAlign
     */
    public function set verticalAlign(verticalAlign:String):void {
        if (_verticalAlign == verticalAlign) {
            return;
        }
        _verticalAlign = verticalAlign;

        invalidate();
    }

    public function get horizontalAlign():String {
        return _horizontalAlign;
    }

    /**
     * Use Constant in LayoutHorizontalAlign for setting
     * Will be ignored at  LayoutOrientation.HORIZONTAL
     *
     * @param horizontalAlign
     * @see LayoutHorizontalAlign
     */
    public function set horizontalAlign(horizontalAlign:String):void {
        if (_horizontalAlign == horizontalAlign) {
            return;
        }
        _horizontalAlign = horizontalAlign;

        invalidate();
    }

    public function get paddingRight():Number {
        return _paddingRight;
    }

    public function set paddingRight(paddingRight:Number):void {
        if (_paddingRight == paddingRight) {
            return;
        }
        _paddingRight = paddingRight;

        invalidate();
    }

    public function get paddingBottom():Number {
        return _paddingBottom;
    }

    public function set paddingBottom(paddingBottom:Number):void {
        if (_paddingBottom == paddingBottom) {
            return;
        }
        _paddingBottom = paddingBottom;

        invalidate();
    }

    public function get paddingLeft():Number {
        return _paddingLeft;
    }

    public function set paddingLeft(paddingLeft:Number):void {
        if (_paddingLeft == paddingLeft) {
            return;
        }
        _paddingLeft = paddingLeft;

        invalidate();
    }

    public function get paddingTop():Number {
        return _paddingTop;
    }

    public function set paddingTop(paddingTop:Number):void {
        if (_paddingTop == paddingTop) {
            return;
        }
        _paddingTop = paddingTop;

        invalidate();
    }

    public function get left():Number {
        return _left;
    }

    /**
     * left sets the item on the left the given amount of pixel ignoring order and paddings
     *
     * @param left
     */
    public function set left(left:Number):void {
        if (_left == left) {
            return;
        }

        _left = left;

        invalidate();
    }

    public function get right():Number {
        return _right;
    }

    public function set right(right:Number):void {
        if (_right == right) {
            return;
        }
        _right = right;

        invalidate();
    }

    public function set percentalWidth(width:Number):void {
        if (_percentalWidth == width) {
            return;
        }
        _percentalWidth = width;

        invalidate();
    }

    public function set percentalHeight(height:Number):void {
        if (_percentalHeight == height) {
            return;
        }
        _percentalHeight = height;

        invalidate();
    }

    public function get percentalWidth():Number {
        return _percentalWidth;
    }

    public function get percentalHeight():Number {
        return _percentalHeight;
    }

    public function get explicitWidth():Number {
        return _explicitWidth;
    }

    public function set explicitWidth(explicitWidth:Number):void {
        if (_explicitWidth == explicitWidth) {
            return;
        }
        _explicitWidth = explicitWidth;

        invalidate();
    }

    public function get explicitHeight():Number {
        return _explicitHeight;
    }

    public function set explicitHeight(explicitHeight:Number):void {
        if (_explicitHeight == explicitHeight) {
            return;
        }
        _explicitHeight = explicitHeight;

        invalidate();
    }

    public function get parent():LayoutContainer {
        return _parent;
    }

    public function set parent(parent:LayoutContainer):void {
        _parent = parent;
    }

    public function get marginLeft():Number {
        return _marginLeft;
    }

    /**
     *
     * @param value
     */
    public function set marginLeft(value:Number):void {
        if (_marginLeft == value) {
            return;
        }
        _marginLeft = value;

        invalidate();
    }

    public function get marginRight():Number {
        return _marginRight;
    }

    /**
     *
     * @param value
     */
    public function set marginRight(value:Number):void {
        if (_marginRight == value) {
            return;
        }
        _marginRight = value;

        invalidate();
    }

    public function get marginTop():Number {
        return _marginTop;
    }

    /**
     *
     * @param value
     */
    public function set marginTop(value:Number):void {
        if (_marginTop == value) {
            return;
        }
        _marginTop = value;

        invalidate();
    }

    public function get marginBottom():Number {
        return _marginBottom;
    }

    /**
     *
     * @param value
     */
    public function set marginBottom(value:Number):void {
        if (_marginBottom == value) {
            return;
        }
        _marginBottom = value;

        invalidate();
    }

    public function get scaleFactor():Number {
        return LayoutContainer.scaleFactor;
    }

    /**
     * Attention!! The scaleFactor of the LayoutContainer is set statically
     * @param scaleFactor
     */
    public function set scaleFactor(scaleFactor:Number):void {
        LayoutContainer.scaleFactor = scaleFactor;
    }

    /**
     *
     */
    public function get view():Sprite {
        return _view;
    }
}
}
