package de.aoemedia.mobile.ui.components.forms
{
	import com.kaizimmer.events.ValidationEvent;
	import com.kaizimmer.ui.form.FormField;
	import com.kaizimmer.ui.form.FormFieldType;
	
	import flash.text.TextField;
	
	/**
	 * Overwrites com.kaizimmer.ui.form.FormField to manage
	 * de.aoemedia.mobile.ui.components.ios.iphone.InputField instead of regular flash.display.TextField objects.
	 * Furthermore adds:
	 * preventLiveCheckRendering property - if set to true the InputField won't change it's visual state when liveChecking is active
	 */
	public class FormField extends com.kaizimmer.ui.form.FormField
	{
		protected var inputField:InputField;
		protected var _preventLiveCheckRendering:Boolean = false;
		
		public function FormField ( inputField:InputField, defaultText:String, isMandatory:Boolean, contentType:uint=FormFieldType.ALL, minLen:uint=0, maxLen:Number=Infinity)
		{
			this.inputField = inputField;
			//var field:ITextField = inputField.field;
			super(inputField.field, defaultText, isMandatory, contentType, minLen, maxLen);
			//super(inputField.textField as TextField, defaultText, isMandatory, contentType, minLen, maxLen);
			// set textFormats to inputfields format
			defaultFormat = inputField.textFormat;
			errorFormat = defaultFormat;
		}
		
		/**
		 * If set to true the InputField won't change it's visual state when liveChecking is active.
		 */
		public function set preventLiveCheckRendering ( prevent:Boolean ):void
		{
			if ( _preventLiveCheckRendering == prevent ) return;
			_preventLiveCheckRendering = prevent;
			if ( prevent ) inputField.normalize();
		}
		
		/**
		 * If set to true the InputField won't change it's visual state when liveChecking is active.
		 */
		public function get preventLiveCheckRendering ( ):Boolean
		{
			return _preventLiveCheckRendering;
		}

		/**
		 * !!!Always updating
		 * Sets the inputfield's mandatory state.
		 *
		 * @param	iM:Boolean	Value indicating wheter the
		 * 			formfield is mandatory or not.
		 */
		override public function set isMandatory ( iM:Boolean ):void
		{
			_isMandatory = iM;
			// If text is the default or empty an string...
			/*/
			 if (_textField.text == _defaultText || _textField.text == "") {
			 // ... don't do anything more here and quit.
			 return;
			 }
			 //*/
			// ...else check txt via invoking update
			update();
		}
		
		/**
		 * Checks if the formfield's validity state changed. (Via invoking the
		 * validate method). If so it changes the applied TextFormat.
		 * (i.e. invoking applyFormat, passing in the respective TextFormat object)
		 */
		override protected function update ( ):void
		{
			// suspend this method as long as liveChecking is false
			if (!liveChecking) {
				return;
			}
			
			// Store previous textfield _isValid state
			var wasValid:Boolean = _isValid;
			_isValid = validate();
			// modify field format only if neccessary
			if (_isValid !== wasValid)
			{
				// State has changed so update the field's format if live check rendering isn't disabled
				if ( !_preventLiveCheckRendering ) updateFieldFormat();
				// ... and dispatch the corresponding event
				dispatchEvent(new ValidationEvent(ValidationEvent.CHANGE, true, false, _isValid));
			}
		}
		
		/**
		 * Changes the formfield's TextFormat according to it's current validity.
		 * (i.e. invoking applyFormat, passing in the respective TextFormat object)
		 */
		override protected function updateFieldFormat ( ):void
		{
			// State has changed so...
			switch ( _isValid )
			{
				case true:
					// ...normalise field
					//applyFormat(_defaultFormat);
					inputField.showSuccess();
					break;
				case false:
					// ...highlight field
					//applyFormat(_errorFormat);
					inputField.showError();
					break;
			}
		}
	}
}
