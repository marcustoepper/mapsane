package de.aoemedia.mobile.ui.components.buttons
{
	import flash.display.Bitmap;
	import de.aoemedia.mobile.ui.components.skins.ButtonBitmaps;
	
	public class IconLabelButton extends AbstractLabelButton
	{
		protected var _leftPadding:uint = 0;
		protected var _leftLabelPadding:uint = 0;
		protected var _rightPadding:uint = 0;
		
		protected var _isLabelCentered:Boolean = true;
		
		protected var _leftIcon:Bitmap;
		protected var _rightIcon:Bitmap;
		
		public function IconLabelButton ( bmps:ButtonBitmaps )
		{
			super(bmps);
		}
		

		public function get isLabelCentered ( ):Boolean
		{
			return _isLabelCentered;
		}

		public function set isLabelCentered ( value:Boolean ):void
		{
			if ( value == _isLabelCentered ) return;
			_isLabelCentered = value;
			invalidate();
		}
		
		public function get leftLabelPadding ( ):uint
		{
			return _leftLabelPadding;
		}
		
		public function set leftLabelPadding ( value:uint ):void
		{
			if ( _leftLabelPadding == value ) return;
			_leftLabelPadding = value;
			invalidate();
		}

		public function get leftPadding ( ):uint
		{
			return _leftPadding;
		}

		public function set leftPadding ( value:uint ):void
		{
			if ( _leftPadding == value ) return;
			_leftPadding = value;
			invalidate();
		}

		public function get rightPadding ( ):uint
		{
			return _rightPadding;
		}

		public function set rightPadding ( value:uint ):void
		{
			if ( _rightPadding == value ) return;
			_rightPadding = value;
			invalidate();
		}

		public function set leftIcon ( icon:Bitmap ):void
		{
			if ( _leftIcon != null ) removeChild(_leftIcon);
			_leftIcon = icon;
			addChild(_leftIcon);
			invalidate();
		}
		
		public function set rightIcon ( icon:Bitmap ):void
		{
			if ( _rightIcon != null ) removeChild(_rightIcon);
			_rightIcon = icon;
			addChild(_rightIcon);
			invalidate();
		}
		
		override public function setSize ( w:Number = -1, h:Number = -1 ):void
		{
			super.setSize(w,h);
			if ( _leftIcon != null )
			{
				_leftIcon.x = _leftPadding;
				_leftIcon.y = componentHeight - _leftIcon.height >> 1;
			}
			if ( _rightIcon != null )
			{
				_rightIcon.x = componentWidth - _rightIcon.width - _rightPadding;
				_rightIcon.y = componentHeight - _rightIcon.height >> 1;
			}
			if ( !_isLabelCentered )
			{
				_label.x = _leftLabelPadding;
			}
		}
		
	}
}