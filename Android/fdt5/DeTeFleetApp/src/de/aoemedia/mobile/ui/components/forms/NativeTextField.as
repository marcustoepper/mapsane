package de.aoemedia.mobile.ui.components.forms
{
	import flash.text.TextFormat;
	
	public class NativeTextField extends NativeText implements ITextField
	{
		public function NativeTextField ( numberOfLines:uint = 1 )
		{
			
		}
		
		/*/
		public function set text(s:String):void
		{
		}
		//*/
		
		public function get text():String
		{
			return null;
		}
		
		public function set textColor(s:uint):void
		{
		}
		
		public function get textColor():uint
		{
			return 0;
		}
		
		public function set defaultTextFormat(f:TextFormat):void
		{
		}
		
		public function get defaultTextFormat():TextFormat
		{
			return null;
		}
		
		public function set embedFonts(b:Boolean):void
		{
		}
		
		public function get embedFonts():Boolean
		{
			return false;
		}
		
		public function set type(t:String):void
		{
		}
		
		public function get type():String
		{
			return null;
		}
		
		/*/
		public function set x(n:Number):void
		{
		}
		
		public function get x():Number
		{
			return 0;
		}
		
		public function set y(n:Number):void
		{
		}
		
		public function get y():Number
		{
			return 0;
		}
		
		public function set width(n:Number):void
		{
		}
		
		public function get width():Number
		{
			return 0;
		}
		
		public function set height(n:Number):void
		{
		}
		
		public function get height():Number
		{
			return 0;
		}
		//*/
		
		public function set border(b:Boolean):void
		{
		}
		
		public function get border():Boolean
		{
			return false;
		}
		
		/*/
		public function set borderColor(n:uint):void
		{
		}
		
		public function get borderColor():uint
		{
			return 0;
		}
		
		public function get multiline():Boolean
		{
			return false;
		}
		//*/
		
		public function set multiline(b:Boolean):void
		{
		}
		
		public function get selectable():Boolean
		{
			return false;
		}
		
		public function set selectable(b:Boolean):void
		{
		}
		
		public function get displayAsPassword():Boolean
		{
			return false;
		}
		
		/*/
		public function set displayAsPassword(b:Boolean):void
		{
		}
		//*/
		
		public function get maxChars():int
		{
			return 0;
		}
		
		/*/
		public function set maxChars(n:int):void
		{
		}
		//*/
		
		/*/
		public function set restrict(s:String):void
		{
		}
		//*/
		
		public function get length():int
		{
			return 0;
		}
		
		public function getTextFormat(beginIndex:int=-1, endIndex:int=-1):TextFormat
		{
			return null;
		}
		
		public function setTextFormat(format:TextFormat, beginIndex:int=-1, endIndex:int=-1):void
		{
		}
		
		/*/
		public function addEventListener(type:String, listener:Function, useCapture:Boolean=false, priority:int=0, useWeakReference:Boolean=false):void
		{
		}
		
		public function removeEventListener(type:String, listener:Function, useCapture:Boolean=false):void
		{
		}
		
		public function dispatchEvent(event:Event):Boolean
		{
			return false;
		}
		
		public function hasEventListener(type:String):Boolean
		{
			return false;
		}
		
		public function willTrigger(type:String):Boolean
		{
			return false;
		}
		//*/
		
	}
}