package de.aoemedia.mobile.ui.components
{
import flash.events.MouseEvent;
import flash.text.TextFormat;

import org.osflash.signals.Signal;

public class ClickableLabelComponent extends LabelComponent
{
	public var signalClicked:Signal;

	public function ClickableLabelComponent(label:String = "", tF:TextFormat = null) {
		signalClicked = new Signal(LabelComponent);

		super(label, tF);
		startListening();
	}

	protected function startListening():void {
		addEventListener(MouseEvent.MOUSE_DOWN, dispatchInteraction);
		addEventListener(MouseEvent.MOUSE_OUT, dispatchInteraction);
		addEventListener(MouseEvent.MOUSE_UP, dispatchInteraction);
	}

	protected function dispatchInteraction(e:MouseEvent):void {
		switch (e.type) {
			case MouseEvent.MOUSE_DOWN:
				isDown = true;
				break;
			case MouseEvent.MOUSE_UP:
				if(isDown) {
					signalClicked.dispatch(this);
				}
			case MouseEvent.MOUSE_OUT:
				isDown = false;
				break;
		}
	}
}
}
