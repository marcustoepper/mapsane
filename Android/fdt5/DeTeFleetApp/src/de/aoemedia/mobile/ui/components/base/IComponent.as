package de.aoemedia.mobile.ui.components.base
{
	import flash.display.Sprite;

	/**
	 * 
	 */
	public interface IComponent
	{
		/**
		 * Returns the component sprite representation
		 * @return 
		 * 
		 */
		function get componentSprite ( ):Sprite;
		/**
		 * Returns the number of "real" pixels based on passed number of resolution + aspect ratio independent "design" pixels.
		 * If you want to calculate the exact number of "real" pixels (i.e. representation as floating point number) you should use the scaleFactor property.
		 * @param designPixels
		 * @return 
		 * 
		 */
		function scale ( designPixels:Number ):int;
	}
}