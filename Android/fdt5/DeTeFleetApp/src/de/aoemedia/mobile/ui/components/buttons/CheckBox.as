package de.aoemedia.mobile.ui.components.buttons
{
	
	import de.aoemedia.mobile.ui.components.skins.ToggleButtonBitmaps;
	
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	
	import org.bytearray.display.ScaleBitmap;

	
	/**
	 * ...
	 * @author dev.mode
	 */
	public class CheckBox extends AbstractButton
	{
		protected var buttonActiveBmp:Bitmap;
		protected var buttonActiveBmpDisabled:Bitmap;
		protected var buttonActiveBmpDown:Bitmap;
		
		protected var _isChecked:Boolean;
		
		public function CheckBox ( bmps:ToggleButtonBitmaps, isChecked:Boolean = false )
		{
			_isChecked = isChecked;
			buttonActiveBmp = bmps.upActive;
			buttonActiveBmpDisabled = bmps.upActiveDisabled;
			buttonActiveBmpDown = bmps.downActive;
			super(bmps);
		}
		

		public function get isChecked ( ):Boolean
		{
			return _isChecked;
		}

		public function set isChecked ( value:Boolean ):void
		{
			if ( value == _isChecked ) return;
			_isChecked = value;
			invalidate();
		}

		/**
		 * 
		 */
		override protected function createChildren ( ):void
		{
			if ( buttonActiveBmp == null || 
				buttonActiveBmpDisabled == null || 
				buttonActiveBmpDown == null )
			{
				throw new Error("ERROR: AbstractCheckBox .createChildren | Make sure all button bitmaps are defined!");
			}
			super.createChildren();
		}
		
		/**
		 * 
		 */
		override protected function draw ( ):void
		{
			if ( renderedBmp != null ) removeChild(renderedBmp);
			if ( isChecked )
			{
				switch ( buttonState )
				{
					case BUTTON_STATE_DEFAULT:
						renderedBmp = buttonActiveBmp;
						break;
					case BUTTON_STATE_DISABLED:
					case BUTTON_STATE_DOWN_DISABLED:
						renderedBmp = buttonActiveBmpDisabled;
						break;
					case BUTTON_STATE_DOWN:
						renderedBmp = buttonActiveBmpDown;
						break;
				}
			}
			else {
				switch ( buttonState )
				{
					case BUTTON_STATE_DEFAULT:
						renderedBmp = buttonBmp;
						break;
					case BUTTON_STATE_DISABLED:
						renderedBmp = buttonBmpDisabled;
						break;
					case BUTTON_STATE_DOWN:
						renderedBmp = buttonBmpDown;
						break;
					case BUTTON_STATE_DOWN_DISABLED:
						renderedBmp = buttonBmpDownDisabled;
						break;
				}
			}
			addChildAt(renderedBmp, 0);
		}
		
		protected function changeState ( e:MouseEvent ):void
		{
			isChecked = !isChecked;
		}
		
		override protected function startListening ( ):void
		{
			addEventListener(MouseEvent.MOUSE_UP, changeState);
			super.startListening();
		}
		
		override protected function stopListening ( ):void
		{
			removeEventListener(MouseEvent.MOUSE_UP, changeState);
			super.stopListening();
		}
		
	}
	
}