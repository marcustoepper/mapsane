package de.aoemedia.mobile.ui.components.buttons {
import de.aoemedia.mobile.ui.components.LabelComponent;
import de.aoemedia.mobile.ui.components.skins.ButtonBitmaps;

import flash.display.DisplayObject;
import flash.text.TextFormat;

public class AbstractLabelButton extends AbstractButton {
    protected var _labelColor:Number = 0;
    protected var _labelColorDown:Number = 0;
    protected var _labelColorDisabled:Number = 0;
    protected var _labelColorDownDisabled:Number = 0;

    protected var _label:LabelComponent;

    public function AbstractLabelButton(bmps:ButtonBitmaps, label:String = "") {
        super(bmps);
        if (label != "") this.label = label;
    }


    public function get labelColor():Number {
        return _labelColor;
    }

    public function set labelColor(value:Number):void {
        if (value == _labelColor) {
            return;
        }
        _labelColor = value;
        if (buttonState == BUTTON_STATE_DEFAULT) {
            invalidate();
        }
    }

    public function get labelColorDown():Number {
        return _labelColorDown;
    }

    public function set labelColorDown(value:Number):void {
        if (value == _labelColorDown) return;
        _labelColorDown = value;
        if (buttonState == BUTTON_STATE_DOWN) invalidate();
    }

    public function get labelColorDisabled():Number {
        return _labelColorDisabled;
    }

    public function set labelColorDisabled(value:Number):void {
        if (value == _labelColorDisabled) return;
        _labelColorDisabled = value;
        if (buttonState == BUTTON_STATE_DISABLED) invalidate();
    }

    public function get labelColorDownDisabled():Number {
        return _labelColorDownDisabled;
    }

    public function set labelColorDownDisabled(value:Number):void {
        if (value == _labelColorDownDisabled) return;
        _labelColorDownDisabled = value;
        if (buttonState == BUTTON_STATE_DOWN_DISABLED) invalidate();
    }

    public function get label():String {
        return _label.label;
    }

    public function set label(value:String):void {
        _label.label = value;
        invalidate();
    }

    public function set textFormat(tFormat:TextFormat):void {
        _label.textFormat = tFormat;
        invalidate();
    }

    public function get textFormat():TextFormat {
        return _label.textFormat;
    }

    override public function setSize(w:Number = -1, h:Number = -1):void {
        super.setSize(w, h);
        _label.x = componentWidth - _label.width >> 1;
        _label.y = componentHeight - _label.height >> 1;
    }

    override protected function createChildren():void {
        _label = new LabelComponent();
        addChild(DisplayObject(_label));
        super.createChildren();
    }

    /**
     *
     */
    override protected function draw():void {
        switch (buttonState) {
            case BUTTON_STATE_DEFAULT:
                _label.color = _labelColor;
                break;
            case BUTTON_STATE_DISABLED:
                _label.color = _labelColorDisabled;
                break;
            case BUTTON_STATE_DOWN:
                _label.color = _labelColorDown;
                break;
            case BUTTON_STATE_DOWN_DISABLED:
                _label.color = _labelColorDownDisabled;
                break;
        }
        super.draw();
    }

    public function get labelComponent():LabelComponent {
        return _label;
    }
}
}
