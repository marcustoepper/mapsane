package de.aoemedia.animation
{
	import flash.display.BitmapData;
	import flash.geom.Point;

	public class BlitSprite implements IJuggable
	{
		public var sheetBmd:BitmapData;
		public var rects:Array;
		public var canvas:BitmapData;
		public var pos:Point = new Point();
		//public var x:Number = 0;
		//public var y:Number = 0;
		public var animate:Boolean = true;
		
		protected var animIndex:int = 0;
		protected var count:int = 0;
		
		public function BlitSprite ( sheetBmd:BitmapData, rects:Array, canvas:BitmapData )
		{
			this.sheetBmd = sheetBmd;
			this.rects = rects;
			this.canvas = canvas;
		}
		
		public function  advanceTime ( deltaTime:Number ):void
		{
			/*/
			pos.x = x - rects[animIndex].width * 0.5;
			pos.y = y - rects[animIndex].height * 0.5;
			//*/
			/*/
			pos.x = x;
			pos.y = y;
			//*/
			canvas.copyPixels(sheetBmd, rects[animIndex], pos);
			//if(count%10 == 0 && animate)
			if ( animate ) animIndex = (animIndex == rects.length-1) ? 0 : animIndex+1; 
			count++;
		}
	}
}
