package de.aoemedia.animation
{
	import flash.display.Stage;
	import flash.events.Event;
	import flash.utils.getTimer;

	public class Juggler
	{
		protected var _juggables:Vector.<IJuggable>;
		protected var _stage:Stage;
		protected var _lastUpdate:Number = 0;
		
		public function Juggler ( stage:Stage )
		{
			init(stage);
		}
		
		protected function init ( stage:Stage ):void
		{
			_stage = stage;
			_juggables = new <IJuggable>[];
		}

		protected function juggle ( e:Event ):void
		{
			var delta:Number = getTimer() - _lastUpdate;
			for ( var i:uint = _juggables.length; --i >= 0; )
			{
				// unnecessary uint cast shall boost performance!?!
				_juggables[uint(i)].advanceTime(delta);
			}
		}
		
		public function start ( ):void
		{
			stop();
			_stage.addEventListener(Event.ENTER_FRAME, juggle);
		}
		
		public function stop ( ):void
		{
			_stage.removeEventListener(Event.ENTER_FRAME, juggle);
		}
		
		public function add ( juggable:IJuggable ):void
		{
			_juggables.push(juggable);
		}
		
		public function remove ( juggable:IJuggable ):Boolean
		{
			for ( var i:uint = _juggables.length-1; i >= 0; --i )
			{
				// unnecessary uint cast shall boost performance!?!
				if ( _juggables[uint(i)] == juggable )
				{
					_juggables.splice(i,1);
					return true;
				}
			}
			return false;
		}
	}
}