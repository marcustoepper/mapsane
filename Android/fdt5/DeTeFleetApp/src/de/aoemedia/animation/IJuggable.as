package de.aoemedia.animation
{
	public interface IJuggable
	{
		function advanceTime ( deltaTime:Number ):void;
	}
}