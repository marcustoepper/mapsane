package de.telekom.detefleet.app.models.vo
{
import com.adobe.utils.DateUtil;

import flash.globalization.LocaleID;
import flash.globalization.NumberFormatter;

/**
 * Represents a FillingStation
 * 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class FillingStationVO
{
	public var id:String;
	public var distance:Number;
	public var address:AddressVO;
	public var price:Number;
	public var brand:String;
	public var phone:String;

	public var priceValidFrom:Date;

    public var lastUpdateInMilliseconds:Number;

	public var shop:Boolean;
	public var garage:Boolean;
	public var carWash:Boolean;

	public var businessHours:Vector.<String>;

    protected static var dateFormatter:NumberFormatter;

	/**
	 * Construct
	 * 
	 * @param object Anonymous Object which data will be set the properties of this object 
	 */
	public function FillingStationVO(object:Object = null)
	{
        if (dateFormatter == null) {
            dateFormatter = new NumberFormatter(LocaleID.DEFAULT);
            dateFormatter.fractionalDigits = 0;
            dateFormatter.trailingZeros = true;
        }

		if (object == null) {
			return;
		}
		id = object.id;
		distance = object.distance;
		address = new AddressVO(object.address);
		price = object.price;
		brand = object.brand;
		phone = object.phone;
		shop = object.shop;
		garage = object.garage;
		carWash = object.carWash;

		priceValidFrom = DateUtil.parseW3CDTF(object.priceValidFrom);

		var now:Date = new Date();
        lastUpdateInMilliseconds = now.time - priceValidFrom.time;

		businessHours = new Vector.<String>();
		for (var i:int = 0; i < object.businessHoursFormatted.length; i++) {
			businessHours[i] = object.businessHoursFormatted[i];
		}
	}

    /**
     *
     * @return
     */
    public function getLastUpdateString():String{
        var text:String;

        if(lastUpdateInMilliseconds > 28800000){ // Check if bigger than 8 hours 8*60*60000
            text = "älter als 8h";
        } else if(lastUpdateInMilliseconds > 3600000){ // Check if bigger than one hour 60*60000
            var hours:Number = lastUpdateInMilliseconds/3600000;
            text = "vor " + dateFormatter.formatNumber(hours) + "h";
        } else {
            text = "vor " + dateFormatter.formatNumber(lastUpdateInMilliseconds/60000) + " Min";
        }
        return text;
    }

}
}
