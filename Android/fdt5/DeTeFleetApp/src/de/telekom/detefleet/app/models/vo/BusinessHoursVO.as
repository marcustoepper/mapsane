package de.telekom.detefleet.app.models.vo
{
/**
 * Represents the Business Hours. E.g. of an FillingStation
 * 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class BusinessHoursVO
{
	public var day:String;
	public var open:String;
	public var close:String;

	/**
	 * Construct
	 * 
	 * @param object Anonymous Object which data will be set the properties of this object 
	 */
	public function BusinessHoursVO(object:Object = null):void
	{
		if (object == null) {
			return;
		}

		day = object.day;
		open = object.open;
		close = object.close;
	}


}
}
