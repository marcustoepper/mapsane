package de.telekom.detefleet.app.views.components.dialog
{
import de.aoemedia.mobile.ui.MultiDpiHelper;
import de.aoemedia.mobile.ui.components.buttons.AbstractButton;
import de.telekom.detefleet.app.assets.Color;
import de.telekom.detefleet.app.assets.IconRepository;
import de.telekom.detefleet.app.assets.TextFormatTemplate;
import de.telekom.detefleet.app.views.components.buttons.DefaultLabelButton;

import flash.display.Bitmap;
import flash.display.DisplayObject;
import flash.geom.Rectangle;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import flash.text.TextFormat;

import org.osflash.signals.Signal;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class CallPhoneDialogView extends DialogView
{
	public var phone:String;
	public var extraInformation:String;
	
	protected const BUTTON_PADDING_BOTTOM:uint = 11;
	
	protected var icon:Bitmap;
	
	protected var okButton:DefaultLabelButton;
	protected var cancelButton:DefaultLabelButton;
	
	public var okClicked:Signal;
	public var cancelClicked:Signal;
	private var extraInformationTextField:TextField;
	
	public function CallPhoneDialogView(viewport:Rectangle, phone:String, extraInformation:String = "")
	{
		this.phone = phone;
		this.extraInformation = extraInformation;

		super(viewport, "Anrufen", "Wollen Sie wirklich " + phone + " anrufen?");

		okClicked = new Signal();
		cancelClicked = new Signal();
	}
	
	override protected function invalidate():void
	{
		super.invalidate();

		extraInformationTextField.width = containerWidth;
		extraInformationTextField.x = 0;
		extraInformationTextField.y = messageTextField.y + messageTextField.height + scale(5);

		okButton.setSize(126, 44);
		cancelButton.setSize(126, 44);

		var buttonGap:Number = scale(5);
		
		okButton.x = containerWidth/2-buttonGap/2-okButton.width;
		okButton.y = extraInformationTextField.y + extraInformationTextField.height + scale(BUTTON_PADDING_BOTTOM);
		
		cancelButton.x = containerWidth/2+buttonGap/2;
		cancelButton.y = extraInformationTextField.y + extraInformationTextField.height + scale(BUTTON_PADDING_BOTTOM);
		
		icon.x = 0;
		icon.y = scale(4);
		icon.width = MultiDpiHelper.scale( 22 );
		icon.height = MultiDpiHelper.scale(22);

		headlineTextField.x = icon.x + icon.width + scale(10);

        resetContainerPosition();
		invalidateBackground();
	}
	
	override protected function createChildren():void
	{
		super.createChildren();

		var tFormat:TextFormat = TextFormatTemplate.getInstance().dialogButton;
		
		okButton = new DefaultLabelButton("anrufen");
		okButton.textFormat = tFormat;
		okButton.labelColor = Color.MEDIUM_GREY;
		okButton.labelColorDisabled = Color.DARK_GREY;
		okButton.signalClicked.add(okButtonClicked);
		container.addChild(DisplayObject(okButton));
		
		cancelButton = new DefaultLabelButton("abbrechen");
		cancelButton.textFormat = tFormat;
		cancelButton.labelColor = Color.MEDIUM_GREY;
		cancelButton.labelColorDisabled = Color.DARK_GREY;
		cancelButton.signalClicked.add(cancelButtonClicked);
		container.addChild(DisplayObject(cancelButton));

		extraInformationTextField = new TextField();
		extraInformationTextField.autoSize = TextFieldAutoSize.LEFT;
		extraInformationTextField.wordWrap = true;
		extraInformationTextField.multiline = true;
		extraInformationTextField.defaultTextFormat = TextFormatTemplate.getInstance().dialogExtraInformation;
		extraInformationTextField.embedFonts = true;
		extraInformationTextField.text = extraInformation;

		container.addChild(extraInformationTextField);

		icon = IconRepository.warningIcon;
		container.addChild(icon);
	}

	private function okButtonClicked(button:AbstractButton):void
	{
		okClicked.dispatch();
	}

	private function cancelButtonClicked(button:AbstractButton):void
	{
		cancelClicked.dispatch();
	}

	public function okButtonLabel(label:String):void{
		okButton.label = label;
	}
	
	public function cancelButtonLabel(label:String):void{
		cancelButton.label = label;
	}
}
}
