package de.telekom.detefleet.app.services.remote
{
import de.telekom.detefleet.app.models.vo.GarageSearchRequestVO;
import de.telekom.detefleet.app.models.vo.ServiceAuthVO;

/**
 * Implement this for the Service to get Garages
 * 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public interface IGarageService
{
	/**
	 * Search for Garages
	 * 
	 * If successful it fills the GarageModel with the Result an dispatch an GarageLoaded Signal.
	 * If unseccessful it fills the GarageLoadFault with the error Message
	 * 
	 * @param auth The Authentification for the Webservice
	 * @param request The request for the Search
	 */
	function search(auth:ServiceAuthVO, request:GarageSearchRequestVO):void;


	/**
	 * Ask the Service for existing Vehicle Service Cases
	 * 
	 * If successful it fills the GarageModel.serviceCases with the Result an disptach an ServiceCasesLoaded Signal.
	 * If unseccessful it fills the ServiceCasesLoadFault with the error Message
	 * 
	 * @param auth The Authentification for the Webservice
	 */
	function getVehicleServiceCases(auth:ServiceAuthVO):void;
}
}