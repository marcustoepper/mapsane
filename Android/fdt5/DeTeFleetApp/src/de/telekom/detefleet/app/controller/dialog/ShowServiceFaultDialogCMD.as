package de.telekom.detefleet.app.controller.dialog
{
import de.telekom.detefleet.app.models.DialogModel;
import de.telekom.detefleet.app.models.LayoutData;
import de.telekom.detefleet.app.views.components.dialog.ServiceFaultDialogView;

import org.robotlegs.mvcs.Command;

/**
 * ...
 * @author dev.mode
 */
public class ShowServiceFaultDialogCMD extends Command
{

	[Inject]
	public var message:String;

	[Inject]
	public var layout:LayoutData;

	[Inject]
	public var model:DialogModel;

	public override function execute():void {
		var dialog:ServiceFaultDialogView = new ServiceFaultDialogView(layout.viewport, message);

		model.addDialog(dialog);
	}
}
}
