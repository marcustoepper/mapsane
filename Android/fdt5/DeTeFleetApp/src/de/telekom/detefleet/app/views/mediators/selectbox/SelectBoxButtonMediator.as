package de.telekom.detefleet.app.views.mediators.selectbox {
import de.telekom.detefleet.app.events.RequestSelectBoxList;
import de.telekom.detefleet.app.events.SelectionCanceled;
import de.telekom.detefleet.app.events.SelectionDone;
import de.telekom.detefleet.app.events.ShowSelectBoxList;
import de.telekom.detefleet.app.views.components.selectbox.SelectBoxButton;

import org.robotlegs.mvcs.Mediator;

public class SelectBoxButtonMediator extends Mediator {
    [Inject]
    public var button:SelectBoxButton;

    [Inject]
    public var selectionDone:SelectionDone;

    [Inject]
    public var selectionCanceled:SelectionCanceled;

    [Inject]
    public var requestSelectBoxList:RequestSelectBoxList;

    public override function onRegister():void {
        button.signalSelectBoxListRequested.add(dispatchSelectBoxListRequest);
        selectionDone.add(handleSelectionDone);
    }

    protected function dispatchSelectBoxListRequest():void {
        requestSelectBoxList.dispatch(button.dataProvider, button);
    }

    protected function handleSelectionDone(selection:Object, caller:SelectBoxButton):void {
        if (button != caller) return;
        button.selectedItem = selection;
    }

    protected function destroy():void {
        button.signalSelectBoxListRequested.remove(dispatchSelectBoxListRequest);
        selectionDone.remove(handleSelectionDone);
    }

}
}