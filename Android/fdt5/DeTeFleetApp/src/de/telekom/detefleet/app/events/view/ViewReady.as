package de.telekom.detefleet.app.events.view
{
import de.telekom.detefleet.app.views.components.views.IView;

import org.osflash.signals.Signal;

/**
 * Dispatched if an View is ready (includes loading of Services)
 * 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class ViewReady extends Signal
{
	public function ViewReady()
	{
		super(IView);
	}
}
}
