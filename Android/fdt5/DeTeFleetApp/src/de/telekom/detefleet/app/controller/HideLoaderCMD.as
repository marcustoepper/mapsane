package de.telekom.detefleet.app.controller
{
import de.telekom.detefleet.app.events.loader.HideLoader;

import org.robotlegs.mvcs.SignalCommand;

public class HideLoaderCMD extends SignalCommand
{
	[Inject]
	public var hideLoader:HideLoader;

	public override function execute():void {
		hideLoader.dispatch();
	}
}
}
