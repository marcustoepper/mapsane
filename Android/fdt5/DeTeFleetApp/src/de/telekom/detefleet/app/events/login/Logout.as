package de.telekom.detefleet.app.events.login
{
import org.osflash.signals.Signal;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class Logout extends Signal
{
	public function Logout()
	{
		super();
	}
}
}
