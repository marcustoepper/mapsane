package de.telekom.detefleet.app.controller.selectbox {
import de.telekom.detefleet.app.events.ShowSelectBoxList;
import de.telekom.detefleet.app.models.ViewsModel;
import de.telekom.detefleet.app.views.components.selectbox.SelectBoxButton;
import de.telekom.detefleet.app.views.components.selectbox.SelectBoxListView;

import flash.display.DisplayObject;
import flash.display.Sprite;

import org.robotlegs.mvcs.SignalCommand;

/**
 *
 * @author Kai Zimmer <kai.zimmer@aoemedia.de>
 */
public class CreateSelectBoxListCMD extends SignalCommand {
    [Inject]
    public var showSelectBoxList:ShowSelectBoxList;

    [Inject]
    public var dataProvider:Vector.<Object>;

    [Inject]
    public var caller:SelectBoxButton;

    [Inject]
    public var viewsModel:ViewsModel;

    override public function execute():void {
        var container:Sprite = contextView as Sprite;
        var listView:SelectBoxListView = viewsModel.getView(SelectBoxListView) as SelectBoxListView;

        if (listView == null) {
            listView = new SelectBoxListView();
            viewsModel.addView(SelectBoxListView, listView);
        }

        if (!container.contains(DisplayObject(listView))) {
            container.addChild(DisplayObject(listView));
        }

        showSelectBoxList.dispatch(dataProvider, caller);
    }
}
}