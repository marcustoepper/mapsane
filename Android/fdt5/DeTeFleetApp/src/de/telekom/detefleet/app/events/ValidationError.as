/**
 * Created with IntelliJ IDEA.
 * User: daniel.kopp
 * Date: 29.10.12
 * Time: 10:19
 * To change this template use File | Settings | File Templates.
 */
package de.telekom.detefleet.app.events
{
import org.osflash.signals.Signal;

public class ValidationError extends Signal
{
	public function ValidationError() {
		super(String);
	}
}
}
