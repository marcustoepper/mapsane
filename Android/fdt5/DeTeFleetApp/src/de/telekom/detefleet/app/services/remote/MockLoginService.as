package de.telekom.detefleet.app.services.remote
{
import de.telekom.detefleet.app.events.RenewSessionFault;
import de.telekom.detefleet.app.events.RenewSessionSuccess;
import de.telekom.detefleet.app.events.login.LoginFault;
import de.telekom.detefleet.app.events.login.LoginSuccess;
import de.telekom.detefleet.app.events.login.LogoutFailure;
import de.telekom.detefleet.app.events.login.LogoutSuccess;
import de.telekom.detefleet.app.models.UserModel;
import de.telekom.detefleet.app.models.vo.AddressVO;
import de.telekom.detefleet.app.models.vo.PersonVO;
import de.telekom.detefleet.app.models.vo.ServiceAuthVO;
import de.telekom.detefleet.app.models.vo.VehicleVO;

import flash.utils.setTimeout;

import org.as3commons.logging.api.ILogger;
import org.as3commons.logging.api.getLogger;
import org.robotlegs.mvcs.Actor;

/**
 * Implementation for an Mock Service fpr Login.
 * It returns only some Dummy Data to test locally.
 *
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class MockLoginService extends Actor implements ILoginService
{
	private static const logger:ILogger = getLogger(MockLoginService);

	// Model
	[Inject]
	public var userModel:UserModel;

	// Signals
	[Inject]
	public var renewSessionSuccess:RenewSessionSuccess;
	[Inject]
	public var renewSessionFault:RenewSessionFault;
	[Inject]
	public var loginSuccess:LoginSuccess;
	[Inject]
	public var loginFault:LoginFault;
	[Inject]
	public var logoutSuccessful:LogoutSuccess;
	[Inject]
	public var logoutFailure:LogoutFailure;

	/**
	 * Fake the Login. Is valid if username and password is valid.
	 *
	 * If sucessful dispatch a LoginSuccess Signal with the Logedin User.
	 * Else dispatch a LoginFault with the Error Message.
	 *
	 * @param username Use test for valid login. Use faultSession if the renewSession should be fail
	 * @param password Use test for valid login.
	 * @param deviceKey won't be used in the fake
	 *
	 */
	public function login(username:String, password:String, deviceKey:String):void {
		setTimeout(endServiceCall, 125);

		function endServiceCall():void {
			if((username == "test" || username == "faultSession") && password == "test") {
				var auth:ServiceAuthVO = new ServiceAuthVO();
				auth.authorizationToken = username;
				var person:PersonVO = createMockPerson();
				userModel.userData = person;
				loginSuccess.dispatch(person, auth);
				return;
			}
			loginFault.dispatch("Zu diesem User und Passwort wurden keine Daten gefunden");
		}
	}

	/**
	 * Renews an existing User Session
	 *
	 * If succesfull dispatch a RenewSessionSuccess.
	 * Else dispatch a RenewSessionFault with the Error Message.
	 *
	 * @param auth
	 *
	 */
	public function renewSession(auth:ServiceAuthVO):void {
		setTimeout(endServiceCall, 125);

		function endServiceCall():void {
			logger.debug("renewSession authorizationToken: " + auth.authorizationToken);
			if(auth.authorizationToken == "faultSession") {
				renewSessionFault.dispatch("die Session ist abgelaufen");
				return;
			}
			var person:PersonVO = createMockPerson();
			userModel.userData = person;
			renewSessionSuccess.dispatch(person);
		}
	}

	/**
	 * Logout a User
	 *
	 *
	 * If sucessful dispatch a LogoutSuccess Signal.
	 * Else dispatch a LogoutFault with the Error Message.
	 *
	 * @param auth Authenification Data of User
	 */
	public function logout(auth:ServiceAuthVO):void {
		setTimeout(endServiceCall, 125);

		function endServiceCall():void {
			logger.debug("Logout Request: " + auth.toJSON());
			if(auth.authorizationToken == "faultLogout") {
				logoutFailure.dispatch();
				return;
			}
			logoutSuccessful.dispatch();
		}
	}

	/**
	 * Creates an PersonVO with dummy data
	 *
	 * @return A dummy person Hans Mustermann
	 */
	protected function createMockPerson():PersonVO {
		var address:AddressVO = new AddressVO();
		address.address = "Somewhere";

		var person:PersonVO = new PersonVO();
		person.address = address;
		person.firstName = "Hans";
		person.lastName = "Mustermann";
		person.telephoneNumber = "0123456";

		var vehicle:VehicleVO = new VehicleVO();
		vehicle.plateNo = "BN-SY2677";
		vehicle.id = 600699745;
		vehicle.modelName = "3er 320 d Touring 135 kW";
		vehicle.recommendedFuelType = null;
		person.vehicles.push(vehicle);

		return person;
	}
}
}
