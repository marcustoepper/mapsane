package de.telekom.detefleet.app.controller.view
{

/**
 * Request the next view to Show. Blocks History addition
 * 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class BlockHistoryAdditionRequestViewCMD extends RequestViewCMD
{
	override public function execute():void
	{
		viewsModel.blockHistoryAddition = true;
		super.execute();
	}
}
}