package de.telekom.detefleet.app.views.components.buttons
{
import de.aoemedia.mobile.ui.components.buttons.AbstractLabelButton;
import de.aoemedia.mobile.ui.components.skins.ButtonBitmaps;
import de.telekom.detefleet.app.assets.ScaleBitmaps;

import flash.display.Bitmap;

public class DefaultLabelButton extends AbstractLabelButton
{
	protected var _icon:Bitmap;
	protected var _gap:Number = 3;
	 
	public function DefaultLabelButton(label:String = "", bmps:ButtonBitmaps = null)
	{
		if(bmps == null){
			var sBmps:ScaleBitmaps = ScaleBitmaps.getInstance();
			bmps = new ButtonBitmaps(sBmps.defaultButton, sBmps.defaultButtonDown, sBmps.defaultButtonDisabled, sBmps.defaultButtonDisabled);
		}
		super(bmps, label);
	}

	override protected function createChildren():void
	{
		super.createChildren();
	}

	public function get icon():Bitmap {
		return _icon;
	}

	public function set icon(icon:Bitmap):void {
		if (icon == _icon) {
			return;
		}
		if ( _icon != null ) {
			removeChild(_icon);
		}
		_icon = icon;
		addChild(_icon);
		invalidate();
	}

	override public function setSize(w:Number = -1, h:Number = -1):void
	{
		super.setSize(w, h);

		if ( _icon != null ) {
			_icon.x = componentWidth/2-(_icon.width+_label.width+_gap)/2;
			_icon.y = componentHeight - _icon.height >> 1;

			_label.x = componentWidth/2 - (_icon.width+_label.width+_gap)/2 + _icon.width+_gap;
			_label.y = componentHeight - _label.height >> 1;
		} else {
			_label.x = componentWidth - _label.width >> 1;
			_label.y = componentHeight - _label.height >> 1;
		}

	}
}
}