package de.telekom.detefleet.app.events
{
import org.osflash.signals.Signal;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class ViewContextChanged extends Signal
{
	public function ViewContextChanged()
	{
		super(String);
	}
}
}
