package de.telekom.detefleet.app.services.persistence
{
/**
 * This Servcice store the key/value pair native device KeyChain System. 
 * 
 * Seems like the EncryptedFileStorage (implemented in LocalKeyChainService) use on IOS the KeyChain. So we doesn't have to implement this
 * 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 * 
 */
public class NativeKeyChainService implements IKeyChainService
{
	public function NativeKeyChainService()
	{
	}

	public function setItem(key:String, value:String):void
	{
	}

	public function getItem(key:String):String
	{
		return null;
	}

	public function removeItem(key:String):void
	{
	}
}
}