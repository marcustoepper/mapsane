package de.telekom.detefleet.app.models.vo {
	/**
	 * ...
	 * @author k.zimmer aka engimono
	 */
	public class LoginFormData {
		public var user : String;
		public var password : String;
		public var storePassword : Boolean;

		/**
		 * LoginFormData Constructor
		 */
		public function LoginFormData() {
		}

		public function toString() : String {
			return "[LoginFormData object [ mail: " + user + " password: " + password + " ]]";
		}
	}
}