package de.telekom.detefleet.app.views.components.views
{
import com.kaizimmer.ui.Screen;

import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Sprite;

public class LoaderView extends AbstractView
{
	protected var loaderAnim:LoaderAnim;
	protected var modalBG:Bitmap;

	override protected function createChildren():void
	{
		var w:Number = Screen.stage.stageWidth;
		var h:Number = Screen.stage.stageHeight;
		
		var centerX:Number = w >> 1;
		var centerY:Number = h >> 1;
		
		var s:Sprite = new Sprite();
		s.graphics.beginFill(0x000000, .125);
		s.graphics.drawRect(0, 0, w, h);
		s.graphics.endFill();
		var bmpD:BitmapData = new BitmapData(w, h, true, 0x22000000);
		bmpD.draw(s);
		modalBG = new Bitmap(bmpD);
		s = null;

		loaderAnim = new LoaderAnim();

		loaderAnim.x = centerX;
		loaderAnim.y = centerY;
		
		addChild(modalBG);
		addChild(loaderAnim);
	}
}
}
