package de.telekom.detefleet.app.controller.view
{
import de.telekom.detefleet.app.models.vo.ViewRequestVO;
import de.aoemedia.motion.TransitionType;
import de.telekom.detefleet.app.events.view.RequestView;
import de.telekom.detefleet.app.views.components.views.LoginView;
import org.robotlegs.mvcs.Command;


/**
 * Show the Login view
 * 
 * @author Kai Zimmer <kai.zimmer@aoemedia.de>
 */
public class ShowLoginViewCMD extends Command
{
	[Inject]
	public var requestView:RequestView;

	public override function execute():void
	{
		requestView.dispatch(new ViewRequestVO(LoginView, TransitionType.SLIDE_RIGHT));
	}
}
}