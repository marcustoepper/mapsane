package de.telekom.detefleet.app.services.webview
{
import flash.events.ErrorEvent;
import flash.events.Event;
import flash.events.LocationChangeEvent;
import flash.media.StageWebView;
import flash.net.URLRequest;
import flash.net.navigateToURL;
import flash.utils.getTimer;

import de.telekom.detefleet.app.events.StageWebViewBridgeEvent;

import org.robotlegs.mvcs.Actor;

/**
 * ...
 * @author Kai Zimmer <kai.zimmer@aoemedia.de>
 */
public class StageWebViewBridge extends Actor
{
	protected const SEPERATOR_TOKEN:String = "?bridgedata=";
	protected const WEB_VIEW_INIT_EVENT:String = "onWebViewInitialized";
	protected const JS_LOG_PROPERTY_NAME:String = "jsLog";

	protected var _webView:StageWebView;
	protected var _isWebViewLoaded:Boolean = false;
	protected var _isWebViewInitialized:Boolean = false;
	
	public function registerWebView(view:StageWebView):void
	{
		//if ( webView != null ) unregisterWebView();
		_webView = view;
		_webView.addEventListener(LocationChangeEvent.LOCATION_CHANGING, handleLocationChanging);
		_webView.addEventListener(Event.COMPLETE, handleLoadComplete);
		_webView.addEventListener(ErrorEvent.ERROR, handleError);
	}

	protected function handleError(event:ErrorEvent):void
	{
		trace(event);
	}

	public function unregisterWebView():void
	{
		_webView.removeEventListener(Event.COMPLETE, handleLoadComplete);
		_webView.removeEventListener(LocationChangeEvent.LOCATION_CHANGING, handleLocationChanging);
		_webView = null;
	}

	public function invokeWebViewFunction(func:String, ...args):void
	{
		// handle args that are neither numbers nor booleans as strings
		for ( var i:int = args.length; i >= 0; i-- )
		{
			var arg:* = args[i];
			switch ( true )
			{
				case arg == null:
				case arg is Boolean:
				case arg is Number:
					continue;
				case arg is String:
				default:
					args[i] = "'" + arg + "'";
			}
		}
	
		var jsInjection:String = "javascript: " + func + "( ";
		if (args.length > 0) {
			jsInjection += args.join(",");
		}
		jsInjection += ");";
		trace("StageWebViewBridge - invokeWebViewFunction invoked - jsInjection: " + jsInjection);
		webView.loadURL(jsInjection);
	}

	protected function handleLocationChanging(e:LocationChangeEvent):void
	{
		trace("handleLocationChanging: " + e.location);

		switch ( true ) {
			case e.location.indexOf("http://") == 0:
			case e.location.indexOf("https://") == 0:
				// quit and open via device browser
				e.preventDefault();
				navigateToURL(new URLRequest(e.location));
				return;
			case e.location.indexOf("mailto:") == 0:
			case e.location.indexOf("javascript:") == 0:
			case e.location.indexOf("about:blank") == 0:
				// do nothing
				return;
			default:
			// try to parse data
		}

		e.preventDefault();

		var data:Object;
		var dataStart:uint = e.location.indexOf(SEPERATOR_TOKEN) + SEPERATOR_TOKEN.length;
		var json:String = unescape(e.location.substr(dataStart));
		
		trace("json: " + json);

		try {
			data = JSON.parse(json);
		}
		catch ( err:Error )
		{
			trace("ERROR: StageWebViewBridge .handleLocationChanging() couldn't parse json. Error: " + err);
			return;
		}

		processReceivedWebViewData(data);
	}

	protected function processReceivedWebViewData(data:Object):void
	{
		//trace("processReceivedWebViewData: " + data);
		
		if ( data == WEB_VIEW_INIT_EVENT )
		{
			_isWebViewInitialized = true;
			dispatch(new StageWebViewBridgeEvent(StageWebViewBridgeEvent.ON_WEB_VIEW_INITIALIZED));
		}
		else if ( data[JS_LOG_PROPERTY_NAME] != null )
		{
			handleJSLogMessage(data[JS_LOG_PROPERTY_NAME]);
		}
		else {
			dispatch(new StageWebViewBridgeEvent(StageWebViewBridgeEvent.ON_RECEIVED_WEB_VIEW_DATA, data));
		}
		
	}

	protected function handleJSLogMessage ( msg:Object ):void
	{
		trace("\n------------------");
		trace("[LOG | StageWebViewBridge | app uptime: " + flash.utils.getTimer() +  "ms]: ");
		trace(JSON.stringify(msg));
		trace("------------------\n");	
	}
	

	protected function handleLoadComplete ( e:Event ):void
	{
		e.stopImmediatePropagation();
		_isWebViewLoaded = true;
		dispatch(new StageWebViewBridgeEvent(StageWebViewBridgeEvent.ON_LOADING_COMPLETE));
	}

	public function get webView():StageWebView
	{
		return _webView;
	}

	public function get isWebViewLoaded():Boolean
	{
		return _isWebViewLoaded;
	}

	public function get isWebViewInitialized():Boolean
	{
		return _isWebViewInitialized;
	}

}

}