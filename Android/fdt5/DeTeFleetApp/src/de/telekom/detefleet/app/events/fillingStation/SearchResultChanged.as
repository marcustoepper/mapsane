package de.telekom.detefleet.app.events.fillingStation
{
import de.telekom.detefleet.app.models.vo.FillingStationVO;

import org.osflash.signals.Signal;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class SearchResultChanged extends Signal
{
	public function SearchResultChanged()
	{
		super(Vector.<FillingStationVO>);
	}
}
}
