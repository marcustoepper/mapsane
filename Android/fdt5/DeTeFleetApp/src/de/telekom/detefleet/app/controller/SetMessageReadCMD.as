package de.telekom.detefleet.app.controller
{
import de.telekom.detefleet.app.models.DatabaseModel;
import de.telekom.detefleet.app.models.MessageModel;
import de.telekom.detefleet.app.models.vo.MessageVO;

import org.robotlegs.mvcs.SignalCommand;

public class SetMessageReadCMD extends SignalCommand
{
	[Inject]
	public var databaseModel:DatabaseModel;

	[Inject]
	public var messageModel:MessageModel;

	[Inject]
	public var messageVO:MessageVO;

	public override function execute():void {
		messageModel.read(messageVO, databaseModel.connection);
	}
}
}
