package de.telekom.detefleet.app.events.fillingStation
{
import de.telekom.detefleet.app.models.vo.FuelTypeVO;

import org.osflash.signals.Signal;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class FuelTypesChanged extends Signal
{
	public function FuelTypesChanged()
	{
		super(Vector.<FuelTypeVO>);
	}
}
}
