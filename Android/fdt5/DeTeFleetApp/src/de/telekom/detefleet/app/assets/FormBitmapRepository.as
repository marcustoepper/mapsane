package de.telekom.detefleet.app.assets {
import de.aoemedia.mobile.ui.components.BitmapComponent;

/**
 * Here you find all the Elements for forms
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class FormBitmapRepository extends MultiResolutionBitmaps {
    [Embed(source="../../../../../../assets/gfx/mdpi/buttons/checkbox_embossed_m_checked_active.png")]
    protected const checkBox_active_xhdpi:Class;
    [Embed(source="../../../../../../assets/gfx/mdpi/buttons/checkbox_embossed_m_checked_taking-input.png")]
    protected const checkBox_active_down_xhdpi:Class;
    [Embed(source="../../../../../../assets/gfx/mdpi/buttons/checkbox_embossed_m_checked_unavailable.png")]
    protected const checkBox_active_unavailable_xhdpi:Class;
    [Embed(source="../../../../../../assets/gfx/mdpi/buttons/checkbox_embossed_m_unchecked_available.png")]
    // [Embed(source="../../../../../../assets/gfx/mdpi/buttons/checkbox_embossed_m_checked_active.png")]
    protected const checkBox_inactive_xhdpi:Class;
    [Embed(source="../../../../../../assets/gfx/mdpi/buttons/checkbox_embossed_m_unchecked_taking-input.png")]
    protected const checkBox_inactive_down_xhdpi:Class;
    [Embed(source="../../../../../../assets/gfx/mdpi/buttons/checkbox_embossed_m_unchecked_unavailable.png")]
    protected const checkBox_inactive_unavailable_xhdpi:Class;

    public function get checkBoxActive():BitmapComponent {
        return getBitmapByResolution(checkBox_active_xhdpi, checkBox_active_xhdpi, checkBox_active_xhdpi);
    }

    public function get checkBoxActiveDown():BitmapComponent {
        return getBitmapByResolution(checkBox_active_down_xhdpi, checkBox_active_down_xhdpi, checkBox_active_down_xhdpi);
    }

    public function get checkBoxActiveDisabled():BitmapComponent {
        return getBitmapByResolution(checkBox_active_unavailable_xhdpi, checkBox_active_unavailable_xhdpi, checkBox_active_unavailable_xhdpi);
    }

    public function get checkBoxInactive():BitmapComponent {
        return getBitmapByResolution(checkBox_inactive_xhdpi, checkBox_inactive_xhdpi, checkBox_inactive_xhdpi);
    }

    public function get checkBoxInactiveDown():BitmapComponent {
        return getBitmapByResolution(checkBox_inactive_down_xhdpi, checkBox_inactive_down_xhdpi, checkBox_inactive_down_xhdpi);
    }

    public function get checkBoxInactiveDisabled():BitmapComponent {
        return getBitmapByResolution(checkBox_inactive_unavailable_xhdpi, checkBox_inactive_unavailable_xhdpi, checkBox_inactive_unavailable_xhdpi);
    }

    protected static var _instance:FormBitmapRepository;

    public function FormBitmapRepository(sE:SingletonEnforcer) {
        super();
    }

    /**
     *
     */
    public static function getInstance():FormBitmapRepository {
        if (_instance == null) {
            _instance = new FormBitmapRepository(new SingletonEnforcer());
        }
        return _instance;
    }
}
}

internal class SingletonEnforcer {
}