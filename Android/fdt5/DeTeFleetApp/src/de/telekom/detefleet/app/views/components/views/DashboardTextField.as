package de.telekom.detefleet.app.views.components.views {
import de.aoemedia.mobile.ui.MultiDpiHelper;
import de.telekom.detefleet.app.assets.Color;
import de.telekom.detefleet.app.assets.ScaleBitmaps;
import de.telekom.detefleet.app.assets.TextFormatTemplate;

import flash.display.DisplayObject;

import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import flash.text.TextFormat;

import org.bytearray.display.ScaleBitmap;
import org.osflash.signals.Signal;

public class DashboardTextField extends Sprite {
    public var paddingLeft:Number = 10;
    public var paddingRight:Number = 10;
    public var paddingTop:Number = 11;

    protected var _width:Number;
    protected var _height:Number;

    public var linkClicked:Signal;
    private var links:Object;

    private var dashboardInfoBox:ScaleBitmap;

    private var content:Array = [
        {content:'Bei Fragen oder Anmerkungen schreiben Sie bitte an'},
        {content:'info.mobilitysolutions@telekom.de', link:"mailto:info.mobilitysolutions@telekom.de"}
    ];

    public function DashboardTextField() {
        super();

        links = {};
        linkClicked = new Signal(String);
        cacheAsBitmap = true;

        addEventListener(Event.ADDED_TO_STAGE, addedToStage);
    }

    private function addedToStage(event:Event):void {
        removeEventListener(Event.ADDED_TO_STAGE, addedToStage);

        createChildren();
    }

    protected function createChildren():void {
        var actualY:Number = MultiDpiHelper.scale(paddingTop);

        for (var i:int = 0; i < content.length; i++) {
            if (content[i].content) {
                var contentTextField:TextField = new TextField();

                contentTextField.selectable = false;
                contentTextField.embedFonts = true;
                contentTextField.multiline = true;
                contentTextField.wordWrap = true;
                contentTextField.mouseEnabled = false;
                var contentTextFormat:TextFormat = TextFormatTemplate.getInstance().dashboardInfoBox;
                contentTextField.defaultTextFormat = contentTextFormat;

                contentTextField.text = content[i].content;

                if (content[i].link) {
                    contentTextFormat.color = Color.HIGHLIGHT;
                    contentTextField.setTextFormat(contentTextFormat);
                    contentTextField.addEventListener(MouseEvent.CLICK, handleLinkClicked);

                    if (links == null) {
                        links = {};
                    }
                    contentTextField.mouseEnabled = true;
                    contentTextField.selectable = false;
                    links[content[i].content] = content[i].link;
                }

                contentTextField.width = stage.stageWidth - MultiDpiHelper.scale(paddingLeft + paddingRight);

                contentTextField.autoSize = TextFieldAutoSize.LEFT;

                contentTextField.y = actualY;
                contentTextField.x = MultiDpiHelper.scale(paddingLeft);

                addChild(contentTextField);

                actualY = contentTextField.y + contentTextField.height;
            }
        }

        dashboardInfoBox = ScaleBitmaps.getInstance().dashboardInfoBox;

        addChildAt(DisplayObject(dashboardInfoBox), 0);

        invalidate();

    }

    protected function invalidate():void{
        graphics.clear();
        graphics.beginFill(0xb1dcf1, 1);
        graphics.drawRect(0, 0, _width, _height);
        graphics.endFill();

        if(dashboardInfoBox){
            dashboardInfoBox.width = _width;
            dashboardInfoBox.height = _height;
        }
    }

    private function handleLinkClicked(event:MouseEvent):void {
        linkClicked.dispatch(links[TextField(event.currentTarget).text]);

    }

    public function setSize(width:Number, height:Number):void {
        _width = width;
        _height = height;

        invalidate();

    }
}
}
