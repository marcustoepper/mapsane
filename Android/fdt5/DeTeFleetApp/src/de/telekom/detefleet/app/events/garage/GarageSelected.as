package de.telekom.detefleet.app.events.garage
{
import de.telekom.detefleet.app.models.vo.GarageVO;

import org.osflash.signals.Signal;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class GarageSelected extends Signal
{
	public function GarageSelected()
	{
		super(GarageVO);
	}
}
}
