package de.telekom.detefleet.app.services.security
{
	public interface ISSLCertificateService
	{
		/**
		 * Loads SSLCertificate of the passed host.
		 */
		function loadCertificate ( host:String ):void;
		
		/**
		 * Returns first SSL fingerprint as SHA1 sum
		 */
		function getCertificates ( ):Vector.<CertificateVO>;
	}
}