package de.telekom.detefleet.app.events.garage
{
import de.telekom.detefleet.app.models.vo.GarageVO;

import org.osflash.signals.Signal;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class SearchResultChanged extends Signal
{
	public function SearchResultChanged()
	{
		super(Vector.<GarageVO>);
	}
}
}
