package de.telekom.detefleet.app.models {
	import org.robotlegs.mvcs.Actor;
	
	/**
	 * ...
	 * @author dev.mode
	 */
	public class NetworkViewsModel extends Actor
	{
		protected var _viewClassesRequiringNetwork:Vector.<Class>;
		
		public function NetworkViewsModel ( )
		{
			_viewClassesRequiringNetwork = new <Class>[];
		}
		
		/**
		 * Adds view class that requires network access
		 * @param viewClass
		 * 
		 */
		public function addViewClass ( viewClass:Class ):void
		{
			_viewClassesRequiringNetwork.push(viewClass);
		}
		
		public function contains ( viewClass:Class ):Boolean
		{
			for ( var i:uint = 0; i < _viewClassesRequiringNetwork.length; i++ )
			{
				if ( _viewClassesRequiringNetwork[i] == viewClass ) return true; 
			}
			return false;
		}
		
	}
	
}