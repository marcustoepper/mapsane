package de.telekom.detefleet.app.models.vo
{
/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class VehicleVO
{
	public var id:int;
	public var modelName:String;
	public var plateNo:String;
	public var recommendedFuelType:String;

	public function VehicleVO(object:Object = null)
	{
		if (object == null) {
			return;
		}
		
		id = object.id;
		modelName = object.modelName;
		plateNo = object.plateNo;
		recommendedFuelType = object.recommendedFuelType;
	}
}
}