package de.telekom.detefleet.app.controller.mileage
{
import de.telekom.detefleet.app.models.DatabaseModel;
import de.telekom.detefleet.app.models.MileageModel;

import org.robotlegs.mvcs.Command;



/**
 * Persist the Mileage on the device
 * 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 * 
 */
public class PersistMileageCMD extends Command
{
	[Inject]
	public var databaseModel:DatabaseModel;

	[Inject]
	public var model:MileageModel;

	[Inject]
	public var mileage:String;

	/**
	 * 
	 * 
	 */
	public override function execute():void
	{
		model.persistMileage(mileage, databaseModel.connection);
	}
}
}