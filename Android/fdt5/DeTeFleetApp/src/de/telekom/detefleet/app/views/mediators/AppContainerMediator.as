package de.telekom.detefleet.app.views.mediators
{
import flash.display.DisplayObject;

import de.telekom.detefleet.app.events.HideDialog;
import de.telekom.detefleet.app.events.RenewSessionSuccess;
import de.telekom.detefleet.app.events.ShowDialog;
import de.telekom.detefleet.app.events.loader.HideLoader;
import de.telekom.detefleet.app.events.loader.ShowLoader;
import de.telekom.detefleet.app.events.view.OverlayHidden;
import de.telekom.detefleet.app.events.view.ShowView;
import de.telekom.detefleet.app.events.view.ShowingOverlay;
import de.telekom.detefleet.app.events.view.TransitionChanged;
import de.telekom.detefleet.app.events.view.TransitionCompleted;
import de.telekom.detefleet.app.events.view.ViewReady;
import de.telekom.detefleet.app.models.LayoutData;
import de.telekom.detefleet.app.models.ViewsModel;
import de.telekom.detefleet.app.models.vo.PersonVO;
import de.telekom.detefleet.app.models.vo.TransitionStateVO;
import de.telekom.detefleet.app.views.components.dialog.DialogView;
import de.telekom.detefleet.app.views.components.views.AppContainer;
import de.telekom.detefleet.app.views.components.views.IView;

import org.as3commons.logging.api.ILogger;
import org.as3commons.logging.api.getLogger;
import org.robotlegs.mvcs.Mediator;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class AppContainerMediator extends Mediator
{
	[Inject]
	public var showView:ShowView;
	[Inject]
	public var viewReady:ViewReady;
	[Inject]
	public var renewSessionSuccess:RenewSessionSuccess;

	[Inject]
	public var showDialog:ShowDialog;
	[Inject]
	public var hideDialog:HideDialog;

	[Inject]
	public var signalShowLoader:ShowLoader;
	[Inject]
	public var signalHideLoader:HideLoader;
	[Inject]
	public var signalShowingOverlay:ShowingOverlay;
	[Inject]
	public var signalOverlayHidden:OverlayHidden;

	[Inject]
	public var viewModel:ViewsModel;

	[Inject]
	public var transitionChanged:TransitionChanged;

	[Inject]
	public var transitionCompleted:TransitionCompleted;

	[Inject]
	public var layout:LayoutData;

	[Inject]
	public var view:AppContainer;

	private var _overlays:uint = 0;
	private var _isShowingLoader:Boolean = false;
	
	private var _viewClassToShow:Class;

	private static const logger:ILogger = getLogger(AppContainerMediator);

	/**
	 * 
	 */
	public override function onRegister():void
	{
		showView.add(handleShowView);
		viewReady.add(handleViewReady);
		renewSessionSuccess.add(handleRenewSessionSuccess);

		showDialog.add(handleShowDialog);
		hideDialog.add(handleHideDialog);
		
		signalShowLoader.add(showLoader);
		signalHideLoader.add(hideLoader);
	}
	
	private function showLoader():void
	{
		if ( _isShowingLoader ) return;
		
		_isShowingLoader = true;
		view.showLoader();
		if ( _overlays == 0 ) 
		{
			signalShowingOverlay.dispatch();
			trace("### signalShowingOverlay");
		}
		_overlays++;
	}
	
	private function hideLoader():void
	{
		if ( !_isShowingLoader ) return;
		
		_isShowingLoader = false;
		view.hideLoader();
		_overlays--;
		if ( _overlays == 0 ) 
		{
			signalOverlayHidden.dispatch();
			trace("### signalOverlayHidden");
		}
	}

	private function handleHideDialog(dialog:DialogView):void
	{
		if ( !view.contains(DisplayObject(dialog)) ) return;
		
		view.removeChild(DisplayObject(dialog));
		_overlays--;
		if ( _overlays == 0 )
		{
			signalOverlayHidden.dispatch();
			trace("### signalOverlayHidden");
		}
	}

	private function handleShowDialog(dialog:DialogView):void
	{
		if ( view.contains(DisplayObject(dialog)) ) return;
		
		if ( _overlays == 0 )
		{
			trace("### signalShowingOverlay");
			signalShowingOverlay.dispatch();
		}
		_overlays++;
		view.addChild(DisplayObject(dialog));
	}

	private function handleRenewSessionSuccess(person:PersonVO):void
	{
		handleShowView();
	}

	/**
	 * Check if given View is the one we waiting for. Then Transition. Else do nothing
	 */
	private function handleViewReady(view:IView):void
	{
		if (viewModel.requestedView == view) {
			startPageTransition();
		}
	}

	/**
	 * 
	 */
	private function handleShowView():void
	{
		logger.info("Show view: " + viewModel.requestedViewClass);
		
		showLoader();

		var viewToShow:IView = viewModel.getView(viewModel.requestedViewClass);

		if ( viewToShow == null ) {
			viewToShow = new viewModel.requestedViewClass() as IView;
			viewModel.addView(viewModel.requestedViewClass, viewToShow);
			view.addView(viewToShow.componentSprite);
		} else if ( viewToShow == viewModel.currentView ) {
			return;
		}

		if (viewModel.isViewReady(viewToShow)) {
			startPageTransition();
		}
	}

	/**
	 * Start Transition of Views
	 */
	private function startPageTransition():void
	{
		hideLoader();
		view.transitionChanged.add(proxyTransitionChanged);
		view.transitionCompleted.add(proxyTransitionCompleted);
		view.startPageTransition(viewModel.requestedView, viewModel.currentView, viewModel.requestedTransitionType);
	}

	private function proxyTransitionChanged(transitionState:TransitionStateVO):void
	{
		transitionChanged.dispatch(transitionState);

	}

	private function proxyTransitionCompleted(transitionState:TransitionStateVO):void
	{
		transitionCompleted.dispatch(transitionState);
		view.transitionCompleted.remove(proxyTransitionCompleted);
		view.transitionChanged.remove(proxyTransitionChanged); 
	}

	override public function preRemove():void
	{
		showView.remove(handleShowView);
		viewReady.remove(handleViewReady);
		renewSessionSuccess.remove(handleRenewSessionSuccess);
		super.preRemove();
	}
}
}
