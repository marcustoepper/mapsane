package de.telekom.detefleet.app.events
{
import org.osflash.signals.Signal;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class GeolocationStatusUpdated extends Signal
{
	public function GeolocationStatusUpdated()
	{
		super();
	}
}
}
