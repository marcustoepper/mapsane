package de.telekom.detefleet.app.assets {

import de.aoemedia.mobile.ui.components.BitmapComponent;

/**
 * Here you find the Brand Icons of the FillingStations
 *
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class BrandBitmapRepository {
    [Embed(source="../../../../../../assets/gfx/xhdpi/icons/ts_aral-xhdpi.png")]
    protected static const aral_xhdpi:Class;
    [Embed(source="../../../../../../assets/gfx/hdpi/icons/ts_aral-hdpi.png")]
    protected static const aral_hdpi:Class;
    [Embed(source="../../../../../../assets/gfx/mdpi/icons/ts_aral-mdpi.png")]
    protected static const aral_mdpi:Class;

    public static function get aral():BitmapComponent {
        var bitmap:BitmapComponent = MultiResolutionBitmaps.getBitmapByResolution(aral_xhdpi, aral_hdpi, aral_mdpi);
        bitmap.baseWidth = 48;
        bitmap.baseHeight = 48;
        return bitmap;
    }

    [Embed(source="../../../../../../assets/gfx/xhdpi/icons/ts_shell-xhdpi.png")]
    protected static const shell_xhdpi:Class;
    [Embed(source="../../../../../../assets/gfx/hdpi/icons/ts_shell-hdpi.png")]
    protected static const shell_hdpi:Class;
    [Embed(source="../../../../../../assets/gfx/mdpi/icons/ts_shell-mdpi.png")]
    protected static const shell_mdpi:Class;

    public static function get shell():BitmapComponent {
        var bitmap:BitmapComponent = MultiResolutionBitmaps.getBitmapByResolution(shell_xhdpi, shell_hdpi, shell_mdpi);
        bitmap.baseWidth = 48;
        bitmap.baseHeight = 48;
        return bitmap;
    }

    [Embed(source="../../../../../../assets/gfx/xhdpi/icons/ts_total-xhdpi.png")]
    protected static const total_xhdpi:Class;
    [Embed(source="../../../../../../assets/gfx/hdpi/icons/ts_total-hdpi.png")]
    protected static const total_hdpi:Class;
    [Embed(source="../../../../../../assets/gfx/mdpi/icons/ts_total-mdpi.png")]
    protected static const total_mdpi:Class;

    public static function get total():BitmapComponent {
        var bitmap:BitmapComponent = MultiResolutionBitmaps.getBitmapByResolution(total_xhdpi, total_hdpi, total_mdpi);
        bitmap.baseWidth = 48;
        bitmap.baseHeight = 48;
        return bitmap;
    }

    /**
     * Get the Brand icon by Name
     */
    public static function getBrandIcon(brand:String):BitmapComponent {
        var bitmap:BitmapComponent;
        try {
            bitmap = BrandBitmapRepository[cleanBrandName(brand)];
        } catch (e:Error) {
            bitmap = null;
        }
        return bitmap;
    }

    /**
     * Cleans the Brand Name. Removes Whitespaces and LowerCase the Name
     *
     * @return The cleaned Brand Name
     */
    public static function cleanBrandName(brand:String):String {
        // Remove whitespaces
        var rex:RegExp = /[\s\r\n]*/gim;
        brand = brand.replace(rex, '');
        return brand.toLowerCase();
    }

}
}