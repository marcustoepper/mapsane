package de.telekom.detefleet.app.events.view
{
import de.telekom.detefleet.app.models.vo.ViewRequestVO;

import org.osflash.signals.Signal;
	
	public class BlockHistoryAdditionRequestView extends Signal
	{
		public function BlockHistoryAdditionRequestView()
		{
			super(ViewRequestVO);
		}
	}
}