package de.telekom.detefleet.app.events.view
{
import de.telekom.detefleet.app.models.vo.TransitionStateVO;

import org.osflash.signals.Signal;
	
	public class TransitionChanged extends Signal
	{
		public function TransitionChanged ()
		{
			super(TransitionStateVO);
		}
	}
}