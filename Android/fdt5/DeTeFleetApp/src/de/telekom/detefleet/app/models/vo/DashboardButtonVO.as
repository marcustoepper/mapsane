package de.telekom.detefleet.app.models.vo
{
import flash.display.Bitmap;

public class DashboardButtonVO
{
	public var icon:Bitmap;
	public var title:String;
	public var callback:Function;
	public var hideArrow:Boolean = false;
	public var targetView:Class;
	public var onlineView:Boolean = false;
	public var badgeCount:int = -1;

	/**
	 *
	 * @param icon
	 * @param title
	 * @param targetView
	 * @param callback optional. If set this callback will be triggered if pressed
	 * @param hideArrow
	 * @param onlineView
	 */
	public function DashboardButtonVO(icon:Bitmap, title:String, targetView:Class, callback:Function = null, hideArrow:Boolean = false, onlineView:Boolean = false):void {
		this.icon = icon;
		this.title = title;
		this.targetView = targetView;
		this.callback = callback;
		this.hideArrow = hideArrow;
		this.onlineView = onlineView;
	}
}
}
