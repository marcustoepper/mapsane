package de.telekom.detefleet.app.events
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author dev.mode
	 */
	public class StageWebViewBridgeEvent extends Event
	{
		public static const ON_LOADING_COMPLETE:String = 'ON_LOADING_COMPLETE';
		public static const ON_WEB_VIEW_INITIALIZED:String = 'ON_WEB_VIEW_INITIALIZED';
		public static const ON_RECEIVED_WEB_VIEW_DATA:String = 'ON_RECEIVED_WEB_VIEW_DATA';
		
		protected var _body:*;
		
		/**
		 * 
		 */
		public function StageWebViewBridgeEvent ( type:String, data:* = null )
		{
			super(type);
			_body = data;
		}
		
		public function get data ( ):*
		{
			return _body;
		}
		
		public override function clone ( ):Event
		{
			return new StageWebViewBridgeEvent(type, data);
		}
		
	}
	
}