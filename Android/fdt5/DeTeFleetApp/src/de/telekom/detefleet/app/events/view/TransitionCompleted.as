package de.telekom.detefleet.app.events.view
{
import de.telekom.detefleet.app.models.vo.TransitionStateVO;

import org.osflash.signals.Signal;

public class TransitionCompleted extends Signal
{
	public function TransitionCompleted()
	{
		super(TransitionStateVO);
	}
}
}