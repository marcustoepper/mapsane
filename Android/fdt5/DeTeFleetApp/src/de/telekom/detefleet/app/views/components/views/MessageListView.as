package de.telekom.detefleet.app.views.components.views {
import com.kaizimmer.ui.Screen;
import com.thanksmister.touchlist.renderers.ITouchListItemRenderer;

import de.telekom.detefleet.app.models.vo.MessageVO;
import de.telekom.detefleet.app.views.components.list.ListComponent;
import de.telekom.detefleet.app.views.components.listItem.MessageListItemRenderer;

import flash.display.DisplayObject;
import flash.system.System;
import flash.utils.setTimeout;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class MessageListView extends NavigationBarView {
    public var listItemClickedCallback:Function;
    private var messagesList:ListComponent;

    /**
     *
     */
    public function MessageListView() {
        super("Meldungen");
    }

    /**
     *
     *
     */
    override protected function createChildren():void {
        super.createChildren();

        messagesList = new ListComponent(Screen.stage.stageWidth, Screen.stage.stageHeight - navbar.height);

        messagesList.itemRenderer = MessageListItemRenderer;
        messagesList.visible = false;
        var object:MessageListItemRenderer = new MessageListItemRenderer();
        messagesList.rowHeight = object.itemHeight;
        object = null;

        messagesList.visualListItemBuffer = 4;
        messagesList.maxTapDelayTime = .5;
        messagesList.y = navbar.height;

        messagesList.itemSelected.add(handleListItemClicked);

        addChildAt(DisplayObject(messagesList), 0);
    }

    /**
     * Handles if item is clicked
     */
    private function handleListItemClicked(item:ITouchListItemRenderer):void {
        messagesList.mouseEnabled = false;
        // TODO: MOVE DELAY TO MEDIATOR AND DISABLE VIEW FOR FURTHER INPUT
        const CLICK_TO_TRANSITION_DELAY:Number = 125;

        setTimeout(dispatchItemClick, CLICK_TO_TRANSITION_DELAY);

        function dispatchItemClick():void {
            listItemClickedCallback(MessageVO(item.data));
            MessageListItemRenderer(item).setRead();
            messagesList.mouseEnabled = true;
        }
    }

    /**
     * Add Messages to List
     *
     *
     * @param messages
     */
    public function populateMessages(messages:Vector.<MessageVO>):void {
        if (messages == null || messages.length == 0) {
            return;
        }

        messagesList.visible = false;
        messagesList.removeAllListItems();

        var itemDescriptions:Array = [];
        for (var i:uint = 0; i < messages.length; i++) {
            itemDescriptions[i] = messages[i];
        }

        if (messages.length > 0) {
            messagesList.dataProvider = itemDescriptions;
            messagesList.visible = true;
        }
    }


    public function unselectAll():void{
        messagesList.unselectAll();
    }

}
}
