package de.telekom.detefleet.app.services.local
{
import air.net.URLMonitor;

import de.telekom.detefleet.app.events.NetworkStatusChanged;
import de.telekom.detefleet.app.models.NetworkModel;
import de.telekom.detefleet.app.services.remote.JSONServiceConfiguration;

import flash.events.StatusEvent;
import flash.net.URLRequest;

import org.as3commons.logging.api.ILogger;
import org.as3commons.logging.api.getLogger;
import org.robotlegs.mvcs.Actor;

/**
 * Checks if the Webservice URL is reachable. It uses the domain in the JSONServiceConfiguration
 *
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class URLMonitorReachable extends Actor implements IReachable
{
	private static const logger:ILogger = getLogger(URLMonitorReachable);

	[Inject]
	public var networkStatusChanged:NetworkStatusChanged;
	[Inject]
	public var networkModel:NetworkModel;

	protected var monitor:URLMonitor;

	/**
	 * Construct
	 */
	public function URLMonitorReachable():void {
		monitor = new URLMonitor(new URLRequest(JSONServiceConfiguration.endpointUrl + "/login/serviceAvailable"));

	}

	/**
	 * Starts monitoring if Server URL is reachable
	 *
	 */
	public function start():void {
		logger.info("Start URL Monitor");
		monitor.pollInterval = 3000;
		monitor.start();
		monitor.addEventListener(StatusEvent.STATUS, announceStatus);
	}

	/**
	 * Handles Event if the OnlineStatus Changed
	 * @param event
	 *
	 */
	protected function announceStatus(event:StatusEvent):void {
		if(monitor.pollInterval < 10000) {
			monitor.stop();
			monitor.pollInterval = 10000;
			monitor.start();
		}

		if(URLMonitor(event.currentTarget).available) {
			networkModel.networkAvailable = true;
			networkStatusChanged.dispatch(NetworkStatusChanged.ONLINE);
			logger.info("Network Status changed online");
			return;
		}

		networkModel.networkAvailable = false;
		networkStatusChanged.dispatch(NetworkStatusChanged.OFFLINE);
		logger.info("Network Status changed offline");
	}

	/**
	 * Stops the monitoring
	 */
	public function stop():void {
		if(monitor && monitor.running) {
			monitor.stop();
		}
	}
}
}
