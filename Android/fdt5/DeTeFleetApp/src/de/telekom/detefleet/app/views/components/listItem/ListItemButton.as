package de.telekom.detefleet.app.views.components.listItem {
import com.kaizimmer.ui.Screen;
import com.thanksmister.touchlist.renderers.ITouchListItemRenderer;

import de.telekom.detefleet.app.assets.Color;

import flash.display.Sprite;

public class ListItemButton extends Sprite implements ITouchListItemRenderer {

    protected var _index:Number;
    protected var _itemWidth:Number;
    protected var _itemHeight:Number;
    protected var _itemSelectedCallback:Function;
    protected var _itemPressedCallback:Function;

    protected function drawLine():void {
        graphics.clear();

        graphics.beginFill(0xffffff);
        graphics.drawRect(0, 0, Screen.stage.stageWidth, itemHeight);
        graphics.endFill();

        graphics.lineStyle(1, Color.LIGHT_GREY);
        graphics.moveTo(0, itemHeight - 1);
        graphics.lineTo(Screen.stage.stageWidth, itemHeight - 1);
    }

    /**
     *
     */
    protected function drawBackground():void {
        graphics.clear();

        graphics.beginFill(Color.SELECTED_BACKGROUND);
        graphics.drawRect(0, 0, Screen.stage.stageWidth, itemHeight);
        graphics.endFill();
    }

    public function set itemWidth(value:Number):void {
        _itemWidth = value;
    }

    public function get itemWidth():Number {
        return _itemWidth;
    }

    public function set itemHeight(value:Number):void {
        _itemHeight = value;
    }

    public function get itemHeight():Number {
        return _itemHeight;
    }

    public function set index(value:Number):void {
        _index = value;
    }

    public function get index():Number {
        return _index;
    }

    public function set data(value:Object):void {
    }

    public function get data():Object {
        return null;
    }

    public function selectable(value:Boolean):void {
    }

    public function unselect():void {
    }

    public function select():void {
    }

    public function destroy():void {
    }

    public function get itemSelectedCallback():Function {
        return _itemSelectedCallback;
    }

    public function set itemSelectedCallback(value:Function):void {
        _itemSelectedCallback = value;
    }

    public function get itemPressedCallback():Function {
        return _itemPressedCallback;
    }

    public function set itemPressedCallback(value:Function):void {
        _itemPressedCallback = value;
    }

    public function get labelField():String {
        return "";
    }

    public function set labelField(value:String):void {
    }
}
}
