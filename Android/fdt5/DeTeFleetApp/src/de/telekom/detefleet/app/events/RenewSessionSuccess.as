package de.telekom.detefleet.app.events {
	import de.telekom.detefleet.app.models.vo.PersonVO;

	import org.osflash.signals.Signal;

	/**
	 * 
	 */
	public class RenewSessionSuccess extends Signal {
		/**
		 * 
		 */
		public function RenewSessionSuccess() {
			super(PersonVO);
		}
	}
}