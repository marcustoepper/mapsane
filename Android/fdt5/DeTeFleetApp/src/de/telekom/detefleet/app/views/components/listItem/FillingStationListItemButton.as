package de.telekom.detefleet.app.views.components.listItem {
import com.kaizimmer.ui.Screen;

import de.aoemedia.mobile.ui.MultiDpiHelper;
import de.telekom.detefleet.app.assets.Color;
import de.telekom.detefleet.app.assets.IconRepository;
import de.telekom.detefleet.app.assets.TextFormatTemplate;
import de.telekom.detefleet.app.models.vo.FillingStationVO;

import flash.display.Bitmap;
import flash.display.DisplayObject;
import flash.events.MouseEvent;
import flash.globalization.LocaleID;
import flash.globalization.NumberFormatter;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import flash.text.TextFormat;

public class FillingStationListItemButton extends ListItemButton {
    protected const _leftPadding:uint = 12;
    protected const _titlePaddingLeft:uint = 90;
    protected const _distancePaddingRight:uint = 10;
    protected const _rightPadding:uint = 12;

    protected var _rightIcon:Bitmap;

    protected var _priceTF:TextField;
    protected var _timeTF:TextField;
    protected var _distanceTF:TextField;
    private var _label:TextField;

    protected static var _distanceFormat:TextFormat;

    protected static var distanceFormatter:NumberFormatter;
    protected static var priceFormatter:NumberFormatter;

    protected var childsCreated:Boolean = false;
    protected var backgroundDrawn:Boolean = false;

    protected var _data:FillingStationVO;

    public function FillingStationListItemButton() {
        super();

        if (distanceFormatter == null) {
            distanceFormatter = new NumberFormatter(LocaleID.DEFAULT);
            distanceFormatter.fractionalDigits = 1;
            distanceFormatter.trailingZeros = true;
        }
        if (priceFormatter == null) {
            priceFormatter = new NumberFormatter(LocaleID.DEFAULT);
            priceFormatter.fractionalDigits = 3;

            priceFormatter.trailingZeros = true;
        }

        _itemHeight = MultiDpiHelper.scale(49);

        createChildren();
        invalidate();
    }

    /**
     *
     */
    protected function createChildren():void {
        _distanceFormat = TextFormatTemplate.getInstance().listItemDistance;
        _distanceTF = new TextField();
        _distanceTF.mouseEnabled = false;
        _distanceTF.autoSize = TextFieldAutoSize.LEFT;
        _distanceTF.defaultTextFormat = _distanceFormat;
        _distanceTF.setTextFormat(_distanceFormat);
        _distanceTF.embedFonts = true;
        _distanceTF.text = " km";
        addChild(_distanceTF);

        _priceTF = new TextField();
        _distanceFormat = TextFormatTemplate.getInstance().listItemPrice;
        _priceTF.autoSize = TextFieldAutoSize.LEFT;
        _priceTF.mouseEnabled = false;
        _priceTF.defaultTextFormat = _distanceFormat;
        _priceTF.setTextFormat(_distanceFormat);
        _priceTF.embedFonts = true;
        _priceTF.text = "€";
        addChild(_priceTF);

        _timeTF = new TextField();
        _distanceFormat = TextFormatTemplate.getInstance().listItemTime;
        _timeTF.autoSize = TextFieldAutoSize.LEFT;
        _timeTF.mouseEnabled = false;
        _timeTF.defaultTextFormat = _distanceFormat;
        _timeTF.setTextFormat(_distanceFormat);
        _timeTF.embedFonts = true;
        _timeTF.text = "vor " + " Min";
        addChild(_timeTF);

        _rightIcon = IconRepository.iconArrow;
        addChild(_rightIcon);

        _label = new TextField();
        _label.text = "";
        _label.mouseEnabled = false;
        _label.defaultTextFormat = TextFormatTemplate.getInstance().listItemTitle;
        _label.embedFonts = true;
        addChild(DisplayObject(_label));

        addEventListener(MouseEvent.MOUSE_DOWN, pressHandler);

        childsCreated = true;
    }

    /**
     *
     */
    protected function invalidate():void {
        if (!childsCreated) {
            return;
        }

        _label.x = MultiDpiHelper.scale(_titlePaddingLeft);
        _timeTF.x = _priceTF.x + (_priceTF.width >> 1) - (_timeTF.width >> 1);
        _priceTF.y = itemHeight - (_priceTF.height + _timeTF.height) >> 1;

        _rightIcon.x = Screen.stage.stageWidth - _rightIcon.width - MultiDpiHelper.scale(_rightPadding);
        _distanceTF.x = int(_rightIcon.x - _distanceTF.width - MultiDpiHelper.scale(_distancePaddingRight));

        _label.width = _distanceTF.x - _label.x - MultiDpiHelper.scale(_distancePaddingRight);
        _label.height = _label.textHeight + MultiDpiHelper.scale(4);

        _label.y = itemHeight - _label.height >> 1;
        _priceTF.x = MultiDpiHelper.scale(_leftPadding);
        _timeTF.y = _priceTF.y + _priceTF.height;
        _rightIcon.y = itemHeight - _rightIcon.height >> 1;
        _distanceTF.y = itemHeight - _distanceTF.height >> 1;

        if (!backgroundDrawn) {
            drawLine();

            backgroundDrawn = true;
        }
    }

    /**
     *
     * @param data
     */
    override public function set data(data:Object):void {
        if (_data == data) {
            return;
        }
        _data = data as FillingStationVO;
        _label.text = _data.brand;
        _distanceTF.text = distanceFormatter.formatNumber(_data.distance) + " km";
        _priceTF.text = priceFormatter.formatNumber(_data.price) + "€";

        _timeTF.text = _data.getLastUpdateString();

        invalidate();
    }

    /**
     *
     */
    override public function get data():Object {
        return _data;
    }


    /**
     * makes the Item selectable
     * @param value
     */
    override public function selectable(value:Boolean):void {
        if (value) {
            addEventListener(MouseEvent.MOUSE_UP, selectHandler);
        } else {
            removeEventListener(MouseEvent.MOUSE_UP, selectHandler);
        }
    }

    /**
     * Dispatched when item is first pressed on tap or mouse down.
     * */
    protected function pressHandler(event:MouseEvent):void {
        _itemPressedCallback(this);

    }

    /**
     * Dispatched when item is selected, usually on touch end or mouse up.
     * */
    protected function selectHandler(event:MouseEvent):void {
        _itemSelectedCallback(this);

    }

    override public function destroy():void {
        removeEventListener(MouseEvent.MOUSE_UP, selectHandler);

        _itemPressedCallback = null;
        _itemSelectedCallback = null;

        this.graphics.clear();
        unselect();

        _data = null;
    }


    override public function unselect():void {
        _label.textColor = Color.LIST_ITEM;
        drawLine();
    }

    override public function select():void {
        _label.textColor = Color.DARK_GREY;
        drawBackground();
    }
}
}
