package de.telekom.detefleet.app.views.components.views
{

import de.telekom.detefleet.app.views.components.buttons.DragButton;

import com.greensock.TweenLite;

import org.osflash.signals.Signal;

import flash.display.DisplayObject;
import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.geom.Rectangle;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de> / Kai Zimmer <kai.zimmer@aoemedia.de>
 */
public class SplitView extends NavigationBarView
{
	protected const TOGGLE_TWEEN_TIME:Number = .75;

	protected var dragRect:Rectangle;
	protected var dragHandle:DragButton;

	protected var viewsContainer:Sprite;
	protected var upperView:Sprite;
	protected var lowerView:Sprite;

	public var signalUpperViewOpened:Signal;
	public var signalUpperViewClosed:Signal;

	protected var _minYUpperView:Number = 0;

	public function SplitView(label:String = "", hideBackBtn:Boolean = false)
	{
		super(label, hideBackBtn);
		signalUpperViewOpened = new Signal();
		signalUpperViewClosed = new Signal();
	}

	public function addChildToUpperView ( child:DisplayObject ):DisplayObject
	{
		upperView.addChild(child);
		updateViewsContainer();
		return child;
	}

	public function addChildToUpperViewAt ( child:DisplayObject, index:int ):DisplayObject
	{
		upperView.addChildAt(child, index);
		updateViewsContainer();
		return child;
	}

	public function addChildToLowerView ( child:DisplayObject ):DisplayObject
	{
		return lowerView.addChild(child);
	}

	public function addChildToLowerViewAt ( child:DisplayObject, index:int ):DisplayObject
	{
		return lowerView.addChildAt(child, index);
	}

	override protected function createChildren ( ):void
	{
		super.createChildren();

		// create handle bar to drag 
		dragRect = new Rectangle();

		dragHandle = new DragButton();
		dragHandle.setSize(320, 11);
		dragHandle.x = 0;//>> 1;
		dragHandle.y = int(_navbar.height);

		dragHandle.addEventListener(MouseEvent.MOUSE_DOWN, startDragging);
		dragHandle.addEventListener(MouseEvent.MOUSE_UP, stopDragging);

		// create child view containers
		lowerView = new Sprite();
		lowerView.y = dragHandle.y + dragHandle.height;

		upperView = new Sprite();
		upperView.y = dragHandle.y;

		// create views container
		viewsContainer = new Sprite();
		viewsContainer.addChild(lowerView);
		viewsContainer.addChild(upperView);
		viewsContainer.addChild(DisplayObject(dragHandle));
		viewsContainer.y = _minYUpperView;

		addChildAt(viewsContainer, 0);
	}

	protected function startDragging ( event:MouseEvent = null ):void
	{
		TweenLite.killTweensOf(viewsContainer);

		stage.addEventListener(MouseEvent.MOUSE_UP, stopDragging);
		stage.addEventListener(MouseEvent.RELEASE_OUTSIDE, stopDragging);

		dragRect.height = upperView.height;
		viewsContainer.startDrag(false, dragRect);
	}

	protected function stopDragging ( event:MouseEvent = null ):void
	{
		if ( event != null ) event.stopImmediatePropagation();

		stage.removeEventListener(MouseEvent.RELEASE_OUTSIDE, stopDragging);
		stage.removeEventListener(MouseEvent.MOUSE_UP, stopDragging);

		viewsContainer.stopDrag();
	}

	/**
	 *
	 *
	 */
	protected function openUpperView ( ):void
	{
		var dragDistancePercentage:Number = 1 - viewsContainer.y / dragRect.height;
		TweenLite.to(viewsContainer, TOGGLE_TWEEN_TIME * dragDistancePercentage, { y: upperView.height, onComplete: dispatchSignal } );

		disableViews();

		function dispatchSignal ( ):void
		{
			enableViews();
			signalUpperViewOpened.dispatch();
		}
	}

	/**
	 *
	 *
	 */
	protected function closeUpperView ( ):void
	{
		var dragDistancePercentage:Number = viewsContainer.y / dragRect.height;
		TweenLite.to(viewsContainer, TOGGLE_TWEEN_TIME * dragDistancePercentage, { y: _minYUpperView, onComplete: dispatchSignal } );

		disableViews();

		function dispatchSignal ( ):void
		{
			enableViews();
			signalUpperViewClosed.dispatch();
		}
	}

	protected function enableViews ( ):void
	{
		upperView.mouseEnabled = true;
		lowerView.mouseEnabled = true;
		lowerView.mouseChildren = true;
	}

	protected function disableViews ( ):void
	{
		upperView.mouseEnabled = false;
		lowerView.mouseEnabled = false;
		lowerView.mouseChildren = false;
	}

	protected function updateViewsContainer ( ):void
	{
		upperView.y = dragHandle.y - upperView.height;
		dragRect.height = upperView.height;
	}

	public function get minYUpperView():Number {
		return _minYUpperView;
	}

	public function set minYUpperView(minYUpperView:Number):void {
		if(_minYUpperView == minYUpperView){
			return;
		}
		_minYUpperView = minYUpperView;
	}

}
}
