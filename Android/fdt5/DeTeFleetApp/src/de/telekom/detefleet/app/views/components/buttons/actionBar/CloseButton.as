package de.telekom.detefleet.app.views.components.buttons.actionBar
{
import de.telekom.detefleet.app.assets.IconRepository;

public class CloseButton extends IconButton
{
	public function CloseButton()
	{
		super();
	}

	override protected function createChildren():void
	{
		_icon = IconRepository.cross;
		addChild(_icon);

		super.createChildren();
	}
}
}