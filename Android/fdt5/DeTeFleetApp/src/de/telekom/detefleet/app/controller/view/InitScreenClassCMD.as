package de.telekom.detefleet.app.controller.view
{
import com.kaizimmer.ui.Screen;

import de.aoemedia.mobile.ui.components.container.LayoutContainer;

import org.robotlegs.mvcs.Command;

/**
 * ...
 * @author dev.mode
 */
public class InitScreenClassCMD extends Command
{
	public override function execute():void {
		Screen.init(contextView.stage, 163, 320, 460);
        LayoutContainer.scaleFactor = Screen.scaleFactor;
	}

}

}
