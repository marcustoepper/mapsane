package de.telekom.detefleet.app.views.mediators
{
	import de.telekom.detefleet.app.events.view.TransitionChanged;
	import de.telekom.detefleet.app.models.vo.TransitionStateVO;
	import de.telekom.detefleet.app.views.components.selectbox.SelectBoxListView;
	import de.telekom.detefleet.app.views.components.views.IView;
	import org.robotlegs.mvcs.Mediator;
	
	
	/**
	 * ...
	 * @author dev.mode
	 */
	public class ListTestMediator extends Mediator
	{
		[Inject]
		public var view:SelectBoxListView;
		
		[Inject]
		public var transitionChanged:TransitionChanged;
		
		public function ListTestMediator ( )
		{
			
		}

		public override function onRegister ( ):void
		{
			// Listen to the view
			//eventMap.mapListener(view, MouseEvent.CLICK, onClick);
			// Listen to the context
			transitionChanged.add(handleTransitionChanged);
		}
		
		protected function handleTransitionChanged ( state:TransitionStateVO ):void
		{
			switch ( state.type )
			{
				case TransitionStateVO.IN:
					showWebView(state.viewToShow);
					break;
				case TransitionStateVO.TRANSITIONING:
					hideWebView(state.viewToHide);
					break;
				default:
					//
			}
		}
		
		protected function showWebView ( view:IView ):void
		{
			//if ( this.view == view ) this.view.showList();
		}
		
		protected function hideWebView ( view:IView ):void
		{
			//if ( this. == view ) this.view.hideWebView();
		}
		
		override public function preRemove ( ):void
		{
			transitionChanged.remove(handleTransitionChanged);
			super.preRemove();
		}
	}
}