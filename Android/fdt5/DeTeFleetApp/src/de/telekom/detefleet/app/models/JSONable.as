package de.telekom.detefleet.app.models {
	/**
	 * VOs which should converted into a Json String should implement this interface
	 *  
	 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
	 * 
	 */
	public interface JSONable {
		/**
		 * Return the Object as an JSON String
		 *  
		 * @return json presentation of the object
		 * 
		 */
		function toJSON() : String;
	}
}