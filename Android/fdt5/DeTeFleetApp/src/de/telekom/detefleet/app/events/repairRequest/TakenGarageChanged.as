package de.telekom.detefleet.app.events.repairRequest
{
import de.telekom.detefleet.app.models.vo.GarageVO;
import org.osflash.signals.Signal;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class TakenGarageChanged extends Signal
{
	public function TakenGarageChanged()
	{
		super(GarageVO);
	}
}
}
