package de.telekom.detefleet.app.views.components.listItem {
import com.kaizimmer.ui.Screen;
import com.thanksmister.touchlist.renderers.ITouchListItemRenderer;

import de.aoemedia.mobile.ui.MultiDpiHelper;
import de.aoemedia.utils.TextFieldUtil;
import de.telekom.detefleet.app.assets.Color;
import de.telekom.detefleet.app.assets.IconRepository;
import de.telekom.detefleet.app.assets.TextFormatTemplate;
import de.telekom.detefleet.app.models.vo.GarageVO;

import flash.display.Bitmap;
import flash.events.MouseEvent;
import flash.globalization.LocaleID;
import flash.globalization.NumberFormatter;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import flash.text.TextFormat;

public class GarageListItemButton extends ListItemButton implements ITouchListItemRenderer {
    protected var _titlePaddingLeft:uint = 12;
    protected var _distancePaddingRight:uint = 12;
    protected var _rightPadding:uint = 12;

    protected var _leftIcon:Bitmap;
    protected var _rightIcon:Bitmap;
    private var _label:TextField;

    protected var _distanceTF:TextField;

    protected static var _distanceFormat:TextFormat;

    protected static var distanceFormatter:NumberFormatter;

    protected var childsCreated:Boolean = false;
    protected var backgroundDrawn:Boolean = false;

    private var _data:GarageVO;

    public function GarageListItemButton() {
        if (distanceFormatter == null) {
            distanceFormatter = new NumberFormatter(LocaleID.DEFAULT);
            distanceFormatter.fractionalDigits = 1;
            distanceFormatter.trailingZeros = true;
        }

        if (_distanceFormat == null) {
            _distanceFormat = TextFormatTemplate.getInstance().listItemDistance;
        }

        _itemHeight = MultiDpiHelper.scale(49);
        _titlePaddingLeft =  MultiDpiHelper.scale(12);
        _distancePaddingRight = MultiDpiHelper.scale(12);
        _rightPadding = MultiDpiHelper.scale(12);

        createChildren();
        invalidate();
    }

    /**
     *
     */
    protected function createChildren():void {

        _distanceTF = new TextField();
        _distanceTF.mouseEnabled = false;
        _distanceFormat.size = MultiDpiHelper.scale(19);
        _distanceTF.autoSize = TextFieldAutoSize.LEFT;
        _distanceTF.defaultTextFormat = _distanceFormat;
        _distanceTF.embedFonts = true;
        _distanceTF.text = " km";
        addChild(_distanceTF);

        _rightIcon = IconRepository.iconArrow;
        addChild(_rightIcon);

        _label = new TextField();
        _label.mouseEnabled = false;
        _label.defaultTextFormat = TextFormatTemplate.getInstance().listItemTitle;
        _label.embedFonts = true;
        addChild(_label);

        addEventListener(MouseEvent.MOUSE_DOWN, pressHandler);

        childsCreated = true;
    }

    /**
     *
     */
    protected function invalidate():void {
        if (!childsCreated) {
            return;
        }

        _rightIcon.x = Screen.stage.stageWidth - _rightIcon.width - _rightPadding;
        _rightIcon.y = itemHeight - _rightIcon.height >> 1;

        _distanceTF.x = int(_rightIcon.x - _distanceTF.width - _distancePaddingRight);
        _distanceTF.y = itemHeight - _distanceTF.height >> 1;

        if (_leftIcon) {
            _leftIcon.x = _titlePaddingLeft;
            _leftIcon.y = itemHeight - _leftIcon.height >> 1;

            _label.x = _titlePaddingLeft * 2 + _leftIcon.width;
        } else {
            _label.x = _titlePaddingLeft;

        }
        recalculateLabelPosition();

        if (!backgroundDrawn) {
            drawLine();

            backgroundDrawn = true;
        }
    }

    private function recalculateLabelPosition():void {
        _label.width = _distanceTF.x - _label.x - _titlePaddingLeft;
        _label.height = _label.textHeight + MultiDpiHelper.scale(4);
        _label.y = itemHeight - _label.height >> 1;
    }

    public function set leftIcon(icon:Bitmap):void {
        if (icon == _leftIcon) {
            // Do nothing if it is the same
            return;
        }

        if (_leftIcon != null) {
            removeChild(_leftIcon);
        }
        _leftIcon = icon;

        if (_leftIcon != null) {
            addChild(_leftIcon);
        }

        invalidate();
    }

    /**
     *
     * @param data
     */
    override public function set data(data:Object):void {
        if (data == _data) {
            return;
        }

        _data = data as GarageVO;
        if (_data.type == 1) {
            // Default Werkstatt
            leftIcon = IconRepository.iconGarage;
        } else {
            leftIcon = null;
        }
        _distanceTF.text = distanceFormatter.formatNumber(_data.distance) + " km";

        invalidate();

        TextFieldUtil.fillText(_label, _data.name);

        recalculateLabelPosition();
    }

    override public function get data():Object {
        return _data;
    }

    /**
     * makes the Item selectable
     * @param value
     */
    override public function selectable(value:Boolean):void {
        if (value) {
            addEventListener(MouseEvent.MOUSE_UP, selectHandler);
        } else {
            removeEventListener(MouseEvent.MOUSE_UP, selectHandler);
        }
    }

    /**
     * Dispatched when item is first pressed on tap or mouse down.
     * */
    protected function pressHandler(event:MouseEvent):void {
        _itemPressedCallback(this);
    }

    /**
     * Dispatched when item is selected, usually on touch end or mouse up.
     * */
    protected function selectHandler(event:MouseEvent):void {
        _itemSelectedCallback(this);
    }

    override public function destroy():void {
        removeEventListener(MouseEvent.MOUSE_UP, selectHandler);

        _itemPressedCallback = null;
        _itemSelectedCallback = null;

        this.graphics.clear();
        unselect();
        _data = null;
    }

    override public function unselect():void {
        _label.textColor = Color.LIST_ITEM;
        drawLine();
    }

    override public function select():void {
        _label.textColor = Color.DARK_GREY;
        drawBackground();
    }

}
}
