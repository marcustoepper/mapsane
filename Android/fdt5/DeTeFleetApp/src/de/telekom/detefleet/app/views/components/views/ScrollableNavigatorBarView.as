package de.telekom.detefleet.app.views.components.views
{

import flash.display.DisplayObject;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.geom.Rectangle;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class ScrollableNavigatorBarView extends NavigationBarView
{
	protected var draggingView:Sprite;


	public function ScrollableNavigatorBarView(label:String = "", hideBackBtn:Boolean = false)
	{
		super(label, hideBackBtn);
		addEventListener(Event.ADDED_TO_STAGE, handleAddedToStage);
	}

	protected function handleAddedToStage(event:Event):void
	{
		draggingView.y = 0;
        removeEventListener(Event.ADDED_TO_STAGE, handleAddedToStage);
	}

    /**
     * Returns the View to first Position
     */
    public function gotoStart():void{
        draggingView.y = 0;
    }

	override public function addChild(child:DisplayObject):DisplayObject
	{
		var displayObject:DisplayObject = draggingView.addChild(child);
		drawDragRect();
		return displayObject;
	}

	override public function addChildAt(child:DisplayObject, index:int):DisplayObject
	{
		var displayObject:DisplayObject = draggingView.addChildAt(child, index);
		drawDragRect();
		return displayObject;
	}

	override public function removeChild(child:DisplayObject):DisplayObject
	{
		var displayObject:DisplayObject = draggingView.removeChild(child);
		drawDragRect();
		return displayObject;
	}

	override public function removeChildAt(index:int):DisplayObject
	{
		var displayObject:DisplayObject = draggingView.removeChildAt(index);
		drawDragRect();
		return displayObject;
	}

	override protected function createChildren():void
	{
		super.createChildren();

		draggingView = new Sprite();
		super.addChildAt(draggingView, 0);

		draggingView.addEventListener(MouseEvent.MOUSE_DOWN, startDragging);
		draggingView.addEventListener(MouseEvent.MOUSE_UP, stopDragging);
		
		invalidate();
	}
	
	override protected function invalidate():void{
		drawDragRect();
		
		super.invalidate();
	}

	protected function drawDragRect():void
	{
		draggingView.graphics.clear();
		draggingView.graphics.beginFill(0x000000, 0);
		draggingView.graphics.drawRect(0, 0, draggingView.width+scale(paddingRight), draggingView.height+scale(navbar.height));
		draggingView.graphics.endFill();
	}

	protected function stopDragging(event:MouseEvent):void
	{
		stage.removeEventListener(MouseEvent.RELEASE_OUTSIDE, stopDragging);
		stage.removeEventListener(MouseEvent.MOUSE_UP, stopDragging);

		draggingView.stopDrag();
	}

	protected function startDragging(event:MouseEvent):void
	{
		var viewHeight:Number = draggingView.height + navbar.height;
		if(viewHeight <= stage.fullScreenHeight){
			return;
		}
		
		stage.addEventListener(MouseEvent.MOUSE_UP, stopDragging);
		stage.addEventListener(MouseEvent.RELEASE_OUTSIDE, stopDragging);
		
		var rectHeight:Number = stage.fullScreenHeight-(draggingView.height+scale(paddingBottom));
		draggingView.startDrag(false, new Rectangle(0, 0, 0, rectHeight));
	}

}
}
