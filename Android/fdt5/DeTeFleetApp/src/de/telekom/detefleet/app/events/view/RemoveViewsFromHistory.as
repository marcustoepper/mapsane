package de.telekom.detefleet.app.events.view
{
import org.osflash.signals.Signal;

public class RemoveViewsFromHistory extends Signal
{
	public function RemoveViewsFromHistory() {
		super(int);
	}
}
}
