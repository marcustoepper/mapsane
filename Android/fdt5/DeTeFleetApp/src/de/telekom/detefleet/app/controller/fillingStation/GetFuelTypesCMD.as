package de.telekom.detefleet.app.controller.fillingStation
{
import de.telekom.detefleet.app.controller.AuthCommand;
import de.telekom.detefleet.app.services.remote.IFillingStationService;

/**
 * Send a request to get Fuel Types
 *
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class GetFuelTypesCMD extends AuthCommand
{
	[Inject]
	public var service:IFillingStationService;

	public override function execute():void {
		service.getFuelTypes(auth);
	}
}
}
