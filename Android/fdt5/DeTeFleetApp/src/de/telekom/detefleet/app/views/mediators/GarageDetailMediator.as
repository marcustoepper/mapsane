package de.telekom.detefleet.app.views.mediators
{
import de.telekom.detefleet.app.events.PhoneCallRequested;
import de.telekom.detefleet.app.events.garage.GarageSelected;
import de.telekom.detefleet.app.events.repairRequest.TakeGarage;
import de.telekom.detefleet.app.events.view.ViewReady;
import de.telekom.detefleet.app.models.GarageModel;
import de.telekom.detefleet.app.models.RepairRequestModel;
import de.telekom.detefleet.app.models.ViewsModel;
import de.telekom.detefleet.app.models.vo.GarageVO;
import de.telekom.detefleet.app.models.vo.PhoneCallRequestVO;
import de.telekom.detefleet.app.views.components.views.GarageDetailView;

import org.robotlegs.mvcs.Mediator;

import flash.net.URLRequest;
import flash.net.navigateToURL;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class GarageDetailMediator extends Mediator
{
	[Inject]
	public var garageSelected:GarageSelected;

	[Inject]
	public var takeGarage:TakeGarage;

	[Inject]
	public var view:GarageDetailView;
	
	[Inject]
	public var model:GarageModel;

	[Inject]
	public var viewModel:ViewsModel;

	[Inject]
	public var viewReady:ViewReady;

	[Inject]
	public var phoneCallRequested:PhoneCallRequested;
	
	/**
	 * 
	 */
	public override function onRegister():void
	{
		// Load Details
		view.routeButtonClicked.add(handleRouteButtonClick);
		view.takeButtonClicked.add(handleTakeButtonClicked);
		view.telephoneButtonClicked.add(handleTelephoneButtonClicked);

		garageSelected.add(fillGarage);

		viewReady.dispatch(view);
	}

	protected function fillGarage(garage:GarageVO):void
	{
		if(viewModel.context == RepairRequestModel.CONTEXT){
			view.showTakeButton();
			view.hideRouteButton();
		} else {
			view.showRouteButton();
			view.hideTakeButton();
		}
		
		view.garage = garage;
	}

	private function handleTakeButtonClicked(garage:GarageVO):void
	{
		takeGarage.dispatch(garage);
	}
	
	private function handleTelephoneButtonClicked():void
	{
		phoneCallRequested.dispatch(new PhoneCallRequestVO(view.garage.phone));
	}

	private function handleRouteButtonClick():void
	{
		var longitude:Number = view.garage.address.longitude;
		var latitude:Number = view.garage.address.latitude;
		navigateToURL(new URLRequest("http://maps.google.com/?q=" + latitude.toString() + "," + longitude.toString()));
	}

	override public function preRemove():void
	{
		view.routeButtonClicked.remove(handleRouteButtonClick);
		view.telephoneButtonClicked.remove(handleTelephoneButtonClicked);
        view.takeButtonClicked.remove(handleTakeButtonClicked);

		garageSelected.remove(fillGarage);
	}

}
}
