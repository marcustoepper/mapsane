package de.telekom.detefleet.app.views.components.list
{
import com.thanksmister.touchlist.controls.TouchList;

import de.aoemedia.mobile.ui.components.ILayoutComponent;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class ListComponent extends TouchList implements ILayoutComponent
{
	protected var _paddingLeft:Number = 0;
	protected var _paddingRight:Number = 0;
	protected var _paddingTop:Number = 0;
	protected var _paddingBottom:Number = 0;

	protected var _left:Number;
	protected var _right:Number;
    private var _scaleFactor:Number = 1;
    private var _marginBottom:Number = 0;
    private var _marginTop:Number = 0;
    private var _marginRight:Number = 0;
    private var _marginLeft:Number = 0;

	public function ListComponent(viewWidth:Number, viewHeight:Number) {
		super();
		setSize(viewWidth, viewHeight);
	}

	public function get paddingLeft():Number {
		return _paddingLeft;
	}

	public function set paddingLeft(paddingLeft:Number):void {
		_paddingLeft = paddingLeft;
	}

	public function get paddingRight():Number {
		return _paddingRight;
	}

	public function set paddingRight(paddingRight:Number):void {
		_paddingRight = paddingRight;
	}

	public function get paddingTop():Number {
		return _paddingTop;
	}

	public function set paddingTop(paddingTop:Number):void {
		_paddingTop = paddingTop;
	}

	public function get paddingBottom():Number {
		return _paddingBottom;
	}

	public function set paddingBottom(paddingBottom:Number):void {
		_paddingBottom = paddingBottom;
	}

	public function get left():Number {
		return _left;
	}

	public function set left(left:Number):void {
		_left = left;
	}

	public function get right():Number {
		return _right;
	}

	public function set right(right:Number):void {
		_right = right;
	}


    override public function get height():Number {
        return super.height + _paddingBottom*scaleFactor;
    }

    override public function get width():Number {
        return super.width + _paddingRight*scaleFactor;
    }

    public function get marginLeft():Number {
        return _marginLeft;
    }

    public function set marginLeft(marginLeft:Number):void {
        _marginLeft = marginLeft;
    }

    public function get marginRight():Number {
        return _marginRight;
    }

    public function set marginRight(marginRight:Number):void {
        _marginRight = marginRight
    }

    public function get marginTop():Number {
        return _marginTop;
    }

    public function set marginTop(marginTop:Number):void {
        _marginTop = marginTop;
    }

    public function get marginBottom():Number {
        return _marginBottom;
    }

    public function set marginBottom(marginBottom:Number):void {
        _marginBottom = marginBottom;
    }

    override public function set x(value:Number):void{
        super.x = value + paddingLeft*scaleFactor;
    }

    override public function set y(value:Number):void{
        super.y = value + paddingTop*scaleFactor;
    }

    public function get scaleFactor():Number {
        return _scaleFactor;
    }

    public function set scaleFactor(scaleFactor:Number):void {
        _scaleFactor = scaleFactor;
    }
}
}
