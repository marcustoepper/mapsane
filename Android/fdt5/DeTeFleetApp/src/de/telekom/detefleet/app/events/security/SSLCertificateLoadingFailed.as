package de.telekom.detefleet.app.events.security
{
	import org.osflash.signals.Signal;
	
	/**
	 * @author 
	 */
	public class SSLCertificateLoadingFailed extends Signal
	{
		public function SSLCertificateLoadingFailed()
		{
			super();
		}
	}
}
