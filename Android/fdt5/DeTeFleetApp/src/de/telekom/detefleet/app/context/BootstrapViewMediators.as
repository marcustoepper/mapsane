package de.telekom.detefleet.app.context {
import de.telekom.detefleet.app.views.components.dialog.CallPhoneDialogView;
import de.telekom.detefleet.app.views.components.dialog.ConfirmDialogView;
import de.telekom.detefleet.app.views.components.dialog.ErrorDialogView;
import de.telekom.detefleet.app.views.components.dialog.RepairRequestResendConfirmationDialogView;
import de.telekom.detefleet.app.views.components.dialog.ServiceFaultDialogView;
import de.telekom.detefleet.app.views.components.navigationbar.NavigationBar;
import de.telekom.detefleet.app.views.components.selectbox.SelectBoxButton;
import de.telekom.detefleet.app.views.components.selectbox.SelectBoxListView;
import de.telekom.detefleet.app.views.components.views.AppContainer;
import de.telekom.detefleet.app.views.components.views.BreakDownServiceView;
import de.telekom.detefleet.app.views.components.views.BreakdownChecklistView;
import de.telekom.detefleet.app.views.components.views.CurrentLocationView;
import de.telekom.detefleet.app.views.components.views.DashboardView;
import de.telekom.detefleet.app.views.components.views.DummyView;
import de.telekom.detefleet.app.views.components.views.FillingStationDetailView;
import de.telekom.detefleet.app.views.components.views.FillingStationListView;
import de.telekom.detefleet.app.views.components.views.FillingStationMapView;
import de.telekom.detefleet.app.views.components.views.GarageDetailView;
import de.telekom.detefleet.app.views.components.views.GarageListView;
import de.telekom.detefleet.app.views.components.views.GarageMapView;
import de.telekom.detefleet.app.views.components.views.ImprintView;
import de.telekom.detefleet.app.views.components.views.LoginView;
import de.telekom.detefleet.app.views.components.views.MessageDetailView;
import de.telekom.detefleet.app.views.components.views.MessageListView;
import de.telekom.detefleet.app.views.components.views.MileageView;
import de.telekom.detefleet.app.views.components.views.RepairRequestConfirmationCallGarageView;
import de.telekom.detefleet.app.views.components.views.RepairRequestConfirmationView;
import de.telekom.detefleet.app.views.components.views.RepairRequestFormView;
import de.telekom.detefleet.app.views.components.views.RepairRequestHistoryView;
import de.telekom.detefleet.app.views.mediators.AppContainerMediator;
import de.telekom.detefleet.app.views.mediators.BreakDownServiceMeditator;
import de.telekom.detefleet.app.views.mediators.BreakdownChecklistMeditator;
import de.telekom.detefleet.app.views.mediators.CallPhoneDialogMediator;
import de.telekom.detefleet.app.views.mediators.ConfirmDialogMediator;
import de.telekom.detefleet.app.views.mediators.CurrentLocationMeditator;
import de.telekom.detefleet.app.views.mediators.DashboardMediator;
import de.telekom.detefleet.app.views.mediators.DummyMeditator;
import de.telekom.detefleet.app.views.mediators.FillingStationDetailMediator;
import de.telekom.detefleet.app.views.mediators.FillingStationListMediator;
import de.telekom.detefleet.app.views.mediators.FillingStationMapMediator;
import de.telekom.detefleet.app.views.mediators.GarageDetailMediator;
import de.telekom.detefleet.app.views.mediators.GarageListMediator;
import de.telekom.detefleet.app.views.mediators.GarageMapMediator;
import de.telekom.detefleet.app.views.mediators.ImprintMeditator;
import de.telekom.detefleet.app.views.mediators.LoginMediator;
import de.telekom.detefleet.app.views.mediators.MessageDetailMediator;
import de.telekom.detefleet.app.views.mediators.MessageListMediator;
import de.telekom.detefleet.app.views.mediators.MileageMediator;
import de.telekom.detefleet.app.views.mediators.RepairRequestConfirmationCallGarageMediator;
import de.telekom.detefleet.app.views.mediators.RepairRequestConfirmationMediator;
import de.telekom.detefleet.app.views.mediators.RepairRequestFormMediator;
import de.telekom.detefleet.app.views.mediators.RepairRequestHistoryMediator;
import de.telekom.detefleet.app.views.mediators.RepairRequestResendConfirmationDialogMediator;
import de.telekom.detefleet.app.views.mediators.navigationbar.NavigationBarMediator;
import de.telekom.detefleet.app.views.mediators.selectbox.SelectBoxButtonMediator;
import de.telekom.detefleet.app.views.mediators.selectbox.SelectBoxListViewMediator;

import org.robotlegs.core.IMediatorMap;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class BootstrapViewMediators {
    public function BootstrapViewMediators(mediatorMap:IMediatorMap) {

        mediatorMap.mapView(AppContainer, AppContainerMediator, null, true, false);

        // bind Mediator classes to View classes
        mediatorMap.mapView(LoginView, LoginMediator);
        mediatorMap.mapView(NavigationBar, NavigationBarMediator);
        mediatorMap.mapView(DashboardView, DashboardMediator, null, true, false);
        mediatorMap.mapView(FillingStationListView, FillingStationListMediator, null, true, false);
        mediatorMap.mapView(FillingStationMapView, FillingStationMapMediator, null, true, false);
        mediatorMap.mapView(FillingStationDetailView, FillingStationDetailMediator, null, true, false);
        mediatorMap.mapView(SelectBoxButton, SelectBoxButtonMediator, null, true, false);
        mediatorMap.mapView(SelectBoxListView, SelectBoxListViewMediator, null, true, false);

        mediatorMap.mapView(GarageListView, GarageListMediator, null, true, false);
        mediatorMap.mapView(GarageDetailView, GarageDetailMediator, null, true, false);
        mediatorMap.mapView(GarageMapView, GarageMapMediator, null, true, false);

        mediatorMap.mapView(RepairRequestHistoryView, RepairRequestHistoryMediator, null, true, false);
        // mediatorMap.mapView(ListTestView, ListTestMediator);

        mediatorMap.mapView(MileageView, MileageMediator, null, true, false);

        mediatorMap.mapView(ImprintView, ImprintMeditator, null, true, false);
        mediatorMap.mapView(DummyView, DummyMeditator, null, true, false);
        mediatorMap.mapView(BreakDownServiceView, BreakDownServiceMeditator, null, true, false);
        mediatorMap.mapView(BreakdownChecklistView, BreakdownChecklistMeditator, null, true, false);
        mediatorMap.mapView(CurrentLocationView, CurrentLocationMeditator, null, true, false);

        // Dialogs
        mediatorMap.mapView(ConfirmDialogView, ConfirmDialogMediator);
        mediatorMap.mapView(ErrorDialogView, ConfirmDialogMediator, ConfirmDialogView);
        mediatorMap.mapView(ServiceFaultDialogView, ConfirmDialogMediator, ConfirmDialogView);
        mediatorMap.mapView(RepairRequestConfirmationView, RepairRequestConfirmationMediator);
        mediatorMap.mapView(CallPhoneDialogView, CallPhoneDialogMediator, null, true, false);
        mediatorMap.mapView(RepairRequestResendConfirmationDialogView, RepairRequestResendConfirmationDialogMediator);

        mediatorMap.mapView(RepairRequestConfirmationCallGarageView, RepairRequestConfirmationCallGarageMediator);

        mediatorMap.mapView(RepairRequestFormView, RepairRequestFormMediator);

        mediatorMap.mapView(MessageListView, MessageListMediator);
        mediatorMap.mapView(MessageDetailView, MessageDetailMediator);

    }
}
}
