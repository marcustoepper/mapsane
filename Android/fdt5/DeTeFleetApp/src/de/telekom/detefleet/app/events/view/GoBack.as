package de.telekom.detefleet.app.events.view {
	import org.osflash.signals.Signal;
	
	public class GoBack extends Signal
	{
		public function GoBack()
		{
			super();
		}
	}
}