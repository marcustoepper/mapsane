package de.telekom.detefleet.app.events.login {
	import de.telekom.detefleet.app.models.vo.LoginFormData;
	
	import org.osflash.signals.Signal;
	
	
	public class LoginRequest extends Signal
	{
		public function LoginRequest()
		{
			super(LoginFormData);
		}
	}
}
