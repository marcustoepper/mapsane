package de.telekom.detefleet.app.views.components.buttons
{
import de.aoemedia.mobile.ui.components.buttons.CheckBox;
import de.aoemedia.mobile.ui.components.skins.ToggleButtonBitmaps;
import de.telekom.detefleet.app.assets.FormBitmapRepository;

import com.kaizimmer.events.ValidationEvent;
import com.kaizimmer.ui.form.ICheckBox;

import flash.events.MouseEvent;

	public class DefaultCheckBox extends CheckBox implements ICheckBox
	{
		protected var _isMandatory:Boolean = false;
		
		public function DefaultCheckBox ( bmps:ToggleButtonBitmaps = null, isChecked:Boolean = false )
		{
			var mRBmps:FormBitmapRepository = FormBitmapRepository.getInstance();
			bmps = bmps != null ? bmps :
					new ToggleButtonBitmaps(mRBmps.checkBoxInactive,
											mRBmps.checkBoxInactiveDown,
											mRBmps.checkBoxInactiveDisabled,
											mRBmps.checkBoxInactiveDisabled,
											mRBmps.checkBoxActive,
											mRBmps.checkBoxActiveDown,
											mRBmps.checkBoxActiveDisabled,
											mRBmps.checkBoxActiveDisabled);
			super(bmps, isChecked);
			setSize(24,24);
		}
		
		public function get isMandatory ( ):Boolean
		{
			return _isMandatory;
		}

		public function set isMandatory ( iM:Boolean ):void
		{
			_isMandatory = iM;
		}

		public function get isSelected ( ):Boolean
		{
			return _isChecked;
		}

		public function set isSelected ( selected:Boolean ):void
		{
			isChecked = selected;
			if ( isMandatory ) dispatchValidationEvent();
		}

		public function highlight ( ):void
		{
			//TODO Auto-generated method stub
		}

		public function reset ( ):void
		{
			//TODO Auto-generated method stub
		}

		public function validate ( ):Boolean
		{
			if ( !_isMandatory ) return true;
			return _isChecked;
		}
		
		override protected function changeState ( e:MouseEvent ):void
		{
			super.changeState(e);
			if ( isMandatory ) dispatchValidationEvent();
		}
		
		protected function dispatchValidationEvent ( ):void
		{
			dispatchEvent(new ValidationEvent(ValidationEvent.CHANGE, true, false, _isChecked));
		}

	}
}