package de.telekom.detefleet.app.views.components.list
{
import com.kaizimmer.ui.controls.IStamp;
import com.kaizimmer.ui.controls.IStamp;

import de.polygonal.ds.DLL;
import de.polygonal.ds.pooling.LinkedObjectPool;

import flash.display.DisplayObject;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.geom.Rectangle;
import flash.utils.clearTimeout;
import flash.utils.describeType;
import flash.utils.setTimeout;

import org.as3commons.logging.api.ILogger;
import org.as3commons.logging.api.getLogger;
import org.osflash.signals.Signal;

/**
 *
 */
public class List extends Sprite
{
	public var itemSelected:Signal;

	protected static const TAP_DELAY:Number = 325;

	private var _dataProvider:Array;
	private var _itemRenderer:Class;
	protected var _scrollCanvas:Sprite;
	protected var _lastItemIndex:int = 0;
	protected var _firstItemIndex:int = -1;
	private var _maxNumberVisibleItems:int;
	private var _maxNumberItems:int;

	private var _currentVisibleItems:DLL;
	protected var _itemHeight:Number;

	protected var _renderRect:Rectangle;
	protected var _draggingStartY:Number;

	private var _viewHeight:Number;

	private const NUMBER_NONVISIBLE_ITEMS:int = 8;

	protected var _nextUpdateUpY:Number = 0;
	protected var _nextUpdateDownY:Number;

	private static const logger:ILogger = getLogger(List);

	private var _objectPool:LinkedObjectPool;

	/**
	 *
	 * @param viewWidth
	 * @param viewHeight
	 */
	public function List(viewWidth:Number, viewHeight:Number) {
		_viewHeight = viewHeight;

		_renderRect = new Rectangle(0, 0, viewWidth, viewHeight);

		_currentVisibleItems = new DLL();

		itemSelected = new Signal(Object);

		createChildren();
	}

	protected function createChildren():void {
		_scrollCanvas = new Sprite();
	//	_scrollCanvas.mouseChildren = false;

		_scrollCanvas.graphics
		addChild(_scrollCanvas);



		_scrollCanvas.addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
		_scrollCanvas.addEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
		_scrollCanvas.addEventListener(MouseEvent.MOUSE_OUT, stopDragging);
	}

	/**
	 * Calc how much item shown on the view
	 * @return
	 */
	public function calculateNumberVisibleItems():int {
		return int(_viewHeight / _itemHeight);
	}

	public function get dataProvider():Array {
		return _dataProvider;
	}

	public function set dataProvider(value:Array):void {
		if(_dataProvider == value) {
			return;
		}
		_dataProvider = value;

		invalidate();
	}

	public function get itemRenderer():Class {
		return _itemRenderer;
	}

	public function set itemRenderer(value:Class):void {
		if(_itemRenderer == value) {
			return;
		}
		if(!describeType(value).factory.implementsInterface.(@type == "IStamp")) {
			throw new Error("itemRenderer must implement IStamp");
		}

		_itemRenderer = value;

		var object:* = new _itemRenderer();
		_itemHeight = object.height;
		_maxNumberVisibleItems = calculateNumberVisibleItems();
		_maxNumberItems = _maxNumberVisibleItems + NUMBER_NONVISIBLE_ITEMS;

		_objectPool = new LinkedObjectPool(_maxNumberItems + 4, false);
		_objectPool.allocate(_itemRenderer);

		invalidate();
	}

	/**
	 *
	 */
	public function clear():void {
		var i:int = _currentVisibleItems.size();
		if(i == 0) {
			return;
		}

		for (null; i > 0; i--) {
			_objectPool.put(_currentVisibleItems.removeTail());
		}

		_scrollCanvas.removeChildren();
		_scrollCanvas.y = 0;
		_renderRect.y = 0;
		isDragging = false;
		_lastItemIndex = 0;
		_firstItemIndex = -1;

	}

	private function invalidate():void {
		if(_dataProvider && _itemRenderer) {
			clear();

			var startY:Number = 0;

			_renderRectBoundY = _itemHeight * _dataProvider.length - _renderRect.height;

			if(_dataProvider.length < _maxNumberItems) {
				_maxNumberItems = _dataProvider.length;
			}

			for (_lastItemIndex = 0; _lastItemIndex < _maxNumberItems; _lastItemIndex++) {
				var item:Object = _objectPool.get();
				item.data = _dataProvider[_lastItemIndex];
				item.y = startY;
				_currentVisibleItems.append(item);

				startY += _itemHeight;

				_scrollCanvas.addChild(DisplayObject(item));
			}
		}
	}


	/**
	 *
	 */
	public function updateItems(direction:int):void {
		trace("_currentVisibleItems.size(): " + _currentVisibleItems.size());

		var item:Object;
		var lastItem:DisplayObject;
		if(direction == 0) { // scroll from bottom to top
			// No item to add available
			if(_lastItemIndex >= _dataProvider.length) {
				return;
			}
			// Add next item
			item = _objectPool.get();
			item.data = _dataProvider[_lastItemIndex];
			lastItem = _currentVisibleItems.tail.val as DisplayObject;
			item.y = lastItem.y + lastItem.height;
			_lastItemIndex++;

			_currentVisibleItems.append(item);
			_scrollCanvas.addChild(DisplayObject(item));

			if(_currentVisibleItems.size() >= _maxNumberItems) {
				// Remove item id exist
				item = _currentVisibleItems.removeHead() as DisplayObject;
				_firstItemIndex++;
				if(_scrollCanvas.contains(DisplayObject(item))) {
					_scrollCanvas.removeChild(DisplayObject(item));
				}
				_objectPool.put(item as _itemRenderer);

			}
		} else { // scroll from top to bottom
			// No item to add available
			if(_firstItemIndex < 0) {
				return;
			}
			// Add next item
			item = _objectPool.get() as _itemRenderer;
			IStamp(item).data = _dataProvider[_firstItemIndex];
			_firstItemIndex--;
			lastItem = _currentVisibleItems.head.val as DisplayObject;
			item.y = lastItem.y - lastItem.height;
			_scrollCanvas.addChild(DisplayObject(item));

			_currentVisibleItems.insertBefore(_currentVisibleItems.head, item);

			// Remove last Item
			if(_currentVisibleItems.size() >= _maxNumberItems) {
				item = _currentVisibleItems.removeTail();
				if(_scrollCanvas.contains(DisplayObject(item))) {
					_scrollCanvas.removeChild(DisplayObject(item));
				}
				_lastItemIndex--;
				_objectPool.put(item as _itemRenderer);
			}

		}
	}

	/**
	 * SCROLLING
	 */
	protected var tapTimeout:uint;
	protected var vY:Number = 0;
	protected var isDragging:Boolean = false;
	protected var OFFSETS_NUM:uint = 3;
	protected var offsetHistory:Vector.<Number> = new <Number>[];
	protected var offset:Number = 0;
	protected var targetY:Number = 0;
	protected var _renderRectBoundY:Number;

	protected function killTapTimeout():void {
		if(tapTimeout == 0) return;
		clearTimeout(tapTimeout);
		tapTimeout = 0;
	}

	protected function handleMouseDown(event:MouseEvent):void {
		if(_dataProvider.length <= _maxNumberVisibleItems) {
			return;
		}
		event.preventDefault();
		event.stopImmediatePropagation();

		// ignore mouse downs above or below the rendering area
		if(mouseY > _renderRect.height) {
			trace("InterActiveStampList .handleMouseDown() | prevent interaction with non visible area of viewcanvas!");
			return;
		}

		killTapTimeout();
		// prevent taps while list is scrolling
		if(vY == 0) {
			tapTimeout = setTimeout(performTap, TAP_DELAY);
		}

		startDragging();
		_scrollCanvas.addEventListener(MouseEvent.MOUSE_MOVE, handleMouseMove);
	}

	protected function handleMouseMove(event:MouseEvent):void {
		event.preventDefault();
		event.stopImmediatePropagation();
		killTapTimeout();
		//scrollBarNeedsRendering = true;
		event.updateAfterEvent();
	}

	protected function handleMouseUp(event:MouseEvent):void {
		//event.preventDefault();
		//event.stopImmediatePropagation();
		// tapTimeout != 0 means that the use tapped on the list while the list was standing still
		// we clearly consider this as an tap action
		// so we kill the tap timeout and perform the tap method immediately
		if(tapTimeout != 0) {
			// don't recognize mouse ups above or below the rendering area
			if(mouseY >= 0 && mouseY <= _renderRect.height) {
				performTap(event);
			}
		}
		stopDragging();
	}

	protected function performTap(event:MouseEvent):void {
		killTapTimeout();
		logger.debug("item clicked");

		itemSelected.dispatch(IStamp(event.currentTarget).data);
	}

	protected function startDragging():void {
		trace("startDragging");

		// prevent dragging if there are less items than the rendering area is capable to render at once
		if(_dataProvider.length * _itemHeight <= _renderRect.height) return;

		isDragging = true;

		// Reset the offset hsitory
		vY = 0;
		for (var i:uint = 0; i < OFFSETS_NUM; i++) {
			offsetHistory[i] = 0;
		}

		_draggingStartY = _scrollCanvas.y;

		offset = mouseY;
		if(_scrollCanvas.y == 0) {
			_nextUpdateUpY = _scrollCanvas.y - _itemHeight * 2;
			_nextUpdateDownY = _scrollCanvas.y - _itemHeight * 2;
		} else {
			_nextUpdateUpY = _scrollCanvas.y - _itemHeight;
			_nextUpdateDownY = _scrollCanvas.y - _itemHeight;
		}

		_scrollCanvas.removeEventListener(Event.ENTER_FRAME, updateRenderRectangle);
		_scrollCanvas.addEventListener(Event.ENTER_FRAME, updateRenderRectangle);
	}

	protected function stopDragging(event:MouseEvent = null):void {
		if(!isDragging) return;
		trace("stopDragging");
		if(event != null) {
			event.preventDefault();
			event.stopImmediatePropagation();
		}

		// When runing on high frame rates (i.e. 60 fps for example) it may happen that there are two sequent frames
		// (before the mouse up event) where no change in mouseY is detected (even though the user moved his finger)
		// in that case vY would be 0 and the list stops instantly.
		// To make sure this won't happen we take the last OFFSETS_NUM frames into consideration:
		vY = 0;
		for (var i:uint = 0; i < OFFSETS_NUM; i++) {
			vY += offsetHistory[i];
		}
		vY /= OFFSETS_NUM;

		trace("VY: " + vY);

		isDragging = false;

		_scrollCanvas.removeEventListener(MouseEvent.MOUSE_MOVE, handleMouseMove);
	}

	protected function updateRenderRectangle(event:Event):void {
		//trace("updateRenderRectangle vY: " + vY);

		event.preventDefault();
		event.stopImmediatePropagation();

		const FRICTION:Number = .95;

		if(isDragging) {
			vY = mouseY - offset;

			offsetHistory.shift();
			offsetHistory.push(vY);

			offset = mouseY;
			targetY = _renderRect.y - vY;
			_renderRect.y = targetY;
		}
		else {
			_renderRect.y -= vY;
			vY *= FRICTION;
			if(vY > -.01 && vY < .01) {
				vY = 0;
//				scrollBarNeedsRendering = false;
				_scrollCanvas.removeEventListener(Event.ENTER_FRAME, updateRenderRectangle);
			}
		}
		if(_renderRect.y < 0) {
			_renderRect.y = 0;
			vY = 0;
		}
		else if(_renderRect.y > _renderRectBoundY) {
			_renderRect.y = _renderRectBoundY;
			vY = 0;
		}

		_scrollCanvas.y = -_renderRect.y;

		if(vY < 0 && _nextUpdateUpY >= _scrollCanvas.y) {
			trace("========================================");
			updateItems(0);

			trace("_nextUpdateUpY: " + _nextUpdateUpY);
			trace("_nextUpdateDownY: " + _nextUpdateDownY);

			_nextUpdateUpY = _scrollCanvas.y - _itemHeight;
			_nextUpdateDownY = _scrollCanvas.y;

			trace("_nextUpdateUpY: " + _nextUpdateUpY);
			trace("_nextUpdateDownY: " + _nextUpdateDownY);
		}

		if(vY > 0 && _nextUpdateDownY <= _scrollCanvas.y) {
			trace("========================================");
			updateItems(1);

			trace("_nextUpdateUpY: " + _nextUpdateUpY);
			trace("_nextUpdateDownY: " + _nextUpdateDownY);

			_nextUpdateUpY = _scrollCanvas.y;
			_nextUpdateDownY = _scrollCanvas.y + _itemHeight;

			trace("_nextUpdateUpY: " + _nextUpdateUpY);
			trace("_nextUpdateDownY: " + _nextUpdateDownY);
		}

	}
}
}
