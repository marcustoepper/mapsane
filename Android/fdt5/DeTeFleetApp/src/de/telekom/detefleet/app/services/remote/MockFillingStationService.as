package de.telekom.detefleet.app.services.remote
{
import de.telekom.detefleet.app.events.fillingStation.FillingStationLoadFault;
import de.telekom.detefleet.app.models.FillingStationModel;
import de.telekom.detefleet.app.models.vo.FillingStationRequestVO;
import de.telekom.detefleet.app.models.vo.FillingStationVO;
import de.telekom.detefleet.app.models.vo.FuelTypeVO;
import de.telekom.detefleet.app.models.vo.ServiceAuthVO;

import flash.utils.setTimeout;

import org.robotlegs.mvcs.Actor;

/**
 * Implementation for an Mock Service to get some Filling Stations.
 * It returns only some Dummy Data to test locally.
 *
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class MockFillingStationService extends Actor implements IFillingStationService
{
	// Model
	[Inject]
	public var model:FillingStationModel;

	// Signals
	[Inject]
	public var fillingStationLoadFault:FillingStationLoadFault;

	/**
	 * Search for Filling Stations.
	 *
	 * If successful it fills the FillingStationModel.lastSearchResult  with the Result an disptach an FillingStationsLoaded Signal.
	 * If unseccessful it fills the FillingStationLoadFault with the error Message
	 *
	 * @param auth The Authentification for the Webservice
	 * @param request The request for the Search
	 *
	 */
	public function search(auth:ServiceAuthVO, request:FillingStationRequestVO):void {
		setTimeout(endServiceCall, 125);

		function endServiceCall():void {
			if(request.location.address == "error") {
				fillingStationLoadFault.dispatch("Tankstellen konnten nicht geladen werden");
				model.searchResult = null;
				return;
			}
			var fillingStations:Vector.<FillingStationVO> = new Vector.<FillingStationVO>();

			if(request.location.address != "empty") {
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":1,"address":{"address":"Augsburger Str. 704, 70329 Stuttgart","city":"Stuttgart","houseNumber":"704","latitude":48.76139,"longitude":9.27076,"street":"Augsburger Str.","zip":"70329"},"brand":"1 ARAL","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":false,"distance":189.79,"garage":true,"id":105696876,"phone":"0711 3201641","price":"0.12","priceValidFrom":"2012-08-29T10:31:17+02:00","shop":true}')));
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":2,"address":{"address":"Augsburger Str. 704, 70329 Stuttgart","city":"Stuttgart","houseNumber":"704","latitude":48.76139,"longitude":9.27076,"street":"Augsburger Str.","zip":"70329"},"brand":"2 ARAL","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":false,"distance":189.79,"garage":true,"id":105696876,"phone":"0711 3201641","price":"0.22","priceValidFrom":"2012-08-29T10:31:17+02:00","shop":true}')));
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":3,"address":{"address":"Waiblinger Str. 23-25, 70372 Stuttgart","city":"Stuttgart","houseNumber":"23-25","latitude":48.80419,"longitude":9.221,"street":"Waiblinger Str.","zip":"70372"},"brand":"3 ARAL","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":true,"distance":190.8,"garage":true,"id":105693596,"phone":"0711 9561039 ","price":"0.32","priceValidFrom":"2012-08-29T10:25:34+02:00","shop":true}')));
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":4,"address":{"address":"Weimarische Str. 29, 99438 Bad Berka","city":"Bad Berka","houseNumber":"29","latitude":50.90151,"longitude":11.28108,"street":"Weimarische Str.","zip":"99438"},"brand":"4 TOTAL","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":true,"distance":151.15,"garage":true,"id":105438949,"phone":"036458 42107","price":"1.098","priceValidFrom":"2012-08-07T16:05:13+02:00","shop":false}')));
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":5,"address":{"address":"Prinzenstr. 29, 10969 Berlin","city":"Berlin","houseNumber":"29","latitude":52.50179,"longitude":13.40936,"street":"Prinzenstr.","zip":"10969"},"brand":"5 ARAL","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":true,"distance":351.62,"garage":true,"id":105692108,"phone":"030 6149860 ","price":"1.1","priceValidFrom":"2012-08-29T12:46:30+02:00","shop":true}')));
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":6,"address":{"address":"Eiffestr. 506, 20537 Hamburg","city":"Hamburg","houseNumber":"506","latitude":53.55196,"longitude":10.04999,"street":"Eiffestr.","zip":"20537"},"brand":"6 ARAL","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":true,"distance":456.75,"garage":true,"id":105695635,"phone":"040 213820 ","price":"1.1","priceValidFrom":"2012-08-29T11:02:25+02:00","shop":true}')));
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":7,"address":{"address":"Aldinger Str 74, 70378 Stuttgart","city":"Stuttgart","houseNumber":"74","latitude":48.84056,"longitude":9.22871,"street":"Aldinger Str","zip":"70378"},"brand":"7 OMV","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":false,"distance":188.5,"garage":true,"id":105696637,"phone":"0711 3652812 ","price":"1.268","priceValidFrom":"2012-08-27T14:32:10+02:00","shop":false}')));
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":8,"address":{"address":" ,    ","city":" ","houseNumber":"","latitude":47.29849,"longitude":7.93454,"street":" ","zip":" "},"brand":"8 BP","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":false,"distance":367.87,"garage":true,"id":105439038,"phone":"","price":"1.406","priceValidFrom":"2012-07-10T10:05:30+02:00","shop":false}')));
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":9,"address":{"address":"Hedelfinger Stra\u00dfe 25 A, 70327 Stuttgart","city":"Stuttgart","houseNumber":"25 A","latitude":48.76886,"longitude":9.24786,"street":"Hedelfinger Stra\u00dfe","zip":"70327"},"brand":"9 ARAL","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":false,"distance":190.87,"garage":true,"id":105695809,"phone":"0711 4077791 ","price":"1.999","priceValidFrom":"2012-08-29T10:56:33+02:00","shop":true}')));
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":10,"address":{"address":"Vor dem Schlesischen Tor 2, 10997 Berlin","city":"Berlin","houseNumber":"2","latitude":52.49666,"longitude":13.45003,"street":"Vor dem Schlesischen Tor","zip":"10997"},"brand":"10 ARAL","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":true,"distance":352.14,"garage":true,"id":105697658,"phone":"030 61286590","price":"2.1","priceValidFrom":"2012-08-29T12:48:21+02:00","shop":true}')));
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":11,"address":{"address":"Augsburger Str. 704, 70329 Stuttgart","city":"Stuttgart","houseNumber":"704","latitude":48.76139,"longitude":9.27076,"street":"Augsburger Str.","zip":"70329"},"brand":"11 ARAL","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":false,"distance":189.79,"garage":true,"id":105696876,"phone":"0711 3201641","price":"0.222","priceValidFrom":"2012-08-29T10:31:17+02:00","shop":true}')));
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":12,"address":{"address":"Augsburger Str. 704, 70329 Stuttgart","city":"Stuttgart","houseNumber":"704","latitude":48.76139,"longitude":9.27076,"street":"Augsburger Str.","zip":"70329"},"brand":"12 ARAL","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":false,"distance":189.79,"garage":true,"id":105696876,"phone":"0711 3201641","price":"0.222","priceValidFrom":"2012-08-29T10:31:17+02:00","shop":true}')));
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":13,"address":{"address":"Waiblinger Str. 23-25, 70372 Stuttgart","city":"Stuttgart","houseNumber":"23-25","latitude":48.80419,"longitude":9.221,"street":"Waiblinger Str.","zip":"70372"},"brand":"13 ARAL","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":true,"distance":190.8,"garage":true,"id":105693596,"phone":"0711 9561039 ","price":"0.222","priceValidFrom":"2012-08-29T10:25:34+02:00","shop":true}')));
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":14,"address":{"address":"Weimarische Str. 29, 99438 Bad Berka","city":"Bad Berka","houseNumber":"29","latitude":50.90151,"longitude":11.28108,"street":"Weimarische Str.","zip":"99438"},"brand":"14 TOTAL","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":true,"distance":151.15,"garage":true,"id":105438949,"phone":"036458 42107","price":"1.098","priceValidFrom":"2012-08-07T16:05:13+02:00","shop":false}')));
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":15,"address":{"address":"Prinzenstr. 29, 10969 Berlin","city":"Berlin","houseNumber":"29","latitude":52.50179,"longitude":13.40936,"street":"Prinzenstr.","zip":"10969"},"brand":"15 ARAL","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":true,"distance":351.62,"garage":true,"id":105692108,"phone":"030 6149860 ","price":"1.1","priceValidFrom":"2012-08-29T12:46:30+02:00","shop":true}')));
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":16,"address":{"address":"Eiffestr. 506, 20537 Hamburg","city":"Hamburg","houseNumber":"506","latitude":53.55196,"longitude":10.04999,"street":"Eiffestr.","zip":"20537"},"brand":"16 ARAL","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":true,"distance":456.75,"garage":true,"id":105695635,"phone":"040 213820 ","price":"1.1","priceValidFrom":"2012-08-29T11:02:25+02:00","shop":true}')));
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":17,"address":{"address":"Aldinger Str 74, 70378 Stuttgart","city":"Stuttgart","houseNumber":"74","latitude":48.84056,"longitude":9.22871,"street":"Aldinger Str","zip":"70378"},"brand":"17 OMV","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":false,"distance":188.5,"garage":true,"id":105696637,"phone":"0711 3652812 ","price":"1.268","priceValidFrom":"2012-08-27T14:32:10+02:00","shop":false}')));
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":18,"address":{"address":" ,    ","city":" ","houseNumber":"","latitude":47.29849,"longitude":7.93454,"street":" ","zip":" "},"brand":"18 BP","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":false,"distance":367.87,"garage":true,"id":105439038,"phone":"","price":"1.406","priceValidFrom":"2012-07-10T10:05:30+02:00","shop":false}')));
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":19,"address":{"address":"Hedelfinger Stra\u00dfe 25 A, 70327 Stuttgart","city":"Stuttgart","houseNumber":"25 A","latitude":48.76886,"longitude":9.24786,"street":"Hedelfinger Stra\u00dfe","zip":"70327"},"brand":"19 ARAL","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":false,"distance":190.87,"garage":true,"id":105695809,"phone":"0711 4077791 ","price":"1.999","priceValidFrom":"2012-08-29T10:56:33+02:00","shop":true}')));
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":20,"address":{"address":"Vor dem Schlesischen Tor 2, 10997 Berlin","city":"Berlin","houseNumber":"2","latitude":52.49666,"longitude":13.45003,"street":"Vor dem Schlesischen Tor","zip":"10997"},"brand":"20 ARAL","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":true,"distance":352.14,"garage":true,"id":105697658,"phone":"030 61286590","price":"2.1","priceValidFrom":"2012-08-29T12:48:21+02:00","shop":true}')));
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":21,"address":{"address":"Augsburger Str. 704, 70329 Stuttgart","city":"Stuttgart","houseNumber":"704","latitude":48.76139,"longitude":9.27076,"street":"Augsburger Str.","zip":"70329"},"brand":"21 ARAL","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":false,"distance":189.79,"garage":true,"id":105696876,"phone":"0711 3201641","price":"0.222","priceValidFrom":"2012-08-29T10:31:17+02:00","shop":true}')));
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":22,"address":{"address":"Augsburger Str. 704, 70329 Stuttgart","city":"Stuttgart","houseNumber":"704","latitude":48.76139,"longitude":9.27076,"street":"Augsburger Str.","zip":"70329"},"brand":"22 ARAL","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":false,"distance":189.79,"garage":true,"id":105696876,"phone":"0711 3201641","price":"0.222","priceValidFrom":"2012-08-29T10:31:17+02:00","shop":true}')));
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":23,"address":{"address":"Waiblinger Str. 23-25, 70372 Stuttgart","city":"Stuttgart","houseNumber":"23-25","latitude":48.80419,"longitude":9.221,"street":"Waiblinger Str.","zip":"70372"},"brand":"23 ARAL","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":true,"distance":190.8,"garage":true,"id":105693596,"phone":"0711 9561039 ","price":"0.222","priceValidFrom":"2012-08-29T10:25:34+02:00","shop":true}')));
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":24,"address":{"address":"Weimarische Str. 29, 99438 Bad Berka","city":"Bad Berka","houseNumber":"29","latitude":50.90151,"longitude":11.28108,"street":"Weimarische Str.","zip":"99438"},"brand":"24 TOTAL","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":true,"distance":151.15,"garage":true,"id":105438949,"phone":"036458 42107","price":"1.098","priceValidFrom":"2012-08-07T16:05:13+02:00","shop":false}')));
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":25,"address":{"address":"Prinzenstr. 29, 10969 Berlin","city":"Berlin","houseNumber":"29","latitude":52.50179,"longitude":13.40936,"street":"Prinzenstr.","zip":"10969"},"brand":"25 ARAL","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":true,"distance":351.62,"garage":true,"id":105692108,"phone":"030 6149860 ","price":"1.1","priceValidFrom":"2012-08-29T12:46:30+02:00","shop":true}')));
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":26,"address":{"address":"Eiffestr. 506, 20537 Hamburg","city":"Hamburg","houseNumber":"506","latitude":53.55196,"longitude":10.04999,"street":"Eiffestr.","zip":"20537"},"brand":"26 ARAL","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":true,"distance":456.75,"garage":true,"id":105695635,"phone":"040 213820 ","price":"1.1","priceValidFrom":"2012-08-29T11:02:25+02:00","shop":true}')));
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":27,"address":{"address":"Aldinger Str 74, 70378 Stuttgart","city":"Stuttgart","houseNumber":"74","latitude":48.84056,"longitude":9.22871,"street":"Aldinger Str","zip":"70378"},"brand":"27 OMV","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":false,"distance":188.5,"garage":true,"id":105696637,"phone":"0711 3652812 ","price":"1.268","priceValidFrom":"2012-08-27T14:32:10+02:00","shop":false}')));
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":28,"address":{"address":" ,    ","city":" ","houseNumber":"","latitude":47.29849,"longitude":7.93454,"street":" ","zip":" "},"brand":"28 BP","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":false,"distance":367.87,"garage":true,"id":105439038,"phone":"","price":"1.406","priceValidFrom":"2012-07-10T10:05:30+02:00","shop":false}')));
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":29,"address":{"address":"Hedelfinger Stra\u00dfe 25 A, 70327 Stuttgart","city":"Stuttgart","houseNumber":"25 A","latitude":48.76886,"longitude":9.24786,"street":"Hedelfinger Stra\u00dfe","zip":"70327"},"brand":"29 ARAL","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":false,"distance":190.87,"garage":true,"id":105695809,"phone":"0711 4077791 ","price":"1.999","priceValidFrom":"2012-08-29T10:56:33+02:00","shop":true}')));
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":30,"address":{"address":"Vor dem Schlesischen Tor 2, 10997 Berlin","city":"Berlin","houseNumber":"2","latitude":52.49666,"longitude":13.45003,"street":"Vor dem Schlesischen Tor","zip":"10997"},"brand":"30 ARAL","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":true,"distance":352.14,"garage":true,"id":105697658,"phone":"030 61286590","price":"2.1","priceValidFrom":"2012-08-29T12:48:21+02:00","shop":true}')));
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":31,"address":{"address":"Augsburger Str. 704, 70329 Stuttgart","city":"Stuttgart","houseNumber":"704","latitude":48.76139,"longitude":9.27076,"street":"Augsburger Str.","zip":"70329"},"brand":"31 ARAL","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":false,"distance":189.79,"garage":true,"id":105696876,"phone":"0711 3201641","price":"0.222","priceValidFrom":"2012-08-29T10:31:17+02:00","shop":true}')));
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":32,"address":{"address":"Augsburger Str. 704, 70329 Stuttgart","city":"Stuttgart","houseNumber":"704","latitude":48.76139,"longitude":9.27076,"street":"Augsburger Str.","zip":"70329"},"brand":"32 ARAL","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":false,"distance":189.79,"garage":true,"id":105696876,"phone":"0711 3201641","price":"0.222","priceValidFrom":"2012-08-29T10:31:17+02:00","shop":true}')));
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":33,"address":{"address":"Waiblinger Str. 23-25, 70372 Stuttgart","city":"Stuttgart","houseNumber":"23-25","latitude":48.80419,"longitude":9.221,"street":"Waiblinger Str.","zip":"70372"},"brand":"33 ARAL","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":true,"distance":190.8,"garage":true,"id":105693596,"phone":"0711 9561039 ","price":"0.222","priceValidFrom":"2012-08-29T10:25:34+02:00","shop":true}')));
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":34,"address":{"address":"Weimarische Str. 29, 99438 Bad Berka","city":"Bad Berka","houseNumber":"29","latitude":50.90151,"longitude":11.28108,"street":"Weimarische Str.","zip":"99438"},"brand":"34 TOTAL","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":true,"distance":151.15,"garage":true,"id":105438949,"phone":"036458 42107","price":"1.098","priceValidFrom":"2012-08-07T16:05:13+02:00","shop":false}')));
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":35,"address":{"address":"Prinzenstr. 29, 10969 Berlin","city":"Berlin","houseNumber":"29","latitude":52.50179,"longitude":13.40936,"street":"Prinzenstr.","zip":"10969"},"brand":"35 ARAL","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":true,"distance":351.62,"garage":true,"id":105692108,"phone":"030 6149860 ","price":"1.1","priceValidFrom":"2012-08-29T12:46:30+02:00","shop":true}')));
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":36,"address":{"address":"Eiffestr. 506, 20537 Hamburg","city":"Hamburg","houseNumber":"506","latitude":53.55196,"longitude":10.04999,"street":"Eiffestr.","zip":"20537"},"brand":"36 ARAL","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":true,"distance":456.75,"garage":true,"id":105695635,"phone":"040 213820 ","price":"1.1","priceValidFrom":"2012-08-29T11:02:25+02:00","shop":true}')));
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":37,"address":{"address":"Aldinger Str 74, 70378 Stuttgart","city":"Stuttgart","houseNumber":"74","latitude":48.84056,"longitude":9.22871,"street":"Aldinger Str","zip":"70378"},"brand":"37 OMV","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":false,"distance":188.5,"garage":true,"id":105696637,"phone":"0711 3652812 ","price":"1.268","priceValidFrom":"2012-08-27T14:32:10+02:00","shop":false}')));
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":38,"address":{"address":" ,    ","city":" ","houseNumber":"","latitude":47.29849,"longitude":7.93454,"street":" ","zip":" "},"brand":"38 BP","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":false,"distance":367.87,"garage":true,"id":105439038,"phone":"","price":"1.406","priceValidFrom":"2012-07-10T10:05:30+02:00","shop":false}')));
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":39,"address":{"address":"Hedelfinger Stra\u00dfe 25 A, 70327 Stuttgart","city":"Stuttgart","houseNumber":"25 A","latitude":48.76886,"longitude":9.24786,"street":"Hedelfinger Stra\u00dfe","zip":"70327"},"brand":"39 ARAL","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":false,"distance":190.87,"garage":true,"id":105695809,"phone":"0711 4077791 ","price":"1.999","priceValidFrom":"2012-08-29T10:56:33+02:00","shop":true}')));
				fillingStations.push(new FillingStationVO(JSON.parse('{"id":40,"address":{"address":"Vor dem Schlesischen Tor 2, 10997 Berlin","city":"Berlin","houseNumber":"2","latitude":52.49666,"longitude":13.45003,"street":"Vor dem Schlesischen Tor","zip":"10997"},"brand":"40 ARAL","businessHoursFormatted":["Werktags: 00:00 - 24:00","Samstag: 00:00 - 24:00","Sonntag: 00:00 - 24:00"],"carWash":true,"distance":352.14,"garage":true,"id":105697658,"phone":"030 61286590","price":"2.1","priceValidFrom":"2012-08-29T12:48:21+02:00","shop":true}')));
			}

			model.searchResult = fillingStations;
		}
	}

	/**
	 * Ask the Service for the existing Fuel Types
	 *
	 * If successful it fills the FillingStationModel.fuelTypes  with the Result an disptach an FuelTypesLoaded Signal.
	 * If unseccessful it fills the FuelTypesLoadFault with the error Message
	 *
	 * The Method only ask the Webservice for the Fueltypes if they are not on Memory.
	 *
	 * @param auth The Authentification for the Webservice
	 */
	public function getFuelTypes(auth:ServiceAuthVO):void {
		setTimeout(endServiceCall, 125);

		function endServiceCall():void {
			var fuelTypes:Vector.<FuelTypeVO> = new Vector.<FuelTypeVO>();
			fuelTypes.push(new FuelTypeVO(JSON.parse('{"id": 1, "fuelType": "Diesel"}')));
			fuelTypes.push(new FuelTypeVO(JSON.parse('{"id": 2, "fuelType": "Super E10"}')));
			fuelTypes.push(new FuelTypeVO(JSON.parse('{"id": 3, "fuelType": "Super Plus"}')));
			fuelTypes.push(new FuelTypeVO(JSON.parse('{"id": 4, "fuelType": "Super E5"}')));
			fuelTypes.push(new FuelTypeVO(JSON.parse('{"id": 5, "fuelType": "Autogas LPG"}')));
			fuelTypes.push(new FuelTypeVO(JSON.parse('{"id": 6, "fuelType": "Erdgas CNG"}')));
			fuelTypes.push(new FuelTypeVO(JSON.parse('{"id": 7, "fuelType": "Biodiesel"}')));

			model.fuelTypes = fuelTypes;
		}
	}
}
}
