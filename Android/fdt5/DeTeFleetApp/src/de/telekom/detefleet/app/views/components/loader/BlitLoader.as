package de.telekom.detefleet.app.views.components.loader
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	
	import de.aoemedia.animation.BlitSprite;
	import de.aoemedia.animation.IJuggable;
	import de.telekom.detefleet.app.assets.loader.R;
	
	public class BlitLoader extends Sprite implements IJuggable
	{
		private var canvas:BitmapData;
		private var loader:BlitSprite;
		public function BlitLoader()
		{
			super();
			init ();
		}
		
		protected function init ( ):void
		{
			canvas = new BitmapData(R._0.width, R._0.height, true, 0x00000000);
			addChild(new Bitmap(canvas));
			
			loader = new BlitSprite(R.sheet, 
				[
					R._0,
					R._1,
					R._2,
					R._3,
					R._4,
					R._5,
					R._6,
					R._7,
					R._8,
					R._9,
					R._10,
					R._11,
					R._12,
					R._13,
					R._14,
					R._15,
					R._16,
					R._17,
					R._18,
					R._19,
					R._20,
					R._21,
					R._22,
					R._23,
					R._24,
					R._25,
					R._26,
					R._27,
					R._28,
					R._29,
					R._30,
					R._31,
					R._32,
					R._33,
					R._34,
					R._35,
					R._36,
					R._37,
					R._38,
					R._39,
					R._40,
					R._41,
					R._42,
					R._43,
					R._44,
					R._45,
					R._46,
					R._47,
					R._48,
					R._49,
					R._50,
					R._51,
					R._52,
					R._53,
					R._54,
					R._55,
					R._56,
					R._57,
					R._58,
					R._59,
					R._60
				], 
				canvas);
		}
		
		public function advanceTime ( deltaTime:Number ):void
		{
			loader.advanceTime(deltaTime);
		}
	}
}