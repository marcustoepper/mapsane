package de.telekom.detefleet.app.events.view
{
	import org.osflash.signals.Signal;
	
	public class OverlayHidden extends Signal
	{
		public function OverlayHidden()
		{
			super();
		}
	}
}