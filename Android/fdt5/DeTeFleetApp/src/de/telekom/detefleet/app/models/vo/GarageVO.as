package de.telekom.detefleet.app.models.vo
{
/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class GarageVO
{
	public var address:AddressVO;
	public var distance:Number;
	public var id:Number;
	public var name:String;
	public var supportedBrands:Vector.<CarBrandVO>;
	public var type:int;
	public var phone:String;

	public function GarageVO(object:Object = null):void
	{
		if (object == null) {
			return;
		}

		address = new AddressVO(object.address);
		distance = object.distance ;
		id = object.id ;
		name = object.name ;
		supportedBrands = new Vector.<CarBrandVO>();
		if(object.supportedBrands){
			for (var i:int = 0; i < object.supportedBrands.length; i++) {
				supportedBrands[i] = new CarBrandVO(object.supportedBrands[i]);
			}
		}
		phone = object.phone;
		type = object.type;
	}
}
}
