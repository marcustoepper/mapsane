package de.telekom.detefleet.app.views.components.listItem {
import com.kaizimmer.ui.Screen;
import com.thanksmister.touchlist.renderers.ITouchListItemRenderer;

import de.aoemedia.mobile.ui.MultiDpiHelper;
import de.aoemedia.mobile.ui.components.buttons.AbstractButton;
import de.aoemedia.utils.TextFieldUtil;
import de.telekom.detefleet.app.assets.TextFormatTemplate;
import de.telekom.detefleet.app.models.vo.RepairRequestVO;
import de.telekom.detefleet.app.views.components.buttons.DefaultLabelButton;

import flash.display.DisplayObject;
import flash.events.MouseEvent;
import flash.globalization.DateTimeFormatter;
import flash.globalization.LocaleID;
import flash.text.TextField;
import flash.text.TextFormat;

import org.osflash.signals.Signal;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class RepairRequestListStamp extends ListItemButton implements ITouchListItemRenderer {
    protected var _leftPadding:uint;
    protected var _rightPadding:uint;

    public var resendClicked:Signal;

    private static const gap:Number = 3;

    protected var titleTextField:TextField;
    protected var dateTextField:TextField;

    protected var resendButton:DefaultLabelButton;

    protected static var dateFormat:TextFormat;
    protected static var dateTimeFormatter:DateTimeFormatter;

    protected var childsCreated:Boolean = false;

    private var _data:RepairRequestVO;

    /**
     * Construct
     */
    public function RepairRequestListStamp() {

        if (dateTimeFormatter == null) {
            dateTimeFormatter = new DateTimeFormatter(LocaleID.DEFAULT);
            dateTimeFormatter.setDateTimePattern("dd. MMMM yyyy, HH:mm");
        }

        resendClicked = new Signal(RepairRequestVO);
        _leftPadding = MultiDpiHelper.scale(12);
        _rightPadding = MultiDpiHelper.scale(12);

        _itemHeight = MultiDpiHelper.scale(67);

        addEventListener(MouseEvent.MOUSE_DOWN, pressHandler);

        createChildren();
        invalidate();
    }

    /**
     *
     */
    protected function createChildren():void {

        titleTextField = new TextField();
        titleTextField.mouseEnabled = false;
        titleTextField.defaultTextFormat = TextFormatTemplate.getInstance().listItemTitle;
        titleTextField.embedFonts = true;
        addChild(titleTextField);

        dateFormat = TextFormatTemplate.getInstance().listItemDate;
        dateTextField = new TextField();
        dateTextField.mouseEnabled = false;
        dateTextField.defaultTextFormat = dateFormat;
        dateTextField.embedFonts = true;
        addChild(dateTextField);

        resendButton = new DefaultLabelButton("Erneut senden");
        resendButton.setSize(80, 25);
        resendButton.textFormat = dateFormat;
        resendButton.signalClicked.add(resendClickedProxy);
        addChild(DisplayObject(resendButton));

        childsCreated = true;

        drawLine();
    }

    /**
     *
     */
    protected function invalidate():void {
        if (!childsCreated) {
            return;
        }

        titleTextField.height = titleTextField.textHeight + MultiDpiHelper.scale(4);
        dateTextField.height = dateTextField.textHeight + MultiDpiHelper.scale(4);

        resendButton.x = Screen.stage.stageWidth - resendButton.width - _rightPadding;
        resendButton.y = itemHeight - resendButton.height >> 1;

        titleTextField.x = _leftPadding;
        dateTextField.x = _leftPadding;

        titleTextField.width = resendButton.x - _rightPadding - titleTextField.x;
        dateTextField.width = resendButton.x - _rightPadding - dateTextField.x;

        dateTextField.y = itemHeight - (dateTextField.height + titleTextField.height + MultiDpiHelper.scale(gap)) >> 1;
        titleTextField.y = dateTextField.y + dateTextField.height + MultiDpiHelper.scale(gap);
    }

    private function resendClickedProxy(button:AbstractButton):void {
        resendClicked.dispatch(_data);
    }

    override public function set data(data:Object):void {
        if (_data == data) {
            return;
        }
        _data = data as RepairRequestVO;

        var title:String = _data.garage.name + ", " + _data.vehicleServiceCase.serviceCase;
        TextFieldUtil.fillText(titleTextField, title);

        dateTextField.text = dateTimeFormatter.format(_data.createDate);

        invalidate();
    }

    override public function get data():Object {
        return _data;
    }

    /**
     * makes the Item selectable
     * @param value
     */
    override public function selectable(value:Boolean):void {
        if (value) {
            addEventListener(MouseEvent.MOUSE_UP, selectHandler);
        } else {
            removeEventListener(MouseEvent.MOUSE_UP, selectHandler);
        }
    }

    /**
     * Dispatched when item is first pressed on tap or mouse down.
     */
    protected function pressHandler(event:MouseEvent):void {
        _itemPressedCallback(this);
    }

    /**
     * Dispatched when item is selected, usually on touch end or mouse up.
     */
    protected function selectHandler(event:MouseEvent):void {
        _itemSelectedCallback(this);
    }

    override public function destroy():void {
        removeEventListener(MouseEvent.MOUSE_UP, selectHandler);

        _itemPressedCallback = null;
        _itemSelectedCallback = null;

        this.graphics.clear();
        unselect();
        _data = null;
    }

}
}
