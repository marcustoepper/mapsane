package de.telekom.detefleet.app.controller.view
{
import de.telekom.detefleet.app.models.ViewsModel;

import org.robotlegs.mvcs.SignalCommand;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class RemoveViewContextCMD extends SignalCommand
{
	[Inject]
	public var model:ViewsModel;

	[Inject]
	public var context:String;

	public override function execute():void
	{
		model.removeContext(context);
	}
}
}
