package de.telekom.detefleet.app.views.components.buttons
{
import de.aoemedia.mobile.ui.components.skins.ButtonBitmaps;
import de.telekom.detefleet.app.assets.ScaleBitmaps;
import de.telekom.detefleet.app.assets.TextFormatTemplate;

import flash.text.TextFormat;

/**
 * The Purple Button
 * 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class ActionButton extends DefaultLabelButton
{
	protected static var templateTF:TextFormat;


	public function ActionButton(label:String = "")
	{
		var sBmps:ScaleBitmaps = ScaleBitmaps.getInstance();
		var bmps:ButtonBitmaps = new ButtonBitmaps(sBmps.actionButton, sBmps.actionButtonDown, sBmps.actionButton, sBmps.actionButtonDown);
		super(label, bmps);
	}

	override protected function createChildren():void
	{
		super.createChildren();
		stopInvalidation = true;
		templateTF = TextFormatTemplate.getInstance().actionbarButton;
		templateTF.color = 0xffffff;
		textFormat = templateTF;
		labelColor = 0xffffff;
        labelColorDown = 0xffffff;
		stopInvalidation = false;
	}
}
}
