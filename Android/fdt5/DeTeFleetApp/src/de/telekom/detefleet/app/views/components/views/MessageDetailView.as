package de.telekom.detefleet.app.views.components.views {
import com.kaizimmer.ui.Screen;

import de.aoemedia.mobile.ui.components.LabelComponent;
import de.aoemedia.mobile.ui.components.container.LayoutContainer;
import de.telekom.detefleet.app.assets.TextFormatTemplate;
import de.telekom.detefleet.app.models.vo.MessageVO;

import flash.globalization.DateTimeFormatter;
import flash.globalization.LocaleID;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class MessageDetailView extends ScrollableNavigatorBarView {
    protected static var dateTimeFormatter:DateTimeFormatter;

    protected var _message:MessageVO;

    public var headlineLabel:LabelComponent;
    public var address:LabelComponent;

    protected var siteLayout:LayoutContainer;
    private var textLabel:LabelComponent;
    private var dateLabel:LabelComponent;

    /**
     * Construct
     */
    public function MessageDetailView():void {
        super("Meldungen");

        if (dateTimeFormatter == null) {
            dateTimeFormatter = new DateTimeFormatter(LocaleID.DEFAULT);
            dateTimeFormatter.setDateTimePattern("dd. MMMM yyyy, HH:mm");
        }

        paddingBottom = 11;
        paddingRight = 11;
    }

    /**
     *
     */
    public function set message(value:MessageVO):void {
        if (_message == value) {
            return;
        }

        _message = value;

        headlineLabel.label = value.title;
        dateLabel.label = dateTimeFormatter.format(value.date);
        textLabel.label = value.message;

        siteLayout.invalidate();

        invalidate();

        gotoStart();
    }

    /**
     *
     */
    public function get message():MessageVO {
        return _message;
    }

    override protected function createChildren():void {
        super.createChildren();

        var paddingLeft:Number = 11;
        var paddingRight:Number = 11;

        siteLayout = new LayoutContainer(this);
        siteLayout.stopInvalidation = true;
        siteLayout.y = navbar.height;
        siteLayout.explicitWidth = Screen.stage.stageWidth;
        siteLayout.gap = 4;
        siteLayout.paddingTop = 11;
        siteLayout.paddingLeft = 11;
        siteLayout.paddingRight = 11;
        siteLayout.paddingBottom = 11;

        dateLabel = new LabelComponent();
        dateLabel.disableScrolling();
        siteLayout.addItem(dateLabel);

        headlineLabel = new LabelComponent();
        headlineLabel.disableScrolling();
        headlineLabel.textField.multiline = true;
        headlineLabel.textField.wordWrap = true;
        headlineLabel.textField.width = Screen.stage.stageWidth - scale(paddingLeft + paddingRight);
        siteLayout.addItem(headlineLabel);

        textLabel = new LabelComponent();
        textLabel.disableScrolling();
        textLabel.textField.multiline = true;
        textLabel.textField.wordWrap = true;
        textLabel.textField.width = Screen.stage.stageWidth - scale(paddingLeft + paddingRight);
        siteLayout.addItem(textLabel);

        headlineLabel.textFormat = TextFormatTemplate.getInstance().headline;
        dateLabel.textFormat = TextFormatTemplate.getInstance().listItemDate;
        textLabel.textFormat = TextFormatTemplate.getInstance().content;

        siteLayout.stopInvalidation = false;
        siteLayout.invalidate();
        siteLayout.addItemsToView();
    }

}
}
