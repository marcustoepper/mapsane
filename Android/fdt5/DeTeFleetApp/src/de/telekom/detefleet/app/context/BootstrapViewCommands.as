package de.telekom.detefleet.app.context {
import de.telekom.detefleet.app.controller.HideLoaderCMD;
import de.telekom.detefleet.app.controller.dialog.RemoveDialogCMD;
import de.telekom.detefleet.app.controller.dialog.ShowEmptyResultDialogCMD;
import de.telekom.detefleet.app.controller.dialog.ShowErrorDialogCMD;
import de.telekom.detefleet.app.controller.dialog.ShowServiceFaultDialogCMD;
import de.telekom.detefleet.app.controller.repairRequest.ShowRepairRequestSendConfirmationCMD;
import de.telekom.detefleet.app.controller.repairRequest.ShowRepairRequestSuccessfulResendConfirmationDialogCMD;
import de.telekom.detefleet.app.controller.view.AddReadyViewCMD;
import de.telekom.detefleet.app.controller.view.AddViewToHistoryCMD;
import de.telekom.detefleet.app.controller.view.BlockHistoryAdditionRequestViewCMD;
import de.telekom.detefleet.app.controller.view.ChangeCurrentViewCMD;
import de.telekom.detefleet.app.controller.view.RemoveViewsFromHistoryCMD;
import de.telekom.detefleet.app.controller.view.RequestViewCMD;
import de.telekom.detefleet.app.controller.view.ShowDashboardViewCMD;
import de.telekom.detefleet.app.controller.view.ShowLastViewCMD;
import de.telekom.detefleet.app.controller.view.ShowLoginViewCMD;
import de.telekom.detefleet.app.events.CloseDialog;
import de.telekom.detefleet.app.events.RenewSessionFault;
import de.telekom.detefleet.app.events.RepairRequestSuccessfulSend;
import de.telekom.detefleet.app.events.RestartRepairRequestSuccessfulSend;
import de.telekom.detefleet.app.events.SearchResultIsEmpty;
import de.telekom.detefleet.app.events.ServiceFaultSignal;
import de.telekom.detefleet.app.events.ValidationError;
import de.telekom.detefleet.app.events.login.LoginFault;
import de.telekom.detefleet.app.events.login.LoginSuccess;
import de.telekom.detefleet.app.events.view.BlockHistoryAdditionRequestView;
import de.telekom.detefleet.app.events.view.GoBack;
import de.telekom.detefleet.app.events.view.RemoveViewsFromHistory;
import de.telekom.detefleet.app.events.view.RequestView;
import de.telekom.detefleet.app.events.view.TransitionCompleted;
import de.telekom.detefleet.app.events.view.ViewReady;

import org.robotlegs.core.ICommandMap;
import org.robotlegs.core.ISignalCommandMap;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class BootstrapViewCommands {
    public function BootstrapViewCommands(commandMap:ICommandMap, signalCommandMap:ISignalCommandMap) {

        signalCommandMap.mapSignalClass(BlockHistoryAdditionRequestView, BlockHistoryAdditionRequestViewCMD);
        signalCommandMap.mapSignalClass(RequestView, RequestViewCMD);

        signalCommandMap.mapSignalClass(GoBack, ShowLastViewCMD);

        signalCommandMap.mapSignalClass(RenewSessionFault, ShowLoginViewCMD);
        signalCommandMap.mapSignalClass(LoginSuccess, ShowDashboardViewCMD);

        signalCommandMap.mapSignalClass(ViewReady, AddReadyViewCMD);

        signalCommandMap.mapSignalClass(TransitionCompleted, AddViewToHistoryCMD);
        signalCommandMap.mapSignalClass(TransitionCompleted, ChangeCurrentViewCMD);

        // Dialogs
        signalCommandMap.mapSignalClass(LoginFault, ShowErrorDialogCMD);
        signalCommandMap.mapSignalClass(ValidationError, ShowErrorDialogCMD);
        signalCommandMap.mapSignalClass(ServiceFaultSignal, ShowServiceFaultDialogCMD);

        signalCommandMap.mapSignalClass(RepairRequestSuccessfulSend, HideLoaderCMD);
        signalCommandMap.mapSignalClass(RepairRequestSuccessfulSend, ShowRepairRequestSendConfirmationCMD);

        signalCommandMap.mapSignalClass(RestartRepairRequestSuccessfulSend, HideLoaderCMD);
        signalCommandMap.mapSignalClass(RestartRepairRequestSuccessfulSend, ShowRepairRequestSuccessfulResendConfirmationDialogCMD);
        signalCommandMap.mapSignalClass(CloseDialog, RemoveDialogCMD);

        signalCommandMap.mapSignalClass(RemoveViewsFromHistory, RemoveViewsFromHistoryCMD);
        signalCommandMap.mapSignalClass(SearchResultIsEmpty, ShowEmptyResultDialogCMD);
    }

}
}
