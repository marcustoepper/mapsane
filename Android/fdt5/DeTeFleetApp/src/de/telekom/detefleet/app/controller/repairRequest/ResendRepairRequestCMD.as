package de.telekom.detefleet.app.controller.repairRequest
{
import de.telekom.detefleet.app.controller.AuthCommand;
import de.telekom.detefleet.app.models.vo.RepairRequestVO;
import de.telekom.detefleet.app.services.remote.IRepairRequestService;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class ResendRepairRequestCMD extends AuthCommand
{
	[Inject]
	public var service:IRepairRequestService;
	[Inject]
	public var request:RepairRequestVO;

	public override function execute():void {
		service.restartRepairRequest(auth, request.id.toString());
	}
}
}
