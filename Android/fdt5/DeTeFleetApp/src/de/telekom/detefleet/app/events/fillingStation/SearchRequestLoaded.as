package de.telekom.detefleet.app.events.fillingStation
{
import de.telekom.detefleet.app.models.vo.FillingStationRequestVO;

import org.osflash.signals.Signal;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class SearchRequestLoaded extends Signal
{
	public function SearchRequestLoaded()
	{
		super(FillingStationRequestVO);
	}
}
}
