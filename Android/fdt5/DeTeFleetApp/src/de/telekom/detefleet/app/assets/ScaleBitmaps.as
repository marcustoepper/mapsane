package de.telekom.detefleet.app.assets {
import com.kaizimmer.ui.Screen;

import de.aoemedia.mobile.ui.components.ScaleBitmapComponent;

import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.geom.Rectangle;

import org.bytearray.display.ScaleBitmap;

public class ScaleBitmaps {

    [Embed(source="../../../../../../assets/gfx/mdpi/buttons/button_beveled_m_active-magenta-a.png")]
    protected static var _button_beveled_m_action_png:Class;

    public function get actionButton():ScaleBitmap {
        return getScaleBitmapByResolution(_button_beveled_m_action_png, 4, 5, 13, 28,
                _button_beveled_m_action_png, 4, 5, 13, 28,
                _button_beveled_m_action_png, 4, 5, 13, 28);
    }

    [Embed(source="../../../../../../assets/gfx/mdpi/buttons/button_beveled_m_active-magenta-b.png")]
    protected static var _button_beveled_m_action_down_png:Class;

    public function get actionButtonDown():ScaleBitmap {
        return getScaleBitmapByResolution(_button_beveled_m_action_down_png, 4, 5, 13, 28,
                _button_beveled_m_action_down_png, 4, 5, 13, 28,
                _button_beveled_m_action_down_png, 4, 5, 13, 28);
    }

    [Embed(source="../../../../../../assets/gfx/mdpi/buttons/button_beveled_m_transparent.png")]
    protected static var _button_beveled_m_transparent_png:Class;

    public function get transparentButton():ScaleBitmap {
        return getScaleBitmapByResolution(_button_beveled_m_transparent_png, 5, 5, 29, 63,
                _button_beveled_m_transparent_png, 5, 5, 29, 63,
                _button_beveled_m_transparent_png, 5, 5, 29, 63);
    }

    [Embed(source="../../../../../../assets/gfx/mdpi/buttons/button_beveled_m_transparent_down.png")]
    protected static var _button_beveled_m_transparent_down_png:Class;

    public function get transparentButtonDown():ScaleBitmap {
        return getScaleBitmapByResolution(_button_beveled_m_transparent_down_png, 5, 5, 29, 63,
                _button_beveled_m_transparent_down_png, 5, 5, 29, 63,
                _button_beveled_m_transparent_down_png, 5, 5, 29, 63);
    }

    [Embed(source="../../../../../../assets/gfx/mdpi/icons/header-bg-mdpi.png")]
    protected static var _navigationBarBackground_mdpi:Class;
    [Embed(source="../../../../../../assets/gfx/hdpi/icons/header-bg-hdpi.png")]
    protected static var _navigationBarBackground_hdpi:Class;
    // TODO Missing xhdpi navbar background
    //[Embed(source="../../../../../../assets/gfx/xhdpi/icons/header_bg-shadow-xhdpi.png")]
    //protected static var _navigationBarBackground_xhdpi:Class;
    public function get navigationBar():ScaleBitmap {
        return getScaleBitmapByResolution(_navigationBarBackground_hdpi, 0, 0, 1, 91,
                _navigationBarBackground_hdpi, 0, 0, 1, 91,
                _navigationBarBackground_mdpi, 0, 0, 1, 61);
    }

    [Embed(source="../../../../../../assets/gfx/mdpi/icons/header_bg-shadow-mdpi.png")]
    protected static var _navigationBarShadow_mdpi:Class;
    [Embed(source="../../../../../../assets/gfx/hdpi/icons/header_bg-shadow-hdpi.png")]
    protected static var _navigationBarShadow_hdpi:Class;
    [Embed(source="../../../../../../assets/gfx/xhdpi/icons/header_bg-shadow-xhdpi.png")]
    protected static var _navigationBarShadow_xhdpi:Class;

    public function get navigationBarShadow():ScaleBitmap {
        return getScaleBitmapByResolution(_navigationBarShadow_xhdpi, 0, 0, 1, 23,
                _navigationBarShadow_hdpi, 0, 0, 1, 17,
                _navigationBarShadow_mdpi, 0, 0, 1, 12);
    }

    [Embed(source="../../../../../../assets/gfx/mdpi/buttons/home_btn_default.png")]
    protected static var _home_btn_default_png:Class;

    public function get homeButton():ScaleBitmap {
        return getScaleBitmapByResolution(_home_btn_default_png, 2, 2, 16, 16,
                _home_btn_default_png, 2, 2, 16, 16,
                _home_btn_default_png, 2, 2, 16, 16);
    }

    [Embed(source="../../../../../../assets/gfx/mdpi/buttons/home_btn_down.png")]
    protected static var _home_btn_down_png:Class;

    public function get homeButtonDown():ScaleBitmap {
        return getScaleBitmapByResolution(_home_btn_down_png, 2, 2, 16, 16,
                _home_btn_down_png, 2, 2, 16, 16,
                _home_btn_down_png, 2, 2, 16, 16);
    }

    [Embed(source="../../../../../../assets/gfx/mdpi/buttons/single_list_btn_default.png")]
    protected static var _single_list_btn_default_png:Class;

    public function get singleListButton():ScaleBitmap {
        return getScaleBitmapByResolution(_single_list_btn_default_png, 2, 2, 16, 16,
                _single_list_btn_default_png, 2, 2, 16, 16,
                _single_list_btn_default_png, 2, 2, 16, 16);
    }

    [Embed(source="../../../../../../assets/gfx/mdpi/buttons/single_list_btn_down.png")]
    protected static var _single_list_btn_down_png:Class;

    public function get singleListButtonDown():ScaleBitmap {
        return getScaleBitmapByResolution(_single_list_btn_down_png, 2, 2, 16, 16,
                _single_list_btn_down_png, 2, 2, 16, 16,
                _single_list_btn_down_png, 2, 2, 16, 16);
    }

    [Embed(source="../../../../../../assets/gfx/mdpi/buttons/select_list_item_btn.png")]
    protected static var _select_list_item_btn:Class;

    public function get selectListItemButton():ScaleBitmap {
        return getScaleBitmapByResolution(_select_list_item_btn, 2, 2, 16, 16,
                _select_list_item_btn, 2, 2, 16, 16,
                _select_list_item_btn, 2, 2, 16, 16);
    }

    [Embed(source="../../../../../../assets/gfx/mdpi/buttons/select_list_item_btn_down.png")]
    protected static var _select_list_item_btn_down:Class;

    public function get selectListItemButtonDown():ScaleBitmap {
        return getScaleBitmapByResolution(_select_list_item_btn_down, 2, 2, 16, 16,
                _select_list_item_btn_down, 2, 2, 16, 16,
                _select_list_item_btn_down, 2, 2, 16, 16);
    }

    [Embed(source="../../../../../../assets/gfx/mdpi/buttons/button_beveled_l_available.png")]
    protected static var _button_default:Class;

    public function get defaultButton():ScaleBitmap {
        return getScaleBitmapByResolution(_button_default, 4, 5, 13, 40,
                _button_default, 4, 5, 13, 40,
                _button_default, 4, 5, 13, 40);
    }

    [Embed(source="../../../../../../assets/gfx/mdpi/buttons/button_beveled_l_taking-input.png")]
    protected static var _button_default_down:Class;

    public function get defaultButtonDown():ScaleBitmap {
        return getScaleBitmapByResolution(_button_default_down, 4, 5, 13, 40,
                _button_default_down, 4, 5, 13, 40,
                _button_default_down, 4, 5, 13, 40);
    }

    [Embed(source="../../../../../../assets/gfx/mdpi/buttons/button_beveled_l_available_disabled.png")]
    protected static var _button_default_disabled:Class;

    public function get defaultButtonDisabled():ScaleBitmap {
        return getScaleBitmapByResolution(_button_default_disabled, 4, 5, 13, 40,
                _button_default_disabled, 4, 5, 13, 40,
                _button_default_disabled, 4, 5, 13, 40);
    }

    [Embed(source="../../../../../../assets/gfx/mdpi/buttons/input-field_embossed_active.png")]
    protected const input_2x:Class;

    public function get input():ScaleBitmap {
        return getScaleBitmapByResolution(input_2x, 8, 8, 3, 3,
                input_2x, 8, 8, 3, 3,
                input_2x, 8, 8, 3, 3);
    }

    [Embed(source="../../../../../../assets/gfx/mdpi/buttons/input-field_embossed_unavailable.png")]
    protected const input_unavailable_2x:Class;

    public function get inputDisabled():ScaleBitmap {
        return getScaleBitmapByResolution(input_unavailable_2x, 8, 8, 3, 3,
                input_unavailable_2x, 8, 8, 3, 3,
                input_unavailable_2x, 8, 8, 3, 3);
    }

    [Embed(source="../../../../../../assets/gfx/mdpi/buttons/input-field_embossed_taking_input.png")]
    protected const input_taking_input_2x:Class;

    public function get inputTakingInput():ScaleBitmap {
        return getScaleBitmapByResolution(input_taking_input_2x, 8, 8, 3, 3,
                input_taking_input_2x, 8, 8, 3, 3,
                input_taking_input_2x, 8, 8, 3, 3);
    }

    [Embed(source="../../../../../../assets/gfx/mdpi/buttons/input-field_embossed_validating_input_n.png")]
    protected const input_error_2x:Class;

    public function get inputError():ScaleBitmap {
        return getScaleBitmapByResolution(input_error_2x, 8, 8, 3, 3,
                input_error_2x, 8, 8, 3, 3,
                input_error_2x, 8, 8, 3, 3);
    }

    [Embed(source="../../../../../../assets/gfx/mdpi/buttons/input-field_embossed_validating_input_p.png")]
    protected const input_success_2x:Class;

    public function get inputSuccess():ScaleBitmap {
        return getScaleBitmapByResolution(input_success_2x, 8, 8, 3, 3,
                input_success_2x, 8, 8, 3, 3,
                input_success_2x, 8, 8, 3, 3);
    }

    // dashboard info box bg
    [Embed(source="../../../../../../assets/gfx/mdpi/windows/message-bg.png")]
    protected const dashboardInfoBox_1x:Class;

    public function get dashboardInfoBox():ScaleBitmap {
        return getScaleBitmapByResolution(dashboardInfoBox_1x, 0, 0, 1, 152,
                dashboardInfoBox_1x, 0, 0, 1, 152,
                dashboardInfoBox_1x, 0, 0, 1, 152);
    }

    [Embed(source="../../../../../../assets/gfx/mdpi/icons/notif-window-mdpi.png")]
    protected const dialogWindow_mdpi:Class;
    [Embed(source="../../../../../../assets/gfx/hdpi/icons/notif-window-hdpi.png")]
    protected const dialogWindow_hdpi:Class;
    [Embed(source="../../../../../../assets/gfx/xhdpi/icons/notif-window-xhdpi.png")]
    protected const dialogWindow_xhdpi:Class;

    /**
     * Dialog Window Background
     */
    public function get dialogWindow():ScaleBitmap {
        return getScaleBitmapByResolution(dialogWindow_xhdpi, 24, 16, 40, 332,
                dialogWindow_hdpi, 12, 12, 12, 122,
                dialogWindow_mdpi, 12, 8, 20, 166);
    }

    [Embed(source="../../../../../../assets/gfx/mdpi/stage/stage_single-color.png")]
    protected static var _stageBG_m:Class;
    [Embed(source="../../../../../../assets/gfx/xhdpi/stage/stage_single-color@2.png")]
    protected static var _stageBG_xh:Class;

    public function get stageBG():ScaleBitmap {
        return getScaleBitmapByResolution(_stageBG_xh, 29, 22, 2, 48,
                _stageBG_m, 15, 12, 1, 53,
                _stageBG_m, 15, 12, 1, 53);
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    protected static var _instance:ScaleBitmaps;

    public function ScaleBitmaps(sE:SingletonEnforcer) {
        //
    }

    public static function getInstance():ScaleBitmaps {
        if (_instance == null) {
            _instance = new ScaleBitmaps(new SingletonEnforcer());
        }
        return _instance;
    }

    /**
     *
     */
    protected function calcSBMP(bitmapData:BitmapData, rectX:uint, rectY:uint, rectWidth:uint, rectHeight:uint):ScaleBitmapComponent {
        var s9BMP:ScaleBitmapComponent = new ScaleBitmapComponent(bitmapData);
        s9BMP.scale9Grid = new Rectangle(rectX, rectY, rectWidth, rectHeight);
        return s9BMP;
    }

    /**
     *
     */
    protected function getScaleBitmapByResolution(xhdpi:Class, xhRectX:uint, xhRectY:uint, xhRectWidth:uint, xhRectHeight:uint, hdpi:Class, hRectX:uint, hRectY:uint, hRectWidth:uint, hRectHeight:uint, mdpi:Class, mRectX:uint, mRectY:uint, mRectWidth:uint, mRectHeight:uint):ScaleBitmapComponent {
        var dpiClass:uint = Screen.dpiClass;
        switch (dpiClass) {
            case Screen.AT2X:
            case Screen.XHDPI:
                var xhdpiBitmap:Bitmap = new xhdpi();
                var xhdpiComponent:ScaleBitmapComponent
                        = calcSBMP(xhdpiBitmap.bitmapData, xhRectX, xhRectY, xhRectWidth, xhRectHeight);
                return xhdpiComponent;
                break;
            case Screen.HDPI:
                var hdpiBitmap:Bitmap = new hdpi();
                var hdpiComponent:ScaleBitmapComponent
                        = calcSBMP(hdpiBitmap.bitmapData, hRectX, hRectY, hRectWidth, hRectHeight);
                return hdpiComponent;
                break;
            case Screen.MDPI:
            case Screen.AT1X:
            case Screen.LDPI:
            default:
                var mdpiBitmap:Bitmap = new mdpi();
                var mdpiComponent:ScaleBitmapComponent
                        = calcSBMP(mdpiBitmap.bitmapData, mRectX, mRectY, mRectWidth, mRectHeight);
                return mdpiComponent;
                break;
        }
    }

}
}

internal class SingletonEnforcer {
}