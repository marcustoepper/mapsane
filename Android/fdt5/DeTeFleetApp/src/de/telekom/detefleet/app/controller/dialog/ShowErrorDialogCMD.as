package de.telekom.detefleet.app.controller.dialog
{
import de.telekom.detefleet.app.models.DialogModel;
import de.telekom.detefleet.app.models.LayoutData;
import de.telekom.detefleet.app.views.components.dialog.ErrorDialogView;

import org.robotlegs.mvcs.Command;

/**
 * ...
 * @author dev.mode
 */
public class ShowErrorDialogCMD extends Command
{

	[Inject]
	public var model:DialogModel;

	[Inject]
	public var layout:LayoutData;

	[Inject]
	public var message:String;

	public override function execute():void {
		var dialog:ErrorDialogView = new ErrorDialogView(layout.viewport, message);
		model.addDialog(dialog);
	}
}

}
