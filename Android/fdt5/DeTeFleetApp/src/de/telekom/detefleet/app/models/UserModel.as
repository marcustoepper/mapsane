package de.telekom.detefleet.app.models
{
import de.telekom.detefleet.app.models.vo.VehicleVO;
import de.telekom.detefleet.app.models.vo.PersonVO;
import de.telekom.detefleet.app.services.persistence.IKeyChainService;

import org.robotlegs.mvcs.Actor;

/**
 * Store data of the loged in User
 * 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class UserModel extends Actor
{
	protected var _userData:PersonVO;
	protected var _vehicleData:VehicleVO;

	/**
	 * Indicated if the Auth Token should be stored
	 */
	public var storeAuthToken:Boolean = false;

	protected var _authToken:String;

	/**
	 * remove all data from Memory
	 */
	public function cleanData():void{
		_userData = null;
		_authToken = null;
	}

	/**
	 * The Authorization key for Service Requests
	 */
	public function set authToken(value:String):void {

		_authToken = value;
	}

	public function get authToken():String {
		return _authToken;
	}

	/**
	 * The User
	 */
	public function get userData():PersonVO {
		return _userData;
	}

	public function set userData(userData:PersonVO):void {
		this._userData = userData;
	}

	public function get vehicleData():VehicleVO {
		return _vehicleData;
	}

	public function set vehicleData(vehicleData:VehicleVO):void {
		_vehicleData = vehicleData;
	}
}
}
