package de.telekom.detefleet.app.models.vo
{

/**
 * Represents a Fuel Type. E.g. Diesel or Super
 * 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class FuelTypeVO
{
	public var id:int;
	public var fuelType:String;

	/**
	 * Construct
	 * 
	 * @param object Anonymous Object which data will be set the properties of this object 
	 */
	public function FuelTypeVO(object:Object = null)
	{
		if (object == null) {
			return;
		}

		id = object.id as int;
		fuelType = object.fuelType;
	}
}
}