package de.telekom.detefleet.app.models.vo
{
import de.telekom.detefleet.app.models.JSONable;

/**
 * Request data for Login
 * 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class LoginRequestVO implements JSONable
{
	public var name:String;
	public var password:String;
	public var deviceKey:String;

	/**
	 * Convert Object to JSON String
	 * 
	 * @return JSON string
	 */
	public function toJSON():String
	{
		return '{"loginRequest":{"userName": "' + name + '","password": "' + password + '"},"key": "' + deviceKey + '"}';
	}
}
}