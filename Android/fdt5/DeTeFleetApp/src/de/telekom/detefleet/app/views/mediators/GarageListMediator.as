package de.telekom.detefleet.app.views.mediators
{
import de.aoemedia.motion.TransitionType;
import de.telekom.detefleet.app.events.ChoosenVehicleServiceCaseChanged;
import de.telekom.detefleet.app.events.GeolocationStatusUpdated;
import de.telekom.detefleet.app.events.SearchResultIsEmpty;
import de.telekom.detefleet.app.events.ViewContextChanged;
import de.telekom.detefleet.app.events.garage.GarageSelected;
import de.telekom.detefleet.app.events.garage.GetLastGarageSearchRequest;
import de.telekom.detefleet.app.events.garage.GetVehicleServiceCases;
import de.telekom.detefleet.app.events.garage.LastGarageRequestLoaded;
import de.telekom.detefleet.app.events.garage.SearchGarages;
import de.telekom.detefleet.app.events.garage.SearchResultChanged;
import de.telekom.detefleet.app.events.garage.VehicleServiceCasesChanged;
import de.telekom.detefleet.app.events.loader.HideLoader;
import de.telekom.detefleet.app.events.loader.ShowLoader;
import de.telekom.detefleet.app.events.login.Logout;
import de.telekom.detefleet.app.events.view.BlockHistoryAdditionRequestView;
import de.telekom.detefleet.app.events.view.RequestView;
import de.telekom.detefleet.app.events.view.TransitionCompleted;
import de.telekom.detefleet.app.events.view.ViewReady;
import de.telekom.detefleet.app.models.GarageModel;
import de.telekom.detefleet.app.models.GeolocationModel;
import de.telekom.detefleet.app.models.RepairRequestModel;
import de.telekom.detefleet.app.models.ViewsModel;
import de.telekom.detefleet.app.models.vo.GarageSearchRequestVO;
import de.telekom.detefleet.app.models.vo.GarageVO;
import de.telekom.detefleet.app.models.vo.TransitionStateVO;
import de.telekom.detefleet.app.models.vo.VehicleServiceCaseVO;
import de.telekom.detefleet.app.models.vo.ViewRequestVO;
import de.telekom.detefleet.app.views.components.views.GarageDetailView;
import de.telekom.detefleet.app.views.components.views.GarageListView;
import de.telekom.detefleet.app.views.components.views.GarageMapView;

import flash.sensors.Geolocation;

import org.as3commons.logging.api.ILogger;
import org.as3commons.logging.api.getLogger;
import org.robotlegs.mvcs.Mediator;

/**
 *
 */
public class GarageListMediator extends Mediator
{
	/**
	 * SIGNALS
	 */
	[Inject]
	public var serviceCasesChanged:VehicleServiceCasesChanged;

	[Inject]
	public var searchResultChanged:SearchResultChanged;
	[Inject]
	public var getVehicleServiceCases:GetVehicleServiceCases;
	[Inject]
	public var getLastSearchRequest:GetLastGarageSearchRequest;
	[Inject]
	public var searchGarages:SearchGarages;
	[Inject]
	public var lastGarageSearchRequestLoaded:LastGarageRequestLoaded;
	[Inject]
	public var garageSelected:GarageSelected;
	[Inject]
	public var choosenVehicleServiceCaseChanged:ChoosenVehicleServiceCaseChanged;
	[Inject]
	public var transitionCompleted:TransitionCompleted;
	[Inject]
	public var geolocationStatusUpdated:GeolocationStatusUpdated;

	[Inject]
	public var searchResultIsEmpty:SearchResultIsEmpty;
	[Inject]
	public var viewContextChanged:ViewContextChanged;

	[Inject]
	public var viewReady:ViewReady;
	[Inject]
	public var logout:Logout;

	[Inject]
	public var signaleShowLoader:ShowLoader;
	[Inject]
	public var signalHideLoader:HideLoader;
	/**
	 * Model
	 */
	[Inject]
	public var model:GarageModel;
	[Inject]
	public var geolocationModel:GeolocationModel;

	[Inject]
	public var viewModel:ViewsModel;
	/**
	 * VIEW
	 */
	[Inject]
	public var view:GarageListView;

	[Inject]
	public var gotoMapView:BlockHistoryAdditionRequestView;

	[Inject]
	public var gotoView:RequestView;

	protected var latitude:Number;
	protected var longitude:Number;
	private static const logger:ILogger = getLogger(GarageListMediator);

	/**
	 *
	 *
	 */
	public override function onRegister():void {
		// Add Listener
		searchResultChanged.add(populateGarageList);
		serviceCasesChanged.add(populateVehicleServiceCases);
		transitionCompleted.add(showFilter);
		logout.add(handleLogout);
		viewContextChanged.add(handleViewContextChange);

		choosenVehicleServiceCaseChanged.add(handleChoosenVehicleServiceCaseChanged);

		view.signalToggleMapButtonClicked.add(toggleMap);

		view.searchButtonClickedCallback = sendSearchRequest;
		view.listItemClickedCallback = listItemClicked;

		geolocationStatusUpdated.add(geoStatusHandler);

        geolocationModel.start();

		if(model.vehicleServiceCases) {
			populateVehicleServiceCases(model.vehicleServiceCases);
		} else {
			getVehicleServiceCases.dispatch();
		}

		// Find actual Location
		if(!Geolocation.isSupported || geolocationModel.geolocation.muted) {
			view.setDefaultLocationText("");
		}
	}

	private function handleViewContextChange(context:String):void {
		view.vehicleServiceCaseSelectBoxBtn.enabled = context != RepairRequestModel.CONTEXT;
	}

	private function handleChoosenVehicleServiceCaseChanged(choosenVehicleServiceCase:VehicleServiceCaseVO):void {
		view.setSelectedVehicleServiceCase(choosenVehicleServiceCase);
	}

	protected function showFilter(transitionState:TransitionStateVO):void {
		if(transitionState.viewToShow == view) {
			if(model.searchResult == null || model.searchResult.length == 0) {
				view.openFilter();
			}
		}
        if(transitionState.viewToHide == view) {
            view.unselectAll();
        }

	}

	protected function listItemClicked(garage:GarageVO):void {
		gotoView.dispatch(new ViewRequestVO(GarageDetailView));
		garageSelected.dispatch(garage);
	}

	/**
	 *
	 */
	protected function toggleMap():void {
		gotoMapView.dispatch(new ViewRequestVO(GarageMapView, TransitionType.SLIDE_UP));
	}

	/**
	 *
	 */
	private function handleLogout():void {
		view.resetForm();
	}

	/**
	 *
	 * @param event
	 *
	 */
	protected function geoStatusHandler():void {
		if(geolocationModel.geolocation.muted) {
			view.setDefaultLocationText("");
		} else {
			view.setDefaultLocationText(view.DEFAULT_LOCATION_TEXT);
		}
	}

	/**
	 *
	 *
	 */
	private function sendSearchRequest(request:GarageSearchRequestVO):void {
		view.clear();
		if((request.location.address == null || request.location.address == "" ) && Geolocation.isSupported && !geolocationModel.geolocation.muted) {
			request.location.latitude = geolocationModel.latitude;
			request.location.longitude = geolocationModel.longitude;
		}
		signaleShowLoader.dispatch();
		searchGarages.dispatch(request);
	}

	/**
	 *
	 * @param stations
	 *
	 */
	private function populateGarageList(garages:Vector.<GarageVO>):void {
		if(garages != null && garages.length == 0) {
			searchResultIsEmpty.dispatch();
		}
		view.populateGarages(garages);
		signalHideLoader.dispatch();
	}

	/**
	 * Fill the Fuel Types Drow Down with Data
	 *
	 * @param cases
	 */
	private function populateVehicleServiceCases(cases:Vector.<VehicleServiceCaseVO>):void {
		view.setVehicleServiceCaseDataProvider(cases);

		if(model.choosenVehicleServiceCase) {
			view.setSelectedVehicleServiceCase(model.choosenVehicleServiceCase);
		} else {
			view.setSelectedVehicleServiceCase(cases[0]);
		}

		view.vehicleServiceCaseSelectBoxBtn.enabled = viewModel.context != RepairRequestModel.CONTEXT;

		viewReady.dispatch(view);
	}

	override public function preRemove():void {
		serviceCasesChanged.remove(populateVehicleServiceCases);
		searchResultChanged.remove(populateGarageList);

		view.signalToggleMapButtonClicked.remove(toggleMap);

		super.preRemove();
	}
}
}
