package de.telekom.detefleet.app.views.components.views
{
import com.kaizimmer.ui.Screen;
import com.thanksmister.touchlist.renderers.ITouchListItemRenderer;

import de.aoemedia.mobile.ui.components.buttons.AbstractButton;
import de.telekom.detefleet.app.models.vo.DashboardButtonVO;
import de.telekom.detefleet.app.views.components.buttons.actionBar.ImprintButton;
import de.telekom.detefleet.app.views.components.listItem.DashboardButton;
import de.telekom.detefleet.app.views.components.listItem.DashboardButton;
import de.telekom.detefleet.app.views.components.list.DashboardList;
import de.telekom.detefleet.app.views.components.list.ListComponent;

import flash.display.DisplayObject;
import flash.events.Event;
import flash.net.URLRequest;
import flash.net.navigateToURL;
import flash.utils.setTimeout;

import org.osflash.signals.Signal;

public class DashboardView extends NavigationBarView
{
	public var buttonClicked:Signal;

	public var signalImprintBtnClicked:Signal;

	protected var imprintButton:ImprintButton;

	public var dashboardList:DashboardList;

	private var isButtonClicked:Boolean = false;
	private var infoTextField:DashboardTextField;

	/**
	 *
	 */
	public function DashboardView() {
		super("Mobility App", true);

		buttonClicked = new Signal(DashboardButtonVO);
		signalImprintBtnClicked = new Signal();

		addEventListener(Event.ADDED_TO_STAGE, addedToStage);
	}

	private function addedToStage(event:Event):void {
		removeEventListener(Event.ADDED_TO_STAGE, addedToStage);
		invalidateSize();
	}

	public function resetButtonColors():void {
//		for (var i:uint = 0; i < dashboardBtns.length; i++) {
//			dashboardBtns[i].labelColor = Color.LIST_ITEM;
//		}
	}

	override protected function createChildren():void {
		super.createChildren();

		// imprint icon
		imprintButton = new ImprintButton();
		imprintButton.setSize(39, 39);
		imprintButton.signalClicked.add(onImprintMouseUp);
		_navbar.addActionButton(imprintButton);


		infoTextField = new DashboardTextField();
        infoTextField.setSize(Screen.stage.stageWidth, scale(60));
		infoTextField.linkClicked.add(handleLinkClick);
		addChild(infoTextField);


		dashboardList = new DashboardList(Screen.stage.stageWidth, Screen.stage.stageHeight - _navbar.height - infoTextField.height);
		dashboardList.itemRenderer = DashboardButton;
		var object:DashboardButton = new DashboardButton();
		dashboardList.rowHeight = object.itemHeight;
		object = null;

		dashboardList.visualListItemBuffer = 8;
		dashboardList.maxTapDelayTime = .5;

		dashboardList.itemSelected.add(handleListItemClicked);
		dashboardList.y = _navbar.height;

		addChildAt(DisplayObject(dashboardList), 0);
	}

	private function handleLinkClick(link:String):void {
		navigateToURL(new URLRequest(link));
	}

	public function invalidateSize():void{
		infoTextField.y = Screen.stage.stageHeight-infoTextField.height;
	}

	protected function onImprintMouseUp(button:AbstractButton):void {
		signalImprintBtnClicked.dispatch();
	}

	/**
	 *
	 */
	public function setOffline():void {
		dashboardList.online = false;
	}

	public function setOnline():void {
		dashboardList.online = true;
	}

	protected function handleListItemClicked(item:ITouchListItemRenderer):void {
		if(!dashboardList.online && DashboardButton(item).data.onlineView){
			return;
		}

		if(isButtonClicked) {
			trace("already clicked. do nothing");
			return;
		}

		isButtonClicked = true;

		// TODO: MOVE DELAY TO MEDIATOR AND DISABLE VIEW FOR FURTHER INPUT
		const CLICK_TO_TRANSITION_DELAY:Number = 125;

		//DashboardButton(btn).labelColor = Color.COLOR_MAGENTA;
		setTimeout(dispatchViewChange, CLICK_TO_TRANSITION_DELAY);

		function dispatchViewChange():void {
			buttonClicked.dispatch(item.data as DashboardButtonVO);
			isButtonClicked = false;
		}
	}


    public function unselectAll():void{
        dashboardList.unselectAll();
    }

}
}
