package de.telekom.detefleet.app.controller.dialog
{
import de.telekom.detefleet.app.models.DialogModel;
import de.telekom.detefleet.app.models.LayoutData;
import de.telekom.detefleet.app.models.vo.PhoneCallRequestVO;
import de.telekom.detefleet.app.views.components.dialog.CallPhoneDialogView;

import org.robotlegs.mvcs.Command;

/**
 *
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class ShowCallPhoneDialogCMD extends Command
{
	[Inject]
	public var model:DialogModel;

	[Inject]
	public var layout:LayoutData;

	[Inject]
	public var request:PhoneCallRequestVO;

	public override function execute():void {
		var dialog:CallPhoneDialogView = new CallPhoneDialogView(layout.viewport, request.number, request.extraInformation);
		model.addDialog(dialog);
	}
}
}
