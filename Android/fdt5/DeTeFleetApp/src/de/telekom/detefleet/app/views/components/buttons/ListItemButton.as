package de.telekom.detefleet.app.views.components.buttons
{
import de.aoemedia.mobile.ui.components.buttons.IconLabelButton;
import de.aoemedia.mobile.ui.components.skins.ButtonBitmaps;
import de.telekom.detefleet.app.assets.Color;
import de.telekom.detefleet.app.assets.IconRepository;
import de.telekom.detefleet.app.assets.ScaleBitmaps;

import flash.display.Bitmap;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import flash.text.TextFormat;
	
	public class ListItemButton extends IconLabelButton
	{
		protected var _subTitle:TextField;
		protected var _subTitleTF:TextFormat;
		
		public function ListItemButton( label:String, 
										tFormat:TextFormat, 
										icon:Bitmap = null, 
										subTitle:String = null, 
										subTitleTF:TextFormat = null )
		{
			var sBmps:ScaleBitmaps = ScaleBitmaps.getInstance();
			var bmps:ButtonBitmaps
					= new ButtonBitmaps(sBmps.homeButton,
										sBmps.homeButtonDown,
										sBmps.homeButton,
										sBmps.homeButtonDown);
			

			
			super(bmps);
			stopInvalidation = true;
			_leftPadding = 12 * scaleFactor;
			_leftLabelPadding = 51 * scaleFactor;
			_rightPadding = 12 * scaleFactor;
			isLabelCentered = false;
			
			icon.width = scale(23);
			icon.height = scale(23);
			leftIcon = icon;
			
			var arrIcon:Bitmap = IconRepository.iconArrow;
			rightIcon = arrIcon;
			this.label = label;
			labelColor = Color.HIGHLIGHT;
			labelColorDown = Color.DARK_GREY;
			setSize(320, 49);
			textFormat = tFormat;
			
			_subTitleTF = subTitleTF;
			this.subTitle = subTitle;
			
			stopInvalidation = false;
		}
		

		public function get subTitle ( ):String
		{
			return _subTitle != null ? _subTitle.text : "";
		}

		public function set subTitle ( value:String ):void
		{
			if ( _subTitle == null )
			{
				_subTitle = new TextField();
				_subTitle.autoSize = TextFieldAutoSize.LEFT;
				if ( _subTitleTF != null )
				{
					_subTitle.defaultTextFormat = _subTitleTF;
					_subTitle.setTextFormat(_subTitleTF);
					_subTitle.embedFonts = true;
				}
				invalidate();
				addChild(_subTitle);
			}
			_subTitle.cacheAsBitmap = false;
			_subTitle.text = value;
			_subTitle.cacheAsBitmap = true;
			invalidate();
		}

		public override function set enabled ( value:Boolean ):void
		{
			super.enabled = value;
			
			if(value){
				labelColor = Color.HIGHLIGHT;
			} else {
				labelColor = Color.DARK_GREY;
			}
			
			invalidate();
		}
		
		override public function setSize ( w:Number = -1, h:Number = -1 ):void
		{
			super.setSize(w,h);
			if ( _subTitle != null )
			{
				_subTitle.x = _rightIcon.x - _subTitle.width - scale(8);
				_subTitle.y = componentHeight - _subTitle.height >> 1;
			}
			/*/
			if ( !_isLabelCentered )
			{
				_label.x = _leftLabelPadding;
			}
			//*/
		}
		
	}
}