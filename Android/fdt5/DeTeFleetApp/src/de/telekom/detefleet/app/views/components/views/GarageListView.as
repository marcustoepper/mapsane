package de.telekom.detefleet.app.views.components.views
{
import com.kaizimmer.ui.Screen;
import com.kaizimmer.ui.form.FormFieldType;
import com.kaizimmer.ui.form.FormValidator;
import com.thanksmister.touchlist.renderers.ITouchListItemRenderer;

import de.aoemedia.mobile.ui.MultiDpiHelper;
import de.aoemedia.mobile.ui.components.buttons.AbstractButton;
import de.aoemedia.mobile.ui.components.container.LayoutContainer;
import de.aoemedia.mobile.ui.components.container.LayoutVerticalAlign;
import de.aoemedia.mobile.ui.components.forms.FormField;
import de.telekom.detefleet.app.assets.TextFormatTemplate;
import de.telekom.detefleet.app.models.vo.GarageSearchRequestVO;
import de.telekom.detefleet.app.models.vo.GarageVO;
import de.telekom.detefleet.app.models.vo.LocationRequestVO;
import de.telekom.detefleet.app.models.vo.VehicleServiceCaseVO;
import de.telekom.detefleet.app.views.components.buttons.ActionButton;
import de.telekom.detefleet.app.views.components.buttons.actionBar.MapButton;
import de.telekom.detefleet.app.views.components.form.InputFieldWrapper;
import de.telekom.detefleet.app.views.components.list.ListComponent;
import de.telekom.detefleet.app.views.components.list.ListWrapper;
import de.telekom.detefleet.app.views.components.listItem.GarageListItemButton;
import de.telekom.detefleet.app.views.components.selectbox.SelectBoxButton;
import de.telekom.detefleet.app.views.components.selectbox.SelectBoxButtonWrapper;

import flash.display.Sprite;
import flash.globalization.LocaleID;
import flash.globalization.NumberFormatter;
import flash.utils.setTimeout;

import org.osflash.signals.Signal;

/**
 * This view show the List and Filter of the Garages
 *
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 *
 */
public class GarageListView extends FilterView
{

	public var signalToggleMapButtonClicked:Signal;

	// Callbacks
	public var searchButtonClickedCallback:Function;
	public var listItemClickedCallback:Function;

	// Const
	public const DEFAULT_LOCATION_TEXT:String = "Aktuelle Position";

	protected var listHeightMaximized:Number;
	protected var listHeightMinimized:Number;

	// Form
	protected var ffLocation:FormField;
	protected var searchFormViewValidator:FormValidator;

	// Components
	protected var locationInputField:InputFieldWrapper;
	protected var toggleMapButton:MapButton;
	public var vehicleServiceCaseSelectBoxBtn:SelectBoxButton;

	protected var background:Sprite;
	protected var listWrapper:ListWrapper;
	protected var searchButton:ActionButton;

	protected static var distanceFormatter:NumberFormatter;
	private var garagesList:ListComponent;

	/**
	 * Construct
	 */
	public function GarageListView() {
		super("Werkstattsuche");

		if(distanceFormatter == null) {
			distanceFormatter = new NumberFormatter(LocaleID.DEFAULT);
			distanceFormatter.fractionalDigits = 1;
		}
		signalToggleMapButtonClicked = new Signal();

	}

	/**
	 *
	 *
	 */
	override protected function createChildren():void {
		super.createChildren();

		drawBackground();

		// NAVBAR BUTTONS
        navbar.textFormat = TextFormatTemplate.getInstance().actionbarSmall;

		toggleMapButton = new MapButton();
		toggleMapButton.setSize(39, 39);
		toggleMapButton.signalClicked.add(toggleMap);
		toggleMapButton.visible = false;
		_navbar.addActionButton(toggleMapButton);

		// FILTER FORM
		var formLayout:LayoutContainer = new LayoutContainer(upperView);
        formLayout.stopInvalidation = true;
		formLayout.percentalWidth = 100;
		formLayout.gap = 10;
		formLayout.paddingLeft = 10;
		formLayout.paddingRight = 10;
		formLayout.paddingTop = 10;
		formLayout.paddingBottom = 10;

		locationInputField = new InputFieldWrapper(upperView);
		locationInputField.label = "Im Umkreis von";
		locationInputField.percentalWidth = 100;
		locationInputField.verticalAlign = LayoutVerticalAlign.MIDDLE;
		locationInputField.labelPosition = InputFieldWrapper.POSITION_LEFT;

		formLayout.addItem(locationInputField);

		var serviceCasesWrapper:SelectBoxButtonWrapper = new SelectBoxButtonWrapper(upperView, "Servicefall");
		vehicleServiceCaseSelectBoxBtn = serviceCasesWrapper.selectBoxButton;
		serviceCasesWrapper.percentalWidth = 100;
		serviceCasesWrapper.label = "Servicefall:";
		serviceCasesWrapper.verticalAlign = LayoutVerticalAlign.MIDDLE;
		vehicleServiceCaseSelectBoxBtn.textFieldWidth = 150;
		vehicleServiceCaseSelectBoxBtn.labelField = "serviceCase";

		formLayout.addItem(serviceCasesWrapper);

		searchButton = new ActionButton("Suchen");
		searchButton.setSize(320 - formLayout.paddingLeft - formLayout.paddingRight, 37);
		searchButton.signalClicked.add(handleSearchButtonClicked);

		formLayout.addItem(searchButton);

		formLayout.addItem(filterOptionLabelLayout);

        formLayout.stopInvalidation = false;
        formLayout.invalidate();
		formLayout.addItemsToView();

		minYUpperView = filterOptionLabelLayout.height;
		viewsContainer.y = _minYUpperView;

		createList();
		initForms();

		updateViewsContainer();
		drawBackground();
		invalidate();
	}

	/**
	 *
	 *
	 */
	protected function createList():void {
		listWrapper = new ListWrapper(lowerView);
		listWrapper.explicitWidth = Screen.stage.stageWidth;
		listWrapper.gap = 0;
		listWrapper.paddingTop = 12;
		listWrapper.visible = false;

		listWrapper.addColumn("Werkstatt", 196);
		listWrapper.addColumn("Entfernung", 90);

		listHeightMaximized = Screen.stage.stageHeight - (dragHandle.y + dragHandle.height + listWrapper.getHeaderHeight() + filterOptionLabelLayout.height);
		listHeightMinimized = Screen.stage.stageHeight - (upperView.height + dragHandle.y + dragHandle.height + listWrapper.getHeaderHeight() );

		garagesList = new ListComponent(Screen.stage.stageWidth, listHeightMaximized);
		garagesList.itemRenderer = GarageListItemButton;
		var object:GarageListItemButton = new GarageListItemButton();
		garagesList.rowHeight = object.itemHeight;
		object = null;

		garagesList.setSize(Screen.stage.stageWidth, listHeightMaximized);
		garagesList.visualListItemBuffer = 4;
		garagesList.maxTapDelayTime = .5;

		garagesList.itemSelected.add(handleListItemClicked);

		listWrapper.list = garagesList;

		listWrapper.addItemsToView();
	}

	/**
	 *
	 *
	 */
	protected function initForms():void {
		searchFormViewValidator = new FormValidator();

		ffLocation = new FormField(locationInputField.inputField, DEFAULT_LOCATION_TEXT, true, FormFieldType.ALL);
		ffLocation.defaultText = DEFAULT_LOCATION_TEXT;
		ffLocation.liveChecking = true;
		ffLocation.preventLiveCheckRendering = true;

		searchFormViewValidator.addFormObject(ffLocation);
	}

	/**
	 * Resets all Form data
	 */
	public function resetForm():void {
		ffLocation.reset();
		if(vehicleServiceCaseSelectBoxBtn.dataProvider != null && vehicleServiceCaseSelectBoxBtn.dataProvider.length > 0) {
			vehicleServiceCaseSelectBoxBtn.selectedItem = vehicleServiceCaseSelectBoxBtn.dataProvider[0];
		}
	}

	public function setDefaultLocationText(text:String):void {
		ffLocation.defaultText = text;
	}

	/**
	 *
	 */
	protected function toggleMap(button:AbstractButton):void {
		signalToggleMapButtonClicked.dispatch();
	}

	/**
	 *
	 */
	protected function handleSearchButtonClicked(button:AbstractButton):void {
		if(searchFormViewValidator.isFormValid || DEFAULT_LOCATION_TEXT == locationInputField.inputField.textField.text) {
			searchButtonClickedCallback(createSearchRequest());
			locationInputField.inputField.normalize();
		} else {
			locationInputField.inputField.showError();
		}
	}

	/**
	 *
	 * @return
	 *
	 */
	public function createSearchRequest():GarageSearchRequestVO {
		var requestData:GarageSearchRequestVO = new GarageSearchRequestVO();

		var location:LocationRequestVO = new LocationRequestVO();
		requestData.location = location;
		location.address = null;
		if(locationInputField.inputField.textField.text != DEFAULT_LOCATION_TEXT) {
			location.address = locationInputField.inputField.textField.text;
		}
		requestData.serviceCase = vehicleServiceCaseSelectBoxBtn.selectedItem.id;

		return requestData;
	}

	/**
	 *
	 *
	 * @param garages
	 */
	public function populateGarages(garages:Vector.<GarageVO>):void {
		clear();

		if(garages == null || garages.length == 0) {
			return;
		}

		listWrapper.visible = false;

		var itemDescriptions:Array = [];
		for (var i:uint = 0; i < garages.length; i++) {
			itemDescriptions[i] = garages[i];
		}

		if(garages.length > 0) {
			listWrapper.visible = true;
			toggleMapButton.visible = true;

			garagesList.dataProvider = itemDescriptions;
			closeUpperView();
		}
	}

	/**
	 *
	 *
	 */
	public function clear():void {
		listWrapper.clearList();
		toggleMapButton.visible = false;
	}

	/**
	 *
	 */
	protected function handleListItemClicked(item:ITouchListItemRenderer):void {
		garagesList.mouseEnabled = false;
		// TODO: MOVE DELAY TO MEDIATOR AND DISABLE VIEW FOR FURTHER INPUT
		const CLICK_TO_TRANSITION_DELAY:Number = 125;

		setTimeout(dispatchItemClick, CLICK_TO_TRANSITION_DELAY);

		function dispatchItemClick():void {
			garagesList.mouseEnabled = true;
			listItemClickedCallback(GarageVO(item.data));
		}

	}

	/**
	 *
	 */
	public function setVehicleServiceCaseDataProvider(selections:Vector.<VehicleServiceCaseVO>):void {
		vehicleServiceCaseSelectBoxBtn.dataProvider = selections;
	}

	/**
	 *
	 */
	public function setSelectedVehicleServiceCase(selection:Object):void {
		vehicleServiceCaseSelectBoxBtn.selectedItem = selection;
	}

	/**
	 *
	 */
	protected function drawBackground():void {
		if(background == null) {
			background = new Sprite();

			background.graphics.clear();
			background.graphics.beginFill(0xd6d6d6);
			background.graphics.drawRect(0, 0, Screen.stage.stageWidth, 1);
			background.graphics.endFill();
			upperView.addChild(background);
			background.cacheAsBitmap = true;
		}
		background.height = upperView.height;
	}



    public function unselectAll():void{
        garagesList.unselectAll();
    }

}
}
