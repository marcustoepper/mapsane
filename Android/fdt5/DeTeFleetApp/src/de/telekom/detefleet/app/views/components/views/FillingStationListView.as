package de.telekom.detefleet.app.views.components.views
{
import com.kaizimmer.ui.Screen;
import com.kaizimmer.ui.form.FormFieldType;
import com.kaizimmer.ui.form.FormValidator;
import com.thanksmister.touchlist.renderers.ITouchListItemRenderer;

import de.aoemedia.mobile.ui.components.buttons.AbstractButton;
import de.aoemedia.mobile.ui.components.container.LayoutContainer;
import de.aoemedia.mobile.ui.components.container.LayoutVerticalAlign;
import de.aoemedia.mobile.ui.components.forms.FormField;
import de.telekom.detefleet.app.models.vo.FillingStationRequestVO;
import de.telekom.detefleet.app.models.vo.FillingStationVO;
import de.telekom.detefleet.app.models.vo.FuelTypeVO;
import de.telekom.detefleet.app.models.vo.LocationRequestVO;
import de.telekom.detefleet.app.views.components.buttons.ActionButton;
import de.telekom.detefleet.app.views.components.buttons.actionBar.MapButton;
import de.telekom.detefleet.app.views.components.listItem.FillingStationListItemButton;
import de.telekom.detefleet.app.views.components.form.InputFieldWrapper;
import de.telekom.detefleet.app.views.components.list.ListComponent;
import de.telekom.detefleet.app.views.components.list.ListWrapper;
import de.telekom.detefleet.app.views.components.selectbox.SelectBoxButton;
import de.telekom.detefleet.app.views.components.selectbox.SelectBoxButtonWrapper;

import flash.display.DisplayObject;

import flash.display.Sprite;
import flash.utils.setTimeout;

import org.osflash.signals.Signal;

/**
 *
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 *
 */
public class FillingStationListView extends FilterView
{
	public var signalToggleMapButtonClicked:Signal;

	// Callbacks
	public var searchButtonClickedCallback:Function;
	public var listItemClickedCallback:Function;

	// Const
	public const DEFAULT_LOCATION_TEXT:String = "Aktuelle Position";

	// Defaults
	protected var listHeightMaximized:Number;
	protected var listHeightMinimized:Number;

	// Form
	protected var ffLocation:FormField;

	protected var searchFormViewValidator:FormValidator;

	// Components
	protected var locationInputField:InputFieldWrapper;
	protected var toggleMapButton:MapButton;
	protected var fuelTypeSelectBoxBtn:SelectBoxButton;

	protected var background:Sprite;
	protected var listWrapper:ListWrapper;

	public var sortBySelectBoxBtn:SelectBoxButton;
	protected var searchButton:ActionButton;
	private var stationsList:ListComponent;

	private const MAX_ITEMS:uint = 70;

	/**
	 * Construct
	 */
	public function FillingStationListView() {
		super("Tankstelle");

		signalToggleMapButtonClicked = new Signal();
	}

	/**
	 *
	 *
	 */
	override protected function createChildren():void {
		super.createChildren();

		drawBackground();

		// NAVBAR BUTTONS
		//navbar.textFormat = TextFormatTemplate.getInstance().actionbarNormal;

		toggleMapButton = new MapButton();
		toggleMapButton.setSize(39, 39);
		toggleMapButton.signalClicked.add(toggleMap);
		toggleMapButton.visible = false;
		_navbar.addActionButton(toggleMapButton);

		// FILTER FORM
		var formLayout:LayoutContainer = new LayoutContainer(upperView);
        formLayout.stopInvalidation = true;
		formLayout.explicitWidth = Screen.stage.stageWidth;
		formLayout.gap = 10;
		formLayout.paddingLeft = 10;
		formLayout.paddingRight = 10;
		formLayout.paddingTop = 10;
		formLayout.paddingBottom = 10;

		locationInputField = new InputFieldWrapper(upperView);
		locationInputField.label = "Im Umkreis von";
		locationInputField.percentalWidth = 100;
		locationInputField.verticalAlign = LayoutVerticalAlign.MIDDLE;
		locationInputField.labelPosition = InputFieldWrapper.POSITION_LEFT;

		var fuelTypesWrapper:SelectBoxButtonWrapper = new SelectBoxButtonWrapper(upperView, "Sorte");
		fuelTypeSelectBoxBtn = fuelTypesWrapper.selectBoxButton;
		fuelTypeSelectBoxBtn.labelField = "fuelType";
		fuelTypesWrapper.percentalWidth = 100;
		fuelTypesWrapper.verticalAlign = LayoutVerticalAlign.MIDDLE;
		fuelTypesWrapper.label = "Sorte wählen:";

		var sortByWrapper:SelectBoxButtonWrapper = new SelectBoxButtonWrapper(upperView, "Sortierung");
		sortBySelectBoxBtn = sortByWrapper.selectBoxButton;
		sortByWrapper.verticalAlign = LayoutVerticalAlign.MIDDLE;
		sortByWrapper.percentalWidth = 100;
		sortByWrapper.label = "Sortieren nach:";

		searchButton = new ActionButton("Suchen");
		searchButton.setSize(320 - formLayout.paddingLeft - formLayout.paddingRight, 37);
		searchButton.signalClicked.add(handleSearchButtonClicked);

        formLayout.addItem(locationInputField);
        formLayout.addItem(sortByWrapper);
        formLayout.addItem(fuelTypesWrapper);
		formLayout.addItem(searchButton);
		formLayout.addItem(filterOptionLabelLayout);

        formLayout.stopInvalidation = false;
        formLayout.invalidate();
		formLayout.addItemsToView();

		minYUpperView = filterOptionLabelLayout.height;
		viewsContainer.y = _minYUpperView;

		createList();
		initForms();

		updateViewsContainer();
		drawBackground();
		invalidate();
	}

	/**
	 *
	 *
	 */
	protected function createList():void {
		listWrapper = new ListWrapper(lowerView);
		listWrapper.explicitWidth = scale(320);
		listWrapper.gap = 0;

		listWrapper.addColumn("Preis", 76);
		listWrapper.addColumn("Name", 120);
		listWrapper.addColumn("Entfernung", 90);

		listWrapper.paddingTop = 12;
		listWrapper.visible = false;

		listHeightMaximized = Screen.stage.stageHeight - (dragHandle.y + dragHandle.height + listWrapper.getHeaderHeight() + filterOptionLabelLayout.height ) - scale(2);
		listHeightMinimized = Screen.stage.stageHeight - (upperView.height + dragHandle.y + dragHandle.height + listWrapper.getHeaderHeight() );

		stationsList = new ListComponent(Screen.stage.stageWidth, listHeightMaximized);
		stationsList.itemRenderer = FillingStationListItemButton;
		var object:FillingStationListItemButton = new FillingStationListItemButton();
		stationsList.rowHeight = object.itemHeight;
		object = null;

		stationsList.visualListItemBuffer = 8;
		stationsList.maxTapDelayTime = .5;

		stationsList.itemSelected.add(handleListItemClicked);

		listWrapper.list = stationsList as ListComponent;

		listWrapper.addItemsToView();
	}

	/**
	 *
	 * @param stations
	 *
	 */
	public function populateFillingStations(stations:Vector.<FillingStationVO>):void {
		clear();

		if(stations.length == 0) {
			return;
		}

		listWrapper.visible = false;

		var itemDescriptions:Array= [];
		for (var i:uint = 0; i < stations.length; i++) {
			itemDescriptions[i] = stations[i];
		}

		if(stations.length > 0) {
			toggleMapButton.visible = true;
			listWrapper.visible = true;

			stationsList.dataProvider = itemDescriptions;
			closeUpperView();
		}
	}

	/**
	 *
	 *
	 */
	protected function initForms():void {
		searchFormViewValidator = new FormValidator();

		ffLocation = new FormField(locationInputField.inputField, DEFAULT_LOCATION_TEXT, true, FormFieldType.ALL);
		ffLocation.defaultText = DEFAULT_LOCATION_TEXT;
		ffLocation.liveChecking = true;
		ffLocation.preventLiveCheckRendering = true;

		searchFormViewValidator.addFormObject(ffLocation);
	}

	/**
	 * Resets all Form data
	 */
	public function resetForm():void {
		ffLocation.reset();
		if(sortBySelectBoxBtn.dataProvider != null && sortBySelectBoxBtn.dataProvider.length > 0) {
			sortBySelectBoxBtn.selectedItem = sortBySelectBoxBtn.dataProvider[0];
		}
		if(fuelTypeSelectBoxBtn.dataProvider != null && fuelTypeSelectBoxBtn.dataProvider.length > 0) {
			fuelTypeSelectBoxBtn.selectedItem = fuelTypeSelectBoxBtn.dataProvider[0];
		}
		clear();
	}

	public function setDefaultLocationText(text:String):void {
		ffLocation.defaultText = text;
	}

	/**
	 *
	 */
	protected function toggleMap(button:AbstractButton):void {
		signalToggleMapButtonClicked.dispatch();
	}

	/**
	 *
	 */
	protected function handleSearchButtonClicked(button:AbstractButton):void {
		if(searchFormViewValidator.isFormValid || DEFAULT_LOCATION_TEXT == locationInputField.inputField.textField.text) {
			var searchRequest:FillingStationRequestVO = createSearchRequest();
			searchButtonClickedCallback(searchRequest);
			locationInputField.inputField.normalize();
		} else {
			locationInputField.inputField.showError();
		}
	}

	/**
	 *
	 * @return
	 *
	 */
	public function createSearchRequest():FillingStationRequestVO {
		var requestData:FillingStationRequestVO = new FillingStationRequestVO();

		var location:LocationRequestVO = new LocationRequestVO();
		requestData.location = location;
		location.address = null;
		if(locationInputField.inputField.textField.text != DEFAULT_LOCATION_TEXT) {
			location.address = locationInputField.inputField.textField.text;
		}
		requestData.sortby = sortBySelectBoxBtn.selectedItem.data;
		requestData.fueltype = FuelTypeVO(fuelTypeSelectBoxBtn.selectedItem).id;

		return requestData;
	}

	/**
	 *
	 *
	 */
	public function clear():void {
		if(listWrapper) {
			listWrapper.clearList();
		}
		toggleMapButton.visible = false;
	}

	/**
	 * Handles if an Item in the FillingStation List is clicked
	 */
	protected function handleListItemClicked(item:ITouchListItemRenderer):void {
		stationsList.mouseEnabled = false;
		// TODO: MOVE DELAY TO MEDIATOR AND DISABLE VIEW FOR FURTHER INPUT
		const CLICK_TO_TRANSITION_DELAY:Number = 125;

		setTimeout(dispatchItemClick, CLICK_TO_TRANSITION_DELAY);

		function dispatchItemClick():void {
			listItemClickedCallback(FillingStationVO(item.data));
			stationsList.mouseEnabled = true;
		}
	}

	/**
	 *
	 */
	protected function drawBackground():void {
		if(background == null) {
			background = new Sprite();

			background.graphics.clear();
			background.graphics.beginFill(0xd6d6d6);
			background.graphics.drawRect(0, 0, Screen.stage.stageWidth, 1);
			background.graphics.endFill();
			addChildToUpperViewAt(DisplayObject(background), 0);
		}

		background.height = upperView.height;
	}

	/**
	 * Fill the Search Form with an existing Request.
	 *
	 */
	public function fillSearchRequestForm(request:FillingStationRequestVO):void {
		if(request.location.address) {
			locationInputField.inputField.textField.text = request.location.address;
		} else {
			locationInputField.inputField.textField.text = DEFAULT_LOCATION_TEXT;
		}

		for (var i:int = 0; i < fuelTypeSelectBoxBtn.dataProvider.length; i++) {
			if(fuelTypeSelectBoxBtn.dataProvider[i].id == request.fueltype) {
				fuelTypeSelectBoxBtn.selectedItem = fuelTypeSelectBoxBtn.dataProvider[i];
				break;
			}
		}

		for (i = 0; i < sortBySelectBoxBtn.dataProvider.length; i++) {
			if(sortBySelectBoxBtn.dataProvider[i].data == request.sortby) {
				sortBySelectBoxBtn.selectedItem = sortBySelectBoxBtn.dataProvider[i];
				break;
			}
		}
		if(listWrapper) {
			listWrapper.clearList();
		}
	}

	/**
	 *
	 */
	public function setFuelTypeDataProvider(selections:Vector.<FuelTypeVO>):void {
		fuelTypeSelectBoxBtn.dataProvider = selections;
	}

	/**
	 *
	 */
	public function setSelectedFuelType(selection:Object):void {
		fuelTypeSelectBoxBtn.selectedItem = selection;
	}

    public function unselectAll():void{
        stationsList.unselectAll();
    }
}
}
