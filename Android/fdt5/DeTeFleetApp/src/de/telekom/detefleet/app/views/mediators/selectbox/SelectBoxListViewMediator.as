package de.telekom.detefleet.app.views.mediators.selectbox {
import de.telekom.detefleet.app.events.SelectionCanceled;
import de.telekom.detefleet.app.events.SelectionDone;
import de.telekom.detefleet.app.events.ShowSelectBoxList;
import de.telekom.detefleet.app.events.view.OverlayHidden;
import de.telekom.detefleet.app.events.view.ShowingOverlay;
import de.telekom.detefleet.app.views.components.selectbox.SelectBoxButton;
import de.telekom.detefleet.app.views.components.selectbox.SelectBoxListView;

import org.robotlegs.mvcs.Mediator;

/**
 *
 * @author Kai Zimmer <kai.zimmer@aoemedia.de>
 */
public class SelectBoxListViewMediator extends Mediator {
    [Inject]
    public var showSelectBoxList:ShowSelectBoxList;

    // sender signals
    [Inject]
    public var signalSelectionDone:SelectionDone;

    [Inject]
    public var signalSelectionCanceled:SelectionCanceled;

    // receiver signals
	
    [Inject]
    public var signalShowingOverlay:ShowingOverlay;

    [Inject]
    public var signalOverlayHidden:OverlayHidden;

    // view

    [Inject]
    public var view:SelectBoxListView;

    override public function onRegister():void {
        // context listener
        showSelectBoxList.add(onSelectBoxRequest);

        // view listener
		view.signalOnHidden.add(dispatchOverlayHidden);
        view.signalSelectionConfirmed.add(dispatchSelectionDone);
        view.signalSelectionCanceled.add(dispatchSelectionCanceled);
    }

    protected function onSelectBoxRequest(data:Vector.<Object>, caller:SelectBoxButton):void {
        view.caller = caller;
        // only set the items if they changed
        if (view.getItems() != data) {
            view.labelField = caller.labelField;
            view.removeItems();
            view.setItems(data);

            view.list.list.selectedData = caller.selectedItem;
        }
        signalShowingOverlay.dispatch();
        view.show();

    }

    override public function preRemove():void {
		view.signalOnHidden.remove(dispatchOverlayHidden);
        view.signalSelectionConfirmed.remove(dispatchSelectionDone);
        view.signalSelectionCanceled.remove(dispatchSelectionCanceled);
		showSelectBoxList.remove(onSelectBoxRequest);
    }

    protected function dispatchSelectionDone(selection:Object):void {
        signalSelectionDone.dispatch(selection, view.caller);
    }

    protected function dispatchSelectionCanceled():void {
        signalSelectionCanceled.dispatch(view.caller);
    }

    protected function dispatchOverlayHidden():void {
        signalOverlayHidden.dispatch();
    }

}
}