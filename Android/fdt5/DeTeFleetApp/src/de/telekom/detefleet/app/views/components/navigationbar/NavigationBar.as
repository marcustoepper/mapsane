package de.telekom.detefleet.app.views.components.navigationbar {
import com.kaizimmer.ui.Screen;

import de.aoemedia.mobile.ui.MultiDpiHelper;

import de.aoemedia.mobile.ui.MultiDpiHelper;
import de.aoemedia.mobile.ui.components.LabelComponent;
import de.aoemedia.mobile.ui.components.buttons.AbstractButton;
import de.polygonal.ds.pooling.LinkedObjectPool;
import de.telekom.detefleet.app.assets.ScaleBitmaps;
import de.telekom.detefleet.app.views.components.buttons.actionBar.BackButton;

import flash.display.DisplayObject;
import flash.display.Sprite;
import flash.text.AntiAliasType;
import flash.text.TextFieldAutoSize;
import flash.text.TextFormat;

import org.bytearray.display.ScaleBitmap;
import org.osflash.signals.Signal;

/**
 * @todo Calc Title Size automatically
 */
public class NavigationBar extends Sprite {
    protected const HEIGHT:Number = 62;

    public var signalBackBtnClicked:Signal;

    protected static var _backgroundPool:LinkedObjectPool;
    protected static var _shadowPool:LinkedObjectPool;

    protected var tFormat:TextFormat;
    protected var _label:LabelComponent;
    protected var _backButton:BackButton;
    protected var _isHistoryBackDisabled:Boolean = false;

    protected var _actionButton:AbstractButton;

    private var background:ScaleBitmap;
    private var shadow:ScaleBitmap;

    public function NavigationBar() {
        super();
        signalBackBtnClicked = new Signal();

        createChildren();

        _backgroundPool = new LinkedObjectPool(2);
        _backgroundPool.allocate(null, fabricateBackground);

        _shadowPool = new LinkedObjectPool(2);
        _shadowPool.allocate(null, fabricateShadow);
    }

    private function fabricateShadow():ScaleBitmap {
        var shadow:ScaleBitmap = ScaleBitmaps.getInstance().navigationBarShadow;
        shadow.setSize(Screen.stage.stageWidth, MultiDpiHelper.scale(10));

        return shadow;
    }

    private function fabricateBackground():ScaleBitmap {
        var background:ScaleBitmap = ScaleBitmaps.getInstance().navigationBar;
        background.setSize(Screen.stage.stageWidth, MultiDpiHelper.scale(HEIGHT));

        return background;
    }

    public function showBackButton():void {
        _backButton.visible = true;
    }

    public function hideBackButton():void {
        _backButton.visible = false;
    }

    public function get label():String {
        return _label.label;
    }

    public function set label(value:String):void {
        _label.label = value;
        invalidate();
    }

    public function set textFormat(tFormat:TextFormat):void {
        _label.textFormat = tFormat;
        invalidate();
    }

    public function get textFormat():TextFormat {
        return _label.textFormat;
    }

    /**
     *
     */
    public function addActionButton(button:AbstractButton):void {
        _actionButton = button;
        addChild(DisplayObject(_actionButton));

        invalidate();
    }

    override public function get height():Number {
        return MultiDpiHelper.scale(HEIGHT);
    }

    public function get heightIncludingShadow():Number {
        return super.height;
    }

    public function set isHistoryBackDisabled(b:Boolean):void {
        _isHistoryBackDisabled = b;
    }

    public function get isHistoryBackDisabled():Boolean {
        return _isHistoryBackDisabled;
    }

    public function set backButtonLabel(label:String):void {
        _backButton.label = label;
    }

    public function get backButtonLabel():String {
        return _backButton.label;
    }

    /**
     *
     */
    protected function invalidate():void {
        if(background == null || _label == null){
            return;
        }
        _label.x = Screen.stage.stageWidth - _label.width >> 1;
        _label.y = background.height - _label.height >> 1;

        _backButton.x = MultiDpiHelper.scale(12);
        _backButton.y = background.height - _backButton.height >> 1;

        if(_actionButton){
            _actionButton.x = Screen.stage.stageWidth - _actionButton.width - MultiDpiHelper.scale(11);
            _actionButton.y = background.height - _actionButton.height >> 1;
        }

    }

    /**
     *
     */
    protected function createChildren():void {
        // create label
        _label = new LabelComponent();
        _label.textField.selectable = false;
        _label.textField.autoSize = TextFieldAutoSize.LEFT;
        _label.textField.antiAliasType = AntiAliasType.ADVANCED;
        addChild(DisplayObject(_label));

        // create ui
        _backButton = new BackButton("Zurück");
        _backButton.setSize(68, 36);
        addChild(DisplayObject(_backButton));
        _backButton.signalClicked.add(backButtonClicked);
    }

    /**
     *
     * @param button
     */
    protected function backButtonClicked(button:AbstractButton):void {
        signalBackBtnClicked.dispatch();
    }

    /**
     *
     */
    public function onHide():void {
        if (background) {
            removeChild(background);
            _backgroundPool.put(background);
            background = null;
        }
        if (shadow) {
            removeChild(shadow);
            _shadowPool.put(shadow);
            shadow = null;
        }
    }

    /**
     *
     */
    public function onShow():void {
        background = _backgroundPool.get() as ScaleBitmap;
        addChildAt(background, 0);

        shadow = _shadowPool.get() as ScaleBitmap;
        shadow.y = background.height;
        addChildAt(shadow, 1);

        invalidate();
    }
}
}
