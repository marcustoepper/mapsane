package de.telekom.detefleet.app.views.mediators
{
import flash.display.StageQuality;

import de.telekom.detefleet.app.events.view.TransitionCompleted;
import de.telekom.detefleet.app.events.view.ViewReady;
import de.telekom.detefleet.app.models.vo.TransitionStateVO;
import de.telekom.detefleet.app.views.components.views.ImprintView;

import flash.net.URLRequest;
import flash.net.navigateToURL;

import org.robotlegs.mvcs.Mediator;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class ImprintMeditator extends Mediator
{
	[Inject]
	public var view:ImprintView;
	
	[Inject]
	public var viewReady:ViewReady;
	
	[Inject]
	public var transitionCompleted:TransitionCompleted;
	
	public override function onRegister():void
	{
		viewReady.dispatch(view);
		
		transitionCompleted.add(handleTransitionCompleted);

		view.linkClicked.add(handleLinkClicked);
	}

	private function handleLinkClicked(link:String):void {
		navigateToURL(new URLRequest(link));
	}
	
	public function handleTransitionCompleted(state:TransitionStateVO):void
	{
		trace("###############");
		trace("stage.quality: " + view.stage.quality);
		trace("set stage quality low:");
		view.stage.quality = StageQuality.LOW;
		trace("stage.quality should be low now: " + view.stage.quality);
		trace("###############");
		//if ( state.viewToShow != view ) return;
		
	}
}
}
