package de.telekom.detefleet.app.controller.repairRequest
{
import de.telekom.detefleet.app.controller.AuthCommand;
import de.telekom.detefleet.app.services.remote.IRepairRequestService;

/**
 * Send a request to get existing Repair Requests
 *
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class GetRepairRequestsCMD extends AuthCommand
{
	[Inject]
	public var service:IRepairRequestService;

	public override function execute():void {
		service.getRepairRequests(auth);
	}
}
}
