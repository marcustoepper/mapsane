package de.telekom.detefleet.app.views.components.list
{
import de.aoemedia.mobile.ui.MultiDpiHelper;
import de.aoemedia.mobile.ui.components.LabelComponent;
import de.aoemedia.mobile.ui.components.RuleComponent;
import de.aoemedia.mobile.ui.components.container.LayoutContainer;
import de.aoemedia.mobile.ui.components.container.LayoutHorizontalAlign;
import de.aoemedia.mobile.ui.components.container.LayoutOrientation;
import de.telekom.detefleet.app.assets.Color;
import de.telekom.detefleet.app.assets.TextFormatTemplate;

import flash.display.Sprite;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class ListWrapper extends LayoutContainer
{
	protected var _columns:Vector.<Object>;
	protected var _headerLayout:LayoutContainer;
	protected var _list:ListComponent;
	protected var _rule:RuleComponent;

	protected var _visible:Boolean;

	private var _headerLastX:Number = 0;

	/**
	 *
	 */
	public function ListWrapper(view:Sprite) {
		super(view);

		initChildren();
	}

	/**
	 *
	 */
	protected function initChildren():void {
		_columns = new Vector.<Object>();

		_headerLayout = new LayoutContainer(this.view);
		_headerLayout.marginLeft = 11;
		_headerLayout.orientation = LayoutOrientation.HORIZONTAL;

		_rule = new RuleComponent(0.5, this.width, Color.MEDIUM_GREY);
		_rule.paddingTop = 6;
		_rule.paddingBottom = 1;

		addItem(_headerLayout);
		addItem(_rule);
	}

	/**
	 * Adds a Column
	 */
	public function addColumn(title:String, width:Number, align:String = LayoutHorizontalAlign.LEFT):void {
		var component:LabelComponent = new LabelComponent(title);
		component.mouseEnabled = false;
		component.visible = _visible;

		component.left = _headerLastX;
		_headerLastX += width + gap;
		_columns.push({label:component, width:width, align:align});
		_headerLayout.addItem(component);

		invalidate(false);
	}

	/**
	 *
	 */
	public function getHeaderHeight():Number {
		var maxHeight:Number = 0;
		for each (var column:Object in _columns) {
			if(column.label.height > maxHeight) {
				maxHeight = column.label.height;
			}
		}
		return maxHeight;
	}

	/**
	 *
	 */
	override public function invalidate(force:Boolean = false):void {
		applySkin();

		super.invalidate(force);

		_rule.width = MultiDpiHelper.unscale(width);
	}

	/**
	 *
	 */
	public function clearList():void {
		_list.removeAllListItems();
		visible = false;
	}

	/**
	 *
	 */
	protected function applySkin():void {
		for each (var column:Object in _columns) {
			column.label.textFormat = TextFormatTemplate.getInstance().listHeader;
		}
	}

	/**
	 *
	 */
	public function get visible():Boolean {
		return _visible;
	}

	/**
	 *
	 */
	public function set visible(visible:Boolean):void {
		for each (var column:Object in _columns) {
			column.label.visible = visible;
		}
		_rule.visible = visible;
		_visible = visible;
	}

	/**
	 *
	 */
	public function get list():ListComponent {
		return _list;
	}

	/**
	 *
	 */
	public function set list(list:ListComponent):void {
		if(_list == list) {
			return;
		}
		_list = list;

		addItem(list);

		invalidate(false);

	}
}
}
