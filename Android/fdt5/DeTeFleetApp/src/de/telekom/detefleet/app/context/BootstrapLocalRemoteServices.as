package de.telekom.detefleet.app.context {
import de.aoemedia.ane.securityTools.IJailbreakDetection;
import de.aoemedia.ane.securityTools.JailbreakDetection;
import de.telekom.detefleet.app.services.local.IDeviceKey;
import de.telekom.detefleet.app.services.local.IReachable;
import de.telekom.detefleet.app.services.local.MockURLMonitorReachable;
import de.telekom.detefleet.app.services.local.RandomDeviceKey;
import de.telekom.detefleet.app.services.persistence.IKeyChainService;
import de.telekom.detefleet.app.services.persistence.LocalKeyChainService;
import de.telekom.detefleet.app.services.remote.IFillingStationService;
import de.telekom.detefleet.app.services.remote.IGarageService;
import de.telekom.detefleet.app.services.remote.ILoginService;
import de.telekom.detefleet.app.services.remote.IMessageService;
import de.telekom.detefleet.app.services.remote.IRepairRequestService;
import de.telekom.detefleet.app.services.remote.MockFillingStationService;
import de.telekom.detefleet.app.services.remote.MockGarageService;
import de.telekom.detefleet.app.services.remote.MockLoginService;
import de.telekom.detefleet.app.services.remote.MockMessageService;
import de.telekom.detefleet.app.services.remote.MockRepairRequestService;
import de.telekom.detefleet.app.services.security.ISSLCertificateService;
import de.telekom.detefleet.app.services.security.SSLCertificateService;

import org.robotlegs.core.IInjector;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class BootstrapLocalRemoteServices {
    public function BootstrapLocalRemoteServices(injector:IInjector) {
        injector.mapClass(ILoginService, MockLoginService);
        injector.mapClass(IFillingStationService, MockFillingStationService);
        injector.mapClass(IGarageService, MockGarageService);
        injector.mapClass(IRepairRequestService, MockRepairRequestService);
        injector.mapClass(IMessageService, MockMessageService);

        injector.mapClass(IKeyChainService, LocalKeyChainService);
        injector.mapClass(IDeviceKey, RandomDeviceKey);
        injector.mapClass(IReachable, MockURLMonitorReachable);

        injector.mapClass(IJailbreakDetection, JailbreakDetection);
        injector.mapClass(ISSLCertificateService, SSLCertificateService);

    }
}
}
