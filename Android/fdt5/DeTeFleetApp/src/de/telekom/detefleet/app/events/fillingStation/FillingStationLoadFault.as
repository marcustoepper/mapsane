package de.telekom.detefleet.app.events.fillingStation
{
import de.telekom.detefleet.app.events.ServiceFaultSignal;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class FillingStationLoadFault extends ServiceFaultSignal
{
}
}
