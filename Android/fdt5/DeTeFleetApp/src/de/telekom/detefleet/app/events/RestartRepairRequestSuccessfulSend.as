package de.telekom.detefleet.app.events
{
import org.osflash.signals.Signal;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class RestartRepairRequestSuccessfulSend extends Signal
{
	public function RestartRepairRequestSuccessfulSend()
	{
		super();
	}
}
}
