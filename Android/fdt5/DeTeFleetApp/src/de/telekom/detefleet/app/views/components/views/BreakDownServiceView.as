package de.telekom.detefleet.app.views.components.views {
import com.kaizimmer.ui.Screen;

import de.aoemedia.mobile.ui.components.LabelComponent;
import de.aoemedia.mobile.ui.components.buttons.AbstractButton;
import de.aoemedia.mobile.ui.components.container.LayoutContainer;
import de.telekom.detefleet.app.assets.IconRepository;
import de.telekom.detefleet.app.assets.TextFormatTemplate;
import de.telekom.detefleet.app.assets.styleSheet.HTMLContentStyleSheet;
import de.telekom.detefleet.app.views.components.buttons.DefaultLabelButton;
import de.telekom.detefleet.app.views.components.buttons.ListButton;

import flash.text.StyleSheet;
import flash.text.TextFieldAutoSize;

import org.osflash.signals.Signal;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class BreakDownServiceView extends NavigationBarView {
    public var currentLocationButtonClicked:Signal;
    public var checklistButtonClicked:Signal;
    public var callEmergencyButtonClicked:Signal;
    public var callBreakDownServiceButtonClicked:Signal;

    public function BreakDownServiceView() {
        super("Pannendienst");

        currentLocationButtonClicked = new Signal();
        checklistButtonClicked = new Signal();
        callEmergencyButtonClicked = new Signal();
        callBreakDownServiceButtonClicked = new Signal();
    }

    override protected function createChildren():void {
        super.createChildren();

        var siteLayout:LayoutContainer = new LayoutContainer(this);
        siteLayout.y = navbar.height;
        siteLayout.paddingTop = 10;
        siteLayout.gap = 10;

        var checklistButton:ListButton = new ListButton("Checkliste Panne");
        checklistButton.setSize(320, 50);
        checklistButton.signalClicked.add(handleChecklistButtonClicked);

        siteLayout.addItem(checklistButton);

        var callBreakDownServiceButton:DefaultLabelButton = new DefaultLabelButton("Pannendienst");
        callBreakDownServiceButton.icon = IconRepository.call;
        callBreakDownServiceButton.setSize(320 - 11 - 11, 36);
        callBreakDownServiceButton.textFormat = TextFormatTemplate.getInstance().buttonNormal;
        callBreakDownServiceButton.paddingLeft = 11;
        callBreakDownServiceButton.paddingRight = 11;
        callBreakDownServiceButton.signalClicked.add(proxyBreakDownServiceClicked);

        siteLayout.addItem(callBreakDownServiceButton);

        var breakDownServiceInfoText:LabelComponent = new LabelComponent();

        var styleSheet:StyleSheet = new HTMLContentStyleSheet();
        breakDownServiceInfoText.textField.styleSheet = styleSheet;
        breakDownServiceInfoText.textField.htmlText = '<P><font face="Tele-GroteskFet">Wichtig:</font><font face="Tele-GroteskNor"> Melden Sie Ihren genauen Standort mit Fahrtrichtung und beschreiben Ihr Fahrzeug mit Kennzeichen sowie die Pannenursache.</font></P>';
        breakDownServiceInfoText.textField.embedFonts = true;
        breakDownServiceInfoText.textField.multiline = true;
        breakDownServiceInfoText.textField.wordWrap = true;

        breakDownServiceInfoText.textField.autoSize = TextFieldAutoSize.NONE;
        breakDownServiceInfoText.textField.width = Screen.stage.stageWidth - scale(11 + 11);
        breakDownServiceInfoText.textField.height = breakDownServiceInfoText.textField.textHeight + 4;

        breakDownServiceInfoText.paddingLeft = 11;
        breakDownServiceInfoText.paddingRight = 11;
        siteLayout.addItem(breakDownServiceInfoText);

        var currentLocationButton:ListButton = new ListButton("Aktueller Standort");
        currentLocationButton.setSize(320, 50);
        currentLocationButton.signalClicked.add(handleCurrentLocationButtonClicked);
        siteLayout.addItem(currentLocationButton);

        var currentLocationInfoText:LabelComponent = new LabelComponent();

        currentLocationInfoText.textField.styleSheet = styleSheet;
        currentLocationInfoText.textField.htmlText = '<P><font face="Tele-GroteskNor">Hatten Sie einen Unfall mit Personenschaden, wählen sie jetzt den Notruf 112.</font></P>';
        currentLocationInfoText.textField.embedFonts = true;
        currentLocationInfoText.textField.multiline = true;
        currentLocationInfoText.textField.wordWrap = true;

        currentLocationInfoText.textField.autoSize = TextFieldAutoSize.NONE;
        currentLocationInfoText.textField.width = Screen.stage.stageWidth - scale(11 + 11);
        currentLocationInfoText.textField.height = currentLocationInfoText.textField.textHeight + 4;
        currentLocationInfoText.paddingLeft = 11;
        currentLocationInfoText.paddingRight = 11;
        siteLayout.addItem(currentLocationInfoText);

        var callEmergencyButton:DefaultLabelButton = new DefaultLabelButton("Notruf");
        callEmergencyButton.icon = IconRepository.notruf;
        callEmergencyButton.textFormat = TextFormatTemplate.getInstance().buttonNormal;
        callEmergencyButton.setSize(320 - 11 - 11, 36);
        callEmergencyButton.paddingLeft = 11;
        callEmergencyButton.paddingRight = 11;
        callEmergencyButton.signalClicked.add(proxyEmergencyClicked);

        siteLayout.addItem(callEmergencyButton);

        siteLayout.addItemsToView();
    }

    private function handleCurrentLocationButtonClicked(button:AbstractButton):void {
        currentLocationButtonClicked.dispatch();
    }

    private function handleChecklistButtonClicked(button:AbstractButton):void {
        checklistButtonClicked.dispatch();
    }

    private function proxyEmergencyClicked(button:AbstractButton):void {
        callEmergencyButtonClicked.dispatch();
    }

    private function proxyBreakDownServiceClicked(button:AbstractButton):void {
        callBreakDownServiceButtonClicked.dispatch();
    }
}
}
