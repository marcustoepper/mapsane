package de.telekom.detefleet.app.services.remote
{
import de.telekom.detefleet.app.events.RepairRequestSuccessfulSend;
import de.telekom.detefleet.app.events.RestartRepairRequestSuccessfulSend;
import de.telekom.detefleet.app.models.RepairRequestModel;
import de.telekom.detefleet.app.models.vo.RepairRequestVO;
import de.telekom.detefleet.app.models.vo.ServiceAuthVO;

import flash.utils.setTimeout;

import org.robotlegs.mvcs.Actor;

/**
 * Implementation for an Mock Service to get some Garages.
 * It returns only some Dummy Data to test locally.
 *
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class MockRepairRequestService extends Actor implements IRepairRequestService
{
	// Model
	[Inject]
	public var model:RepairRequestModel;
	[Inject]
	public var repairRequestSuccessfulSend:RepairRequestSuccessfulSend;
	[Inject]
	public var restartRepairRequestSuccessfulSend:RestartRepairRequestSuccessfulSend;

	/**
	 * Return existing Repair requests
	 *
	 * If successful it fills the RepairRequestModel with the Result an dispatch an RepairRequestsLoaded Signal.
	 * If unseccessful it fills the RepairRequestsLoadFault with the error Message
	 *
	 * @param auth The Authentification for the Webservice
	 */
	public function getRepairRequests(auth:ServiceAuthVO):void {
		setTimeout(endServiceCall, 125);

		function endServiceCall():void {
			var repairRequests:Vector.<RepairRequestVO> = new Vector.<RepairRequestVO>();

			for (var i:int = 0; i < 20; i++) {
				repairRequests.push(new RepairRequestVO(JSON.parse('{"createDate": "2012-06-27T10:21:36","garage": {"id": 1937,"name": "' + i + ' Daimler AG"},"id": 100143778,"serviceCase": {"id": 1,"title": "Saisonaler Reifenwechsel"},"status": "neu"}')));
			}

			model.lastSearchResult = repairRequests;
		}
	}

	public function createRepairRequest(auth:ServiceAuthVO, request:RepairRequestVO):void {
		setTimeout(endServiceCall, 125);

		function endServiceCall():void {
			request.id = 123154;
			if(request.notes == "callGarage") {
				repairRequestSuccessfulSend.dispatch(request, true);
			} else {
				repairRequestSuccessfulSend.dispatch(request, false);
			}
		}
	}

	public function restartRepairRequest(auth:ServiceAuthVO, id:String):void {
		setTimeout(endServiceCall, 125);
		function endServiceCall():void {
			restartRepairRequestSuccessfulSend.dispatch();
		}
	}
}
}
