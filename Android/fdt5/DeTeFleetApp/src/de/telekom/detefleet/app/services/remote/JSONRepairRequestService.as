package de.telekom.detefleet.app.services.remote
{
import de.telekom.detefleet.app.events.RepairRequestSuccessfulSend;
import de.telekom.detefleet.app.events.RestartRepairRequestSuccessfulSend;
import de.telekom.detefleet.app.events.ServiceFaultSignal;
import de.telekom.detefleet.app.events.repairRequest.RepairRequestsLoadFault;
import de.telekom.detefleet.app.models.RepairRequestModel;
import de.telekom.detefleet.app.models.vo.RepairRequestVO;
import de.telekom.detefleet.app.models.vo.ServiceAuthVO;

import org.as3commons.logging.api.ILogger;
import org.as3commons.logging.api.getLogger;
import org.robotlegs.mvcs.Actor;

import flash.events.Event;
import flash.events.HTTPStatusEvent;
import flash.events.IEventDispatcher;
import flash.events.IOErrorEvent;
import flash.events.SecurityErrorEvent;
import flash.net.URLLoader;
import flash.net.URLRequest;
import flash.net.URLRequestHeader;
import flash.net.URLRequestMethod;



/**
 * Implementation for an Json Service to get some Garages.
 * It uses the JSONServiceConfiguration for getting Configuration
 * 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class JSONRepairRequestService extends Actor implements IRepairRequestService
{
	private static const logger:ILogger = getLogger(JSONRepairRequestService);

	// Model
	[Inject]
	public var model:RepairRequestModel;

	// Signals
	[Inject]
	public var repairRequestLoadFault:RepairRequestsLoadFault;
	[Inject]
	public var serviceFaultSignal:ServiceFaultSignal;
	[Inject]
	public var restartRepairRequestSuccessfulSend:RestartRepairRequestSuccessfulSend;
	[Inject]
	public var repairRequestSuccessfulSend:RepairRequestSuccessfulSend;


	private var sendRepairRequest:RepairRequestVO;

	/**
	 * Return existing Repair requests
	 * 
	 * If successful it fills the RepairRequestModel with the Result an dispatch an RepairRequestsLoaded Signal.
	 * If unseccessful it fills the RepairRequestsLoadFault with the error Message
	 * 
	 * @param auth The Authentification for the Webservice
	 */
	public function getRepairRequests(auth:ServiceAuthVO):void
	{
		var urlRequest:URLRequest = new URLRequest(JSONServiceConfiguration.endpointUrl + "/garage/getRepairRequests");
		// Content Type Header has to be send if the Request is JSON Type
		var header:URLRequestHeader = new URLRequestHeader("Content-Type", "application/json");
		var loader:URLLoader = new URLLoader();

		urlRequest.data = '{' + auth.toJSON() + '}';
		logger.debug("search called: " + urlRequest.data);

		urlRequest.method = URLRequestMethod.POST;
		urlRequest.requestHeaders.push(header);

		addListeners(loader);
		loader.addEventListener(Event.COMPLETE, getRepairRequestsCompleteHandler);
		loader.load(urlRequest);
	}

	/**
	 * Handles the successful Request.
	 * 
	 * @param event
	 * 
	 */
	protected function getRepairRequestsCompleteHandler(event:Event):void
	{
		var loader:URLLoader = URLLoader(event.target);
		logger.debug("Repair Requests loaded: " + loader.data);
		try {
			var data:Object = JSON.parse(loader.data);
		} catch(e:Error) {
			repairRequestLoadFault.dispatch("Couldn't parse JSON data.");
		}

		if (data.success) {
			var repairRequests:Vector.<RepairRequestVO> = new <RepairRequestVO>[];

			for ( var i:uint = 0; i < data.data.length; i++ ) {
				repairRequests[i] = new RepairRequestVO(data.data[i]);
			}

			model.lastSearchResult = repairRequests;
		} else {
			repairRequestLoadFault.dispatch(data.error);
		}

		loader.removeEventListener(Event.COMPLETE, getRepairRequestsCompleteHandler);
		removeListener(loader);
	}

	/**
	 * Creates and send a Repair Request
	 * 
	 * If successful it dispatch an RepairRequestSuccessful Signal.
	 * If unseccessful it fills the RepairRequestFault with the error Message
	 * 
	 * @param auth The Authentification for the Webservice
	 * @param request The RepairRequest which should be send
	 */
	public function createRepairRequest(auth:ServiceAuthVO, request:RepairRequestVO):void
	{
		var urlRequest:URLRequest = new URLRequest(JSONServiceConfiguration.endpointUrl + "/garage/createrepairrequest");
		// Content Type Header has to be send if the Request is JSON Type
		var header:URLRequestHeader = new URLRequestHeader("Content-Type", "application/json");
		var loader:URLLoader = new URLLoader();

		urlRequest.data = '{' + auth.toJSON() + ', "data":{' + request.toJSON() + '}}';
		logger.debug("createRepairRequest called: " + urlRequest.data);

		urlRequest.method = URLRequestMethod.POST;
		urlRequest.requestHeaders.push(header);

		addListeners(loader);
		
		sendRepairRequest = request;
		
		loader.addEventListener(Event.COMPLETE, createRepairRequestCompleteHandler);
		loader.load(urlRequest);
		
	}

	private function createRepairRequestCompleteHandler(event:Event):void
	{
		var loader:URLLoader = URLLoader(event.target);
		logger.debug("createRepairRequest loaded: " + loader.data);
		try {
			var data:Object = JSON.parse(loader.data);
		} catch(e:Error) {
			serviceFaultSignal.dispatch("Couldn't parse JSON data.");
		}

		if (data.success) {
			sendRepairRequest.id = data.data.repairRequestId;
			repairRequestSuccessfulSend.dispatch(sendRepairRequest, data.data.callGarage);
		} else {
			serviceFaultSignal.dispatch(data.error);
		}

		loader.removeEventListener(Event.COMPLETE, getRepairRequestsCompleteHandler);
		removeListener(loader);
		sendRepairRequest = null;
	}

	/**
	 * Restarts an already sended Repair Request
	 * 
	 * If successful it dispatch an RepairRequestSuccessful Signal.
	 * If unseccessful it fills the RepairRequestFault with the error Message
	 * 
	 * @param auth The Authentification for the Webservice
	 * @param id The Id of the RepairRequest which should be send again
	 */
	public function restartRepairRequest(auth:ServiceAuthVO, id:String):void
	{
		var urlRequest:URLRequest = new URLRequest(JSONServiceConfiguration.endpointUrl + "/garage/restartrepairrequest");
		// Content Type Header has to be send if the Request is JSON Type
		var header:URLRequestHeader = new URLRequestHeader("Content-Type", "application/json");
		var loader:URLLoader = new URLLoader();

		urlRequest.data = '{' + auth.toJSON() + ', "data":{"repairRequest":'+ id +'}}';
		logger.debug("restartRepairRequest called: " + urlRequest.data);

		urlRequest.method = URLRequestMethod.POST;
		urlRequest.requestHeaders.push(header);

		addListeners(loader);
		
		loader.addEventListener(Event.COMPLETE, restartRepairRequestCompleteHandler);
		loader.load(urlRequest);
	}

	private function restartRepairRequestCompleteHandler(event:Event):void
	{
		var loader:URLLoader = URLLoader(event.target);
		logger.debug("createRepairRequest loaded: " + loader.data);
		try {
			var data:Object = JSON.parse(loader.data);
		} catch(e:Error) {
			serviceFaultSignal.dispatch("Die Serverdaten konnten nicht verarbeitet werden.");
		}

		if (data.success) {
			restartRepairRequestSuccessfulSend.dispatch();
		} else {
			serviceFaultSignal.dispatch(data.error);
		}

		loader.removeEventListener(Event.COMPLETE, getRepairRequestsCompleteHandler);
		removeListener(loader);
	}


	/**
	 * Handles an Security error of the Url Request.
	 * Should be a Problem in Production
	 */
	protected function securityErrorHandler(event:SecurityErrorEvent):void
	{
		removeListener(URLLoader(event.target));
		logger.error(event);
	}

	/**
	 * Handles HTTP Status Response. If its bigger/equal than 400 dispatch an Error
	 */
	protected function httpResponseStatusHandler(event:HTTPStatusEvent):void
	{
		if (event.status >= 400) {
			serviceFaultSignal.dispatch("Der Server ist nicht erreichbar");
			logger.error("httpResponseStatusHandler: " + event);
		}
	}

	/**
	 * Handles the io Errors
	 */
	protected function ioErrorHandler(event:IOErrorEvent):void
	{
		removeListener(URLLoader(event.target));

		serviceFaultSignal.dispatch("Der Server ist nicht erreichbar");
		logger.error("ioErrorHandler: " + event);
	}

	/**
	 * Adds some basic Listeners
	 */
	protected function addListeners(dispatcher:IEventDispatcher):void
	{
		dispatcher.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
		dispatcher.addEventListener(HTTPStatusEvent.HTTP_RESPONSE_STATUS, httpResponseStatusHandler);
		dispatcher.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
	}

	/**
	 * Remove some Basic Listeners
	 */
	protected function removeListener(dispatcher:IEventDispatcher):void
	{
		dispatcher.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
		dispatcher.removeEventListener(HTTPStatusEvent.HTTP_RESPONSE_STATUS, httpResponseStatusHandler);
		dispatcher.removeEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
	}

}
}
