package de.telekom.detefleet.app.views.components.views
{

public class DummyView extends NavigationBarView
{

	override protected function createChildren():void
	{
		super.createChildren();
		_navbar.label = "Dummy View";
	}
}
}