package de.telekom.detefleet.app.models.vo
{
/**
 * Represents an Adress.
 * 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class AddressVO
{
	public var address:String;
	public var city:String;

	public var houseNumber:String;
	public var latitude:Number;
	public var longitude:Number;
	public var street:String;
	public var zip:String;
	
	/**
	 * Construct
	 * 
	 * @param object Anonymous Object which data will be set the properties of this object 
	 */
	public function AddressVO(object:Object = null)
	{
		if (object) {
			address = object.address;
			city = object.city;

			houseNumber = object.houseNumber;
			latitude = object.latitude;
			longitude = object.longitude;
			street = object.street;
			zip = object.zip;
		}
	}

}
}