package de.telekom.detefleet.app.events.repairRequest {
	import org.osflash.signals.Signal;

	/**
	 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
	 */
	public class GetRepairRequests extends Signal {
		public function GetRepairRequests() {
			super();
		}
	}
}
