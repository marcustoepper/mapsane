package de.telekom.detefleet.app.views.components.selectbox {
import de.aoemedia.mobile.ui.components.buttons.AbstractButton;
import de.aoemedia.mobile.ui.components.buttons.IconLabelButton;
import de.aoemedia.mobile.ui.components.skins.ButtonBitmaps;
import de.telekom.detefleet.app.assets.Color;
import de.telekom.detefleet.app.assets.IconRepository;
import de.telekom.detefleet.app.assets.ScaleBitmaps;
import de.telekom.detefleet.app.assets.Size;
import de.telekom.detefleet.app.assets.TextFormatTemplate;

import flash.display.Bitmap;

import org.osflash.signals.Signal;

public class SelectBoxButton extends IconLabelButton {
    protected var _prompt:String;
    protected var _selectedItem:Object;

    private var _textFieldWidth:Number = 100;

    protected var _dataProvider:Vector.<Object>;
    protected var _labelField:String = "label";

    public var signalSelectBoxListRequested:Signal;
    public var signalSelectionChanged:Signal;

    public function SelectBoxButton(prompt:String = "") {
        var sBmps:ScaleBitmaps = ScaleBitmaps.getInstance();
        var bmps:ButtonBitmaps = new ButtonBitmaps(sBmps.defaultButton,
                sBmps.defaultButtonDown,
                sBmps.defaultButtonDisabled,
                sBmps.defaultButtonDisabled);

        super(bmps);

        stopInvalidation = true;
        _leftLabelPadding = 12 * scaleFactor;
        _rightPadding = 14 * scaleFactor;
        isLabelCentered = false;

        var arrIcon:Bitmap = IconRepository.iconArrowDown;
        rightIcon = arrIcon;
        this.prompt = prompt;
        reset();
        labelColor = Color.HIGHLIGHT;
        labelColorDown = Color.DARK_GREY;
        labelColorDisabled = Color.MEDIUM_GREY;
        labelColorDownDisabled = Color.MEDIUM_GREY;
        setSize(Size.FORM_FIELD_RIGHT, 42);
        textFormat = TextFormatTemplate.getInstance().buttonNormal;
        stopInvalidation = false;

        signalSelectBoxListRequested = new Signal();
        signalSelectionChanged = new Signal(Object);
        signalClicked.add(dispatchSelectBoxListRequest);
    }

    override protected function createChildren():void {
        super.createChildren();

        labelComponent.shortenText = true;
        labelComponent.textField.width = scale(_textFieldWidth);
    }

    public override function set enabled(value:Boolean):void {
        if (value == _enabled) {
            return;
        }
        super.enabled = value;

        invalidate();
    }

    public function reset():void {
        label = _prompt;
    }

    public function get prompt():String {
        return _prompt;
    }

    public function set prompt(p:String):void {
        if (p == _prompt) return;
        var isPromptVisible:Boolean = label == _prompt;
        _prompt = p;
        if (isPromptVisible) reset();
    }

    public function get selectedItem():Object {
        return _selectedItem;
    }

    public function set selectedItem(item:Object):void {
        if (_selectedItem == item) {
            return;
        }
        _selectedItem = item;
        labelComponent.textField.width = scale(_textFieldWidth);
        label = item[_labelField];
        signalSelectionChanged.dispatch(item);
    }

    protected function dispatchSelectBoxListRequest(buttonRef:AbstractButton):void {
        signalSelectBoxListRequested.dispatch();
    }

    public function get labelField():String {
        return _labelField;
    }

    public function set labelField(labelField:String):void {
        _labelField = labelField;
    }

    public function get dataProvider():* {
        return _dataProvider;
    }

    public function set dataProvider(dataProvider:*):void {
        if (!(dataProvider is Array || dataProvider is Vector.<*>)) {
            throw new Error("dataProvider has to be an Array or Vector");
        }
        this._dataProvider = Vector.<Object>(dataProvider);
    }

    public function get textFieldWidth():Number {
        return _textFieldWidth;
    }

    public function set textFieldWidth(value:Number):void {
        _textFieldWidth = value;
    }
}
}
