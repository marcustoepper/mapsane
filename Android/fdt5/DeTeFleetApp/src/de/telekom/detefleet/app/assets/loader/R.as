package de.telekom.detefleet.app.assets.loader
{
	import flash.geom.Rectangle;
	import flash.display.BitmapData;

	public final class R
	{
		[Embed(source="../../../../../../../assets/gfx/xhdpi/icons/loader/loader.png")]
		public static var Sheet:Class;
		public static var sheet:BitmapData = new Sheet().bitmapData;
		
		/*/
		[Embed(source="back.png")]
		public static var Back:Class;
		public static var back:BitmapData = new Back().bitmapData;
		//*/
		
		public static var _0:Rectangle = new Rectangle(252,152,48,48);
		public static var _1:Rectangle = new Rectangle(202,152,48,48);
		public static var _2:Rectangle = new Rectangle(152,452,48,48);
		public static var _3:Rectangle = new Rectangle(152,402,48,48);
		public static var _4:Rectangle = new Rectangle(152,352,48,48);
		public static var _5:Rectangle = new Rectangle(152,302,48,48);
		public static var _6:Rectangle = new Rectangle(152,252,48,48);
		public static var _7:Rectangle = new Rectangle(152,202,48,48);
		public static var _8:Rectangle = new Rectangle(152,152,48,48);
		public static var _9:Rectangle = new Rectangle(452,102,48,48);
		public static var _10:Rectangle = new Rectangle(402,102,48,48);
		public static var _11:Rectangle = new Rectangle(352,102,48,48);
		public static var _12:Rectangle = new Rectangle(302,102,48,48);
		public static var _13:Rectangle = new Rectangle(252,102,48,48);
		public static var _14:Rectangle = new Rectangle(202,102,48,48);
		public static var _15:Rectangle = new Rectangle(152,102,48,48);
		public static var _16:Rectangle = new Rectangle(102,452,48,48);
		public static var _17:Rectangle = new Rectangle(102,402,48,48);
		public static var _18:Rectangle = new Rectangle(102,352,48,48);
		public static var _19:Rectangle = new Rectangle(102,302,48,48);
		public static var _20:Rectangle = new Rectangle(102,252,48,48);
		public static var _21:Rectangle = new Rectangle(102,202,48,48);
		public static var _22:Rectangle = new Rectangle(102,152,48,48);
		public static var _23:Rectangle = new Rectangle(102,102,48,48);
		public static var _24:Rectangle = new Rectangle(452,52,48,48);
		public static var _25:Rectangle = new Rectangle(402,52,48,48);
		public static var _26:Rectangle = new Rectangle(352,52,48,48);
		public static var _27:Rectangle = new Rectangle(302,52,48,48);
		public static var _28:Rectangle = new Rectangle(252,52,48,48);
		public static var _29:Rectangle = new Rectangle(202,52,48,48);
		public static var _30:Rectangle = new Rectangle(152,52,48,48);
		public static var _31:Rectangle = new Rectangle(102,52,48,48);
		public static var _32:Rectangle = new Rectangle(52,452,48,48);
		public static var _33:Rectangle = new Rectangle(52,402,48,48);
		public static var _34:Rectangle = new Rectangle(52,352,48,48);
		public static var _35:Rectangle = new Rectangle(52,302,48,48);
		public static var _36:Rectangle = new Rectangle(52,252,48,48);
		public static var _37:Rectangle = new Rectangle(52,202,48,48);
		public static var _38:Rectangle = new Rectangle(52,152,48,48);
		public static var _39:Rectangle = new Rectangle(52,102,48,48);
		public static var _40:Rectangle = new Rectangle(52,52,48,48);
		public static var _41:Rectangle = new Rectangle(452,2,48,48);
		public static var _42:Rectangle = new Rectangle(402,2,48,48);
		public static var _43:Rectangle = new Rectangle(352,2,48,48);
		public static var _44:Rectangle = new Rectangle(302,2,48,48);
		public static var _45:Rectangle = new Rectangle(252,2,48,48);
		public static var _46:Rectangle = new Rectangle(202,2,48,48);
		public static var _47:Rectangle = new Rectangle(152,2,48,48);
		public static var _48:Rectangle = new Rectangle(102,2,48,48);
		public static var _49:Rectangle = new Rectangle(52,2,48,48);
		public static var _50:Rectangle = new Rectangle(2,452,48,48);
		public static var _51:Rectangle = new Rectangle(2,402,48,48);
		public static var _52:Rectangle = new Rectangle(2,352,48,48);
		public static var _53:Rectangle = new Rectangle(2,302,48,48);
		public static var _54:Rectangle = new Rectangle(2,252,48,48);
		public static var _55:Rectangle = new Rectangle(2,202,48,48);
		public static var _56:Rectangle = new Rectangle(2,152,48,48);
		public static var _57:Rectangle = new Rectangle(2,102,48,48);
		public static var _58:Rectangle = new Rectangle(2,52,48,48);
		public static var _59:Rectangle = new Rectangle(2,2,48,48);
		public static var _60:Rectangle = new Rectangle(252,152,48,48);
	}
}