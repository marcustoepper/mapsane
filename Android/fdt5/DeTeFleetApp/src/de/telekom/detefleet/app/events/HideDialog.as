package de.telekom.detefleet.app.events
{
import de.telekom.detefleet.app.views.components.dialog.DialogView;

import org.osflash.signals.Signal;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class HideDialog extends Signal
{
	public function HideDialog()
	{
		super(DialogView);
	}
}
}
