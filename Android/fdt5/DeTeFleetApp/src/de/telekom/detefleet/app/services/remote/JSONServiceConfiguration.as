package de.telekom.detefleet.app.services.remote
{
public class JSONServiceConfiguration
{
	[Embed(source="../../../../../../../config/services.xml", mimeType="application/octet-stream")]
	protected static const SERVICES_XML:Class;
	protected static var _servicesXml:XML;

	protected static function get servicesXml():XML{
		if(_servicesXml == null){
			_servicesXml = XML(new SERVICES_XML());
		}
		return _servicesXml;
	}

	/**
	 * 
	 */
	public static function get endpointUrl():String {

		var definitionNode:String = 'channel-definition';
		var url:String = servicesXml.channels[definitionNode][0].endpoint[0].@url;

		return url;
	}

	/**
	 * Returns the domain
	 */
	public static function get domain():String {
		var url:String = endpointUrl;
		url = url.substring(0, url.indexOf("/", 9));

		return url;
	}
}
}
