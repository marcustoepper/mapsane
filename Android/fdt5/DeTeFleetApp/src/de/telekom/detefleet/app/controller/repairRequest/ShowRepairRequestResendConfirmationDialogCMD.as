package de.telekom.detefleet.app.controller.repairRequest
{
import de.telekom.detefleet.app.models.DialogModel;
import de.telekom.detefleet.app.models.LayoutData;
import de.telekom.detefleet.app.views.components.dialog.RepairRequestResendConfirmationDialogView;

import org.robotlegs.mvcs.Command;

/**
 *
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class ShowRepairRequestResendConfirmationDialogCMD extends Command
{
	[Inject]
	public var model:DialogModel;

	[Inject]
	public var layout:LayoutData;

	public override function execute():void {
		var dialog:RepairRequestResendConfirmationDialogView = new RepairRequestResendConfirmationDialogView(layout.viewport);
		model.addDialog(dialog);
	}
}
}
