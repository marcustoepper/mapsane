package de.telekom.detefleet.app.events.view {
	import org.osflash.signals.Signal;
	
	/**
	 * Show the View which is in NavigationModel.targetView 
	 * @author daniel.kopp
	 * 
	 */
	public class ShowView extends Signal
	{
		public function ShowView()
		{
			super();
		}
	}
}