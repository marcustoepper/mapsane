package de.telekom.detefleet.app.controller.security
{
	import de.telekom.detefleet.app.events.security.SSLCertificateInvalid;
	import de.telekom.detefleet.app.events.security.SSLCertificateValid;
	import de.telekom.detefleet.app.services.remote.JSONServiceConfiguration;
	import de.telekom.detefleet.app.services.security.CertificateVO;
	import de.telekom.detefleet.app.services.security.ISSLCertificateService;
	import de.telekom.detefleet.app.services.security.SSLCertificatesConfiguration;
	
	import org.robotlegs.mvcs.SignalCommand;
	
	public class CheckCertificateFingerprintCMD extends SignalCommand
	{
		[Inject]
		public var service:ISSLCertificateService;
		
		[Inject]
		public var signalValid:SSLCertificateValid;
		
		[Inject]
		public var signalInvalid:SSLCertificateInvalid;
		
		override public function execute():void
		{
			var domainToCheck:String = JSONServiceConfiguration.domain;
			trace("domainToCheck: " + domainToCheck);
			
			var referenceCerts:Vector.<CertificateVO>
					= SSLCertificatesConfiguration.getCertificatesByHost(domainToCheck);
					
			if ( referenceCerts != null )
			{
				var receivedCerts:Vector.<CertificateVO> = service.getCertificates();
				
				for ( var i:uint = 0; i < receivedCerts.length; i++ )
				{
					for ( var j:uint = 0; j < referenceCerts.length; j++ )
					{
						if ( referenceCerts[j].fingerprint == receivedCerts[i].fingerprint )
						{
							signalValid.dispatch();
							return;
						}
					}
				}
			}
			
			signalInvalid.dispatch();
		}
	}
}
