package de.telekom.detefleet.app.views.mediators
{
import de.telekom.detefleet.app.events.CallPhone;
import de.telekom.detefleet.app.events.CloseDialog;
import de.telekom.detefleet.app.views.components.dialog.CallPhoneDialogView;

import org.robotlegs.mvcs.Mediator;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class CallPhoneDialogMediator extends Mediator
{
	[Inject]
	public var view:CallPhoneDialogView;

	[Inject]
	public var call:CallPhone;

	[Inject]
	public var closeDialog:CloseDialog;

	public override function onRegister():void {
		view.okClicked.add(handleOk);
		view.cancelClicked.add(handleCancel);
	}

	private function handleOk():void {
		closeDialog.dispatch(view);
		call.dispatch(view.phone);
	}

	protected function handleCancel():void {
		closeDialog.dispatch(view);
	}

	public override function preRemove():void {
		view.okClicked.remove(handleOk);
		view.cancelClicked.remove(handleCancel);
		super.preRemove();
	}

}
}
