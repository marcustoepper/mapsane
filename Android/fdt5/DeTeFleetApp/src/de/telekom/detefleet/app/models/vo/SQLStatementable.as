package de.telekom.detefleet.app.models.vo {
	import flash.data.SQLStatement;

	public interface SQLStatementable
	{
		function updateStatement():String;
		function insertStatement():String;
		function setStatementParameters(statement:SQLStatement):void;
		function selectStatement():String;
		function fillStatementResult(row:Object):void;
	}
}