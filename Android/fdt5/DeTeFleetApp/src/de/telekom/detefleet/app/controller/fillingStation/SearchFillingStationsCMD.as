package de.telekom.detefleet.app.controller.fillingStation
{
import de.telekom.detefleet.app.controller.AuthCommand;
import de.telekom.detefleet.app.models.vo.FillingStationRequestVO;
import de.telekom.detefleet.app.services.remote.IFillingStationService;

import org.as3commons.logging.api.ILogger;
import org.as3commons.logging.api.getLogger;

public class SearchFillingStationsCMD extends AuthCommand
{
	[Inject]
	public var service:IFillingStationService;

	[Inject]
	public var request:FillingStationRequestVO;

	private static const logger:ILogger = getLogger(SearchFillingStationsCMD);

	public override function execute():void {
		service.search(auth, request);
	}
}
}
