package de.telekom.detefleet.app.controller.view
{
import de.telekom.detefleet.app.models.ViewsModel;
import de.telekom.detefleet.app.views.components.views.IView;

import org.robotlegs.mvcs.SignalCommand;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class AddReadyViewCMD extends SignalCommand
{

	[Inject]
	public var viewsModel:ViewsModel;

	[Inject]
	public var view:IView;

	public override function execute():void
	{
		viewsModel.addReadyView(view);
	}
}
}
