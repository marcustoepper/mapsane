package de.telekom.detefleet.app.controller
{
import de.telekom.detefleet.app.services.remote.IGarageService;

/**
 * Send a request to get Vehicle Service Cases
 *
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class GetVehicleServiceCasesCMD extends AuthCommand
{
	[Inject]
	public var service:IGarageService;

	public override function execute():void {
		service.getVehicleServiceCases(auth);
	}
}
}
