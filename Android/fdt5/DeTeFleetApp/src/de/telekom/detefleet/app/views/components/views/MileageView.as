package de.telekom.detefleet.app.views.components.views {
import com.kaizimmer.events.ValidationEvent;
import com.kaizimmer.ui.Screen;
import com.kaizimmer.ui.form.FormFieldType;
import com.kaizimmer.ui.form.FormValidator;
import com.kaizimmer.ui.form.IFormObject;

import de.aoemedia.mobile.ui.MultiDpiHelper;
import de.aoemedia.mobile.ui.components.LabelComponent;
import de.aoemedia.mobile.ui.components.buttons.AbstractButton;
import de.aoemedia.mobile.ui.components.container.LayoutContainer;
import de.aoemedia.mobile.ui.components.forms.FormField;
import de.aoemedia.mobile.ui.components.forms.StageInputField;
import de.telekom.detefleet.app.assets.TextFormatTemplate;
import de.telekom.detefleet.app.views.components.buttons.actionBar.OkButton;
import de.telekom.detefleet.app.views.components.form.StageInputFieldWrapper;

import flash.globalization.DateTimeFormatter;
import flash.globalization.LocaleID;
import flash.text.SoftKeyboardType;
import flash.text.TextFieldAutoSize;

import org.osflash.signals.Signal;

/**
 * Show the Mileage Field
 *
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class MileageView extends NavigationBarView {
    protected var mileageFormField:FormField;
    protected var mileageFormViewValidator:FormValidator;

    public var confirmButtonClicked:Signal;

    protected var mileageInputField:StageInputFieldWrapper;

    protected var container:LayoutContainer;
    protected var confirmButton:OkButton;
    protected var lastUpdateLabel:LabelComponent;

    protected var lastUpdateFormatter:DateTimeFormatter;
    private var currentLocationInfoText:LabelComponent;

    /**
     * Construct
     */
    public function MileageView() {
        super("Kilometerstand");

        confirmButtonClicked = new Signal(String);

        lastUpdateFormatter = new DateTimeFormatter(LocaleID.DEFAULT);
        lastUpdateFormatter.setDateTimePattern("dd.MM.yyyy HH:mm:ss");
    }

    /**
     * Create Children
     */
    override protected function createChildren():void {
        super.createChildren();

        confirmButton = new OkButton();
        confirmButton.setSize(39, 39);
        confirmButton.signalClicked.add(handleConfirmButtonClick);
        disableConfirmButton();
        _navbar.addActionButton(confirmButton);

        container = new LayoutContainer(this);
        container.stopInvalidation = true;
        container.y = navbar.height + scaleFactor*8; // TODO remove hardcoded margin
        container.explicitWidth = Screen.stage.stageWidth;
        container.paddingTop = 11;
        container.paddingLeft = 11;
        container.paddingRight = 11;
        container.paddingBottom = 11;
        container.gap = 15;

        mileageInputField = new StageInputFieldWrapper(this);
        mileageInputField.label = "Aktueller Kilometerstand:";
        mileageInputField.percentalWidth = 100;
        mileageInputField.softKeyboardType = SoftKeyboardType.NUMBER;
        StageInputField(mileageInputField.inputField).stageTextField.stageText.maxChars = 7;

        container.addItem(mileageInputField);

        lastUpdateLabel = new LabelComponent("letzte Aktualisierung:", TextFormatTemplate.getInstance().extraInformation);
        container.addItem(lastUpdateLabel);

        currentLocationInfoText = new LabelComponent();
        currentLocationInfoText.textField.text = 'Sie haben Ihr Auto gerade getankt, stehen an der Kasse der Tankstelle und haben den Kilometerstand vergessen? Tragen Sie hier einfach zur Erinnerung Ihren aktuellen Kilometerstand ein.';
        currentLocationInfoText.textField.embedFonts = true;
        currentLocationInfoText.textFormat = TextFormatTemplate.getInstance().content;
        currentLocationInfoText.textField.multiline = true;
        currentLocationInfoText.textField.wordWrap = true;
        currentLocationInfoText.disableScrolling();
        currentLocationInfoText.textField.autoSize = TextFieldAutoSize.LEFT;
        currentLocationInfoText.textField.width = Screen.stage.stageWidth - scale(11 + 11);
        currentLocationInfoText.disableScrolling();
        container.addItem(currentLocationInfoText);

        container.stopInvalidation = false;
        container.invalidate();
        container.addItemsToView();

        _stageInputFields.push(mileageInputField);

        initForms();

    }

    /**
     *
     */
    public function updateLastUpdate(lastUpdate:Date):void {
        lastUpdateLabel.label = "letzte Aktualisierung: ";

        if (lastUpdate) {
            lastUpdateLabel.label += lastUpdateFormatter.format(lastUpdate);
        }

        container.invalidate();
        container.addItemsToView();
    }

    /**
     *
     */
    public function updateMileage(mileage:String):void {
        StageInputField(mileageInputField.inputField).stageTextField.text = mileage;
    }

    /**
     * Handler if ok Button in ActionBar is clicked
     */
    protected function handleConfirmButtonClick(button:AbstractButton):void {
        confirmButtonClicked.dispatch(StageInputField(mileageInputField.inputField).stageTextField.text);
    }

    /**
     * Enables Confirm Button in Actionbar
     */
    protected function enableConfirmButton():void {
        confirmButton.enabled = true;
        confirmButton.visible = true;
    }

    /**
     * Disables Confirm Button in Actionbar
     */
    protected function disableConfirmButton():void {
        confirmButton.enabled = false;
        confirmButton.visible = false;
    }

    /**
     * Initialize Form
     */
    protected function initForms():void {
        mileageFormViewValidator = new FormValidator();

        mileageFormField = new FormField(mileageInputField.inputField, "", true, FormFieldType.NUMBERS, 1, 7);
        mileageFormField.defaultText = "";
        mileageFormField.liveChecking = true;
        mileageFormField.preventLiveCheckRendering = true;

        mileageFormViewValidator.addFormObject(mileageFormField);
        mileageFormViewValidator.addEventListener(ValidationEvent.CHANGE, onFormValidityChanged);
    }

    /**
     *
     */
    protected function onFormValidityChanged(e:ValidationEvent):void {
        if (e.isValid) {
            enableConfirmButton();
        } else {
            disableConfirmButton();
        }
    }

    /**
     *
     */
    protected function highlightBadLoginFormViewObjects():void {
        var invalids:Array = mileageFormViewValidator.invalidFormObjects;
        for (var i:uint = 0; i < invalids.length; i++) {
            highlightBadFormObject(IFormObject(invalids[i]));
        }
    }

    /**
     * Highlights the IFormObject's corresponding marker, not the IFormObject itself!
     */
    protected function highlightBadFormObject(o:IFormObject):void {
        switch (o) {
            case mileageFormField:
                break;
        }
    }

    /**
     *
     */
    public function enableView(enable:Boolean):void {
        mileageInputField.enabled = enable;
        if (enable && mileageFormViewValidator.isFormValid) {
            enableConfirmButton();
        } else {
            disableConfirmButton();
        }
    }

    public function reset():void {
        mileageFormField.reset();
        mileageInputField.inputField.normalize();

        enableView(true);
    }

}
}