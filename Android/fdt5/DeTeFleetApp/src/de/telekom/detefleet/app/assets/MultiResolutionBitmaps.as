package de.telekom.detefleet.app.assets {
import com.kaizimmer.ui.Screen;

import de.aoemedia.mobile.ui.components.BitmapComponent;

import flash.utils.Dictionary;

/**
 *
 */
public class MultiResolutionBitmaps {
    protected static var bitmaps:Dictionary = new Dictionary();

    /**
     *
     */
    public static function getBitmapByResolution(xhdpi:Class, hdpi:Class, mdpi:Class):BitmapComponent {
        var bitmapComponent:BitmapComponent;

        switch (Screen.dpiClass) {
            case Screen.AT2X:
            case Screen.XHDPI:
                if (bitmaps[xhdpi] == null) {
                    bitmaps[xhdpi] = new xhdpi();
                }
                bitmapComponent = new BitmapComponent(bitmaps[xhdpi].bitmapData, bitmaps[xhdpi].pixelSnapping, bitmaps[xhdpi].smoothing);
                break;
            case Screen.HDPI:
                if (bitmaps[hdpi] == null) {
                    bitmaps[hdpi] = new hdpi();
                }
                bitmapComponent = new BitmapComponent(bitmaps[hdpi].bitmapData, bitmaps[hdpi].pixelSnapping, bitmaps[hdpi].smoothing);
                break;
            case Screen.AT1X:
            case Screen.LDPI:
            default:
                if (bitmaps[mdpi] == null) {
                    bitmaps[mdpi] = new mdpi();
                }
                bitmapComponent = new BitmapComponent(bitmaps[mdpi].bitmapData, bitmaps[mdpi].pixelSnapping, bitmaps[mdpi].smoothing);
                break;
        }

        bitmapComponent.scaleFactor = Screen.scaleFactor;
        return bitmapComponent;
    }
}
}