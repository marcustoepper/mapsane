package de.telekom.detefleet.app.views.components.views
{
import com.greensock.TimelineLite;
import com.greensock.TweenAlign;
import com.greensock.TweenLite;
import com.greensock.easing.Sine;
import com.kaizimmer.ui.Screen;

import de.aoemedia.motion.TransitionType;
import de.telekom.detefleet.app.models.vo.TransitionStateVO;

import flash.display.DisplayObject;
import flash.display.Sprite;
import flash.events.Event;
import flash.geom.Matrix;
import flash.geom.PerspectiveProjection;
import flash.geom.Point;

import org.osflash.signals.Signal;

/**
 * @author Kai Zimmer <kai.zimmer@aoemedia.de>, Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class AppContainer extends Sprite
{
	public var transitionChanged:Signal;
	public var transitionCompleted:Signal;

	protected const identityMatrix:Matrix = new Matrix();
	protected const SLIDE_TWEEN_TIME:Number = .75;
	protected const TURN_TWEEN_TIME:Number = .75;

	protected var loaderView:LoaderView;

	public function AppContainer() {
		transitionCompleted = new Signal();
		transitionChanged = new Signal();
	}

	public function addView(child:DisplayObject):DisplayObject {
		child.visible = false;
		return addChild(child);
	}

	public function addViewAt(child:DisplayObject, index:int):DisplayObject {
		child.visible = false;
		return addChildAt(child, index);
	}

	public function showLoader():void {
		if(loaderView == null) {
			loaderView = new LoaderView();
		}
		addChild(DisplayObject(loaderView));
		mouseChildren = false;
	}

	public function hideLoader():void {
		if(!contains(DisplayObject(loaderView))) {
			return;
		}
		removeChild(DisplayObject(loaderView));
		mouseChildren = true;
	}

	public function startPageTransition(viewToShow:IView, viewToHide:IView, transitionType:String = TransitionType.SLIDE_LEFT):void {
		trace("startPageTransition - transitionType: " + transitionType + " | viewToHide: " + viewToHide);

		const TRANSITION_HEIGHT:Number = Math.floor(Screen.referenceHeight * Screen.scaleFactor);

        if(viewToShow is NavigationBarView){
            NavigationBarView(viewToShow).onShow();
        }

		switch (transitionType) {
			case TransitionType.FLIP_LEFT:
			case TransitionType.FLIP_RIGHT:
				var offset:Number = stage.stageWidth >> 1;
				x = offset;
				viewToShow.componentSprite.y = 0;
				viewToShow.componentSprite.x = offset;
				viewToHide.componentSprite.x = -offset;
				viewToShow.componentSprite.rotationY = 180;
				// make sure the new view is covered by the current visible view 
				addChildAt(viewToShow.componentSprite, 0);
				break;
			case TransitionType.SLIDE_UP:
				viewToShow.componentSprite.x = 0;
				viewToShow.componentSprite.y = TRANSITION_HEIGHT;//viewToShow.componentSprite.height;
				break;
			case TransitionType.SLIDE_DOWN:
				viewToShow.componentSprite.x = 0;
				viewToShow.componentSprite.y = -TRANSITION_HEIGHT;//-viewToShow.componentSprite.height;
				break;
			case TransitionType.SLIDE_LEFT:
				viewToShow.componentSprite.y = 0;
				viewToShow.componentSprite.x = viewToShow.componentSprite.width;
				break;
			case TransitionType.SLIDE_RIGHT:
			default:
				viewToShow.componentSprite.y = 0;
				viewToShow.componentSprite.x = -viewToShow.componentSprite.width;
		}

		viewToShow.componentSprite.visible = true;

		if(!viewToShow.componentSprite.cacheAsBitmap) {
			viewToShow.componentSprite.cacheAsBitmap = true;
			viewToShow.componentSprite.cacheAsBitmapMatrix = identityMatrix;
		}
		viewToHide.componentSprite.cacheAsBitmap = true;
		viewToHide.componentSprite.cacheAsBitmapMatrix = identityMatrix;

		mouseEnabled = false;
		mouseChildren = false;

		var tween:TweenLite;

		var originalX:Number = 0;//this.x;
		var _this:AppContainer = this;
		var transitionDescription:Object;

		transitionChanged.dispatch(new TransitionStateVO(TransitionStateVO.TRANSITIONING, viewToHide, viewToShow));

		switch (transitionType) {
			case TransitionType.FLIP_LEFT:
			case TransitionType.FLIP_RIGHT:
				//*/
				var pp1:PerspectiveProjection = new PerspectiveProjection();
				pp1.fieldOfView = 60;
				pp1.projectionCenter = new Point(width / 2, height / 2);
				transform.perspectiveProjection = pp1;
				//*/
				var turn:TimelineLite = new TimelineLite();
				turn.appendMultiple(
						[
							new TweenLite(this, TURN_TWEEN_TIME / 2, { rotationY:70, ease:Sine.easeIn, onComplete:swap }),
							//new TweenLite(this, .001, { frameLabel: "back" } ),
							new TweenLite(this, TURN_TWEEN_TIME / 2, { rotationY:180, ease:Sine.easeOut, onComplete:onTurnComplete })
						], 0, TweenAlign.SEQUENCE);

				//turn.paused = true;
				break;
			case TransitionType.SLIDE_DOWN:
				transitionDescription =
				{
					y:TRANSITION_HEIGHT, //viewToHide.componentSprite.height,
					ease:Sine.easeInOut,
					onComplete:onComplete
				};
				tween = new TweenLite(this, SLIDE_TWEEN_TIME, transitionDescription);
				break;
			case TransitionType.SLIDE_UP:
				transitionDescription =
				{
					y:-TRANSITION_HEIGHT, //-viewToHide.componentSprite.height,
					ease:Sine.easeInOut,
					onComplete:onComplete
				};
				tween = new TweenLite(this, SLIDE_TWEEN_TIME, transitionDescription);
				break;
			case TransitionType.SLIDE_LEFT:
			case TransitionType.SLIDE_RIGHT:
			default:
				transitionDescription =
				{
					x:this.x - viewToShow.componentSprite.x,
					ease:Sine.easeInOut,
					onComplete:onComplete
				};
				tween = new TweenLite(this, SLIDE_TWEEN_TIME, transitionDescription);
		}

		function swap():void {
			trace("swap");
			viewToHide.componentSprite.visible = false;
			swapChildren(viewToShow.componentSprite, viewToHide.componentSprite);
		}

		function onTurnComplete():void {
			trace("AppContainer - onTurnComplete");

			// enable views again
			viewToShow.componentSprite.x = 0;
			viewToShow.componentSprite.y = 0;
			viewToShow.componentSprite.rotationY = 0;
			_this.x = originalX;
			_this.y = 0;
			//_this.rotationY = 0; //BREAKS IOS6
			viewToHide.componentSprite.visible = false;

			viewToShow.componentSprite.cacheAsBitmap = false;
			viewToShow.componentSprite.cacheAsBitmapMatrix = null;
			_this.mouseEnabled = true;
			_this.mouseChildren = true;

			transitionChanged.dispatch(new TransitionStateVO(TransitionStateVO.OUT, viewToHide, viewToShow));
			transitionChanged.dispatch(new TransitionStateVO(TransitionStateVO.IN, viewToHide, viewToShow));
			
			transitionCompleted.dispatch(new TransitionStateVO(TransitionStateVO.OUT, viewToHide, viewToShow));
		}

		function onComplete():void {
			// enable views again
			viewToShow.componentSprite.x = 0;
			viewToShow.componentSprite.y = 0;
			_this.x = originalX;
			_this.y = 0;
			//_this.rotationY = 0; //BREAKS IOS6
			viewToHide.componentSprite.visible = false;

			viewToShow.componentSprite.cacheAsBitmap = false;
			viewToShow.componentSprite.cacheAsBitmapMatrix = null;
			_this.mouseEnabled = true;
			_this.mouseChildren = true;

            if(viewToHide is NavigationBarView){
                NavigationBarView(viewToHide).onHide();
            }
			
			transitionChanged.dispatch(new TransitionStateVO(TransitionStateVO.OUT, viewToHide, viewToShow));
			transitionChanged.dispatch(new TransitionStateVO(TransitionStateVO.IN, viewToHide, viewToShow));
			
			transitionCompleted.dispatch(new TransitionStateVO(TransitionStateVO.OUT, viewToHide, viewToShow));
		}
	}

}
}
