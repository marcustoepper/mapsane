package de.telekom.detefleet.app.views.components.views {
import com.kaizimmer.ui.Screen;

import de.aoemedia.mobile.ui.components.buttons.AbstractButton;
import de.telekom.detefleet.app.assets.TextFormatTemplate;
import de.telekom.detefleet.app.models.vo.RepairRequestVO;
import de.telekom.detefleet.app.views.components.buttons.NewRepairRequestButton;
import de.telekom.detefleet.app.views.components.list.RepairRequestList;
import de.telekom.detefleet.app.views.components.listItem.RepairRequestListStamp;

import flash.display.DisplayObject;

import org.osflash.signals.Signal;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class RepairRequestHistoryView extends NavigationBarView {

    public var resendClicked:Signal;

    protected var repairRequestsList:RepairRequestList;

    // Callbacks
    public var newRepairRequestClickedCallback:Function;

    public function RepairRequestHistoryView() {
        super("Reparaturanstoß");

        resendClicked = new Signal(RepairRequestVO);
    }

    /**
     *
     *
     */
    override protected function createChildren():void {
        super.createChildren();

        navbar.textFormat = TextFormatTemplate.getInstance().actionbarSmall;

        var newRepairButton:NewRepairRequestButton = new NewRepairRequestButton("Neuer Reparaturanstoß");
        newRepairButton.setSize(320, 60);
        newRepairButton.y = navbar.height + scale(11);
        newRepairButton.signalClicked.add(newRepairRequestClicked);
        addChild(DisplayObject(newRepairButton));

        var listHeightMaximized:Number = Screen.stage.stageHeight - scale(newRepairButton.height - 11);

        repairRequestsList = new RepairRequestList(Screen.stage.stageWidth, listHeightMaximized);
        repairRequestsList.resendClicked.add(resendClickedProxy);

        repairRequestsList.itemRenderer = RepairRequestListStamp;
        repairRequestsList.visible = false;
        var object:RepairRequestListStamp = new RepairRequestListStamp();
        repairRequestsList.rowHeight = object.itemHeight;
        object = null;

        repairRequestsList.visualListItemBuffer = 4;
        repairRequestsList.maxTapDelayTime = .5;
        repairRequestsList.y = newRepairButton.y + newRepairButton.height;

        addChildAt(DisplayObject(repairRequestsList), 0);
    }

    private function resendClickedProxy(repairRequest:RepairRequestVO):void {
        resendClicked.dispatch(repairRequest);
    }

    protected function newRepairRequestClicked(button:AbstractButton):void {
        newRepairRequestClickedCallback();
    }

    /**
     *
     *
     * @param requests
     */
    public function populateRepairRequests(requests:Vector.<RepairRequestVO>):void {
        if (requests == null || requests.length == 0) {
            return;
        }

        repairRequestsList.visible = false;

        repairRequestsList.removeAllListItems();
        var itemDescriptions:Array = [];
        for (var i:uint = 0; i < requests.length; i++) {
            itemDescriptions[i] = requests[i];
        }

        if (requests.length > 0) {
            repairRequestsList.visible = true;
            repairRequestsList.dataProvider = itemDescriptions;
        } else {
            // TODO Nothing found
        }
    }

    public function unselectAll():void {
        repairRequestsList.unselectAll();
    }

}
}
