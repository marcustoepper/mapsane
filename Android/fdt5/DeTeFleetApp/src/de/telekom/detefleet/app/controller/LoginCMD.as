package de.telekom.detefleet.app.controller
{
import de.telekom.detefleet.app.models.UserModel;
import de.telekom.detefleet.app.models.vo.LoginFormData;
import de.telekom.detefleet.app.services.local.IDeviceKey;
import de.telekom.detefleet.app.services.persistence.IKeyChainService;
import de.telekom.detefleet.app.services.remote.ILoginService;

import org.robotlegs.mvcs.Command;

/**
 * Login User
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class LoginCMD extends Command
{
	[Inject]
	public var keyChainService:IKeyChainService;

	[Inject]
	public var loginFormData:LoginFormData;

	[Inject]
	public var loginService:ILoginService;

	[Inject]
	public var deviceKeyService:IDeviceKey;

	[Inject]
	public var userModel:UserModel;

	public override function execute():void {
		var key:String = keyChainService.getItem("deviceKey");
		if(key == null) {
			key = deviceKeyService.getKey();
			keyChainService.setItem("deviceKey", key);
		}

		userModel.storeAuthToken = loginFormData.storePassword;

		loginService.login(loginFormData.user, loginFormData.password, key);
	}
}

}
