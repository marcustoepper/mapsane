package de.telekom.detefleet.app.events.garage
{
import de.telekom.detefleet.app.events.ServiceFaultSignal;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class GarageLoadFault extends ServiceFaultSignal
{
}
}
