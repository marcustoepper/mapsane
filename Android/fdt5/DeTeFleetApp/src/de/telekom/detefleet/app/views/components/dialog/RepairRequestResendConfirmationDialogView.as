package de.telekom.detefleet.app.views.components.dialog
{
import de.aoemedia.mobile.ui.components.buttons.AbstractButton;
import de.telekom.detefleet.app.assets.Color;
import de.telekom.detefleet.app.assets.TextFormatTemplate;
import de.telekom.detefleet.app.views.components.buttons.DefaultLabelButton;

import flash.display.DisplayObject;
import flash.geom.Rectangle;
import flash.text.TextFormat;

import org.osflash.signals.Signal;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class RepairRequestResendConfirmationDialogView extends DialogView
{
	protected const BUTTON_PADDING_BOTTOM:uint = 11;

	protected var okButton:DefaultLabelButton;
	protected var cancelButton:DefaultLabelButton;

	public var okClicked:Signal;
	public var cancelClicked:Signal;
	private var _data:Object;

	public function RepairRequestResendConfirmationDialogView(viewport:Rectangle) {
		super(viewport, "Reparaturanstoß senden", "Möchten Sie den Reparaturanstoß erneut senden?");

		okClicked = new Signal();
		cancelClicked = new Signal();
	}

    override protected function invalidate():void{
        super.invalidate();

		okButton.setSize(126, 44);
		cancelButton.setSize(126, 44);

		var buttonGap:Number = scale(5);

		okButton.x = containerWidth / 2 - buttonGap / 2 - okButton.width;
		okButton.y = messageTextField.y + messageTextField.height + scale(BUTTON_PADDING_BOTTOM);

		cancelButton.x = containerWidth / 2 + buttonGap / 2;
		cancelButton.y = messageTextField.y + messageTextField.height + scale(BUTTON_PADDING_BOTTOM);

        resetContainerPosition();
        invalidateBackground();
	}

	override protected function createChildren():void {
		super.createChildren();

		var tFormat:TextFormat = TextFormatTemplate.getInstance().dialogButton;

		okButton = new DefaultLabelButton("OK");
		okButton.textFormat = tFormat;
		okButton.labelColor = Color.MEDIUM_GREY;
		okButton.labelColorDisabled = Color.DARK_GREY;
		okButton.signalClicked.add(okButtonClicked);
		container.addChild(DisplayObject(okButton));

		cancelButton = new DefaultLabelButton("Abbrechen");
		cancelButton.textFormat = tFormat;
		cancelButton.labelColor = Color.MEDIUM_GREY;
		cancelButton.labelColorDisabled = Color.DARK_GREY;
		cancelButton.signalClicked.add(cancelButtonClicked);
		container.addChild(DisplayObject(cancelButton));
	}

	private function okButtonClicked(button:AbstractButton):void {
		okClicked.dispatch();
	}

	private function cancelButtonClicked(button:AbstractButton):void {
		cancelClicked.dispatch();
	}

	public function okButtonLabel(label:String):void {
		okButton.label = label;
	}

	public function cancelButtonLabel(label:String):void {
		cancelButton.label = label;
	}

	public function get data():Object {
		return _data;
	}

	public function set data(value:Object):void {
		_data = value;
	}
}
}
