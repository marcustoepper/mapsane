package de.telekom.detefleet.app.events.garage
{
import de.telekom.detefleet.app.models.vo.VehicleServiceCaseVO;

import org.osflash.signals.Signal;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class VehicleServiceCasesChanged extends Signal
{
	public function VehicleServiceCasesChanged()
	{
		super(Vector.<VehicleServiceCaseVO>);
	}
}
}
