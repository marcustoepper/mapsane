package de.telekom.detefleet.app.services.remote
{
import de.telekom.detefleet.app.models.GarageModel;
import de.telekom.detefleet.app.models.vo.GarageSearchRequestVO;
import de.telekom.detefleet.app.models.vo.GarageVO;
import de.telekom.detefleet.app.models.vo.ServiceAuthVO;
import de.telekom.detefleet.app.models.vo.VehicleServiceCaseVO;

import flash.utils.setTimeout;

import org.robotlegs.mvcs.Actor;

/**
 * Implementation for an Mock Service to get some Garages.
 * It returns only some Dummy Data to test locally.
 *
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class MockGarageService extends Actor implements IGarageService
{
	// Model
	[Inject]
	public var model:GarageModel;

	/**
	 * Search for Garages.
	 *
	 * If successful it fills the GarageModel.lastSearchResult  with the Result an disptach an GaragesLoaded Signal.
	 * If unseccessful it fills the GarageLoadFault with the error Message
	 *
	 * @param auth The Authentification for the Webservice
	 * @param request The request for the Search
	 *
	 */
	public function search(auth:ServiceAuthVO, request:GarageSearchRequestVO):void {
		setTimeout(endServiceCall, 125);

		function endServiceCall():void {
			var garages:Vector.<GarageVO> = new Vector.<GarageVO>();
			if(request.location.address != "empty") {
				garages.push(new GarageVO(JSON.parse(' {"id":"1", "phone": "0123456", "address": {"address": "Bosenheimer Str. 45 a - 77, 55543 Bad Kreuznach", "city": "Bad Kreuznach", "houseNumber": "", "latitude": 49.84199, "longitude": 7.86668,    "street": "Bosenheimer Str. 45 a - 77",    "zip": "55543"},"distance": 3.438,"name": "A.T.U. Auto Teile Unger GmbH und Co KG","supportedBrands": [    {"brand": "DUMMY BRAND","id": 1,"logo": "http://backend.communicar.de/fileadmin/communicar/images/Logos/audi_logo_100.jpg"    },    {"brand": "DUMMY BRAND","id": 6,"logo": "http://backend.communicar.de/fileadmin/communicar/images/Logos/vw-100.jpg"    },    {"brand": "DUMMY BRAND","id": 14,"logo": "http://backend.communicar.de/fileadmin/communicar/images/Logos/dummy_logo_image.jpg"    },    {"brand": "DUMMY BRAND", "id": 20, "logo": "http://backend.communicar.de/fileadmin/communicar/images/Logos/dummy_logo_image.jpg"}],"type": 1}')));
				for (var i:int = 0; i < 20; i++) {
					garages.push(new GarageVO(JSON.parse(' {"id":"' + (i + 2) + '","phone": "0123456", "address": {"address": "Bosenheimer Str. 45 a - 77, 55543 Bad Kreuznach", "city": "Bad Kreuznach", "houseNumber": "", "latitude": 49.84199, "longitude": 7.86668,    "street": "Bosenheimer Str. 45 a - 77",    "zip": "55543"},"distance": 3.438,"name": "Fleischhauer Autozentrum","supportedBrands": [    {"brand": "DUMMY BRAND","id": 1,"logo": "http://backend.communicar.de/fileadmin/communicar/images/Logos/audi_logo_100.jpg"    },    {"brand": "DUMMY BRAND","id": 6,"logo": "http://backend.communicar.de/fileadmin/communicar/images/Logos/vw-100.jpg"    },    {"brand": "DUMMY BRAND","id": 14,"logo": "http://backend.communicar.de/fileadmin/communicar/images/Logos/dummy_logo_image.jpg"    },    {"brand": "DUMMY BRAND","id": 20,"logo": "http://backend.communicar.de/fileadmin/communicar/images/Logos/dummy_logo_image.jpg"    }],"type": 0}')));
				}
			}

			model.searchResult = garages;
		}
	}

	/**
	 * Ask the Service for existing Vehicle Service Cases
	 *
	 * If successful it fills the GarageModel.serviceCases with the Result an disptach an ServiceCasesLoaded Signal.
	 * If unseccessful it fills the ServiceCasesLoadFault with the error Message
	 *
	 * @param auth The Authentification for the Webservice
	 */
	public function getVehicleServiceCases(auth:ServiceAuthVO):void {
		setTimeout(endServiceCall, 125);

		function endServiceCall():void {
			var serviceCases:Vector.<VehicleServiceCaseVO> = new Vector.<VehicleServiceCaseVO>();
			serviceCases.push(new VehicleServiceCaseVO(JSON.parse('{"id": 1, "title": "Saisonaler Reifenwechsel","mandatory":[]}')));
			serviceCases.push(new VehicleServiceCaseVO(JSON.parse('{"id": 2, "title": "Erst- oder Ersatzbereifung","mandatory":[]}')));
			serviceCases.push(new VehicleServiceCaseVO(JSON.parse('{"id": 3, "title": "Hauptuntersuchung","mandatory":[]}')));
			serviceCases.push(new VehicleServiceCaseVO(JSON.parse('{"id": 4, "title": "Sonstige Instandhaltung","mandatory":["notes"]}')));
			serviceCases.push(new VehicleServiceCaseVO(JSON.parse('{"id": 5, "title": "Glas","mandatory":[]}')));

			model.vehicleServiceCases = serviceCases;
		}
	}
}
}
