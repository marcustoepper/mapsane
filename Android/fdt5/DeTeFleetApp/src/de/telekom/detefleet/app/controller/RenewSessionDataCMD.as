package de.telekom.detefleet.app.controller
{
import de.telekom.detefleet.app.models.UserModel;
import de.telekom.detefleet.app.models.vo.PersonVO;

import org.robotlegs.mvcs.Command;

/**
 * Renew session data in model 
 *
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class RenewSessionDataCMD extends Command
{
	[Inject]
	public var userModel:UserModel;

	[Inject]
	public var person:PersonVO;

	public override function execute():void
	{
		userModel.userData = person;
	}
}

}