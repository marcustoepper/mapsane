package de.telekom.detefleet.app.events.login
{
import org.osflash.signals.Signal;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class LogoutSuccess extends Signal
{
	public function LogoutSuccess()
	{
		super();
	}
}
}
