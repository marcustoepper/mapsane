package de.telekom.detefleet.app.views.components.views {
import com.kaizimmer.ui.Screen;

import flash.display.Bitmap;
import flash.system.Capabilities;

/**
 * TODO Jailbreak Image Iphone 5 size is missing
 */
public class JailbreakView extends AbstractView {
    [Embed(source="../../../../../../../../assets/templates/jailbreakImage/Jailbreak.png")]
    protected var jailbreakImage_1x:Class;
    [Embed(source="../../../../../../../../assets/templates/jailbreakImage/Jailbreak@2x.png")]
    protected var jailbreakImage_2x:Class;
    [Embed(source="../../../../../../../../assets/templates/jailbreakImage/Jailbreak-568h@2x.png")]
    protected var jailbreakImage_568h2x:Class;

    protected var jailbreakBmp:Bitmap;

    override protected function createChildren():void {
        var dpiClass:uint = Screen.dpiClass;
        switch (dpiClass) {
            case Screen.AT1X:
            case Screen.LDPI:
                jailbreakBmp = new jailbreakImage_1x() as Bitmap;
                break;
            case Screen.AT2X:
            case Screen.XHDPI:
            default:
                jailbreakBmp = new jailbreakImage_2x() as Bitmap;
                break;
        }

        jailbreakBmp.width = Capabilities.screenResolutionX;
        jailbreakBmp.height = Capabilities.screenResolutionY;
        jailbreakBmp.y = Screen.stage.stageHeight - jailbreakBmp.height;

        addChild(jailbreakBmp);
    }

}
}
