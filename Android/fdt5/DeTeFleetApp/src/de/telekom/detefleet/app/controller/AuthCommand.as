package de.telekom.detefleet.app.controller
{
import de.telekom.detefleet.app.models.UserModel;
import de.telekom.detefleet.app.models.vo.ServiceAuthVO;
import de.telekom.detefleet.app.services.persistence.IKeyChainService;

import org.robotlegs.mvcs.SignalCommand;

/**
 * Extend this Command if you need authentification in your Command
 */
public class AuthCommand extends SignalCommand
{
	public static var _auth:ServiceAuthVO;

	[Inject]
	public var keyChainService:IKeyChainService;

	[Inject]
	public var userModel:UserModel;

	public function get auth():ServiceAuthVO {
		if(_auth == null) {
			_auth = new ServiceAuthVO();
		}

		_auth.deviceKey = keyChainService.getItem('deviceKey');

		if(userModel.authToken == null) {
			userModel.authToken = keyChainService.getItem('authToken');
		}
		_auth.authorizationToken = userModel.authToken;

		return _auth;
	}
}
}
