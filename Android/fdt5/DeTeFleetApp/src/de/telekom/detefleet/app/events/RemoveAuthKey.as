package de.telekom.detefleet.app.events {
	import org.osflash.signals.Signal;
	
	public class RemoveAuthKey extends Signal
	{
		public function RemoveAuthKey()
		{
			super();
		}
	}
}