package de.telekom.detefleet.app.services.local
{
import com.hurlant.crypto.prng.Random;
import com.hurlant.util.Hex;

import flash.utils.ByteArray;

/**
 * Generates an random Key. It is a 256 bit key which will be returned as a Hex
 *  
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 * 
 */
public class RandomDeviceKey implements IDeviceKey
{
	/**
	 * Generates a random key 
	 * 
	 * @return generateRandomBytes
	 */
	public function getKey():String
	{
		return generateKey(256);
	}

	/**
	 * Generates a Hex Key
	 * 
	 * @param length The Length of the generated Key
	 * @return The generated key
	 * 
	 */
	private function generateKey(length:int):String
	{
		var random:Random = new Random();
		var byteArray:ByteArray = new ByteArray();
		random.nextBytes(byteArray, length / 8);
		return Hex.fromArray(byteArray);
	}
}
}