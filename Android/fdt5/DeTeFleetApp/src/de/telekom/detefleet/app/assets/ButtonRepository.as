package de.telekom.detefleet.app.assets {
import de.aoemedia.mobile.ui.components.BitmapComponent;

/**
 * Buttons are in here
 *
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class ButtonRepository {
    [Embed(source="../../../../../../assets/gfx/mdpi/buttons/button_back.png")]
    protected const backButton_xhdpi:Class;

    public function get backButton():BitmapComponent {
        return MultiResolutionBitmaps.getBitmapByResolution(backButton_xhdpi, backButton_xhdpi, backButton_xhdpi);
    }

    [Embed(source="../../../../../../assets/gfx/mdpi/buttons/button_back_down.png")]
    protected const backButton_down_xhdpi:Class;

    public function get backButtonDown():BitmapComponent {
        return MultiResolutionBitmaps.getBitmapByResolution(backButton_down_xhdpi, backButton_down_xhdpi, backButton_down_xhdpi);
    }

    [Embed(source="../../../../../../assets/gfx/mdpi/buttons/drag_button.png")]
    protected const dragButton_mdpi:Class;
    [Embed(source="../../../../../../assets/gfx/hdpi/buttons/drag_button.png")]
    protected const dragButton_hdpi:Class;
    [Embed(source="../../../../../../assets/gfx/xhdpi/buttons/drag_button.png")]
    protected const dragButton_xhdpi:Class;

    public function get dragButton():BitmapComponent {
        return MultiResolutionBitmaps.getBitmapByResolution(dragButton_xhdpi, dragButton_hdpi, dragButton_mdpi);
    }

    protected static var _instance:ButtonRepository;

    public function ButtonRepository(sE:SingletonEnforcer) {
        super();
    }

    /**
     *
     */
    public static function getInstance():ButtonRepository {
        if (_instance == null) {
            _instance = new ButtonRepository(new SingletonEnforcer());
        }
        return _instance;
    }
}
}

internal class SingletonEnforcer {
}
