package de.telekom.detefleet.app.controller.view
{
import de.telekom.detefleet.app.controller.AuthCommand;
import de.telekom.detefleet.app.events.loader.ShowLoader;
import de.telekom.detefleet.app.events.view.ShowView;
import de.telekom.detefleet.app.models.NetworkViewsModel;
import de.telekom.detefleet.app.models.ViewsModel;
import de.telekom.detefleet.app.models.vo.ViewRequestVO;
import de.telekom.detefleet.app.services.remote.ILoginService;

/**
 * Request the next view to Show. Checks if it is allowed
 *
 * @author Kai Zimmer <kai.zimmer@aoemedia.de>
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class RequestViewCMD extends AuthCommand
{
	// models n services
	[Inject]
	public var viewsModel:ViewsModel;

	[Inject]
	public var networkViewsModel:NetworkViewsModel;

	[Inject]
	public var loginService:ILoginService;

	// signal used to dispatch
	[Inject]
	public var showView:ShowView;

	// signal payloads
	[Inject]
	public var viewRequest:ViewRequestVO;

	[Inject]
	public var showLoader:ShowLoader;

	public override function execute():void {
		viewsModel.requestedViewClass = viewRequest.viewClass;
		viewsModel.requestedTransitionType = viewRequest.transitionType;
		// Only set Context if it exist. For removing Context use the RemoveContext Signal
		if(viewRequest.context) {
			viewsModel.context = viewRequest.context;
		}

		if(networkViewsModel.contains(viewRequest.viewClass) && userModel.userData == null) {
			showLoader.dispatch();

			loginService.renewSession(auth);
		} else {
			showView.dispatch();
		}

	}

}
}
