package de.telekom.detefleet.app.controller.dialog
{
import de.telekom.detefleet.app.models.DialogModel;
import de.telekom.detefleet.app.models.LayoutData;
import de.telekom.detefleet.app.views.components.dialog.ConfirmDialogView;

import org.robotlegs.mvcs.SignalCommand;

public class ShowEmptyResultDialogCMD extends SignalCommand
{

	[Inject]
	public var layout:LayoutData;

	[Inject]
	public var model:DialogModel;

	public override function execute():void {
		var dialog:ConfirmDialogView = new ConfirmDialogView(layout.viewport, "Kein Ergebnis", "Zu Ihrer Suche konnte kein Ergebnis gefunden werden.");
		model.addDialog(dialog);
	}
}
}
