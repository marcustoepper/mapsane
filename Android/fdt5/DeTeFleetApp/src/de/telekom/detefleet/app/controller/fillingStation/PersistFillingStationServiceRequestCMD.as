package de.telekom.detefleet.app.controller.fillingStation
{
import de.telekom.detefleet.app.models.DatabaseModel;
import de.telekom.detefleet.app.models.FillingStationModel;
import de.telekom.detefleet.app.models.vo.FillingStationRequestVO;

import org.robotlegs.mvcs.Command;

/**
 * Persist the last FillingStation Request on the device
 * 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 * 
 */
public class PersistFillingStationServiceRequestCMD extends Command
{
	[Inject]
	public var databaseModel:DatabaseModel;

	[Inject]
	public var fillingStationModel:FillingStationModel;

	[Inject]
	public var request:FillingStationRequestVO;

	/**
	 * 
	 */
	public override function execute():void
	{
		fillingStationModel.persistSearchRequest(request, databaseModel.connection);		
	}
}
}