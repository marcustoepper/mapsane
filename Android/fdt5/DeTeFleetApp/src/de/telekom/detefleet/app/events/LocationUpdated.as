package de.telekom.detefleet.app.events
{
import org.osflash.signals.Signal;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class LocationUpdated extends Signal
{
	/**
	 * Latitude, Longitude
	 */
	public function LocationUpdated()
	{
		super(Number, Number);
	}
}
}
