package de.telekom.detefleet.app.views.mediators
{
import de.telekom.detefleet.app.events.PhoneCallRequested;
import de.telekom.detefleet.app.events.fillingStation.FillingStationSelected;
import de.telekom.detefleet.app.events.view.ViewReady;
import de.telekom.detefleet.app.models.vo.FillingStationVO;
import de.telekom.detefleet.app.models.vo.PhoneCallRequestVO;
import de.telekom.detefleet.app.views.components.views.FillingStationDetailView;

import org.robotlegs.mvcs.Mediator;

import flash.net.URLRequest;
import flash.net.navigateToURL;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class FillingStationDetailMediator extends Mediator
{
	[Inject]
	public var fillingStationSelected:FillingStationSelected;

	[Inject]
	public var view:FillingStationDetailView;

	[Inject]
	public var viewReady:ViewReady;
	
	[Inject]
	public var phoneCallRequested:PhoneCallRequested;

	/**
	 * 
	 */
	public override function onRegister():void
	{
		// Load Details
		view.routeButtonClicked.add(handleRouteButtonClick);
		view.telephoneButtonClicked.add(handleTelephoneButtonClicked);

		fillingStationSelected.add(fillFillingStation);

		viewReady.dispatch(view);
	}

	protected function fillFillingStation(station:FillingStationVO):void
	{
		view.fillingStation = station;
	}

	private function handleTelephoneButtonClicked():void
	{
		phoneCallRequested.dispatch(new PhoneCallRequestVO(view.fillingStation.phone));
	}

	private function handleRouteButtonClick():void
	{
		var longitude:Number = view.fillingStation.address.longitude;
		var latitude:Number = view.fillingStation.address.latitude;
		navigateToURL(new URLRequest("http://maps.google.com/?q=" + latitude.toString() + "," + longitude.toString()));
	}

	override public function preRemove():void
	{
		view.routeButtonClicked.remove(handleRouteButtonClick);
		view.telephoneButtonClicked.remove(handleTelephoneButtonClicked);

		fillingStationSelected.remove(fillFillingStation);
	}

}
}
