package de.telekom.detefleet.app.views.components.list
{
import com.thanksmister.touchlist.renderers.ITouchListItemRenderer;

import de.telekom.detefleet.app.views.components.listItem.DashboardButton;

public class DashboardList extends ListComponent
{
	private var _online:Boolean = false;

	public function DashboardList(viewWidth:Number, viewHeight:Number) {
		super(viewWidth, viewHeight);
	}

	public function get online():Boolean {
		return _online;
	}

	public function set online(value:Boolean):void {
		if(_online == value) {
			return;
		}
		_online = value;

		for (var i:int = 0; i < list.numChildren; i++) {
			var item:DashboardButton = list.getChildAt(i) as DashboardButton;
			if(item && item.data.onlineView) {
				item.enabled = _online;
				item.mouseEnabled = _online;
			}
		}
	}

	override protected function addListItem(index:Number, addToTop:Boolean = false):ITouchListItemRenderer {
		var item:ITouchListItemRenderer = super.addListItem(index, addToTop);

		if(item == null) {
			return null;
		}

		if(!_online && item.data.onlineView) {
			DashboardButton(item).enabled = false;
			DashboardButton(item).mouseEnabled = false;
		} else {
			DashboardButton(item).enabled = true;
			DashboardButton(item).mouseEnabled = true;
		}

        DashboardButton(item).resetBadgeCount();

		return item;
	}

    public function resetBadgeCount():void{
        for (var i:int = 0; i < list.numChildren; i++) {
            var item:DashboardButton = list.getChildAt(i) as DashboardButton;
            item.resetBadgeCount();
        }
    }
}
}
