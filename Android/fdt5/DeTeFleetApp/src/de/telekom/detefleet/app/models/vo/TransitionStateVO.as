package de.telekom.detefleet.app.models.vo
{
	import de.telekom.detefleet.app.views.components.views.IView;

	public class TransitionStateVO
	{
		public static const TRANSITIONING:String = "TRANSITIONING";
		public static const OUT:String = "OUT";
		public static const IN:String = "IN";
		
		public var type:String;
		public var viewToHide:IView;
		public var viewToShow:IView;
		
		public function TransitionStateVO ( type:String, viewToHide:IView, viewToShow:IView )
		{
			this.type = type;
			this.viewToHide = viewToHide;
			this.viewToShow = viewToShow;
		}
	}
}