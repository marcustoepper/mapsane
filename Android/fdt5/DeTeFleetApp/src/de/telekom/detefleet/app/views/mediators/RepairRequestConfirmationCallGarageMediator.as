package de.telekom.detefleet.app.views.mediators
{
import de.aoemedia.motion.TransitionType;
import de.telekom.detefleet.app.events.CloseDialog;
import de.telekom.detefleet.app.events.PhoneCallRequested;
import de.telekom.detefleet.app.events.view.BlockHistoryAdditionRequestView;
import de.telekom.detefleet.app.events.view.RemoveViewContext;
import de.telekom.detefleet.app.events.view.RequestView;
import de.telekom.detefleet.app.models.RepairRequestModel;
import de.telekom.detefleet.app.models.vo.PhoneCallRequestVO;
import de.telekom.detefleet.app.models.vo.ViewRequestVO;
import de.telekom.detefleet.app.views.components.views.RepairRequestConfirmationCallGarageView;
import de.telekom.detefleet.app.views.components.views.RepairRequestHistoryView;

import org.robotlegs.mvcs.Mediator;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class RepairRequestConfirmationCallGarageMediator extends Mediator
{
	[Inject]
	public var view:RepairRequestConfirmationCallGarageView;

	[Inject]
	public var phoneCallRequested:PhoneCallRequested;

	[Inject]
	public var closeDialog:CloseDialog;

	[Inject]
	public var model:RepairRequestModel;

	[Inject]
	public var requestView:RequestView;

	[Inject]
	public var blockHistoryAdditionRequestView:BlockHistoryAdditionRequestView;
	[Inject]
	public var removeViewContext:RemoveViewContext;

	public override function onRegister():void {
		view.callGarageClicked.add(handleCallGarageClicked);
		view.closeClicked.add(handleCloseClick);
	}

	private function handleCloseClick():void {
		closeDialog.dispatch(view);

		removeViewContext.dispatch(RepairRequestModel.CONTEXT);
		blockHistoryAdditionRequestView.dispatch(new ViewRequestVO(RepairRequestHistoryView, TransitionType.SLIDE_RIGHT));
	}

	private function handleCallGarageClicked():void {
		phoneCallRequested.dispatch(new PhoneCallRequestVO(model.sucessfulSendedRepairRequest.garage.phone));
		closeDialog.dispatch(view);

		removeViewContext.dispatch(RepairRequestModel.CONTEXT);
		blockHistoryAdditionRequestView.dispatch(new ViewRequestVO(RepairRequestHistoryView, TransitionType.SLIDE_RIGHT));
	}

	public override function preRemove():void {
		view.callGarageClicked.remove(handleCallGarageClicked);
		view.closeClicked.remove(handleCloseClick);
		super.preRemove();
	}
}
}
