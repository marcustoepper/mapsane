package de.telekom.detefleet.app.views.mediators
{
import de.telekom.detefleet.app.events.login.Logout;
import de.telekom.detefleet.app.events.mileage.LastMileageChanged;
import de.telekom.detefleet.app.events.mileage.LoadLastMileage;
import de.telekom.detefleet.app.events.mileage.PersistMileage;
import de.telekom.detefleet.app.events.view.OverlayHidden;
import de.telekom.detefleet.app.events.view.ShowingOverlay;
import de.telekom.detefleet.app.events.view.TransitionChanged;
import de.telekom.detefleet.app.events.view.ViewReady;
import de.telekom.detefleet.app.models.ViewsModel;
import de.telekom.detefleet.app.models.vo.MileageVO;
import de.telekom.detefleet.app.models.vo.TransitionStateVO;
import de.telekom.detefleet.app.views.components.views.IView;
import de.telekom.detefleet.app.views.components.views.MileageView;

import org.robotlegs.mvcs.Mediator;

/**
 * 
 */
public class MileageMediator extends Mediator
{
	[Inject]
	public var viewsModel:ViewsModel;
	
	[Inject]
	public var lastMileageChanged:LastMileageChanged;
	[Inject]
	public var saveMileage:PersistMileage;
	[Inject]
	public var getMileage:LoadLastMileage;
	[Inject]
	public var view:MileageView;

	[Inject]
	public var viewReady:ViewReady;
	[Inject]
	public var transitionChanged:TransitionChanged;
	
	[Inject]
	public var showingOveraly:ShowingOverlay;
	[Inject]
	public var overlayHidden:OverlayHidden;
	
	[Inject]
	public var logout:Logout;

	/**
	 * 
	 */
	public override function onRegister():void
	{
		transitionChanged.add(handleTransitionChanged);
		lastMileageChanged.add(handleMileageChanged);
		
		showingOveraly.add(onShowingOverlay);
		overlayHidden.add(onOverlayHidden);
		
		view.confirmButtonClicked.add(confirmButtonClicked);
		logout.add(handleLogout);

		getMileage.dispatch();

		viewReady.dispatch(view);
	}

	/**
	 * 
	 */
	private function handleLogout():void
	{
		view.updateLastUpdate(null);
		view.updateMileage("");
	}

	protected function confirmButtonClicked(mileage:String):void
	{
		saveMileage.dispatch(mileage);
	}

	protected function handleMileageChanged(mileage:MileageVO):void
	{
		if (mileage) {
			view.updateLastUpdate(mileage.updated);
			view.updateMileage(mileage.mileage);
		}
	}

	override public function preRemove():void
	{
		lastMileageChanged.remove(handleMileageChanged);
		
		showingOveraly.remove(onShowingOverlay);
		overlayHidden.remove(onOverlayHidden);
		
		view.confirmButtonClicked.remove(confirmButtonClicked);

		super.preRemove();
	}
	
	protected function handleTransitionChanged(state:TransitionStateVO):void {
		
		//trace("handleTransitionChanged - state.viewToShow: " + state.viewToShow);
		switch (state.type) {
			case TransitionStateVO.IN:
				showStageTexts(state.viewToShow);
				break;
			case TransitionStateVO.TRANSITIONING:
				hideStageTexts(state.viewToHide);
				break;
			default:
		}
	}
	
	protected function showStageTexts(view:IView):void {
		if(this.view != view) return;
		this.view.showStageTexts();
	}
	
	protected function hideStageTexts(view:IView):void {
		if(this.view == view) {
			this.view.hideStageTexts();
		}
	}
	
	protected function onOverlayHidden():void
	{
		if ( viewsModel.currentView == view ) view.showStageTexts();
	}
	
	protected function onShowingOverlay():void
	{
		if ( viewsModel.currentView == view ) view.hideStageTexts();

	}
}
}
