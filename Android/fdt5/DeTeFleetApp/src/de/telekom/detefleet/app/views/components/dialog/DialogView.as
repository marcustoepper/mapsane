package de.telekom.detefleet.app.views.components.dialog {
import de.aoemedia.mobile.ui.MultiDpiHelper;
import de.telekom.detefleet.app.assets.ScaleBitmaps;
import de.telekom.detefleet.app.assets.TextFormatTemplate;

import flash.display.Sprite;
import flash.geom.Rectangle;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import flash.text.TextFormat;

import org.bytearray.display.ScaleBitmap;

public class DialogView extends Sprite {
    protected var windowBG:ScaleBitmap;

    protected var headline:String;
    protected var message:String;

    protected var containerWidth:Number;
    protected var dialogWidth:Number;

    protected var _marginLeft:Number = 5;
    protected var _marginRight:Number = 5;
    protected var _marginTop:Number = 5;
    protected var _marginBottom:Number = 5;

    protected var _shadowLeft:Number = 5;
    protected var _shadowRight:Number = 5;
    protected var _shadowTop:Number = .5;
    protected var _shadowBottom:Number = 10;

    protected var headlineTextField:TextField;
    protected var messageTextField:TextField;

    protected var dynamicHeight:Boolean;
    protected var viewport:Rectangle;
    protected var container:Sprite;
    private var _paddingLeft:Number;
    private var _paddingRight:Number;
    private var _paddingTop:Number;
    private var _paddingBottom:Number;

    protected var stopInvalidation:Boolean = false;

    public function DialogView(viewport:Rectangle, headline:String, message:String, dynamicHeight:Boolean = true) {
        this.headline = headline;
        this.message = message;
        this.dynamicHeight = dynamicHeight;
        this.viewport = viewport;

        _paddingLeft = 8;
        _paddingRight = 8;
        _paddingTop = 8;
        _paddingBottom = 8;

        createChildren();
        invalidate();
    }

    protected function invalidate():void {
        calculateContainerWidth(viewport.width);

        messageTextField.width = containerWidth;

        headlineTextField.x = 0;
        headlineTextField.y = 0;

        messageTextField.x = 0;
        messageTextField.y = headlineTextField.y + headlineTextField.height + MultiDpiHelper.scale(10);

        resetContainerPosition();
        invalidateBackground();
    }

    protected function calculateContainerWidth(width:Number):void {
        dialogWidth = width - MultiDpiHelper.scale(_marginLeft + _marginRight);
        containerWidth = dialogWidth - MultiDpiHelper.scale(_shadowLeft + _shadowRight + _paddingLeft + _paddingRight);
    }

    protected function resetContainerPosition():void {
        container.x = viewport.width / 2 - container.width / 2;
        container.y = viewport.height / 2 - container.height / 2;
    }

    /**
     *
     */
    protected function invalidateBackground():void {
        windowBG.setSize(dialogWidth, container.height + MultiDpiHelper.scale(_shadowTop + _shadowBottom + _paddingBottom + _paddingTop));

        windowBG.x = viewport.width / 2 - windowBG.width / 2;
        windowBG.y = viewport.height / 2 - (windowBG.height - MultiDpiHelper.scale(_shadowTop + _shadowBottom)) / 2;
    }

    /**
     *
     */
    protected function createChildren():void {
        container = new Sprite();
        addChild(container);

        windowBG = ScaleBitmaps.getInstance().dialogWindow;

        var s:Sprite = new Sprite();
        s.graphics.beginFill(0x000000, .125);
        s.graphics.drawRect(0, 0, viewport.width, viewport.height);
        s.graphics.endFill();
//
//        if (modalBG == null) {
//
//            var bmpD:BitmapData = new BitmapData(viewport.width, viewport.height, true, 0x00000000);
//            bmpD.draw(s);
//            modalBG = new Bitmap(bmpD);
//            s = null;
//        }
        addChildAt(s, numChildren - 1);

        var headlineTextFormat:TextFormat = TextFormatTemplate.getInstance().dialogHeadline;

        headlineTextField = new TextField();
        headlineTextField.autoSize = TextFieldAutoSize.LEFT;
        headlineTextField.defaultTextFormat = headlineTextFormat;
        headlineTextField.setTextFormat(headlineTextFormat);
        headlineTextField.embedFonts = true;
        headlineTextField.text = headline;

        var messageTextFormat:TextFormat = TextFormatTemplate.getInstance().dialogMessage;

        messageTextField = new TextField();
        messageTextField.autoSize = TextFieldAutoSize.LEFT;
        messageTextField.wordWrap = true;
        messageTextField.multiline = true;
        messageTextField.defaultTextFormat = messageTextFormat;
        messageTextField.setTextFormat(messageTextFormat);
        messageTextField.embedFonts = true;
        messageTextField.text = message;

        addChildAt(windowBG, numChildren - 1);
        container.addChild(headlineTextField);
        container.addChild(messageTextField);
    }

    protected function scale(pixels:Number):int {
        return MultiDpiHelper.scale(pixels);
    }
}
}
