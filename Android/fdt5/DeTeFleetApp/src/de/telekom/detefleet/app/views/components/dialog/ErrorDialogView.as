package de.telekom.detefleet.app.views.components.dialog {
import flash.geom.Rectangle;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class ErrorDialogView extends ConfirmDialogView {
    public function ErrorDialogView(viewport:Rectangle, message:String) {
        super(viewport, "Fehler", message);
    }
}
}
