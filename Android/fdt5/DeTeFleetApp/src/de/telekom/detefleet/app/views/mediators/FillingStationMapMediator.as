package de.telekom.detefleet.app.views.mediators {
import de.aoemedia.motion.TransitionType;
import de.telekom.detefleet.app.events.fillingStation.FillingStationSelected;
import de.telekom.detefleet.app.events.view.BlockHistoryAdditionRequestView;
import de.telekom.detefleet.app.events.view.RequestView;
import de.telekom.detefleet.app.events.view.TransitionChanged;
import de.telekom.detefleet.app.events.view.TransitionCompleted;
import de.telekom.detefleet.app.events.view.ViewReady;
import de.telekom.detefleet.app.models.FillingStationModel;
import de.telekom.detefleet.app.models.vo.AddressVO;
import de.telekom.detefleet.app.models.vo.FillingStationVO;
import de.telekom.detefleet.app.models.vo.TransitionStateVO;
import de.telekom.detefleet.app.models.vo.ViewRequestVO;
import de.telekom.detefleet.app.views.components.views.FillingStationDetailView;
import de.telekom.detefleet.app.views.components.views.FillingStationListView;
import de.telekom.detefleet.app.views.components.views.FillingStationMapView;

import org.as3commons.logging.api.ILogger;
import org.as3commons.logging.api.getLogger;
import org.robotlegs.mvcs.Mediator;

/**
 *
 */
public class FillingStationMapMediator extends Mediator {
    /**
     * VIEW
     */
    [Inject]
    public var view:FillingStationMapView;

    // Signals
    [Inject]
    public var transitionChanged:TransitionChanged;
    [Inject]
    public var transitionCompleted:TransitionCompleted;
    [Inject]
    public var blockHistoryAdditionRequestView:BlockHistoryAdditionRequestView;
    [Inject]
    public var fillingStationSelected:FillingStationSelected;
    [Inject]
    public var gotoView:RequestView;
    [Inject]
    public var viewReady:ViewReady;

    // Models
    [Inject]
    public var model:FillingStationModel;

    private static const logger:ILogger = getLogger(FillingStationMapMediator);

    /**
     *
     *
     */
    public override function onRegister():void {
        // Add Listener
        view.signalToggleListButtonClicked.add(toggleList);

        // Listen to the context
        transitionCompleted.add(handleTransitionCompleted);
        transitionChanged.add(handleTransitionChanged);

        viewReady.dispatch(view);
    }

    protected function handleTransitionCompleted(state:TransitionStateVO):void {
        if (state.viewToShow != view) {
            return;
        }
        view.showMap();

        if (state.viewToHide is FillingStationListView) {
            populateFillingStations();
        }

    }

    /**
     *
     * @param state
     */
    protected function handleTransitionChanged(state:TransitionStateVO):void {
        if (state.viewToHide == view && state.type == TransitionStateVO.TRANSITIONING) {
            view.hideMap();
        }
    }

    /**
     *
     * @param message
     */
    protected function calloutSelected(message:Object):void {
		trace("received calloutsignal in app");
        var tmpFillingStation:FillingStationVO;
        for (var i:int = 0; i < model.searchResult.length; i++) {

            if (model.searchResult[i].id == message.markerId) {
                tmpFillingStation = model.searchResult[i];
                break;
            }
        }

        if (tmpFillingStation == null) {
            return;
        }

        view.hideMap();
        gotoView.dispatch(new ViewRequestVO(FillingStationDetailView));
        fillingStationSelected.dispatch(tmpFillingStation);
    }

    /**
     *
     */
    protected function populateFillingStations():void {
        if (model.searchResult.length == 0) {
            return;
        }

        var addressToShow:AddressVO = model.searchResult[0].address;
        view.map.clear();
        view.map.resetCenter(addressToShow.latitude, addressToShow.longitude);
        view.map.calloutSelected.add(calloutSelected);

        for (var i:int = 0; i < model.searchResult.length && i < 10; i++) {
            var fillingStation:FillingStationVO = model.searchResult[i];

            var annotationContent1:Object = {title:fillingStation.price + ' €', subtitle: fillingStation.getLastUpdateString()};
            var calloutContent1:Object = {title:fillingStation.brand, subtitle:fillingStation.address.street};

            view.map.addMarker(Number(fillingStation.id) as int, fillingStation.address.latitude, fillingStation.address.longitude, annotationContent1, calloutContent1);
        }
    }

    /**
     *
     */
    protected function toggleList():void {
        blockHistoryAdditionRequestView.dispatch(new ViewRequestVO(FillingStationListView, TransitionType.SLIDE_DOWN));
    }

    /**
     *
     */
    override public function preRemove():void {
        view.signalToggleListButtonClicked.remove(toggleList);
        transitionChanged.remove(handleTransitionChanged);
        transitionCompleted.remove(handleTransitionChanged);

        super.preRemove();
    }


}
}