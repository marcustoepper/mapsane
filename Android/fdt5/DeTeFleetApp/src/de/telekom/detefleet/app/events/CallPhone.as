package de.telekom.detefleet.app.events
{
import org.osflash.signals.Signal;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class CallPhone extends Signal
{
	public function CallPhone()
	{
		super(String);
	}
}
}
