package de.telekom.detefleet.app.views.components.listItem {
import com.kaizimmer.ui.Screen;
import com.thanksmister.touchlist.renderers.ITouchListItemRenderer;

import de.aoemedia.mobile.ui.MultiDpiHelper;
import de.aoemedia.utils.TextFieldUtil;
import de.telekom.detefleet.app.assets.Color;
import de.telekom.detefleet.app.assets.IconRepository;
import de.telekom.detefleet.app.assets.TextFormatTemplate;
import de.telekom.detefleet.app.models.vo.MessageVO;

import flash.display.Bitmap;
import flash.events.MouseEvent;
import flash.globalization.DateTimeFormatter;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;

public class MessageListItemRenderer extends ListItemButton implements ITouchListItemRenderer {
    protected const _leftPadding:uint = 12;
    protected const _rightPadding:uint = 12;
    protected const _topPadding:uint = 11;
    protected const _bottomPadding:uint = 11;

    protected var _rightIcon:Bitmap;
    private var _label:TextField;

    protected static var dateTimeFormatter:DateTimeFormatter;

    protected var childsCreated:Boolean = false;
    protected var backgroundDrawn:Boolean = false;

    protected var _data:MessageVO;

    private var dateTextField:TextField;
    private var unreadIcon:Bitmap;

    /**
     *
     */
    public function MessageListItemRenderer() {
        if (dateTimeFormatter == null) {
            dateTimeFormatter = new DateTimeFormatter(flash.globalization.LocaleID.DEFAULT);
            dateTimeFormatter.setDateTimePattern("dd. MMMM yyyy, HH:mm");
        }

        _itemHeight = MultiDpiHelper.scale(60);

        createChildren();
        invalidate();
    }

    /**
     *
     */
    protected function createChildren():void {

        dateTextField = new TextField();
        dateTextField.mouseEnabled = false;
        dateTextField.autoSize = TextFieldAutoSize.LEFT;
        dateTextField.defaultTextFormat = TextFormatTemplate.getInstance().listItemDate;
        dateTextField.embedFonts = true;
        dateTextField.text = "";
        addChild(dateTextField);

        unreadIcon = IconRepository.unread;
        addChild(unreadIcon);

        _rightIcon = IconRepository.iconArrow;
        addChild(_rightIcon);

        _label = new TextField();
        _label.text = "";
        _label.mouseEnabled = false;
        _label.defaultTextFormat = TextFormatTemplate.getInstance().listItemTitleHighlight;
        _label.embedFonts = true;
        addChild(_label);

        addEventListener(MouseEvent.MOUSE_DOWN, pressHandler);
        childsCreated = true;
    }

    /**
     *
     */
    protected function invalidate():void {
        if (!childsCreated) {
            return;
        }

        dateTextField.x = MultiDpiHelper.scale(_leftPadding * 2) + unreadIcon.width;
        dateTextField.y = MultiDpiHelper.scale(_topPadding);

        _label.x = MultiDpiHelper.scale(_leftPadding * 2) + unreadIcon.width;
        _label.y = dateTextField.y + dateTextField.height + MultiDpiHelper.scale(3);

        _rightIcon.x = Screen.stage.stageWidth - _rightIcon.width - MultiDpiHelper.scale(_rightPadding);
        _rightIcon.y = itemHeight - _rightIcon.height >> 1;

        _label.width = Screen.stage.stageWidth - _rightIcon.width - unreadIcon.width - MultiDpiHelper.scale(_rightPadding + _leftPadding * 2);

        unreadIcon.y = itemHeight - unreadIcon.height >> 1;
        unreadIcon.x = MultiDpiHelper.scale(_leftPadding);

        if (!backgroundDrawn) {
            drawLine();

            backgroundDrawn = true;
        }
    }

    /**
     *
     * @param value
     */
    override public function set data(value:Object):void {
        if (_data == value) {
            return;
        }
        _data = value as MessageVO;

        TextFieldUtil.fillText(_label, _data.title);
        dateTextField.text = dateTimeFormatter.format(_data.date);

        if (_data.read) {
            _label.setTextFormat(TextFormatTemplate.getInstance().listItemTitle);
            unreadIcon.visible = false;
        }

        invalidate();
    }

    override public function get data():Object {
        return _data;
    }

    /**
     * makes the Item selectable
     * @param value
     */
    override public function selectable(value:Boolean):void {
        if (value) {
            addEventListener(MouseEvent.MOUSE_UP, selectHandler);
        } else {
            removeEventListener(MouseEvent.MOUSE_UP, selectHandler);
        }
    }

    /**
     *
     */
    override public function destroy():void {
        removeEventListener(MouseEvent.MOUSE_UP, selectHandler);

        _itemPressedCallback = null;
        _itemSelectedCallback = null;

        this.graphics.clear();
        unselect();

        _data = null;
    }

    /**
     * Dispatched when item is first pressed on tap or mouse down.
     * */
    protected function pressHandler(event:MouseEvent):void {
        _itemPressedCallback(this);
    }

    /**
     * Dispatched when item is selected, usually on touch end or mouse up.
     * */
    protected function selectHandler(event:MouseEvent):void {
        _itemSelectedCallback(this);
    }

    public function setRead():void {
        _label.setTextFormat(TextFormatTemplate.getInstance().listItemTitle);
        unreadIcon.visible = false;
    }

    override public function unselect():void {
        _label.textColor = Color.LIST_ITEM;
        drawLine();
    }

    override public function select():void {
        _label.textColor = Color.DARK_GREY;
        drawBackground();
    }

}
}
