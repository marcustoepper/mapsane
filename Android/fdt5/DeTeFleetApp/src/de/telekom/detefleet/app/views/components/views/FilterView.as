package de.telekom.detefleet.app.views.components.views
{
import com.kaizimmer.ui.Screen;

import de.aoemedia.mobile.ui.components.LabelComponent;
import de.aoemedia.mobile.ui.components.container.LayoutContainer;
import de.aoemedia.mobile.ui.components.container.LayoutHorizontalAlign;
import de.aoemedia.mobile.ui.components.container.LayoutOrientation;
import de.telekom.detefleet.app.assets.TextFormatTemplate;

import com.greensock.TweenLite;

import flash.events.MouseEvent;
import flash.text.TextFormat;

/**
 * A View with an Filter Area on Top
 *
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class FilterView extends SplitView
{
	// Defaults
	protected var yDragStart:Number;

	protected var filterOptionLabelLayout:LayoutContainer;
	protected var filterOptionLabel:LabelComponent;

	/**
	 * Construct
	 */
	public function FilterView(label:String = "")
	{
		super(label);

	}

	/**
	 *
	 *
	 */
	override protected function createChildren():void
	{
		super.createChildren();

		filterOptionLabelLayout = new LayoutContainer(upperView);

		filterOptionLabelLayout.explicitWidth = Screen.stage.stageWidth;
		filterOptionLabelLayout.horizontalAlign = LayoutHorizontalAlign.CENTER;
		filterOptionLabelLayout.marginBottom = 3;
		filterOptionLabelLayout.marginTop = 3;

		var tFormat:TextFormat = TextFormatTemplate.getInstance().filterOption;

		filterOptionLabel = new LabelComponent("Filteroptionen");
		filterOptionLabel.textFormat = tFormat;
		filterOptionLabelLayout.addItem(filterOptionLabel);


		dragHandle.defineHitArea(scale(44), scale(44));
	}

	public function closeFilter():void{
		closeUpperView();
	}

	public function openFilter():void{
		openUpperView();
	}

	/**
	 *
	 * @param e
	 *
	 */
	override protected function closeUpperView():void
	{
		var dragDistancePercentage:Number = viewsContainer.y / dragRect.height;
		TweenLite.to(filterOptionLabel, TOGGLE_TWEEN_TIME * dragDistancePercentage, {alpha:1});
		super.closeUpperView();
	}

	/**
	 *
	 * @param e
	 *
	 */
	override protected function openUpperView():void
	{
		var dragDistancePercentage:Number = 1 - viewsContainer.y / dragRect.height;
		TweenLite.to(filterOptionLabel, TOGGLE_TWEEN_TIME * dragDistancePercentage, {alpha:0});
		super.openUpperView();
	}


	/**
	 *
	 */
	override protected function startDragging(e:MouseEvent = null):void
	{
		yDragStart = viewsContainer.y;

		super.startDragging();
	}

	/**
	 * Performs closing/opening when at least 1/3 of the total dragging distance was dragged
	 */
	override protected function stopDragging(e:MouseEvent = null):void
	{
		e.stopImmediatePropagation();
		// assuming the viewsContainer origo is still 0 (default in SplitView)

		if ( yDragStart < (dragRect.height >> 1) ) // started dragging in the upper area
		{
			if ( viewsContainer.y >= dragRect.height / 3 ) {
				openUpperView();
			} else {
				closeUpperView();
			}
		} else {
			// started dragging in the lower area
			if ( viewsContainer.y <= 2 / 3 * dragRect.height ) {
				closeUpperView();
			} else {
				openUpperView();
			}
		}
		super.stopDragging();
	}
}
}
