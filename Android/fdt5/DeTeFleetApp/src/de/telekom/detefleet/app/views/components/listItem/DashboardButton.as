package de.telekom.detefleet.app.views.components.listItem {
import com.kaizimmer.ui.Screen;
import com.thanksmister.touchlist.renderers.ITouchListItemRenderer;

import de.aoemedia.mobile.ui.MultiDpiHelper;
import de.telekom.detefleet.app.assets.Color;
import de.telekom.detefleet.app.assets.IconRepository;
import de.telekom.detefleet.app.assets.TextFormatTemplate;
import de.telekom.detefleet.app.models.vo.DashboardButtonVO;
import de.telekom.detefleet.app.views.components.buttons.Badge;

import flash.display.Bitmap;
import flash.display.DisplayObject;
import flash.events.MouseEvent;
import flash.text.TextField;

public class DashboardButton extends ListItemButton implements ITouchListItemRenderer {
    protected var _leftPadding:uint;
    protected var _leftLabelPadding:uint;
    protected var _rightPadding:uint;

    protected var _rightIcon:Bitmap;
    protected var _leftIcon:Bitmap;
    private var _label:TextField;

    protected var childsCreated:Boolean = false;
    protected var backgroundDrawn:Boolean = false;

    protected var _data:DashboardButtonVO;

    private var badge:Badge;

    public function DashboardButton() {
        super();

        _itemHeight = MultiDpiHelper.scale(49);
        _leftPadding = MultiDpiHelper.scale(12);
        _leftLabelPadding = MultiDpiHelper.scale(51);
        _rightPadding = MultiDpiHelper.scale(12);

        createChildren();
        invalidate();
    }

    protected function createChildren():void {
        _rightIcon = IconRepository.iconArrow;
        addChild(_rightIcon);

        badge = new Badge();
        badge.visible = false;
        addChild(DisplayObject(badge));

        _label = new TextField();
        _label.text = "";
        _label.mouseEnabled = false;
        _label.defaultTextFormat = TextFormatTemplate.getInstance().listItemTitleHighlight;
        _label.embedFonts = true;
        addChild(_label);

        addEventListener(MouseEvent.MOUSE_DOWN, pressHandler);

        childsCreated = true;
    }

    /**
     *
     */
    protected function invalidate():void {
        if (!childsCreated) {
            return;
        }

        _label.width = Screen.stage.stageWidth - _leftLabelPadding;
        _label.height = _label.textHeight + MultiDpiHelper.scale(4);

        if (_leftIcon != null) {
            _leftIcon.x = _leftPadding;
            _leftIcon.y = itemHeight - _leftIcon.height >> 1;
        }
        if (_rightIcon != null) {
            _rightIcon.x = Screen.stage.stageWidth - _rightIcon.width - _rightPadding;
            _rightIcon.y = itemHeight - _rightIcon.height >> 1;

            _label.width -= _rightIcon.x - _rightPadding;
        }

        _label.x = _leftLabelPadding;
        _label.y = itemHeight - _label.height >> 1;
        badge.x = _label.x + _label.textWidth - MultiDpiHelper.scale(3);
        badge.y = _label.y - (badge.height / 2) + MultiDpiHelper.scale(3);

        if (!backgroundDrawn) {
            drawLine();

            backgroundDrawn = true;
        }
    }

    public function set label(value:String):void {
        _label.text = value;

        invalidate();
    }

    public function set enabled(enabled:Boolean):void {
        if (enabled) {
            _label.textColor = Color.LIST_ITEM;
            mouseChildren = true;
            mouseEnabled = true;
        } else {
            _label.textColor = Color.MEDIUM_GREY;
            mouseChildren = false;
            mouseEnabled = false;
        }
    }

    /**
     *
     * @param value
     */
    override public function set data(value:Object):void {
        if (_data == value) {
            return;
        }
        _data = value as DashboardButtonVO;

        label = _data.title;

        if (_leftIcon != null && contains(_leftIcon)) {
            removeChild(_leftIcon);
        }

        _leftIcon = _data.icon;
        addChild(_leftIcon);

        if (_data.badgeCount > 0) {
            badge.setCount(_data.badgeCount);
            badge.visible = true;
        } else {
            badge.visible = false;
        }

        _rightIcon.visible = !_data.hideArrow;
        invalidate();
    }

    public function resetBadgeCount():void {
        if (_data.badgeCount > 0) {
            badge.setCount(_data.badgeCount);
            badge.visible = true;
        } else {
            badge.visible = false;
        }
    }

    override public function get data():Object {
        return _data;
    }

    /**
     * makes the Item selectable
     * @param value
     */
    override public function selectable(value:Boolean):void {
        if (value) {
            addEventListener(MouseEvent.MOUSE_UP, selectHandler);
        } else {
            removeEventListener(MouseEvent.MOUSE_UP, selectHandler);
        }
    }

    override public function destroy():void {
        removeEventListener(MouseEvent.MOUSE_UP, selectHandler);

        _itemPressedCallback = null;
        _itemSelectedCallback = null;

        this.graphics.clear();
        unselect();

        _data = null;
    }

    /**
     * Dispatched when item is first pressed on tap or mouse down.
     * */
    protected function pressHandler(event:MouseEvent):void {
        _itemPressedCallback(this);
    }

    /**
     * Dispatched when item is selected, usually on touch end or mouse up.
     * */
    protected function selectHandler(event:MouseEvent):void {
        _itemSelectedCallback(this);
    }

    override public function unselect():void {
        _label.textColor = Color.LIST_ITEM;
        drawLine();
    }

    override public function select():void {
        _label.textColor = Color.DARK_GREY;
        drawBackground();
    }
}
}
