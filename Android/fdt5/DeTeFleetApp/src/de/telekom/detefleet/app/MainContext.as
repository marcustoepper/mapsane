package de.telekom.detefleet.app
{
import flash.display.Sprite;
import de.telekom.detefleet.app.context.BootstrapRemoteServices;

/**
 * This is the Context for use the App with Remote Server Connection
 * 
 * @author Daniel Kopp <danie.kopp@aoemedia.de>
 */
public class MainContext extends BaseContext
{
	public function MainContext(contextView:Sprite)
	{
		super(contextView);
	}
	/**
	 * 
	 */
	public override function startup():void
	{
		new BootstrapRemoteServices(injector);

		super.startup();
	}
}
}