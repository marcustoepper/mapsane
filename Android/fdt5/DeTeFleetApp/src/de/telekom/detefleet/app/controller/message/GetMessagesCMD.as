package de.telekom.detefleet.app.controller.message
{
import de.telekom.detefleet.app.controller.*;
import de.telekom.detefleet.app.services.remote.IMessageService;

/**
 * Send a request to get Messages
 *
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class GetMessagesCMD extends AuthCommand
{
	[Inject]
	public var service:IMessageService;

	public override function execute():void {
		service.getMessages(auth);
	}
}
}
