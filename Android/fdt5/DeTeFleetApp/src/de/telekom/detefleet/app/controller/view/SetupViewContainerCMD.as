package de.telekom.detefleet.app.controller.view {
	import de.telekom.detefleet.app.views.components.views.AppContainer;
	import de.telekom.detefleet.app.models.LayoutData;
	import de.telekom.detefleet.app.models.ViewsModel;

	import com.kaizimmer.ui.Screen;

	import org.robotlegs.mvcs.Command;

	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	
	/**
	 * ...
	 * @author dev.mode
	 */
	public class SetupViewContainerCMD extends Command
	{
		[Inject]
		public var viewsModel:ViewsModel;
		
		[Inject]
		public var layout:LayoutData;
		
		public override function execute ( ):void
		{
			var appContainer:AppContainer = new AppContainer();
			var screenWidth:Number = contextView.stage.fullScreenWidth;
			var screenHeight:Number = contextView.stage.fullScreenHeight;
			var letterBoxFrameWidth:Number = 0;
			
			trace("Screen.needsLetterBox: " + Screen.needsLetterBox);
			
			if ( Screen.needsLetterBox )
			{
				letterBoxFrameWidth = (screenWidth - Screen.referenceWidth * Screen.scaleFactor) / 2;
				
				var canvas:Sprite = new Sprite();
				canvas.graphics.beginFill(0x0);
				canvas.graphics.drawRect(0, 0, letterBoxFrameWidth, screenHeight);
				canvas.graphics.drawRect(screenWidth - letterBoxFrameWidth, 0, letterBoxFrameWidth, screenHeight);
				canvas.graphics.endFill();
				
				var snapshot:BitmapData = new BitmapData(screenWidth, screenHeight, true, 0x00000000);
				snapshot.draw(canvas);
				var letterBox:Bitmap = new Bitmap(snapshot);
				contextView.addChildAt(letterBox, contextView.numChildren);
				// reposition app container
				appContainer.x = letterBoxFrameWidth;
			}
			
			viewsModel.viewContainer = appContainer;
			viewsModel.viewContainerOrigoX = appContainer.x;
			
			layout.viewport.x = appContainer.x;
			layout.viewport.width = screenWidth - 2 * letterBoxFrameWidth;
			layout.viewport.height = screenHeight;
			layout.isLetterBoxed = Screen.needsLetterBox;
			layout.letterBoxBarWidth = letterBoxFrameWidth;
			
			contextView.addChildAt(appContainer, 0);
		}
		
	}
	
}