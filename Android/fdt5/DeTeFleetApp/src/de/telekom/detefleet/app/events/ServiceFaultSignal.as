package de.telekom.detefleet.app.events
{
import org.osflash.signals.Signal;
/**
 * Basic Remote Server Error
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class ServiceFaultSignal extends Signal
{
	public function ServiceFaultSignal():void{
		super(String);
	}
}
}
