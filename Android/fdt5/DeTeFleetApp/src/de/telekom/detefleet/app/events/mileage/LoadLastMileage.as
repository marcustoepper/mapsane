package de.telekom.detefleet.app.events.mileage
{
import org.osflash.signals.Signal;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class LoadLastMileage extends Signal
{
	public function LoadLastMileage()
	{
		super();
	}
}
}
