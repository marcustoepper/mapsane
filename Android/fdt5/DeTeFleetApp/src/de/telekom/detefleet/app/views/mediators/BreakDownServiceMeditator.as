package de.telekom.detefleet.app.views.mediators
{
import de.telekom.detefleet.app.events.PhoneCallRequested;
import de.telekom.detefleet.app.events.view.RequestView;
import de.telekom.detefleet.app.events.view.ViewReady;
import de.telekom.detefleet.app.models.vo.PhoneCallRequestVO;
import de.telekom.detefleet.app.models.vo.ViewRequestVO;
import de.telekom.detefleet.app.views.components.views.BreakDownServiceView;
import de.telekom.detefleet.app.views.components.views.BreakdownChecklistView;
import de.telekom.detefleet.app.views.components.views.CurrentLocationView;

import org.robotlegs.mvcs.Mediator;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class BreakDownServiceMeditator extends Mediator
{
	// Views
	[Inject]
	public var view:BreakDownServiceView;
	
	[Inject]
	public var viewReady:ViewReady;
	
	[Inject]
	public var gotoView:RequestView;
	
	[Inject]
	public var phoneCallRequested:PhoneCallRequested;
	
	public override function onRegister():void
	{
		viewReady.dispatch(view);
		
		view.checklistButtonClicked.add(checklistButtonClicked);
		view.currentLocationButtonClicked.add(currentLocationButtonClicked);
		view.callBreakDownServiceButtonClicked.add(callBreakDownServiceButtonClicked);
		view.callEmergencyButtonClicked.add(callEmergencyButtonClicked);
	}

	private function currentLocationButtonClicked():void
	{
		gotoView.dispatch(new ViewRequestVO(CurrentLocationView));
	}

	private function checklistButtonClicked():void
	{
		gotoView.dispatch(new ViewRequestVO(BreakdownChecklistView));
	}
	private function callBreakDownServiceButtonClicked():void
	{
		phoneCallRequested.dispatch(new PhoneCallRequestVO("+49 (0) 180 11 23 033", "(Festnetzpreis 3,9 ct/min; Mobilfunkpreise höchstens 42 ct/min)"));
	}
	private function callEmergencyButtonClicked():void
	{
		phoneCallRequested.dispatch(new PhoneCallRequestVO("112"));
	}
}
}
