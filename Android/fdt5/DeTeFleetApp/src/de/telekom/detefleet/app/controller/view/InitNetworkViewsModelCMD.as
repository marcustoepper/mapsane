package de.telekom.detefleet.app.controller.view {
import de.telekom.detefleet.app.views.components.views.MessageListView;
import de.telekom.detefleet.app.views.components.views.RepairRequestHistoryView;
	import de.telekom.detefleet.app.views.components.views.GarageListView;
	import de.telekom.detefleet.app.models.NetworkViewsModel;
	import de.telekom.detefleet.app.views.components.views.FillingStationListView;

	import org.robotlegs.mvcs.Command;
	
	/**
	 * ...
	 * @author dev.mode
	 */
	public class InitNetworkViewsModelCMD extends Command
	{
		[Inject]
		public var model:NetworkViewsModel;
		
		public override function execute ( ):void
		{
			model.addViewClass(FillingStationListView);
			model.addViewClass(GarageListView);
			model.addViewClass(RepairRequestHistoryView);
			model.addViewClass(MessageListView);
		}
		
	}
	
}
