package de.telekom.detefleet.app.models.vo
{
import de.telekom.detefleet.app.models.JSONable;

/**
 * Request data for Location based requests
 * 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class LocationRequestVO implements JSONable
{
	public var longitude:Number;
	public var latitude:Number;
	public var address:String;

	/**
	 * Convert Object to JSON String
	 * 
	 * @return JSON string
	 */
	public function toJSON():String
	{
		var json:String = "";

		if (address && address != "") {
			json = '"address":"' + address + '"';
		} else {
			json = '"longitude":"' + longitude + '", "latitude":"' + latitude + '"';
		}

		return json;
	}
}
}