package de.telekom.detefleet.app.views.mediators
{
import de.telekom.detefleet.app.events.CloseDialog;
import de.telekom.detefleet.app.events.DialogConfirmed;
import de.telekom.detefleet.app.views.components.dialog.ConfirmDialogView;

import org.robotlegs.mvcs.Mediator;

/**
 * ...
 * @author dev.mode
 */
public class ConfirmDialogMediator extends Mediator
{
	[Inject]
	public var view:ConfirmDialogView;

	[Inject]
	public var dialogConfirmed:DialogConfirmed;

	[Inject]
	public var closeDialog:CloseDialog;

	public override function onRegister():void {
		view.okClicked.add(handleConfirmation);
	}

	protected function handleConfirmation():void {
		dialogConfirmed.dispatch(view);
		closeDialog.dispatch(view);
	}

	public override function preRemove():void {
		view.okClicked.remove(handleConfirmation);
		super.preRemove();
	}
}
}
