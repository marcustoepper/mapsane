package de.telekom.detefleet.app.events.security
{
	import org.osflash.signals.Signal;
	
	/**
	 * @author 
	 */
	public class SSLCertificateInvalid extends Signal
	{
		public function SSLCertificateInvalid()
		{
			super();
		}
	}
}
