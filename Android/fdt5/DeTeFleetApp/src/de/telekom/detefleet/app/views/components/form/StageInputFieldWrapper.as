package de.telekom.detefleet.app.views.components.form
{

import flash.display.Sprite;
import flash.display.Stage;

import de.aoemedia.mobile.ui.MultiDpiHelper;
import de.aoemedia.mobile.ui.components.ILayoutComponent;
import de.aoemedia.mobile.ui.components.LabelComponent;
import de.aoemedia.mobile.ui.components.container.LayoutContainer;
import de.aoemedia.mobile.ui.components.forms.StageInputField;
import de.aoemedia.mobile.ui.components.skins.InputFieldBitmaps;
import de.telekom.detefleet.app.assets.ScaleBitmaps;

import org.bytearray.display.ScaleBitmap;

/**
 *
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 *
 */
public class StageInputFieldWrapper extends InputFieldWrapper
{
	
	/**
	 *
	 */
	public function StageInputFieldWrapper(view:Sprite) {
		super(view);
	}

	/**
	 *
	 */
	override protected function createChildren():void {

		var sBmps:ScaleBitmaps = ScaleBitmaps.getInstance();
		var bg:ScaleBitmap = sBmps.input;
		var bmps:InputFieldBitmaps = new InputFieldBitmaps(bg, sBmps.inputTakingInput, bg, sBmps.inputError, sBmps.inputSuccess);
		
		_label = new LabelComponent();
		_label.paddingLeft = -2;

		_input = new StageInputField(bmps);
		_input.paddingHorizontal = MultiDpiHelper.scale(12);
		_input.paddingVertical = MultiDpiHelper.scale(6);
		StageInputField(_input).fontSize = MultiDpiHelper.scale(20);
		
		//_input.setSize(272, 37);
		
		addItem(_label);
		addItem(_input);

		applySkin();
	}
	
	public function set displayAsPassword ( b:Boolean ):void
	{
		StageInputField(_input).displayAsPassword = b;
	}
	
	public function get displayAsPassword ( ):Boolean
	{
		return StageInputField(_input).displayAsPassword;
	}
	
	public function set softKeyboardType ( type:String ):void
	{
		StageInputField(_input).softKeyboardType = type;
	}
	
	public function showText ( stage:Stage ):void
	{
		StageInputField(_input).showText(stage);
	}
	
	public function hideText ( ):void
	{
		StageInputField(_input).hideText();
	}
	
	public function freeze ( ):void
	{
		StageInputField(_input).freeze();
	}
	
	public function unfreeze ( ):void
	{
		StageInputField(_input).unfreeze();
	}
	
	public function invalidateStageTextCoords ( ):void
	{
		StageInputField(_input).invalidateStageTextCoords();
	}
	
	public function set fontSize ( size:int ):void
	{
		StageInputField(_input).fontSize = size;
	}
	
	public function set textPaddingVertical ( padding:int ):void
	{
		_input.paddingVertical = padding;
	}
}
}
