package de.telekom.detefleet.app.views.components.buttons {
import de.aoemedia.mobile.ui.MultiDpiHelper;
import de.telekom.detefleet.app.assets.IconRepository;
import de.telekom.detefleet.app.assets.TextFormatTemplate;

import flash.display.Bitmap;
import flash.display.Sprite;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;

public class Badge extends Sprite {
    private var _badgeTextField:TextField;
    private var badgeIcon:Bitmap;

    public function Badge() {
        mouseEnabled = false;
        mouseChildren = false;

        badgeIcon = IconRepository.badge;
        badgeIcon.y = 0;
        badgeIcon.x = 0;
        addChild(badgeIcon);

        _badgeTextField = new TextField();
        _badgeTextField.defaultTextFormat = TextFormatTemplate.getInstance().badge;

        _badgeTextField.autoSize = TextFieldAutoSize.LEFT;
        _badgeTextField.x = badgeIcon.width / 2 - _badgeTextField.width / 2;
        _badgeTextField.y = badgeIcon.height / 2 - _badgeTextField.height / 2 - MultiDpiHelper.scale(1);

        addChild(_badgeTextField);
    }

    public function setCount(count:int):void {
        _badgeTextField.text = "" + count;

        _badgeTextField.x = badgeIcon.width / 2 - _badgeTextField.width / 2;
        _badgeTextField.y = badgeIcon.height / 2 - _badgeTextField.height / 2 - MultiDpiHelper.scale(1);
    }

}
}
