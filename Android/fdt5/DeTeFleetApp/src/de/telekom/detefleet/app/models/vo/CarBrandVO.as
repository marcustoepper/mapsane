package de.telekom.detefleet.app.models.vo
{
/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class CarBrandVO
{
	public var brand:String;
	public var id:int;
	public var logo:String;

	public function CarBrandVO(object:Object = null):void
	{
		if (object == null) {
			return;
		}

		brand = object.brand;
		id = object.id;
		logo = object.logo;
	}
}
}
