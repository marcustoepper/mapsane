package de.telekom.detefleet.app.models.vo
{
import de.telekom.detefleet.app.models.JSONable;
import com.adobe.utils.DateUtil;
/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class RepairRequestVO implements JSONable
{
	public var vehicleServiceCase:VehicleServiceCaseVO;
	public var garage:GarageVO;
	public var createDate:Date;

	public var id:Number;
	public var mileage:Number;
	public var notes:String;
	public var status:String;
	public var phone:String;
	
	public function RepairRequestVO(object:Object = null):void
	{
		if (object == null) {
			return;
		}

		vehicleServiceCase = new VehicleServiceCaseVO(object.serviceCase);
		garage = new GarageVO(object.garage);
		if(object.createDate && object.createDate.length == 19){
			object.createDate += "+00:00";
		}
		createDate = DateUtil.parseW3CDTF(object.createDate);
		id = object.id;
		mileage = object.mileage;
		notes = object.notes;
		status = object.status;
		phone = object.phone;
	}

	public function toJSON():String
	{
		return '"request":{"mileage":'+mileage+', "serviceCase":'+vehicleServiceCase.id+', "phoneNumber":"'+phone+'", "garage":'+garage.id+', "description":"'+notes+'"}';
	}
}
}