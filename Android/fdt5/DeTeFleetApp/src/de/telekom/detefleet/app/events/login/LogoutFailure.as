package de.telekom.detefleet.app.events.login
{
import org.osflash.signals.Signal;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class LogoutFailure extends Signal
{
	public function LogoutFailure()
	{
		super();
	}
}
}
