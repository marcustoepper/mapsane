package de.telekom.detefleet.app.views.components.buttons.actionBar
{
import de.aoemedia.mobile.ui.components.buttons.AbstractButton;
import de.aoemedia.mobile.ui.components.skins.ButtonBitmaps;
import de.telekom.detefleet.app.assets.IconRepository;
import de.telekom.detefleet.app.assets.ScaleBitmaps;

import flash.display.Bitmap;
	
	public class ImprintButton extends AbstractButton
	{
		
		protected var _icon:Bitmap;
		
		public function ImprintButton()
		{
			var sBmps:ScaleBitmaps = ScaleBitmaps.getInstance();
			var bmps:ButtonBitmaps 
					= new ButtonBitmaps(sBmps.transparentButton,
										sBmps.transparentButtonDown,
										sBmps.transparentButton,
										sBmps.transparentButton);
			super(bmps);
		}
		
		override protected function createChildren ( ):void
		{
			_icon = IconRepository.iconImprint;
			addChild(_icon);
			
			super.createChildren();
		}
		
		override public function setSize ( w:Number = -1, h:Number = -1 ):void
		{
			super.setSize(w,h);
			
			_icon.x = (componentWidth - _icon.width) >> 1;
			_icon.y = (componentHeight - _icon.height) >> 1;
			//_icon.y = (componentHeight / 2) - (_icon.height / 2);
		}		
		
	}
}