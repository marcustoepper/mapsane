package de.telekom.detefleet.app.views.mediators
{
import de.telekom.detefleet.app.events.MessageSelected;
import de.telekom.detefleet.app.events.view.ViewReady;
import de.telekom.detefleet.app.models.MessageModel;
import de.telekom.detefleet.app.models.vo.MessageVO;
import de.telekom.detefleet.app.views.components.views.MessageDetailView;

import org.robotlegs.mvcs.Mediator;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class MessageDetailMediator extends Mediator
{
	[Inject]
	public var view:MessageDetailView;

	[Inject]
	public var viewReady:ViewReady;

	[Inject]
	public var messageSelected:MessageSelected;

	/**
	 *
	 */
	public override function onRegister():void {
		messageSelected.add(handleMessageSelected);

		viewReady.dispatch(view);
	}

	private function handleMessageSelected(message:MessageVO):void {
		view.message = message;

	}

	override public function preRemove():void {

		super.preRemove();

		messageSelected.remove(handleMessageSelected);
	}

}
}
