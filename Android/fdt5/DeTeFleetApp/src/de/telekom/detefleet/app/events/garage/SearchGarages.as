package de.telekom.detefleet.app.events.garage
{
import de.telekom.detefleet.app.models.vo.GarageSearchRequestVO;

import org.osflash.signals.Signal;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class SearchGarages extends Signal
{
	public function SearchGarages()
	{
		super(GarageSearchRequestVO);
	}
}
}
