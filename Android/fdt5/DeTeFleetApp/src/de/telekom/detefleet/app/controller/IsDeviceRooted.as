package de.telekom.detefleet.app.controller {
import de.aoemedia.ane.securityTools.IJailbreakDetection;
import de.telekom.detefleet.app.events.JailbreakCheckComplete;
import de.telekom.detefleet.app.models.ViewsModel;
import de.telekom.detefleet.app.views.components.views.JailbreakView;

import flash.display.DisplayObject;

import org.robotlegs.mvcs.SignalCommand;

public class IsDeviceRooted extends SignalCommand {
    [Inject]
    public var jailbreakDetection:IJailbreakDetection;
    [Inject]
    public var viewsModel:ViewsModel;
    [Inject]
    public var jailbreakCheckComplete:JailbreakCheckComplete;

    override public function execute():void {
        if (jailbreakDetection.isRooted()) {
            var initialView:JailbreakView = new JailbreakView();
            viewsModel.viewContainer.addChild(DisplayObject(initialView));
            viewsModel.addView(JailbreakView, initialView);
            viewsModel.currentView = initialView;

            return;
        }

        jailbreakCheckComplete.dispatch();
    }
}
}
