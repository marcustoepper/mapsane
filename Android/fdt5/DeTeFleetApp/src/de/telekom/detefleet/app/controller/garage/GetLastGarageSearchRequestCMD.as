package de.telekom.detefleet.app.controller.garage
{
import de.telekom.detefleet.app.events.garage.LastGarageRequestLoaded;
import de.telekom.detefleet.app.models.DatabaseModel;
import de.telekom.detefleet.app.models.GarageModel;
import de.telekom.detefleet.app.models.vo.GarageSearchRequestVO;
import flash.data.SQLResult;
import flash.data.SQLStatement;
import flash.errors.SQLError;
import org.as3commons.logging.api.ILogger;
import org.as3commons.logging.api.getLogger;
import org.robotlegs.mvcs.SignalCommand;



/**
 * TODO Do we need this?
 * Reads Last Garages Search Request from Memory or from Database
 * 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class GetLastGarageSearchRequestCMD extends SignalCommand
{
	[Inject]
	public var databaseModel:DatabaseModel;

	[Inject]
	public var model:GarageModel;

	[Inject]
	public var lastGarageSearchRequestLoaded:LastGarageRequestLoaded;

	private static const logger:ILogger = getLogger(GetLastGarageSearchRequestCMD);

	/**
	 * TODO move in model
	 * 
	 */
	public override function execute():void
	{
		// First check if lastRequest is in Memory
		if (model.lastRequest) {
			lastGarageSearchRequestLoaded.dispatch(model.lastRequest);
			return;
		}

		// else read from Database
		var request:GarageSearchRequestVO = new GarageSearchRequestVO();
		var selectStatement:SQLStatement = new SQLStatement();
		selectStatement.text = request.selectStatement();
		selectStatement.sqlConnection = databaseModel.connection;

		try {
			selectStatement.execute();
		} catch(error:SQLError) {
			logger.error("DatabaseError: " + error.message);
		}
		var result:SQLResult = selectStatement.getResult();

		if (result.data && result.data.length > 0) {
			request.fillStatementResult(result.data[0]);
			model.lastRequest = request;

			lastGarageSearchRequestLoaded.dispatch(request);
		}
	}
}
}