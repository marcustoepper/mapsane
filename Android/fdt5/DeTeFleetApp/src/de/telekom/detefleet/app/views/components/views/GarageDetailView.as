package de.telekom.detefleet.app.views.components.views
{
import com.kaizimmer.ui.Screen;

import de.aoemedia.mobile.ui.MultiDpiHelper;
import de.aoemedia.mobile.ui.components.BitmapComponent;
import de.aoemedia.mobile.ui.components.ClickableLabelComponent;
import de.aoemedia.mobile.ui.components.LabelComponent;
import de.aoemedia.mobile.ui.components.buttons.AbstractButton;
import de.aoemedia.mobile.ui.components.container.LayoutContainer;
import de.aoemedia.mobile.ui.components.container.LayoutOrientation;
import de.telekom.detefleet.app.assets.Color;
import de.telekom.detefleet.app.assets.IconRepository;
import de.telekom.detefleet.app.assets.TextFormatTemplate;
import de.telekom.detefleet.app.models.vo.AddressVO;
import de.telekom.detefleet.app.models.vo.GarageVO;
import de.telekom.detefleet.app.views.components.buttons.ActionButton;
import de.telekom.detefleet.app.views.components.buttons.DefaultLabelButton;

import flash.text.TextFieldAutoSize;

import org.osflash.signals.Signal;

import flash.globalization.LocaleID;
import flash.globalization.NumberFormatter;
import flash.text.TextFormat;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class GarageDetailView extends NavigationBarView
{
	protected var _garage:GarageVO;

	protected var iconLabelPadding:Number = 24;
	protected var infoLeftPadding:Number = 35;

	public var titleLabel:LabelComponent;
	public var distanceLabel:LabelComponent;
	public var distanceIcon:BitmapComponent;
	public var address:LabelComponent;
	public var telephone:ClickableLabelComponent;
	public var routeButton:DefaultLabelButton;
	public var takeButton:ActionButton;

	protected var addressLayout:LayoutContainer;
	protected var phoneLayout:LayoutContainer;
	protected var phoneLabel:LabelComponent;
	protected var titleLayout:LayoutContainer;

	/**
	 * Signal send if Route Here Button clicked
	 */
	public var routeButtonClicked:Signal = new Signal();
	public var takeButtonClicked:Signal = new Signal(GarageVO);
	public var telephoneButtonClicked:Signal = new Signal();

	protected var siteLayout:LayoutContainer;
	protected var headerLayout:LayoutContainer;



	protected static var distanceFormatter:NumberFormatter;

	/**
	 * Construct
	 */
	public function GarageDetailView():void
	{
		super("Werkstattdetail");

		if (distanceFormatter == null) {
			distanceFormatter = new NumberFormatter(LocaleID.DEFAULT);
			distanceFormatter.fractionalDigits = 1;
			distanceFormatter.trailingZeros = true;
		}

		paddingBottom = 11;
	}

	/**
	 * 
	 */
	public function set garage(value:GarageVO):void {
		_garage = value;

		titleLabel.label = _garage.name;
		// addBrandIcon(_garage.brand);
		distanceLabel.label = distanceFormatter.formatNumber(_garage.distance) + " km";
		setAddressLabel(_garage.address);
		telephone.label = _garage.phone;

		siteLayout.invalidate();
        siteLayout.addItemsToView();

		invalidate();

	}

	/**
	 * 
	 */
	public function get garage():GarageVO {
		return _garage;
	}

	/**
	 * Fill adresslabel with data
	 */
	protected function setAddressLabel(address:AddressVO):void
	{
		var addressString:String = address.street + " " + address.houseNumber + "\n";
		addressString += address.zip + " " + address.city;

		this.address.label = addressString;
	}

	protected function setStyles():void
	{
		var textFormat:TextFormat = TextFormatTemplate.getInstance().headline;
		titleLabel.textFormat = textFormat;

		textFormat = TextFormatTemplate.getInstance().detailDistance;
		distanceLabel.textFormat = textFormat;

		textFormat =  TextFormatTemplate.getInstance().content;
		address.textFormat = textFormat;
		phoneLabel.textFormat = textFormat;
		
		textFormat.color = Color.HIGHLIGHT;
		telephone.textFormat = textFormat;
		
		routeButton.textFormat =  TextFormatTemplate.getInstance().buttonNormal;
	}

	override protected function createChildren():void
	{
		super.createChildren();

		siteLayout = new LayoutContainer(this);

		siteLayout.y = navbar.height;
		siteLayout.explicitWidth = Screen.stage.stageWidth;
		siteLayout.gap = 10;
		siteLayout.paddingTop = 11;
		siteLayout.paddingLeft = 11;
		siteLayout.paddingRight = 11;
		siteLayout.paddingBottom = 11;

		/**
		 * TITLE
		 */
		titleLayout = new LayoutContainer(this);
		titleLayout.orientation = LayoutOrientation.HORIZONTAL;
		titleLayout.percentalWidth = 100;
		titleLabel = new LabelComponent();
		titleLabel.textField.autoSize = TextFieldAutoSize.LEFT;
		titleLabel.textField.multiline = true;
		titleLabel.textField.wordWrap = true;
		titleLabel.textField.width = scale(250);
		titleLayout.addItem(titleLabel);

		distanceLabel = new LabelComponent();
		distanceLabel.right = 0;
		titleLayout.addItem(distanceLabel);

		siteLayout.addItem(titleLayout);

		headerLayout = new LayoutContainer(this);
		headerLayout.percentalWidth = 100;

		/**
		 * ADDRESS
		 */

		headerLayout.orientation = LayoutOrientation.HORIZONTAL;

		addressLayout = new LayoutContainer(this);

		address = new LabelComponent();
		address.disableScrolling();
		addressLayout.addItem(address);

		phoneLayout = new LayoutContainer(this);
		phoneLayout.orientation = LayoutOrientation.HORIZONTAL;
		phoneLayout.gap = 1;
		phoneLabel = new LabelComponent();
		phoneLabel.label = "Tel.: ";
		phoneLabel.disableScrolling();
		phoneLayout.addItem(phoneLabel);
		telephone = new ClickableLabelComponent();
		telephone.signalClicked.add(onTelephoneButtonClicked);
		phoneLayout.addItem(telephone);
		addressLayout.addItem(phoneLayout);

		headerLayout.addItem(addressLayout);

		distanceIcon = IconRepository.road;
		distanceIcon.right = 0;


		headerLayout.addItem(distanceIcon);
		siteLayout.addItem(headerLayout);

		/**
		 * Route Button
		 */
		routeButton = new DefaultLabelButton();
		routeButton.setSize(siteLayout.innerWidth/scaleFactor, 38);
		routeButton.label = "Route hierher";

		routeButton.signalClicked.add(onRouteButtonClicked);

		takeButton = new ActionButton("Übernehmen");
		takeButton.setSize(siteLayout.innerWidth/scaleFactor, 37);
		takeButton.signalClicked.add(handleTakeButtonClicked);

		setStyles();

    }

	private function handleTakeButtonClicked(button:AbstractButton):void
	{
		takeButtonClicked.dispatch(garage);
	}

	protected function onTelephoneButtonClicked(button:LabelComponent):void
	{
		telephoneButtonClicked.dispatch();
	}

	protected function onRouteButtonClicked(button:AbstractButton):void
	{
		routeButtonClicked.dispatch();
	}

	public function showTakeButton():void
	{
        if(!siteLayout.contains(takeButton)){
            siteLayout.addItem(takeButton);
        }
	}

	public function hideTakeButton():void
	{
		siteLayout.removeItem(takeButton);
	}

	public function showRouteButton():void
	{
        if(!siteLayout.contains(routeButton)){
		    siteLayout.addItem(routeButton);
        }
	}

	public function hideRouteButton():void
	{
		siteLayout.removeItem(routeButton);
	}

}
}
