package de.telekom.detefleet.app.views.components.views {
import com.kaizimmer.ui.Screen;

import de.aoemedia.mobile.ui.components.BitmapComponent;
import de.aoemedia.mobile.ui.components.ClickableLabelComponent;
import de.aoemedia.mobile.ui.components.LabelComponent;
import de.aoemedia.mobile.ui.components.RuleComponent;
import de.aoemedia.mobile.ui.components.buttons.AbstractButton;
import de.aoemedia.mobile.ui.components.container.LayoutContainer;
import de.aoemedia.mobile.ui.components.container.LayoutOrientation;
import de.aoemedia.mobile.ui.components.container.LayoutVerticalAlign;
import de.telekom.detefleet.app.assets.BrandBitmapRepository;
import de.telekom.detefleet.app.assets.Color;
import de.telekom.detefleet.app.assets.IconRepository;
import de.telekom.detefleet.app.assets.TextFormatTemplate;
import de.telekom.detefleet.app.models.vo.AddressVO;
import de.telekom.detefleet.app.models.vo.FillingStationVO;
import de.telekom.detefleet.app.views.components.buttons.DefaultLabelButton;

import flash.globalization.LocaleID;
import flash.globalization.NumberFormatter;
import flash.text.TextFormat;

import org.osflash.signals.Signal;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class FillingStationDetailView extends ScrollableNavigatorBarView {
    protected var _lastFillingStation:FillingStationVO;
    protected var _fillingStation:FillingStationVO;

    protected var iconLabelPadding:Number = 24;
    protected var infoLeftPadding:Number = 35;

    public var titleLabel:LabelComponent;
    public var distanceLabel:LabelComponent;

    public var address:LabelComponent;
    public var telephone:ClickableLabelComponent;
    public var routeButton:DefaultLabelButton;

    public var brandIcon:BitmapComponent;
    public static var distanceIcon:BitmapComponent;
    public static var businessHoursIcon:BitmapComponent;
    public static var carWashIcon:BitmapComponent;
    public static var garageIcon:BitmapComponent;
    public static var shopIcon:BitmapComponent;

    public var businessHoursLabel:LabelComponent;
    public var garageLabel:LabelComponent;
    public var carWashLabel:LabelComponent;
    public var shopLabel:LabelComponent;
    public var extrasLayout:LayoutContainer;
    protected var garageLayout:LayoutContainer;
    protected var carWashLayout:LayoutContainer;
    protected var shopLayout:LayoutContainer;
    protected var businessHoursLayout:LayoutContainer;
    protected var addressLayout:LayoutContainer;
    protected var phoneLayout:LayoutContainer;
    protected var phoneLabel:LabelComponent;
    protected var titleLayout:LayoutContainer;

    protected static var distanceFormatter:NumberFormatter;

    /**
     * Signal send if Route Here Button clicked
     */
    public var routeButtonClicked:Signal;
    public var telephoneButtonClicked:Signal;

    // Layouts
    protected var siteLayout:LayoutContainer;
    protected var headerLayout:LayoutContainer;

    /**
     * Construct
     */
    public function FillingStationDetailView():void {
        super("Ihre Tankstelle");

        routeButtonClicked = new Signal();
        telephoneButtonClicked = new Signal();

        if (distanceFormatter == null) {
            distanceFormatter = new NumberFormatter(LocaleID.DEFAULT);
            distanceFormatter.fractionalDigits = 1;
            distanceFormatter.trailingZeros = true;
        }

        paddingBottom = 11;
        paddingRight = 11;
    }

    /**
     * Populate FillingStation
     */
    public function set fillingStation(value:FillingStationVO):void {
        if (_fillingStation == value) {
            return;
        }
        _lastFillingStation = _fillingStation;
        _fillingStation = value;

        titleLabel.label = _fillingStation.brand;
        addBrandIcon(_fillingStation.brand);
        distanceLabel.label = distanceFormatter.formatNumber(_fillingStation.distance) + " km";
        setAddressLabel(_fillingStation.address);
        telephone.label = _fillingStation.phone;

        extrasLayout.clear(true);
        if (_fillingStation.garage) {
            addGarage();
        }
        if (_fillingStation.carWash) {
            addCarWash();
        }
        if (_fillingStation.shop) {
            addShop();
        }
        addBusinessHours(_fillingStation.businessHours);

        siteLayout.stopInvalidation = false;
        siteLayout.invalidate();
        siteLayout.addItemsToView();

        invalidate();
    }

    /**
     *
     */
    public function get fillingStation():FillingStationVO {
        return _fillingStation;
    }

    /**
     * Fill adresslabel with data
     */
    protected function setAddressLabel(address:AddressVO):void {
        var addressString:String = address.street + " " + address.houseNumber + "\n";
        addressString += address.zip + " " + address.city;

        this.address.label = addressString;
    }

    /**
     * Add a Brand Icon to stage
     */
    protected function addBrandIcon(brand:String):void {
        if (_lastFillingStation && brand == _lastFillingStation.brand) {
            return;
        }
        if (brandIcon) {
            headerLayout.removeItem(brandIcon);
        }

        brandIcon = BrandBitmapRepository.getBrandIcon(brand);

        if (brandIcon == null) {
            return;
        }

        brandIcon.x = 10 * scaleFactor;
        brandIcon.y = _navbar.height + 29 * scaleFactor;
        headerLayout.addItemAt(brandIcon, 0);
    }

    /**
     * Add Busines Hours to Label
     * @param hours
     */
    protected function addBusinessHours(hours:Vector.<String>):void {
        var text:String = "";
        for (var i:int = 0; i < hours.length; i++) {
            text += hours[i] + "\n";
        }

        businessHoursLabel.label = text;
    }

    /**
     * Set Styles
     */
    protected function setStyles():void {
        var textFormat:TextFormat = TextFormatTemplate.getInstance().headline;
        titleLabel.textFormat = textFormat;

        textFormat = TextFormatTemplate.getInstance().detailDistance;
        distanceLabel.textFormat = textFormat;

        textFormat = TextFormatTemplate.getInstance().content;
        address.textFormat = textFormat;
        phoneLabel.textFormat = textFormat;
        businessHoursLabel.textFormat = textFormat;
        carWashLabel.textFormat = textFormat;
        shopLabel.textFormat = textFormat;
        garageLabel.textFormat = textFormat;

        textFormat.color = Color.HIGHLIGHT;
        telephone.textFormat = textFormat;

        routeButton.textFormat = TextFormatTemplate.getInstance().buttonNormal;
    }

    /**
     *
     */
    override protected function createChildren():void {
        super.createChildren();

        siteLayout = new LayoutContainer(this);
        siteLayout.stopInvalidation = true;
        siteLayout.y = navbar.height;
        siteLayout.explicitWidth = Screen.stage.stageWidth;
        siteLayout.gap = 10;
        siteLayout.setPadding(11, 11, 11, 11);

        /**
         * TITLE
         */
        titleLayout = new LayoutContainer(this);
        titleLayout.orientation = LayoutOrientation.HORIZONTAL;
        titleLayout.percentalWidth = 100;

        titleLabel = new LabelComponent();
        titleLayout.addItem(titleLabel);

        distanceLabel = new LabelComponent();
        distanceLabel.right = 0;
        titleLayout.addItem(distanceLabel);

        siteLayout.addItem(titleLayout);

        headerLayout = new LayoutContainer(this);
        headerLayout.percentalWidth = 100;
        headerLayout.orientation = LayoutOrientation.HORIZONTAL;

        /**
         * ADDRESS
         */
        addressLayout = new LayoutContainer(this);
        addressLayout.left = infoLeftPadding + iconLabelPadding;

        address = new LabelComponent();
        address.disableScrolling();
        addressLayout.addItem(address);

        phoneLayout = new LayoutContainer(this);
        phoneLayout.orientation = LayoutOrientation.HORIZONTAL;
        phoneLayout.gap = 1;
        phoneLabel = new LabelComponent();
        phoneLabel.label = "Tel.: ";
        phoneLabel.disableScrolling();
        phoneLayout.addItem(phoneLabel);

        telephone = new ClickableLabelComponent();
        telephone.signalClicked.add(onTelephoneButtonClicked);
        phoneLayout.addItem(telephone);
        addressLayout.addItem(phoneLayout);

        headerLayout.addItem(addressLayout);

        if (distanceIcon == null) {
            distanceIcon = IconRepository.road;
        }
        distanceIcon.right = 0;

        headerLayout.addItem(distanceIcon);
        siteLayout.addItem(headerLayout);

        /**
         * RULER 1
         */
        var rule:RuleComponent = new RuleComponent(0.5, 200, Color.LIGHT_GREY);
        rule.paddingLeft = infoLeftPadding - 4;
        siteLayout.addItem(rule);

        addBusinessHoursLayout();

        var rule2:RuleComponent = new RuleComponent(0.5, 200, Color.LIGHT_GREY); // TODO remove hardcoded width
        rule2.marginLeft = infoLeftPadding - 4; // Todo remove hardcoded margin addition
        siteLayout.addItem(rule2);

        extrasLayout = new LayoutContainer(this);
        extrasLayout.paddingLeft = infoLeftPadding;
        extrasLayout.gap = 3;

        siteLayout.addItem(extrasLayout);

        /**
         * Route Button
         */
        routeButton = new DefaultLabelButton();
        routeButton.setSize(152, 38);
        routeButton.left = 50;
        routeButton.label = "Route hierher";

        routeButton.signalClicked.add(onRouteButtonClicked);

        siteLayout.addItem(routeButton);

        carWashLabel = new LabelComponent();
        shopLabel = new LabelComponent();
        garageLabel = new LabelComponent();

        setStyles();
    }

    /**
     *
     */
    private function addBusinessHoursLayout():void {
        businessHoursLayout = new LayoutContainer(this);
        businessHoursLayout.orientation = LayoutOrientation.HORIZONTAL;
        businessHoursLayout.marginLeft = infoLeftPadding;

        if (businessHoursIcon == null) {
            businessHoursIcon = IconRepository.clock;
        }
        businessHoursIcon.paddingTop = 1*scaleFactor;

        businessHoursLabel = new LabelComponent();
        businessHoursLabel.disableScrolling();
        businessHoursLabel.left = iconLabelPadding;

        businessHoursLayout.addItem(businessHoursIcon);
        businessHoursLayout.addItem(businessHoursLabel);

        siteLayout.addItem(businessHoursLayout);
    }

    /**
     *
     */
    private function addCarWash():void {
        carWashLayout = new LayoutContainer(this);
        carWashLayout.orientation = LayoutOrientation.HORIZONTAL;
        carWashLayout.verticalAlign = LayoutVerticalAlign.MIDDLE;

        if (carWashIcon == null) {
            carWashIcon = IconRepository.carwash;
        }
        carWashLabel.label = "Waschanlage";
        carWashLabel.left = iconLabelPadding;

        carWashLayout.addItem(carWashIcon);
        carWashLayout.addItem(carWashLabel);

        extrasLayout.addItem(carWashLayout);
    }

    /**
     *
     */
    private function addShop():void {
        shopLayout = new LayoutContainer(this);
        shopLayout.orientation = LayoutOrientation.HORIZONTAL;
        shopLayout.verticalAlign = LayoutVerticalAlign.MIDDLE;
        if (shopIcon == null) {
            shopIcon = IconRepository.cart;
        }
        shopLabel.label = "Shop";
        shopLabel.left = iconLabelPadding;

        shopLayout.addItem(shopIcon);
        shopLayout.addItem(shopLabel);

        extrasLayout.addItem(shopLayout);
    }

    /**
     *
     */
    private function addGarage():void {
        garageLayout = new LayoutContainer(this);
        garageLayout.orientation = LayoutOrientation.HORIZONTAL;
        garageLayout.verticalAlign = LayoutVerticalAlign.MIDDLE;

        if (garageIcon == null) {
            garageIcon = IconRepository.tool;
        }
        garageLabel.label = "Werkstatt";
        garageLabel.left = iconLabelPadding;

        garageLayout.addItem(garageIcon);
        garageLayout.addItem(garageLabel);

        extrasLayout.addItem(garageLayout);
    }

    protected function onTelephoneButtonClicked(button:LabelComponent):void {
        telephoneButtonClicked.dispatch();
    }

    protected function onRouteButtonClicked(button:AbstractButton):void {
        routeButtonClicked.dispatch();
    }

}
}
