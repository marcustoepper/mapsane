package de.telekom.detefleet.app.views.components.views
{
import com.kaizimmer.ui.Screen;

import flash.display.Bitmap;
import flash.system.Capabilities;

public class LaunchImageView extends AbstractView
{
	[Embed(source="../../../../../../../../assets/templates/launchImage/Default.png")]
	protected var launchImage_1x:Class;
	[Embed(source="../../../../../../../../assets/templates/launchImage/Default@2x.png")]
	protected var launchImage_2x:Class;

	protected var launchBmp:Bitmap;

	override protected function createChildren():void {
		var dpiClass:uint = Screen.dpiClass;
		switch (dpiClass) {
            case Screen.AT1X:
            case Screen.LDPI:
                launchBmp = new launchImage_1x() as Bitmap;
                break;
            case Screen.AT2X:
            case Screen.XHDPI:
            default:
                launchBmp = new launchImage_2x() as Bitmap;
                break;
		}

        launchBmp.width = Capabilities.screenResolutionX;
        launchBmp.height = Capabilities.screenResolutionY;
		launchBmp.y = Screen.stage.stageHeight - launchBmp.height;
		addChild(launchBmp);
	}

}
}
