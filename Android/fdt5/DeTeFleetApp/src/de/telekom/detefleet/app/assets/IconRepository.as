package de.telekom.detefleet.app.assets {

import de.aoemedia.mobile.ui.components.BitmapComponent;

/**
 * Here you find the Icons
 *
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class IconRepository {
    [Embed(source="../../../../../../assets/gfx/xhdpi/icons/unread-xhdpi.png")]
    protected static const unread_xhdpi:Class;
    [Embed(source="../../../../../../assets/gfx/hdpi/icons/unread-hdpi.png")]
    protected static const unread_hdpi:Class;
    [Embed(source="../../../../../../assets/gfx/mdpi/icons/unread-mdpi.png")]
    protected static const unread_mdpi:Class;

    public static function get unread():BitmapComponent {
        var bitmap:BitmapComponent = MultiResolutionBitmaps.getBitmapByResolution(unread_xhdpi, unread_hdpi, unread_mdpi);
        bitmap.baseWidth = 12;
        bitmap.baseHeight = 12;
        return bitmap;
    }

    [Embed(source="../../../../../../assets/gfx/xhdpi/icons/redc_xhdpi.png")]
    protected static const redc_xhdpi:Class;
    [Embed(source="../../../../../../assets/gfx/hdpi/icons/redc_hdpi.png")]
    protected static const redc_hdpi:Class;
    [Embed(source="../../../../../../assets/gfx/mdpi/icons/redc_mdpi.png")]
    protected static const redc_mdpi:Class;

    public static function get badge():BitmapComponent {
        var bitmap:BitmapComponent = MultiResolutionBitmaps.getBitmapByResolution(redc_xhdpi, redc_hdpi, redc_mdpi);
        bitmap.baseWidth = 25;
        bitmap.baseHeight = 25;
        return bitmap;
    }

    [Embed(source="../../../../../../assets/gfx/xhdpi/icons/check-xhdpi.png")]
    protected static const check_xhdpi:Class;
    [Embed(source="../../../../../../assets/gfx/hdpi/icons/check-hdpi.png")]
    protected static const check_hdpi:Class;
    [Embed(source="../../../../../../assets/gfx/mdpi/icons/check-mdpi.png")]
    protected static const check_mdpi:Class;

    public static function get check():BitmapComponent {
        var bitmap:BitmapComponent = MultiResolutionBitmaps.getBitmapByResolution(check_xhdpi, check_hdpi, check_mdpi);
        bitmap.baseWidth = 27;
        bitmap.baseHeight = 22;
        return bitmap;
    }

    [Embed(source="../../../../../../assets/gfx/xhdpi/icons/cross-xhdpi.png")]
    protected static const cross_xhdpi:Class;
    [Embed(source="../../../../../../assets/gfx/hdpi/icons/cross-hdpi.png")]
    protected static const cross_hdpi:Class;
    [Embed(source="../../../../../../assets/gfx/mdpi/icons/cross-mdpi.png")]
    protected static const cross_mdpi:Class;

    public static function get cross():BitmapComponent {
        var bitmap:BitmapComponent = MultiResolutionBitmaps.getBitmapByResolution(cross_xhdpi, cross_hdpi, cross_mdpi);
        bitmap.baseWidth = 12;
        bitmap.baseHeight = 12;
        return bitmap;
    }

    [Embed(source="../../../../../../assets/gfx/xhdpi/icons/mail-xhdpi.png")]
    protected static const mail_xhdpi:Class;
    [Embed(source="../../../../../../assets/gfx/hdpi/icons/mail-hdpi.png")]
    protected static const mail_hdpi:Class;
    [Embed(source="../../../../../../assets/gfx/mdpi/icons/mail-mdpi.png")]
    protected static const mail_mdpi:Class;

    public static function get mail():BitmapComponent {
        var bitmap:BitmapComponent = MultiResolutionBitmaps.getBitmapByResolution(mail_xhdpi, mail_hdpi, mail_mdpi);
        bitmap.baseWidth = 23;
        bitmap.baseHeight = 20;
        return bitmap;
    }

    [Embed(source="../../../../../../assets/gfx/xhdpi/icons/call-xhdpi.png")]
    protected static const call_xhdpi:Class;
    [Embed(source="../../../../../../assets/gfx/hdpi/icons/call-hdpi.png")]
    protected static const call_hdpi:Class;
    [Embed(source="../../../../../../assets/gfx/mdpi/icons/call-mdpi.png")]
    protected static const call_mdpi:Class;

    public static function get call():BitmapComponent {
        var bitmap:BitmapComponent = MultiResolutionBitmaps.getBitmapByResolution(call_xhdpi, call_hdpi, call_mdpi);
        bitmap.baseWidth = 20;
        bitmap.baseHeight = 20;
        return bitmap;
    }

    [Embed(source="../../../../../../assets/gfx/xhdpi/icons/notruf-xhdpi.png")]
    protected static const notruf_xhdpi:Class;
    [Embed(source="../../../../../../assets/gfx/hdpi/icons/notruf-hdpi.png")]
    protected static const notruf_hdpi:Class;
    [Embed(source="../../../../../../assets/gfx/mdpi/icons/notruf-mdpi.png")]
    protected static const notruf_mdpi:Class;

    public static function get notruf():BitmapComponent {
        var bitmap:BitmapComponent = MultiResolutionBitmaps.getBitmapByResolution(notruf_xhdpi, notruf_hdpi, notruf_mdpi);
        bitmap.baseWidth = 16;
        bitmap.baseHeight = 16;
        return bitmap;
    }

    [Embed(source="../../../../../../assets/gfx/xhdpi/icons/plus-xhdpi.png")]
    protected static const plus_xhdpi:Class;
    [Embed(source="../../../../../../assets/gfx/hdpi/icons/plus-hdpi.png")]
    protected static const plus_hdpi:Class;
    [Embed(source="../../../../../../assets/gfx/mdpi/icons/plus-mdpi.png")]
    protected static const plus_mdpi:Class;

    public static function get plus():BitmapComponent {
        var bitmap:BitmapComponent = MultiResolutionBitmaps.getBitmapByResolution(plus_xhdpi, plus_hdpi, plus_mdpi);
        bitmap.baseWidth = 24;
        bitmap.baseHeight = 24;
        return bitmap;
    }

    [Embed(source="../../../../../../assets/gfx/mdpi/icons/dropdown-arrow_mdpi.png")]
    protected static const iconArrowDown_mdpi:Class;
    [Embed(source="../../../../../../assets/gfx/hdpi/icons/dropdown-arrow_hdpi.png")]
    protected static const iconArrowDown_hdpi:Class;
    [Embed(source="../../../../../../assets/gfx/xhdpi/icons/dropdown-arrow_xhdpi.png")]
    protected static const iconArrowDown_xhdpi:Class;

    public static function get iconArrowDown():BitmapComponent {
        var bitmap:BitmapComponent = MultiResolutionBitmaps.getBitmapByResolution(iconArrowDown_xhdpi, iconArrowDown_hdpi, iconArrowDown_mdpi);
        bitmap.baseWidth = 9;
        bitmap.baseHeight = 8;
        return bitmap;
    }

    [Embed(source="../../../../../../assets/gfx/mdpi/icons/right-arrow_mdpi.png")]
    protected static const iconArrow_mdpi:Class;
    [Embed(source="../../../../../../assets/gfx/xhdpi/icons/right-arrow_xhdpi.png")]
    protected static const iconArrow_xhdpi:Class;
    [Embed(source="../../../../../../assets/gfx/hdpi/icons/right-arrow_hdpi.png")]
    protected static const iconArrow_hdpi:Class;

    public static function get iconArrow():BitmapComponent {
        var bitmap:BitmapComponent = MultiResolutionBitmaps.getBitmapByResolution(iconArrow_xhdpi, iconArrow_hdpi, iconArrow_mdpi);
        bitmap.baseWidth = 10;
        bitmap.baseHeight = 12;
        return bitmap;
    }

    /**************************************************
     * FILLINGSTATION DETAIL ICONS
     **************************************************/
    [Embed(source="../../../../../../assets/gfx/xhdpi/icons/cart_xhdpi.png")]
    protected static const cart_xhdpi:Class;
    [Embed(source="../../../../../../assets/gfx/hdpi/icons/cart_hdpi.png")]
    protected static const cart_hdpi:Class;
    [Embed(source="../../../../../../assets/gfx/mdpi/icons/cart_mdpi.png")]
    protected static const cart_mdpi:Class;

    public static function get cart():BitmapComponent {
        var bitmap:BitmapComponent = MultiResolutionBitmaps.getBitmapByResolution(cart_xhdpi, cart_hdpi, cart_mdpi);
        bitmap.baseWidth = 12;
        bitmap.baseHeight = 12;
        return bitmap;
    }

    [Embed(source="../../../../../../assets/gfx/xhdpi/icons/carw_xhdpi.png")]
    protected static const carwash_xhdpi:Class;
    [Embed(source="../../../../../../assets/gfx/hdpi/icons/carw_hdpi.png")]
    protected static const carwash_hdpi:Class;
    [Embed(source="../../../../../../assets/gfx/mdpi/icons/carw_mdpi.png")]
    protected static const carwash_mdpi:Class;

    public static function get carwash():BitmapComponent {
        var bitmap:BitmapComponent = MultiResolutionBitmaps.getBitmapByResolution(carwash_xhdpi, carwash_hdpi, carwash_mdpi);
        bitmap.baseWidth = 12;
        bitmap.baseHeight = 16;
        return bitmap;
    }

    [Embed(source="../../../../../../assets/gfx/xhdpi/icons/clock_xhdpi.png")]
    protected static const clock_xhdpi:Class;
    [Embed(source="../../../../../../assets/gfx/hdpi/icons/clock_hdpi.png")]
    protected static const clock_hdpi:Class;
    [Embed(source="../../../../../../assets/gfx/mdpi/icons/clock_mdpi.png")]
    protected static const clock_mdpi:Class;

    public static function get clock():BitmapComponent {
        var bitmap:BitmapComponent = MultiResolutionBitmaps.getBitmapByResolution(clock_xhdpi, clock_hdpi, clock_mdpi);
        bitmap.baseWidth = 12;
        bitmap.baseHeight = 12;
        return bitmap;
    }

    [Embed(source="../../../../../../assets/gfx/xhdpi/icons/tool_xhdpi.png")]
    protected static const tool_xhdpi:Class;
    [Embed(source="../../../../../../assets/gfx/hdpi/icons/tool_hdpi.png")]
    protected static const tool_hdpi:Class;
    [Embed(source="../../../../../../assets/gfx/mdpi/icons/tool_mdpi.png")]
    protected static const tool_mdpi:Class;

    public static function get tool():BitmapComponent {
        var bitmap:BitmapComponent = MultiResolutionBitmaps.getBitmapByResolution(tool_xhdpi, tool_hdpi, tool_mdpi);
        bitmap.baseWidth = 12;
        bitmap.baseHeight = 12;
        return bitmap;

    }

    [Embed(source="../../../../../../assets/gfx/xhdpi/icons/road_xhdpi.png")]
    protected static const road_xhdpi:Class;
    [Embed(source="../../../../../../assets/gfx/hdpi/icons/road_hdpi.png")]
    protected static const road_hdpi:Class;
    [Embed(source="../../../../../../assets/gfx/mdpi/icons/road_mdpi.png")]
    protected static const road_mdpi:Class;

    public static function get road():BitmapComponent {
        var bitmap:BitmapComponent = MultiResolutionBitmaps.getBitmapByResolution(road_xhdpi, road_hdpi, road_mdpi);
        bitmap.baseWidth = 36;
        bitmap.baseHeight = 36;
        return bitmap;

    }

    /**************************************************
     * DIALOG ICONS
     **************************************************/
    [Embed(source="../../../../../../assets/gfx/mdpi/icons/warning-mdpi.png")]
    protected static const warning_icon_mdpi:Class;
    [Embed(source="../../../../../../assets/gfx/xhdpi/icons/warning-xhdpi.png")]
    protected static const warning_icon_xhdpi:Class;
    [Embed(source="../../../../../../assets/gfx/hdpi/icons/warning-hdpi.png")]
    protected static const warning_icon_hdpi:Class;

    public static function get warningIcon():BitmapComponent {
        var bitmap:BitmapComponent = MultiResolutionBitmaps.getBitmapByResolution(warning_icon_xhdpi, warning_icon_hdpi, warning_icon_mdpi);
        bitmap.baseWidth = 22;
        bitmap.baseHeight = 22;
        return bitmap;
    }

    /**************************************************
     * ACTION BAR ICONS
     **************************************************/
    [Embed(source="../../../../../../assets/gfx/xhdpi/icons/i.png")]
    protected static const iconImprint_mdpi:Class;
    [Embed(source="../../../../../../assets/gfx/xhdpi/icons/i.png")]
    protected static const iconImprint_hdpi:Class;
    [Embed(source="../../../../../../assets/gfx/xhdpi/icons/i.png")]
    protected static const iconImprint_xhdpi:Class;
// TODO Missing i in hdpi and xhdpi
    public static function get iconImprint():BitmapComponent {
        var bitmap:BitmapComponent = MultiResolutionBitmaps.getBitmapByResolution(iconImprint_xhdpi, iconImprint_hdpi, iconImprint_xhdpi);
        bitmap.baseWidth = 8;
        bitmap.baseHeight = 19.5;
        return bitmap;

    }

    [Embed(source="../../../../../../assets/gfx/xhdpi/icons/list.png")]
    protected static const list_xhdpi:Class;
    [Embed(source="../../../../../../assets/gfx/hdpi/icons/list.png")]
    protected static const list_hdpi:Class;
    [Embed(source="../../../../../../assets/gfx/mdpi/icons/list.png")]
    protected static const list_mdpi:Class;

    public static function get list():BitmapComponent {
        var bitmap:BitmapComponent = MultiResolutionBitmaps.getBitmapByResolution(list_xhdpi, list_hdpi, list_mdpi);
        bitmap.baseWidth = 24;
        bitmap.baseHeight = 21;
        return bitmap;
    }

    [Embed(source="../../../../../../assets/gfx/xhdpi/icons/map.png")]
    protected static const map_xhdpi:Class;
    [Embed(source="../../../../../../assets/gfx/hdpi/icons/map.png")]
    protected static const map_hdpi:Class;
    [Embed(source="../../../../../../assets/gfx/mdpi/icons/map.png")]
    protected static const map_mdpi:Class;

    public static function get map():BitmapComponent {
        var bitmap:BitmapComponent = MultiResolutionBitmaps.getBitmapByResolution(map_xhdpi, map_hdpi, map_mdpi);
        bitmap.baseWidth = 26;
        bitmap.baseHeight = 26;
        return bitmap;

    }

    /**************************************************
     * DASHBOARD ICONS
     **************************************************/
    [Embed(source="../../../../../../assets/gfx/xhdpi/icons/Tanken-xhdpi.png")]
    protected static const iconRefueling_xhdpi:Class;
    [Embed(source="../../../../../../assets/gfx/hdpi/icons/Tanken-hdpi.png")]
    protected static const iconRefueling_hdpi:Class;
    [Embed(source="../../../../../../assets/gfx/mdpi/icons/Tanken-mdpi.png")]
    protected static const iconRefueling_mdpi:Class;

    public static function get iconRefueling():BitmapComponent {
        var bitmap:BitmapComponent = MultiResolutionBitmaps.getBitmapByResolution(iconRefueling_xhdpi, iconRefueling_hdpi, iconRefueling_mdpi);
        bitmap.baseWidth = 28;
        bitmap.baseHeight = 33;
        return bitmap;
    }

    [Embed(source="../../../../../../assets/gfx/mdpi/icons/Kilometerstand-mdpi.png")]
    protected static const iconKilometerReading_mdpi:Class;
    [Embed(source="../../../../../../assets/gfx/hdpi/icons/Kilometerstand-hdpi.png")]
    protected static const iconKilometerReading_hdpi:Class;
    [Embed(source="../../../../../../assets/gfx/xhdpi/icons/Kilometerstand-xhdpi.png")]
    protected static const iconKilometerReading_xhdpi:Class;

    public static function get iconKilometerReading():BitmapComponent {
        var bitmap:BitmapComponent = MultiResolutionBitmaps.getBitmapByResolution(iconKilometerReading_xhdpi, iconKilometerReading_hdpi, iconKilometerReading_mdpi);
        bitmap.baseWidth = 29;
        bitmap.baseHeight = 17;
        return bitmap;
    }

    [Embed(source="../../../../../../assets/gfx/xhdpi/icons/triangle-xhdpi.png")]
    protected static const triangle_xhdpi:Class;
    [Embed(source="../../../../../../assets/gfx/hdpi/icons/triangle-hdpi.png")]
    protected static const triangle_hdpi:Class;
    [Embed(source="../../../../../../assets/gfx/mdpi/icons/triangle-mdpi.png")]
    protected static const triangle_mdpi:Class;

    public static function get iconBreakdownService():BitmapComponent {
        var bitmap:BitmapComponent = MultiResolutionBitmaps.getBitmapByResolution(triangle_xhdpi, triangle_hdpi, triangle_mdpi);
        bitmap.baseWidth = 28;
        bitmap.baseHeight = 28;
        return bitmap;
    }

    [Embed(source="../../../../../../assets/gfx/xhdpi/icons/rep-xhdpi.png")]
    protected static const rep_xhdpi:Class;
    [Embed(source="../../../../../../assets/gfx/hdpi/icons/rep-hdpi.png")]
    protected static const rep_hdpi:Class;
    [Embed(source="../../../../../../assets/gfx/mdpi/icons/rep-mdpi.png")]
    protected static const rep_mdpi:Class;

    public static function get repairHistoryDashboard():BitmapComponent {
        var bitmap:BitmapComponent = MultiResolutionBitmaps.getBitmapByResolution(rep_xhdpi, rep_hdpi, rep_mdpi);
        bitmap.baseWidth = 28;
        bitmap.baseHeight = 32;
        return bitmap;
    }

    [Embed(source="../../../../../../assets/gfx/mdpi/icons/Werkstattsuche-mdpi.png")]
    protected static const iconGarage_mdpi:Class;
    [Embed(source="../../../../../../assets/gfx/hdpi/icons/Werkstattsuche-hdpi.png")]
    protected static const iconGarage_hdpi:Class;
    [Embed(source="../../../../../../assets/gfx/xhdpi/icons/Werkstattsuche-xhdpi.png")]
    protected static const iconGarage_xhdpi:Class;

    public static function get iconGarage():BitmapComponent {
        var bitmap:BitmapComponent = MultiResolutionBitmaps.getBitmapByResolution(iconGarage_xhdpi, iconGarage_hdpi, iconGarage_mdpi);
        bitmap.baseWidth = 29;
        bitmap.baseHeight = 29;
        return bitmap;
    }

    [Embed(source="../../../../../../assets/gfx/xhdpi/icons/message-xhdpi.png")]
    protected static const message_xhdpi:Class;
    [Embed(source="../../../../../../assets/gfx/hdpi/icons/message-hdpi.png")]
    protected static const message_hdpi:Class;
    [Embed(source="../../../../../../assets/gfx/mdpi/icons/message-mdpi.png")]
    protected static const message_mdpi:Class;

    public static function get message():BitmapComponent {
        var bitmap:BitmapComponent = MultiResolutionBitmaps.getBitmapByResolution(message_xhdpi, message_hdpi, message_mdpi);
        bitmap.baseWidth = 28;
        bitmap.baseHeight = 28;
        return bitmap;
    }

    [Embed(source="../../../../../../assets/gfx/xhdpi/icons/logout-xhdpi.png")]
    protected static const logout_xhdpi:Class;
    [Embed(source="../../../../../../assets/gfx/hdpi/icons/logout-hdpi.png")]
    protected static const logout_hdpi:Class;
    [Embed(source="../../../../../../assets/gfx/mdpi/icons/logout-mdpi.png")]
    protected static const logout_mdpi:Class;

    public static function get logoutDashboard():BitmapComponent {
        var bitmap:BitmapComponent = MultiResolutionBitmaps.getBitmapByResolution(logout_xhdpi, logout_hdpi, logout_mdpi);
        bitmap.baseWidth = 28;
        bitmap.baseHeight = 28;
        return bitmap;
    }
}
}