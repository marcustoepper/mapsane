package de.telekom.detefleet.app.views.components.selectbox
{
import de.aoemedia.mobile.ui.components.LabelComponent;
import de.aoemedia.mobile.ui.components.container.LayoutContainer;
import de.aoemedia.mobile.ui.components.container.LayoutOrientation;
import de.telekom.detefleet.app.assets.Color;
import de.telekom.detefleet.app.assets.TextFormatTemplate;

import flash.display.Sprite;

public class SelectBoxButtonWrapper extends LayoutContainer
{
	public static const POSITION_TOP:String = "top";
	public static const POSITION_LEFT:String = "left";

	protected var _label:LabelComponent;
	protected var _selectBoxButton:SelectBoxButton;

	protected var _labelPosition:String;

	public function SelectBoxButtonWrapper(view:Sprite, prompt:String = "") {
		super(view);
		orientation = LayoutOrientation.HORIZONTAL;

		createChildren(prompt);
        applySkin();
	}

	/**
	 *
	 */
	protected function createChildren(prompt:String):void {
		_label = new LabelComponent();
		_selectBoxButton = new SelectBoxButton(prompt);
		_selectBoxButton.right = 0;
		_selectBoxButton.labelColor = Color.DARK_GREY;
		_selectBoxButton.labelColorDisabled = Color.MEDIUM_GREY;

		addItem(_label);
		addItem(_selectBoxButton);
	}

	override public function invalidate(force:Boolean = false):void {
		super.invalidate(force);

		if(isNaN(percentalWidth)) {
			return;
		}
		if(_labelPosition == POSITION_TOP) {
			_label.paddingLeft = -2;
			_selectBoxButton.right = NaN;

			var width:Number = innerWidth/scaleFactor;
			if(width < 0) {
				return;
			}

			_selectBoxButton.setSize(width, 37);
		}
	}

	protected function applySkin():void {
		_label.textFormat = TextFormatTemplate.getInstance().formLabel;
	}

	public function get selectBoxButton():SelectBoxButton {
		return _selectBoxButton;
	}

	public function set label(value:String):void {
		_label.label = value;
	}

	public function get label():String {
		return _label.label;
	}

	public function get labelPosition():String {
		return _labelPosition;
	}

	public function set labelPosition(labelPosition:String):void {
		if(_labelPosition == labelPosition) {
			return;
		}
		_labelPosition = labelPosition;

		if(labelPosition == POSITION_TOP) {
			orientation = LayoutOrientation.VERTICAL;
		} else {
			orientation = LayoutOrientation.HORIZONTAL;
		}

		invalidate();
	}

}
}
