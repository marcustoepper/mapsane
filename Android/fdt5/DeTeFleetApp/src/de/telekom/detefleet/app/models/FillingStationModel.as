package de.telekom.detefleet.app.models
{
import de.telekom.detefleet.app.events.fillingStation.FuelTypesChanged;
import de.telekom.detefleet.app.events.fillingStation.SearchRequestLoaded;
import de.telekom.detefleet.app.events.fillingStation.SearchResultChanged;
import de.telekom.detefleet.app.models.vo.FillingStationRequestVO;
import de.telekom.detefleet.app.models.vo.FillingStationVO;
import de.telekom.detefleet.app.models.vo.FuelTypeVO;

import org.as3commons.logging.api.ILogger;
import org.as3commons.logging.api.getLogger;
import org.robotlegs.mvcs.Actor;

import flash.data.SQLConnection;
import flash.data.SQLResult;
import flash.data.SQLStatement;
import flash.errors.SQLError;

/**
 * Store some data related to FillingStations
 * 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class FillingStationModel extends Actor
{
	private static const logger:ILogger = getLogger(FillingStationModel);

	// Signals
	[Inject]
	public var searchResultChanged:SearchResultChanged;
	[Inject]
	public var fuelTypesChanged:FuelTypesChanged;
	[Inject]
	public var searchRequestLoaded:SearchRequestLoaded;

	/**
	 * Includes the Last Search Request for Filling Stations
	 */
	private var _lastRequest:FillingStationRequestVO;

	/**
	 * Includes the FuelTypes of the Request
	 */
	private var _fuelTypes:Vector.<FuelTypeVO>;

	/**
	 * Includes the FillingStations of the Last Search
	 */
	private var _searchResult:Vector.<FillingStationVO>;

	public function get searchResult():Vector.<FillingStationVO> {
		return _searchResult;
	}

	public function set searchResult(searchResult:Vector.<FillingStationVO>):void {
		this._searchResult = searchResult;
		searchResultChanged.dispatch(searchResult);
	}

	public function get fuelTypes():Vector.<FuelTypeVO> {
		return _fuelTypes;
	}

	public function set fuelTypes(fuelTypes:Vector.<FuelTypeVO>):void {
		this._fuelTypes = fuelTypes;
		fuelTypesChanged.dispatch(fuelTypes);
	}

	/**
	 * remove all data from Memory
	 */
	public function cleanData():void{
		_fuelTypes = null;
		_searchResult = null;
		_lastRequest = null;
	}

	/**
	 * Persist Search Request in DB
	 */
	public function persistSearchRequest(request:FillingStationRequestVO, sqlConnection:SQLConnection):void
	{
		// First save in memory
		_lastRequest = request;

		// Then persist in Database
		sqlConnection.begin();

		try {
			var deleteStatement:SQLStatement = new SQLStatement();
			deleteStatement.sqlConnection = sqlConnection;
			deleteStatement.text = request.deleteStatement();
			deleteStatement.execute();

			var insertSearchRequest:SQLStatement = new SQLStatement();
			insertSearchRequest.sqlConnection = sqlConnection;
			insertSearchRequest.text = request.insertStatement();
			request.setStatementParameters(insertSearchRequest);
			insertSearchRequest.execute();

			sqlConnection.commit();
		} catch (error:SQLError) {
			logger.error("DatabaseError: " + error.message);
			sqlConnection.rollback();
		}
	}

	/**
	 * Load Search Request from Memory or DB
	 */
	public function loadSearchRequest(sqlConnection:SQLConnection):void
	{
		// First check if lastRequest is in Memory
		if (_lastRequest) {
			searchRequestLoaded.dispatch(_lastRequest);
			return;
		}

		// else read from Database
		var request:FillingStationRequestVO = new FillingStationRequestVO();
		var selectStatement:SQLStatement = new SQLStatement();
		selectStatement.text = request.selectStatement();
		selectStatement.sqlConnection = sqlConnection;

		try {
			selectStatement.execute();
		} catch(error:SQLError) {
			logger.error("DatabaseError: " + error.message);
		}
		var result:SQLResult = selectStatement.getResult();

		if (result.data && result.data.length > 0) {
			request.fillStatementResult(result.data[0]);
			_lastRequest = request;

			searchRequestLoaded.dispatch(request);
			return;
		}

		searchRequestLoaded.dispatch(null);
	}

	/**
	 * Remove all Data from table
	 */
	public function truncateSearchRequest(sqlConnection:SQLConnection):void
	{
		var deleteStatement:SQLStatement = new SQLStatement();
		deleteStatement.text = "DELETE FROM " + FillingStationRequestVO.TABLE;
		deleteStatement.sqlConnection = sqlConnection;

		try {
			deleteStatement.execute();
		} catch(error:SQLError) {
			logger.error("DatabaseError: " + error.message);
		}
	}

}
}
