package de.telekom.detefleet.app.controller
{
import de.telekom.detefleet.app.services.persistence.IKeyChainService;

import org.robotlegs.mvcs.Command;
	
	/**
	 * Remove the Authentification Key from Device. This is the Logout Function.
	 * 
	 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
	 */
	public class RemoveAuthTokenCMD extends Command
	{
		[Inject]
		public var keyChainService:IKeyChainService;
		
		public override function execute ( ):void
		{
			keyChainService.removeItem("authToken");
		}
	}
	
}