package de.telekom.detefleet.app.controller.view
{
import de.telekom.detefleet.app.events.view.RequestView;
import de.telekom.detefleet.app.models.JugglerModel;
import de.telekom.detefleet.app.models.UserModel;
import de.telekom.detefleet.app.models.ViewsModel;
import de.telekom.detefleet.app.models.vo.ViewRequestVO;
import de.telekom.detefleet.app.services.local.IReachable;
import de.telekom.detefleet.app.services.persistence.IKeyChainService;
import de.telekom.detefleet.app.views.components.views.DashboardView;
import de.telekom.detefleet.app.views.components.views.LaunchImageView;
import de.telekom.detefleet.app.views.components.views.LoginView;

import flash.display.DisplayObject;
import flash.utils.setTimeout;

import org.robotlegs.mvcs.Command;

/**
 * Initialize Views. Checks which View should be shown first
 *
 * @author Kai Zimmer <kai.zimmer@aoemedia.de>
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class InitViewsCMD extends Command
{
	[Inject]
	public var viewsModel:ViewsModel;

	[Inject]
	public var jugglerModel:JugglerModel;

	[Inject]
	public var keyChain:IKeyChainService;

	[Inject]
	public var userModel:UserModel;

	[Inject]
	public var reachableMonitor:IReachable;

	[Inject]
	public var requestView:RequestView;

	/**
	 *
	 */
	public override function execute():void {
		var initialView:LaunchImageView = new LaunchImageView();

		jugglerModel.init(initialView.juggler);
		jugglerModel.startJuggler();

		viewsModel.viewContainer.addChild(DisplayObject(initialView));
		viewsModel.addView(LaunchImageView, initialView);
		viewsModel.currentView = initialView;

		setTimeout(dispatchShowHome, 750);
	}

	/**
	 *
	 */
	protected function dispatchShowHome():void {
		var token:String = keyChain.getItem('authToken');
		if(token == null) {
			token = userModel.authToken;
		}
		reachableMonitor.start();

		if(token) {
			requestView.dispatch(new ViewRequestVO(DashboardView));
		} else {
			requestView.dispatch(new ViewRequestVO(LoginView));
		}
	}

}

}
