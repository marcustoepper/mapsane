package de.telekom.detefleet.app.events.repairRequest
{
import de.telekom.detefleet.app.models.vo.RepairRequestVO;
import org.osflash.signals.Signal;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class ResendRepairRequest extends Signal
{
	public function ResendRepairRequest()
	{
		super(RepairRequestVO);
	}
}
}
