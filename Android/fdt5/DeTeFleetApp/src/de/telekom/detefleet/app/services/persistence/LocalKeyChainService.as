package de.telekom.detefleet.app.services.persistence {
	import org.as3commons.logging.api.ILogger;
	import org.as3commons.logging.api.getLogger;
	import org.robotlegs.mvcs.Actor;

	import flash.data.EncryptedLocalStore;
	import flash.utils.ByteArray;
	
	/**
	 * This Servcice store the key/value pair on the Filesystem. If support by the device use the Encrypted File Storage. It uses AES-CBC 128-bit encryption
	 * 
	 * @see http://help.adobe.com/en_US/air/reference/html/flash/data/EncryptedLocalStore.html#includeExamplesSummary
	 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
	 * 
	 */
	public class LocalKeyChainService extends Actor implements IKeyChainService
	{
		private static const logger: ILogger = getLogger( LocalKeyChainService );
		
		/**
		 * Persist the value for the given key. Overwrite existing values.
		 * Should dispatch an success or fault Event.
		 * 
		 * @param key The key under which the value is stored
		 * @param value The value which is stored
		 * 
		 */
		public function setItem(key:String, value:String):void
		{
			if(EncryptedLocalStore.isSupported){
				logger.info("The device support encrypted local Filestorage");
				storeItemInEncryptedLocalStore(key, value);
			}
		}
		
		/**
		 * Get the stored value of a key
		 * 
		 * @param key The key to search for the value
		 * @return the value for the given key
		 * 
		 */
		public function getItem(key:String):String
		{
			if(EncryptedLocalStore.isSupported){
				logger.info("The device support encrypted local Filestorage");
				return readItemFromEncryptedLocalStore(key);
			}
			return null;
		}
		
		/**
		 * Remove the stored value
		 * 
		 * @param key The key for the value which you want to remove
		 * 
		 */
		public function removeItem(key:String):void{
			if(EncryptedLocalStore.isSupported){
				logger.info("The device support encrypted local Filestorage");
				EncryptedLocalStore.removeItem(key);
			}
		}
		
		/**
		 * Store the value for the key in the encrypted storage
		 * 
		 * @param key The key under which the value is stored
		 * @param value The value which is stored
		 * 
		 */
		private function storeItemInEncryptedLocalStore(key:String, value:String):void{
			var byteArrayValue:ByteArray = new ByteArray();
			byteArrayValue.writeUTFBytes(value);
			
			EncryptedLocalStore.setItem(key, byteArrayValue);
		}
		
		/**
		 * Returns the value from the encrypted storage
		 * 
		 * @param key The key to search for the value
		 * @return the value for the given key
		 * 
		 */
		private function readItemFromEncryptedLocalStore(key:String):String{
			var storedValue:ByteArray = EncryptedLocalStore.getItem(key);
			
			if(!storedValue){
				return null;
			}
			return storedValue.readUTFBytes(storedValue.length);
		}
		
	}
}