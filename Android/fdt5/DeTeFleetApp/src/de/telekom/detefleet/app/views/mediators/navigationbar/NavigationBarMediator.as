package de.telekom.detefleet.app.views.mediators.navigationbar
{
import de.telekom.detefleet.app.events.view.GoBack;
import de.telekom.detefleet.app.views.components.navigationbar.NavigationBar;

import org.robotlegs.mvcs.Mediator;

/**
 * ...
 * @author dev.mode
 */
public class NavigationBarMediator extends Mediator
{
	[Inject]
	public var view:NavigationBar;

	[Inject]
	public var goBack:GoBack;

	public override function onRegister():void
	{
		view.signalBackBtnClicked.add(onBackButton);
	}

	protected function onBackButton():void
	{
		if ( view.isHistoryBackDisabled ) return;
		goBack.dispatch();
	}

	override public function preRemove():void
	{
		view.signalBackBtnClicked.remove(onBackButton);

		super.preRemove();
	}
}
}