package de.telekom.detefleet.app.views.components.buttons
{
import de.aoemedia.mobile.ui.components.buttons.IconLabelButton;
import de.aoemedia.mobile.ui.components.skins.ButtonBitmaps;
import de.telekom.detefleet.app.assets.Color;
import de.telekom.detefleet.app.assets.IconRepository;
import de.telekom.detefleet.app.assets.ScaleBitmaps;

import flash.display.Bitmap;
import flash.text.TextFormat;
	
	public class DashboardButton extends IconLabelButton
	{
		public function DashboardButton( label:String, tFormat:TextFormat, icon:Bitmap, hideArrow:Boolean = false)
		{
			var sBmps:ScaleBitmaps = ScaleBitmaps.getInstance();
			var bmps:ButtonBitmaps
					= new ButtonBitmaps(sBmps.homeButton,
										sBmps.homeButtonDown,
										sBmps.homeButton,
										sBmps.homeButtonDown);
			
			
			super(bmps);
			stopInvalidation = true;
			_leftPadding = 12 * scaleFactor;
			_leftLabelPadding = 51 * scaleFactor;
			_rightPadding = 12 * scaleFactor;
			isLabelCentered = false;
			
			leftIcon = icon;
			
			if(!hideArrow){
				var arrIcon:Bitmap = IconRepository.iconArrow;
				rightIcon = arrIcon;
			}
			
			this.label = label;
			labelColor = Color.LIST_ITEM;
			labelColorDown = Color.DARK_GREY;
			setSize(320, 49);
			textFormat = tFormat;
			stopInvalidation = false;
		}
		
		public override function set enabled ( value:Boolean ):void
		{
			super.enabled = value;
			
			if(value){
				labelColor = Color.LIST_ITEM;
			} else {
				labelColor = Color.DARK_GREY;
			}
			
			invalidate();
		}
		
	}
}
