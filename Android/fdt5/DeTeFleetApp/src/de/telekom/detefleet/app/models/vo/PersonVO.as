package de.telekom.detefleet.app.models.vo
{
/**
 * Represents a Person
 * 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class PersonVO
{
	public var address:AddressVO;
	public var firstName:String;
	public var lastName:String;
	public var telephoneNumber:String;
	public var vehicles:Vector.<VehicleVO>;

	/**
	 * Construct
	 * 
	 * @param object Anonymous Object which data will be set the properties of this object 
	 */
	public function PersonVO(object:Object = null)
	{
		vehicles = new Vector.<VehicleVO>();
		
		if (object == null) {
			return;
		}
		address = new AddressVO(object.address);
		firstName = object.firstName ;
		lastName = object.lastName ;
		telephoneNumber = object.telephoneNumber ;
	}

}
}