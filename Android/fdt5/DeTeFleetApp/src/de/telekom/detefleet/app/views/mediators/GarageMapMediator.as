package de.telekom.detefleet.app.views.mediators {
import de.aoemedia.motion.TransitionType;
import de.telekom.detefleet.app.events.garage.GarageSelected;
import de.telekom.detefleet.app.events.view.BlockHistoryAdditionRequestView;
import de.telekom.detefleet.app.events.view.RequestView;
import de.telekom.detefleet.app.events.view.TransitionChanged;
import de.telekom.detefleet.app.events.view.TransitionCompleted;
import de.telekom.detefleet.app.events.view.ViewReady;
import de.telekom.detefleet.app.models.GarageModel;
import de.telekom.detefleet.app.models.GeolocationModel;
import de.telekom.detefleet.app.models.vo.AddressVO;
import de.telekom.detefleet.app.models.vo.GarageVO;
import de.telekom.detefleet.app.models.vo.TransitionStateVO;
import de.telekom.detefleet.app.models.vo.ViewRequestVO;
import de.telekom.detefleet.app.views.components.views.GarageDetailView;
import de.telekom.detefleet.app.views.components.views.GarageListView;
import de.telekom.detefleet.app.views.components.views.GarageMapView;

import org.as3commons.logging.api.ILogger;
import org.as3commons.logging.api.getLogger;
import org.robotlegs.mvcs.Mediator;

/**
 *
 */
public class GarageMapMediator extends Mediator {
    /**
     * VIEW
     */
    [Inject]
    public var view:GarageMapView;

    // Signals
    [Inject]
    public var transitionChanged:TransitionChanged;
    [Inject]
    public var transitionCompleted:TransitionCompleted;
    [Inject]
    public var gotoMapView:BlockHistoryAdditionRequestView;
    [Inject]
    public var geolocationModel:GeolocationModel;
    [Inject]
    public var garageSelected:GarageSelected;
    [Inject]
    public var gotoView:RequestView;
    [Inject]
    public var viewReady:ViewReady;

    // Models
    [Inject]
    public var model:GarageModel;


    private static const logger:ILogger = getLogger(GarageMapMediator);

    private var searchResultDirty:Boolean = true;

    /**
     *
     *
     */
    public override function onRegister():void {
        // Add Listener
        view.signalToggleListButtonClicked.add(toggleList);

        // Listen to the context
        transitionCompleted.add(handleTransitionCompleted);
        transitionChanged.add(handleTransitionChanged);

        viewReady.dispatch(view);
    }

    /**
     *
     * @param state
     */
    protected function handleTransitionCompleted(state:TransitionStateVO):void {
        if (state.viewToShow != view) {
            return;
        }
        view.showMap();

        if (state.viewToHide is GarageListView) {
            populateGarages();
        }
    }

    /**
     *
     * @param state
     */
    protected function handleTransitionChanged(state:TransitionStateVO):void {
        if (state.viewToHide == view && state.type == TransitionStateVO.TRANSITIONING) {
            view.hideMap();
        }
    }

    /**
     *
     * @param message
     */
    protected function calloutSelected(message:Object):void {
        var tmpGarage:GarageVO;
        for (var i:int = 0; i < model.searchResult.length; i++) {
            if (model.searchResult[i].id == message.markerId) {
                tmpGarage = model.searchResult[i];
                break;
            }
        }

        if (tmpGarage == null) {
            return;
        }

        view.hideMap();
        gotoView.dispatch(new ViewRequestVO(GarageDetailView));
        garageSelected.dispatch(tmpGarage);
    }

    /**
     *
     */
    private function populateGarages():void {
        var addressToShow:AddressVO;

        switch (model.searchResult.length) {
            case 0:
                return;
            // If there are more entries than the "Regelwerkstatt" (which always should be the first entry)
            // show the second garage entry else show the "Regelwerkstatt"
            case 1:
                addressToShow = model.searchResult[0].address;
                break;
            default:
                addressToShow = model.searchResult[1].address;
                break;
        }

        view.map.clear();
        view.map.resetCenter(addressToShow.latitude, addressToShow.longitude);
        view.map.calloutSelected.add(calloutSelected);

        for (var i:int = 0; i < model.searchResult.length; i++) {
            var garage:GarageVO = model.searchResult[i];

            var calloutContent1:Object = {title:garage.name, subtitle:garage.address.street};
            view.map.addMarker(garage.id, garage.address.latitude, garage.address.longitude, {title:"", subtitle:""}, calloutContent1);
        }
    }

    /**
     *
     */
    protected function toggleList():void {
        gotoMapView.dispatch(new ViewRequestVO(GarageListView, TransitionType.SLIDE_DOWN));
    }

    override public function preRemove():void {
        view.signalToggleListButtonClicked.remove(toggleList);
        transitionChanged.remove(handleTransitionChanged);
        transitionCompleted.remove(handleTransitionChanged);

        super.preRemove();
    }


}
}