package de.telekom.detefleet.app.controller.repairRequest
{
import de.telekom.detefleet.app.models.DialogModel;
import de.telekom.detefleet.app.models.LayoutData;
import de.telekom.detefleet.app.models.RepairRequestModel;
import de.telekom.detefleet.app.models.vo.RepairRequestVO;
import de.telekom.detefleet.app.views.components.views.RepairRequestConfirmationCallGarageView;
import de.telekom.detefleet.app.views.components.views.RepairRequestConfirmationView;

import org.robotlegs.mvcs.SignalCommand;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class ShowRepairRequestSendConfirmationCMD extends SignalCommand
{
	[Inject]
	public var layout:LayoutData;

	// Signal Payload
	[Inject]
	public var request:RepairRequestVO;
	[Inject]
	public var callGarage:Boolean;

	[Inject]
	public var dialogModel:DialogModel;

	[Inject]
	public var model:RepairRequestModel;

	override public function execute():void {
		model.sucessfulSendedRepairRequest = request;

		if(callGarage) {
			var callGarageDialog:RepairRequestConfirmationCallGarageView = new RepairRequestConfirmationCallGarageView(layout.viewport);
			dialogModel.addDialog(callGarageDialog);
		} else {
			var confirmDialog:RepairRequestConfirmationView = new RepairRequestConfirmationView(layout.viewport);
			dialogModel.addDialog(confirmDialog);
		}
	}
}
}
