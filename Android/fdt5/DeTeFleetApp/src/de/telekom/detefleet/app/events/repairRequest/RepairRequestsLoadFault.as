package de.telekom.detefleet.app.events.repairRequest
{
import de.telekom.detefleet.app.events.ServiceFaultSignal;
/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class RepairRequestsLoadFault extends ServiceFaultSignal
{
}
}
