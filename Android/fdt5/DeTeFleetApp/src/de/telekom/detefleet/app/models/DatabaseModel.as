package de.telekom.detefleet.app.models
{
import org.robotlegs.mvcs.Actor;

import flash.data.SQLConnection;

/**
 * Store data related to Database Connections
 * 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class DatabaseModel extends Actor
{
	/**
	 * Filename of used DB
	 */
	public static const FILENAME:String = "DeTeFleetApp.db";

	/**
	 * The Database Connection.
	 */
	public var connection:SQLConnection;
}
}