package de.telekom.detefleet.app.controller.garage
{
import de.telekom.detefleet.app.models.GarageModel;

import org.robotlegs.mvcs.SignalCommand;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class CleanGarageListCMD extends SignalCommand
{
	[Inject]
	public var model:GarageModel;
	
	public override function execute():void
	{
		model.searchResult = null;
	}
}
}
