package de.telekom.detefleet.app.services.remote
{
import de.telekom.detefleet.app.events.ServiceFaultSignal;
import de.telekom.detefleet.app.events.fillingStation.FillingStationLoadFault;
import de.telekom.detefleet.app.events.fillingStation.FuelTypesLoadFault;
import de.telekom.detefleet.app.models.FillingStationModel;
import de.telekom.detefleet.app.models.vo.FillingStationRequestVO;
import de.telekom.detefleet.app.models.vo.FillingStationVO;
import de.telekom.detefleet.app.models.vo.FuelTypeVO;
import de.telekom.detefleet.app.models.vo.ServiceAuthVO;

import org.as3commons.logging.api.ILogger;
import org.as3commons.logging.api.getLogger;
import org.robotlegs.mvcs.Actor;

import flash.events.Event;
import flash.events.HTTPStatusEvent;
import flash.events.IEventDispatcher;
import flash.events.IOErrorEvent;
import flash.events.SecurityErrorEvent;
import flash.net.URLLoader;
import flash.net.URLRequest;
import flash.net.URLRequestHeader;
import flash.net.URLRequestMethod;



/**
 * Implementation for an Json Service to get some Filling Stations.
 * It uses the JSONServiceConfiguration for getting Configuration
 * 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class JSONFillingStationService extends Actor implements IFillingStationService
{
	private static const logger:ILogger = getLogger(JSONFillingStationService);

	// Model
	[Inject]
	public var fillingStationModel:FillingStationModel;

	// Signals
	[Inject]
	public var fillingStationLoadFault:FillingStationLoadFault;
	[Inject]
	public var fuelTypesLoadFault:FuelTypesLoadFault;
	[Inject]
	public var serviceFaultSignal:ServiceFaultSignal;

	/**
	 * Search for Filling Stations.
	 * 
	 * If successful it fills the FillingStationModel.lastSearchResult  with the Result an disptach an FillingStationsLoaded Signal.
	 * If unseccessful it fills the FillingStationLoadFault with the error Message
	 * 
	 * @param auth The Authentification for the Webservice
	 * @param request The request for the Search
	 * 
	 */
	public function search(auth:ServiceAuthVO, request:FillingStationRequestVO):void
	{
		var urlRequest:URLRequest = new URLRequest(JSONServiceConfiguration.endpointUrl + "/fillingstation/search");
		// Content Type Header has to be send if the Request is JSON Type
		var header:URLRequestHeader = new URLRequestHeader("Content-Type", "application/json");
		var loader:URLLoader = new URLLoader();

		urlRequest.data = '{' + auth.toJSON() + ', "data":{' + request.toJSON() + '}}';
		logger.debug("search called: " + urlRequest.data);

		urlRequest.method = URLRequestMethod.POST;
		urlRequest.requestHeaders.push(header);

		addListeners(loader);
		loader.addEventListener(Event.COMPLETE, searchCompleteHandler);
		loader.load(urlRequest);
	}

	/**
	 * Handles the successful Request. 
	 * 
	 * @see JSONFillingStationService.search()
	 * @param event
	 * 
	 */
	protected function searchCompleteHandler(event:Event):void
	{
		var loader:URLLoader = URLLoader(event.target);
		logger.debug("Stations loaded: " + loader.data);
		try {
			var data:Object = JSON.parse(loader.data);
		} catch(e:Error) {
			fillingStationLoadFault.dispatch("Couldn't parse JSON data.");
		}

		if (data.success) {
			var fillingStations:Vector.<FillingStationVO> = new <FillingStationVO>[];

			for ( var i:uint = 0; i < data.data.length; i++ ) {
				fillingStations[i] = new FillingStationVO(data.data[i]);
			}

			fillingStationModel.searchResult = fillingStations;
		} else {
			fillingStationLoadFault.dispatch(data.error);
		}

		loader.removeEventListener(Event.COMPLETE, searchCompleteHandler);
		removeListener(loader);
	}

	/**
	 * Ask the Service for the existing Fuel Types
	 * 
	 * If successful it fills the FillingStationModel.fuelTypes  with the Result an disptach an FuelTypesLoaded Signal.
	 * If unseccessful it fills the FuelTypesLoadFault with the error Message
	 * 
	 * The Method only ask the Webservice for the Fueltypes if they are not on Memory.
	 * 
	 * @param auth The Authentification for the Webservice
	 */
	public function getFuelTypes(auth:ServiceAuthVO):void
	{
		var urlRequest:URLRequest = new URLRequest(JSONServiceConfiguration.endpointUrl + "/fillingstation/getfueltypes");
		var header:URLRequestHeader = new URLRequestHeader("Content-Type", "application/json");
		var loader:URLLoader = new URLLoader();

		urlRequest.data = '{' + auth.toJSON() + '}';
		urlRequest.method = URLRequestMethod.POST;
		urlRequest.requestHeaders.push(header);

		addListeners(loader);
		loader.addEventListener(Event.COMPLETE, getFuelTypesCompleteHandler);
		loader.load(urlRequest);
	}

	/**
	 * Handles successful Request of asking Webservice for FuelTypes
	 * @param event
	 * 
	 */
	protected function getFuelTypesCompleteHandler(event:Event):void
	{
		var loader:URLLoader = URLLoader(event.target);

		try {
			var data:Object = JSON.parse(loader.data);
		} catch(e:Error) {
			fuelTypesLoadFault.dispatch("Couldn't parse returned data.");
		}
		if (data.success) {
			// Convert JSON data into VOs
			var fuelTypes:Vector.<FuelTypeVO> = new <FuelTypeVO>[];
			for (var i:int = 0; i < data.data.length; i++) {
				fuelTypes[i] = new FuelTypeVO(data.data[i]);
			}
			fillingStationModel.fuelTypes = fuelTypes;
		} else {
			fuelTypesLoadFault.dispatch(data.error);
		}

		loader.removeEventListener(Event.COMPLETE, getFuelTypesCompleteHandler);
		removeListener(loader);
	}

	/**
	 * Handles an Security error of the Url Request.
	 * Should be a Problem in Production
	 */
	protected function securityErrorHandler(event:SecurityErrorEvent):void
	{
		removeListener(URLLoader(event.target));
		logger.error(event);
	}

	/**
	 * Handles HTTP Status Response. If its bigger/equal than 400 dispatch an Error
	 */
	protected function httpResponseStatusHandler(event:HTTPStatusEvent):void
	{
		if (event.status >= 400) {
			serviceFaultSignal.dispatch("Der Server ist nicht erreichbar");
			logger.error("httpResponseStatusHandler: " + event);
		}
	}

	/**
	 * Handles the io Errors
	 */
	protected function ioErrorHandler(event:IOErrorEvent):void
	{
		removeListener(URLLoader(event.target));
		serviceFaultSignal.dispatch("Der Server ist nicht erreichbar.");
		logger.error("ioErrorHandler: " + event);
	}

	/**
	 * Adds some basic Listeners
	 */
	protected function addListeners(dispatcher:IEventDispatcher):void
	{
		dispatcher.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
		dispatcher.addEventListener(HTTPStatusEvent.HTTP_RESPONSE_STATUS, httpResponseStatusHandler);
		dispatcher.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
	}

	/**
	 * Remove some Basic Listeners
	 */
	protected function removeListener(dispatcher:IEventDispatcher):void
	{
		dispatcher.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
		dispatcher.removeEventListener(HTTPStatusEvent.HTTP_RESPONSE_STATUS, httpResponseStatusHandler);
		dispatcher.removeEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
	}
}
}
