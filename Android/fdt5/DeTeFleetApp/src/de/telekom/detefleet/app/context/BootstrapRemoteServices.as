package de.telekom.detefleet.app.context
{
import de.aoemedia.ane.securityTools.IJailbreakDetection;
import de.aoemedia.ane.securityTools.JailbreakDetection;
import de.telekom.detefleet.app.services.local.IDeviceKey;
import de.telekom.detefleet.app.services.local.IReachable;
import de.telekom.detefleet.app.services.local.RandomDeviceKey;
import de.telekom.detefleet.app.services.local.URLMonitorReachable;
import de.telekom.detefleet.app.services.persistence.IKeyChainService;
import de.telekom.detefleet.app.services.persistence.LocalKeyChainService;
import de.telekom.detefleet.app.services.remote.IFillingStationService;
import de.telekom.detefleet.app.services.remote.IGarageService;
import de.telekom.detefleet.app.services.remote.ILoginService;
import de.telekom.detefleet.app.services.remote.IMessageService;
import de.telekom.detefleet.app.services.remote.IRepairRequestService;
import de.telekom.detefleet.app.services.remote.JSONFillingStationService;
import de.telekom.detefleet.app.services.remote.JSONGarageService;
import de.telekom.detefleet.app.services.remote.JSONLoginService;
import de.telekom.detefleet.app.services.remote.JSONMessageService;
import de.telekom.detefleet.app.services.remote.JSONRepairRequestService;
import de.telekom.detefleet.app.services.security.ISSLCertificateService;
import de.telekom.detefleet.app.services.security.SSLCertificateService;

import org.robotlegs.core.IInjector;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class BootstrapRemoteServices
{
	public function BootstrapRemoteServices(injector:IInjector)
	{
		injector.mapClass(ILoginService, JSONLoginService);
		injector.mapClass(IFillingStationService, JSONFillingStationService);
		injector.mapClass(IGarageService, JSONGarageService);
		injector.mapClass(IRepairRequestService, JSONRepairRequestService);
		injector.mapClass(IMessageService, JSONMessageService);

		injector.mapClass(IKeyChainService, LocalKeyChainService);
		injector.mapClass(IDeviceKey, RandomDeviceKey);
		injector.mapClass(IReachable, URLMonitorReachable);

        injector.mapClass(IJailbreakDetection, JailbreakDetection);
        injector.mapClass(ISSLCertificateService, SSLCertificateService);
		
	}
}
}
