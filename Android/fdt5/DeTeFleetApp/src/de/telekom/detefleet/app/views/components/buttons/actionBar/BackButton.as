package de.telekom.detefleet.app.views.components.buttons.actionBar
{
import de.aoemedia.mobile.ui.components.buttons.AbstractLabelButton;
import de.aoemedia.mobile.ui.components.skins.ButtonBitmaps;
import de.telekom.detefleet.app.assets.ButtonRepository;
import de.telekom.detefleet.app.assets.TextFormatTemplate;

import org.as3commons.logging.api.ILogger;

import org.as3commons.logging.api.getLogger;

public class BackButton extends AbstractLabelButton
{
	private static const logger:ILogger = getLogger(BackButton);

	public function BackButton(label:String) {
		var mBmps:ButtonRepository = ButtonRepository.getInstance();
		var bmps:ButtonBitmaps
				= new ButtonBitmaps(mBmps.backButton,
				mBmps.backButtonDown,
				mBmps.backButton,
				mBmps.backButtonDown);

		super(bmps, label);

		this.textFormat = TextFormatTemplate.getInstance().actionbarButton;
	}

	override public function setSize(w:Number = -1, h:Number = -1):void {
		super.setSize(w, h);
		_label.x = (componentWidth/2 - _label.width/2) + scale(3);
	}

}
}
