package de.telekom.detefleet.app.views.components.views {

import com.kaizimmer.ui.Screen;

import de.aoemedia.mobile.ui.components.RuleComponent;
import de.telekom.detefleet.app.assets.Color;
import de.telekom.detefleet.app.assets.TextFormatTemplate;

import flash.events.MouseEvent;
import flash.text.TextField;

import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import flash.text.TextFormat;
import flash.utils.Dictionary;

import org.osflash.signals.Signal;

public class ImprintView extends ScrollableNavigatorBarView {
	public var linkClicked:Signal;
	private var links:Object;

	private var content:Array = [
		{head:'Telekom MobilitySolutions', content: 'An der Ziegelei 18 \n\
53217 Bonn'},
		{head: 'Geschäftsführer', content: 'Rainer Heil, Hanna Rieke'},
		{head: 'Telefonnummer', content: '0800 338 335 338'},
		{head: 'Faxnummer', content: '0228 90240-99999'},
		{head: 'E-Mail Adresse', content: 'info.mobilitysolutions@telekom.de', link:"mailto:info.mobilitysolutions@telekom.de"},
		{head: 'Aufsichtsrat:', content: 'Dieter Cazzonelli (Vorsitzender)\n\
\n\
Amtsgericht Bonn: HRB 10034\n\
Sitz der Gesellschaft: Bonn\n\
USt-IdNr. DE 157206090'},
		{head: 'realisiert von:', content: 'T-Systems International GmbH\n\
Konzerngeschäftsfeld Vernetztes Fahrzeug'},
	];

	/**
	 *
	 */
	public function ImprintView() {
		links = {};
		super("Impressum");
		linkClicked = new Signal(String);
		cacheAsBitmap = true;
	}

	/**
	 *
	 */
	override protected function createChildren():void {
		super.createChildren();

		navbar.textFormat = TextFormatTemplate.getInstance().actionbarSmall;

		var paddingLeft:Number = 12;
		var paddingRight:Number = 12;
		var gap:Number = scale(18);
		var leading:Number = scale(8);
		var actualY:Number = navbar.height + gap;

		for (var i:int = 0; i < content.length; i++) {
			var head:TextField = new TextField();

			head.selectable = false;
			head.embedFonts = true;
			head.multiline = true;
			head.wordWrap = true;
			head.defaultTextFormat = TextFormatTemplate.getInstance().headline;
			head.mouseEnabled = false;
			head.text = content[i].head;

			head.autoSize = TextFieldAutoSize.NONE;
			head.width = Screen.stage.stageWidth-scale(paddingLeft + paddingRight);
			head.height = head.textHeight + scale(4) - leading;

			head.y = actualY;
			head.x = scale(paddingLeft);

			addChild(head);
			actualY = head.y + head.height + gap;

			if (content[i].content) {
				var contentTextField:TextField = new TextField();

				contentTextField.selectable = false;
				contentTextField.embedFonts = true;
				contentTextField.multiline = true;
				contentTextField.wordWrap = true;
				contentTextField.mouseEnabled = false;
				var contentTextFormat:TextFormat = TextFormatTemplate.getInstance().content;
				contentTextField.defaultTextFormat = contentTextFormat;

				contentTextField.text = content[i].content;

				if(content[i].link){
					contentTextFormat.color = Color.HIGHLIGHT;
					contentTextField.setTextFormat(contentTextFormat);
					contentTextField.addEventListener(MouseEvent.CLICK, handleLinkClicked);

					if(links == null){
						links = {};
					}
					contentTextField.mouseEnabled = true;
					contentTextField.selectable = false;
					links[content[i].content] = content[i].link;
				}

				contentTextField.autoSize = TextFieldAutoSize.NONE;
				contentTextField.width = Screen.stage.stageWidth-scale(paddingLeft + paddingRight);
				contentTextField.height = contentTextField.textHeight + scale(4);

				contentTextField.y = actualY - gap;
				contentTextField.x = scale(paddingLeft);

				addChild(contentTextField);

				actualY = contentTextField.y + contentTextField.height + gap;
			}
		}
	}

	private function handleLinkClicked(event:MouseEvent):void {
		linkClicked.dispatch(links[TextField(event.currentTarget).text]);

	}
}
}
