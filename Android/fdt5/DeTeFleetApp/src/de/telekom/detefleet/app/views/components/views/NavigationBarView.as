package de.telekom.detefleet.app.views.components.views
{
import com.greensock.TweenLite;

import flash.display.DisplayObject;

import de.telekom.detefleet.app.assets.TextFormatTemplate;
import de.telekom.detefleet.app.views.components.navigationbar.NavigationBar;

/**
 *
 */
public class NavigationBarView extends BaseView
{
	protected var _navbar:NavigationBar;

	public function NavigationBarView(label:String = "", hideBackBtn:Boolean = false) {
		super();
		if(label != "") _navbar.label = label;
		if(hideBackBtn) _navbar.hideBackButton();
	}

	override protected function createChildren():void {
		super.createChildren();

		_navbar = new NavigationBar();

		_navbar.textFormat = TextFormatTemplate.getInstance().actionbarNormal;

		super.addChild(DisplayObject(_navbar));
	}

	public function get navbar():NavigationBar {
		return _navbar;
	}

	public function showNavbar(time:Number = 1):void {
        _navbar.onShow();
		TweenLite.killTweensOf(_navbar);
		TweenLite.to(_navbar, time, { y:0 });
	}

	public function hideNavbar(time:Number = 1):void {
		TweenLite.killTweensOf(_navbar);
		TweenLite.to(_navbar, time, { y:-_navbar.heightIncludingShadow, onComplete:onCompleteHide });
	}

    private function onCompleteHide(): void{
        _navbar.onHide();
    }

    public function onHide():void{
        _navbar.onHide();
    }

    public function onShow():void{
        _navbar.onShow();
    }
}
}
