package de.telekom.detefleet.app.views.components.views {
import com.kaizimmer.ui.Screen;

import de.aoemedia.ane.maps.Map;

import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.geom.Point;
import flash.geom.Rectangle;
import flash.system.Capabilities;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class MapView extends NavigationBarView {
    public var mapScreenshot:Bitmap;
    private static var _map:Map;

    public function MapView(label:String = "") {
        super(label);

        if (_map == null) {
            _map = new Map();
            _map.setZoom(15);
            _map.viewPort = new Rectangle(0, _navbar.y + _navbar.height, Screen.stage.stageWidth, Screen.stage.stageHeight - _navbar.height);
            _map.visible = false;
        }
    }

    public function clear():void {
        _map.clear();
    }

    public function showMap():void {
        _map.visible = true;
    }

    public function hideMap():void {
        _map.visible = false;
    }

    public function makeScreenshot():void {
        var bitmapData:BitmapData = new BitmapData(_map.viewPort.width, _map.viewPort.height);
        _map.drawViewPortToBitmapData(bitmapData);
        mapScreenshot = new Bitmap(bitmapData);
        mapScreenshot.y = navbar.height;
        addChild(mapScreenshot);
    }

    public function get map():Map {
        return _map;
    }

    public function set map(value:Map):void {
        _map = value;
    }
}
}
