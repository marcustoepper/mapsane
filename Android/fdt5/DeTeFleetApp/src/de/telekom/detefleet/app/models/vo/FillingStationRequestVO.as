package de.telekom.detefleet.app.models.vo
{
import de.telekom.detefleet.app.models.JSONable;

import flash.data.SQLStatement;

/**
 * The Request Data to search for FillingStations 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class FillingStationRequestVO implements JSONable, SQLStatementable
{
	public static const SORT_BY_PRICE:String = "PRICE";
	public static const SORT_BY_DISTANCE:String = "DISTANCE";
	
	public static const TABLE:String = "fillingStationRequest";
	
	public var fueltype:int;
	public var sortby:String;
	public var limit:Number = 20;
	public var radius:Number = 10;
	public var location:LocationRequestVO;

	/**
	 * Convert Object to JSON String
	 * 
	 * @return JSON string
	 */
	public function toJSON():String
	{
		return '"appSearchRequest": {"radius":"' + radius + '", "fuelType": "' + fueltype + '", "location":{' + location.toJSON() + '},"sortBy": "' + sortby + '"}';
	}

	public function updateStatement():String
	{
		return "";
	}

	public function insertStatement():String
	{
		return "INSERT INTO fillingStationRequest (fueltype, sortby, limitResult, radius, locationAddress, locationLatitude, locationLongitude) " + "VALUES (:fueltype, :sortBy, :limitResult, :radius, :locationAddress, :locationLatitude, :locationLongitude)";
	}

	public function selectStatement():String
	{
		return "SELECT * FROM fillingStationRequest LIMIT 1";
	}

	public function deleteStatement():String
	{
		return "DELETE FROM fillingStationRequest";
	}

	public function fillStatementResult(row:Object):void
	{
		fueltype = row.fueltype;
		sortby = row.sortBy;
		limit = row.limitResult;
		radius = row.radius;

		location = new LocationRequestVO();
		location.address = row.locationAddress;
		location.latitude = row.locationLatitude;
		location.longitude = row.locationLongitude;
	}

	public function setStatementParameters(statement:SQLStatement):void
	{
		statement.parameters[':fueltype'] = fueltype;
		statement.parameters[':sortBy'] = sortby;
		statement.parameters[':limitResult'] = limit;
		statement.parameters[':radius'] = radius;
		statement.parameters[':locationAddress'] = location.address;
		statement.parameters[':locationLatitude'] = location.latitude;
		statement.parameters[':locationLongitude'] = location.longitude;
	}
}
}