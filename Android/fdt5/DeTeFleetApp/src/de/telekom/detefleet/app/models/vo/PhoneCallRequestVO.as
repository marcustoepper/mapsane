package de.telekom.detefleet.app.models.vo
{
public class PhoneCallRequestVO
{
	public var number:String;
	public var extraInformation:String = "";

	public function PhoneCallRequestVO(number:String, extraInformation:String = ""):void {
		this.number = number;
		this.extraInformation = extraInformation;
	}

}
}
