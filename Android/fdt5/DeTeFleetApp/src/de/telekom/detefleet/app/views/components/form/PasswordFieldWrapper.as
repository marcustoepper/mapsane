package de.telekom.detefleet.app.views.components.form
{
import de.aoemedia.mobile.ui.MultiDpiHelper;
import de.aoemedia.mobile.ui.components.LabelComponent;
import de.aoemedia.mobile.ui.components.forms.InputField;
import de.aoemedia.mobile.ui.components.skins.InputFieldBitmaps;
import de.telekom.detefleet.app.assets.ScaleBitmaps;
import de.telekom.detefleet.app.views.components.views.AbstractView;

import flash.display.Sprite;

import org.bytearray.display.ScaleBitmap;

/**
 *
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 *
 */
public class PasswordFieldWrapper extends InputFieldWrapper
{
	/**
	 *
	 */
	public function PasswordFieldWrapper(view:AbstractView) {
		super(Sprite(view));
	}

	/**
	 *
	 */
	override protected function createChildren():void {
		_label = new LabelComponent();
		_label.paddingLeft = -2;

		var sBmps:ScaleBitmaps = ScaleBitmaps.getInstance();
		var bg:ScaleBitmap = sBmps.input;
		var bmps:InputFieldBitmaps = new InputFieldBitmaps(bg, sBmps.inputTakingInput, bg, sBmps.inputError, sBmps.inputSuccess);

		_input = new InputField(bmps);
		_input.paddingHorizontal = MultiDpiHelper.scale(10);
		_input.paddingVertical = MultiDpiHelper.scale(6);
		_input.setSize(272, 37);

		addItem(_label);
		addItem(_input);

		applySkin();
	}
}
}
