package de.telekom.detefleet.app.assets {
import de.aoemedia.mobile.ui.MultiDpiHelper;

import flash.text.TextFormat;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class TextFormatTemplate {
    private var _textFormat:TextFormat;
    protected static var _instance:TextFormatTemplate;

    public static function getInstance():TextFormatTemplate {
        if (_instance == null) {
            _instance = new TextFormatTemplate(new SingletonEnforcer());
            _instance._textFormat = new TextFormat();
        }
        return _instance;
    }

    // ACTION BAR

    public function get actionbarNormal():TextFormat {
        reset();
        _textFormat.font = "Tele-GroteskNor";
        _textFormat.color = Color.DARK_GREY;
        _textFormat.size = MultiDpiHelper.scale(26);

        return _textFormat;
    }

    public function get actionbarSmall():TextFormat {
        reset();
        _textFormat.font = "Tele-GroteskNor";
        _textFormat.color = Color.DARK_GREY;
        _textFormat.size = MultiDpiHelper.scale(20);

        return _textFormat;
    }

    public function get actionbarExtraSmall():TextFormat {
        reset();
        _textFormat.font = "Tele-GroteskNor";
        _textFormat.color = Color.DARK_GREY;
        _textFormat.size = MultiDpiHelper.scale(16);

        return _textFormat;
    }

    public function get actionbarButton():TextFormat {
        reset();
        _textFormat.font = "Tele-GroteskNor";
        _textFormat.color = Color.DARK_GREY;
        _textFormat.size = MultiDpiHelper.scale(16);

        return _textFormat;
    }

    // Badge
    public function get badge():TextFormat {
        reset();
        _textFormat.font = "Tele-GroteskNor";
        _textFormat.color = 0xffffff;
        _textFormat.size = MultiDpiHelper.scale(10);

        return _textFormat;
    }

    // LIST
    public function get listHeader():TextFormat {
        reset();
        _textFormat.font = "Tele-GroteskHal";
        _textFormat.color = Color.LIST_ITEM;
        _textFormat.size = MultiDpiHelper.scale(16);

        return _textFormat;
    }

    // LIST ITEM
    public function get listItemTitle():TextFormat {
        reset();
        _textFormat.font = "Tele-GroteskHal";
        _textFormat.color = Color.LIST_ITEM;
        _textFormat.size = MultiDpiHelper.scale(19);

        return _textFormat;
    }

    public function get listItemTitleSelected():TextFormat {
        reset();
        _textFormat.font = "Tele-GroteskHal";
        _textFormat.color = Color.DARK_GREY;
        _textFormat.size = MultiDpiHelper.scale(19);

        return _textFormat;
    }

    public function get listItemTitleHighlight():TextFormat {
        reset();
        _textFormat.font = "Tele-GroteskFet";
        _textFormat.color = Color.LIST_ITEM;
        _textFormat.size = MultiDpiHelper.scale(19);

        return _textFormat;
    }

    public function get listItemDistance():TextFormat {
        reset();
        _textFormat.font = "Tele-GroteskNor";
        _textFormat.color = Color.DARK_GREY;
        _textFormat.size = MultiDpiHelper.scale(19);

        return _textFormat;
    }

    public function get listItemDate():TextFormat {
        reset();
        _textFormat.font = "Tele-GroteskNor";
        _textFormat.color = Color.MEDIUM_GREY;
        _textFormat.size = MultiDpiHelper.scale(11);

        return _textFormat;
    }

    public function get listItemPrice():TextFormat {
        reset();
        _textFormat.font = "Tele-GroteskNor";
        _textFormat.color = Color.DARK_GREY;
        _textFormat.size = MultiDpiHelper.scale(16);

        return _textFormat;
    }

    public function get listItemTime():TextFormat {
        reset();
        _textFormat.font = "Tele-GroteskNor";
        _textFormat.color = Color.DARK_GREY;
        _textFormat.size = MultiDpiHelper.scale(10);

        return _textFormat;
    }

    // DIALOG

    public function get dialogHeadline():TextFormat {
        reset();
        _textFormat.font = "Tele-GroteskFet";
        _textFormat.color = Color.DARK_GREY;
        _textFormat.size = MultiDpiHelper.scale(25);

        return _textFormat;
    }

    public function get dialogMessage():TextFormat {
        reset();
        _textFormat.font = "Tele-GroteskHal";
        _textFormat.color = Color.DARK_GREY;
        _textFormat.size = MultiDpiHelper.scale(15);

        return _textFormat;
    }

    public function get dialogExtraInformation():TextFormat {
        reset();
        _textFormat.font = "Tele-GroteskHal";
        _textFormat.color = Color.DARK_GREY;
        _textFormat.size = MultiDpiHelper.scale(10);

        return _textFormat;
    }

    public function get dialogButton():TextFormat {
        reset();
        _textFormat.font = "Tele-GroteskHal";
        _textFormat.color = Color.DARK_GREY;
        _textFormat.size = MultiDpiHelper.scale(19);

        return _textFormat;
    }

    // BUTTONS
    public function get buttonNormal():TextFormat {
        reset();
        _textFormat.font = "Tele-GroteskNor";
        _textFormat.color = Color.DARK_GREY;
        _textFormat.size = MultiDpiHelper.scale(15);

        return _textFormat;
    }

    public function get buttonBig():TextFormat {
        reset();
        _textFormat.font = "Tele-GroteskNor";
        _textFormat.color = Color.DARK_GREY;
        _textFormat.size = MultiDpiHelper.scale(22);

        return _textFormat;
    }

    public function get selectBoxListItem():TextFormat {
        reset();
        _textFormat.font = "Tele-GroteskNor";
        _textFormat.color = Color.DARK_GREY;
        _textFormat.size = MultiDpiHelper.scale(19);

        return _textFormat;
    }

    // FORM
    public function get formLabel():TextFormat {
        reset();
        _textFormat.font = "Tele-GroteskNor";
        _textFormat.color = Color.DARK_GREY;
        _textFormat.size = MultiDpiHelper.scale(16);
        _textFormat.letterSpacing = 0.4;

        return _textFormat;
    }

    public function get formLabelHighlight():TextFormat {
        reset();
        _textFormat.font = "Tele-GroteskFet";
        _textFormat.color = Color.HIGHLIGHT;
        _textFormat.size = MultiDpiHelper.scale(16);

        return _textFormat;
    }

    public function get formInputField():TextFormat {
        reset();
        _textFormat.font = "Tele-GroteskNor";
        _textFormat.color = Color.HIGHLIGHT;
        _textFormat.size = MultiDpiHelper.scale(18);
        _textFormat.letterSpacing = 2;

        return _textFormat;
    }

    // Dashboard Text Box
    public function get dashboardInfoBox():TextFormat {
        reset();
        _textFormat.font = "Tele-GroteskNor";
        _textFormat.color = Color.DARK_GREY;
        _textFormat.size = MultiDpiHelper.scale(14);
        _textFormat.leading = MultiDpiHelper.scale(2);

        return _textFormat;
    }

    // Content
    public function get headline():TextFormat {
        reset();
        _textFormat.font = "Tele-GroteskFet";
        _textFormat.color = Color.DARK_GREY;
        _textFormat.size = MultiDpiHelper.scale(16);
        _textFormat.leading = MultiDpiHelper.scale(8);

        return _textFormat;
    }

    public function get content():TextFormat {
        reset();
        _textFormat.font = "Tele-GroteskNor";
        _textFormat.color = Color.DARK_GREY;
        _textFormat.size = MultiDpiHelper.scale(16);
        _textFormat.leading = MultiDpiHelper.scale(8);

        return _textFormat;
    }

    public function get extraInformation():TextFormat {
        reset();
        _textFormat.font = "Tele-GroteskNor";
        _textFormat.color = Color.MEDIUM_GREY;
        _textFormat.size = MultiDpiHelper.scale(14);

        return _textFormat;
    }

    // Detail
    public function get detailDistance():TextFormat {
        reset();
        _textFormat.font = "Tele-GroteskNor";
        _textFormat.color = Color.DARK_GREY;
        _textFormat.size = MultiDpiHelper.scale(16);

        return _textFormat;
    }

    // Filter
    public function get filterOption():TextFormat {
        reset();
        _textFormat.font = "Tele-GroteskHal";
        _textFormat.color = Color.DARK_GREY;
        _textFormat.size = MultiDpiHelper.scale(12);

        return _textFormat;
    }

    /**
     * Reset to Default
     */
    public function reset():void {
        _textFormat.font = null;
        _textFormat.size = null;
        _textFormat.color = null;
        _textFormat.bold = null;
        _textFormat.italic = null;
        _textFormat.underline = null;
        _textFormat.url = null;
        _textFormat.target = null;
        _textFormat.align = null;
        _textFormat.leftMargin = null;
        _textFormat.rightMargin = null;
        _textFormat.indent = null;
        _textFormat.leading = null;
    }

    public function TextFormatTemplate(sE:SingletonEnforcer) {
    }

}
}

internal class SingletonEnforcer {
}