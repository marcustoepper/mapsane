package de.telekom.detefleet.app.controller.security
{
	import de.telekom.detefleet.app.services.remote.JSONServiceConfiguration;
	import de.telekom.detefleet.app.services.security.ISSLCertificateService;
	
	import org.robotlegs.mvcs.SignalCommand;
	
	public class LoadCertificatesCMD extends SignalCommand
	{
		[Inject]
		public var service:ISSLCertificateService;
		
		override public function execute():void
		{
			var domainToCheck:String = JSONServiceConfiguration.domain;
			trace("LoadCertificatesCMD .execute() domainToCheck: " + domainToCheck);
			
			//service.loadCertificate(JSONServiceConfiguration.domain);
		}
	}
}
