package de.telekom.detefleet.app.models.vo
{
	public class DialogVO
	{
		
		public var title:String;
		public var message:String;
		
		public function DialogVO(title:String, message:String)
		{
			this.title = title;
			this.message = message;
		}
	}
}