package de.telekom.detefleet.app.events.view
{
import de.telekom.detefleet.app.models.vo.ViewRequestVO;

import org.osflash.signals.Signal;
	
	public class RequestView extends Signal
	{
		public function RequestView()
		{
			super(ViewRequestVO);
		}
	}
}