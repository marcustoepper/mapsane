package de.telekom.detefleet.app.events
{
import org.osflash.signals.Signal;

/**
 * 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class NetworkStatusChanged extends Signal
{
	public static const ONLINE:String = 'ONLINE';
	public static const OFFLINE:String = 'OFFLINE';

	public function NetworkStatusChanged()
	{
		super(String);
	}
}
}
