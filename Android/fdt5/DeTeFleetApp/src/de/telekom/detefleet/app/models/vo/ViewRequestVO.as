package de.telekom.detefleet.app.models.vo
{
import de.aoemedia.motion.TransitionType;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class ViewRequestVO
{
	public var viewClass:Class;
	public var transitionType:String;
	public var context:String;

	public function ViewRequestVO(viewClass:Class, transitionType:String = TransitionType.SLIDE_LEFT, context:String = null):void
	{
		this.viewClass = viewClass;
		this.transitionType = transitionType;
		this.context = context;
	}
}
}
