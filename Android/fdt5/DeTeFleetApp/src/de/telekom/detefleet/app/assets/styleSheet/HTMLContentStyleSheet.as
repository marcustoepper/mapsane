package de.telekom.detefleet.app.assets.styleSheet
{
import de.aoemedia.mobile.ui.MultiDpiHelper;
import de.telekom.detefleet.app.assets.FontRepository;

import flash.text.Font;
import flash.text.StyleSheet;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class HTMLContentStyleSheet extends StyleSheet
{
	public function HTMLContentStyleSheet()
	{
		var p:Object = {};
		
		p.fontFamily = Font(new FontRepository.TeleGroteskNor()).fontName;
		p.fontSize = MultiDpiHelper.scale(16);
		p.color = "#4b4b4b"; 
		
		var strong:Object = {};
		strong.fontFamily = Font(new FontRepository.TeleGroteskFet()).fontName;
		
		setStyle("B", strong);
		setStyle("P", p);
	}
}
}
