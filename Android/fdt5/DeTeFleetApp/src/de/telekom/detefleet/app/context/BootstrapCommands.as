package de.telekom.detefleet.app.context
{
import com.kaizimmer.mobile.robotlegs.controller.ForcePortraitModeCMD;

import flash.events.Event;

import de.telekom.detefleet.app.controller.CallPhoneCMD;
import de.telekom.detefleet.app.controller.CleanupAfterLogoutCMD;
import de.telekom.detefleet.app.controller.GetMessageIdsCMD;
import de.telekom.detefleet.app.controller.GetVehicleServiceCasesCMD;
import de.telekom.detefleet.app.controller.HandleAppActivationCMD;
import de.telekom.detefleet.app.controller.HandleAppDeactivationCMD;
import de.telekom.detefleet.app.controller.HideLoaderCMD;
import de.telekom.detefleet.app.controller.InitializeDatabaseCMD;
import de.telekom.detefleet.app.controller.IsDeviceRooted;
import de.telekom.detefleet.app.controller.LoginCMD;
import de.telekom.detefleet.app.controller.LogoutCMD;
import de.telekom.detefleet.app.controller.RemoveAuthTokenCMD;
import de.telekom.detefleet.app.controller.RenewSessionDataCMD;
import de.telekom.detefleet.app.controller.SaveAuthKeyCMD;
import de.telekom.detefleet.app.controller.SetMessageReadCMD;
import de.telekom.detefleet.app.controller.dialog.ShowCallPhoneDialogCMD;
import de.telekom.detefleet.app.controller.fillingStation.GetFuelTypesCMD;
import de.telekom.detefleet.app.controller.fillingStation.LoadSearchRequestCMD;
import de.telekom.detefleet.app.controller.fillingStation.PersistFillingStationServiceRequestCMD;
import de.telekom.detefleet.app.controller.fillingStation.SearchFillingStationsCMD;
import de.telekom.detefleet.app.controller.garage.CleanGarageListCMD;
import de.telekom.detefleet.app.controller.garage.GetLastGarageSearchRequestCMD;
import de.telekom.detefleet.app.controller.garage.SearchGaragesCMD;
import de.telekom.detefleet.app.controller.garage.TakeGarageCMD;
import de.telekom.detefleet.app.controller.message.GetMessagesCMD;
import de.telekom.detefleet.app.controller.mileage.GetMileageCMD;
import de.telekom.detefleet.app.controller.mileage.PersistMileageCMD;
import de.telekom.detefleet.app.controller.repairRequest.GetRepairRequestsCMD;
import de.telekom.detefleet.app.controller.repairRequest.ResendRepairRequestCMD;
import de.telekom.detefleet.app.controller.repairRequest.SendRepairRequestCMD;
import de.telekom.detefleet.app.controller.security.HandleInvalidCertificateCMD;
import de.telekom.detefleet.app.controller.security.HandleValidCertificateCMD;
import de.telekom.detefleet.app.controller.security.LoadCertificatesCMD;
import de.telekom.detefleet.app.controller.selectbox.CreateSelectBoxListCMD;
import de.telekom.detefleet.app.controller.view.InitNetworkViewsModelCMD;
import de.telekom.detefleet.app.controller.view.InitScreenClassCMD;
import de.telekom.detefleet.app.controller.view.InitViewsCMD;
import de.telekom.detefleet.app.controller.view.RemoveViewContextCMD;
import de.telekom.detefleet.app.controller.view.SetupViewContainerCMD;
import de.telekom.detefleet.app.events.CallPhone;
import de.telekom.detefleet.app.events.GetMessageIds;
import de.telekom.detefleet.app.events.GetMessages;
import de.telekom.detefleet.app.events.JailbreakCheckComplete;
import de.telekom.detefleet.app.events.MessageSelected;
import de.telekom.detefleet.app.events.PhoneCallRequested;
import de.telekom.detefleet.app.events.RenewSessionFault;
import de.telekom.detefleet.app.events.RenewSessionSuccess;
import de.telekom.detefleet.app.events.RequestSelectBoxList;
import de.telekom.detefleet.app.events.ServiceFaultSignal;
import de.telekom.detefleet.app.events.ShowSelectBoxList;
import de.telekom.detefleet.app.events.fillingStation.GetFuelTypes;
import de.telekom.detefleet.app.events.fillingStation.GetLastFillingStationSearchRequest;
import de.telekom.detefleet.app.events.fillingStation.SearchFillingStations;
import de.telekom.detefleet.app.events.garage.CleanGarageList;
import de.telekom.detefleet.app.events.garage.GetLastGarageSearchRequest;
import de.telekom.detefleet.app.events.garage.GetVehicleServiceCases;
import de.telekom.detefleet.app.events.garage.SearchGarages;
import de.telekom.detefleet.app.events.login.LoginRequest;
import de.telekom.detefleet.app.events.login.LoginSuccess;
import de.telekom.detefleet.app.events.login.Logout;
import de.telekom.detefleet.app.events.login.LogoutFailure;
import de.telekom.detefleet.app.events.login.LogoutSuccess;
import de.telekom.detefleet.app.events.mileage.LoadLastMileage;
import de.telekom.detefleet.app.events.mileage.PersistMileage;
import de.telekom.detefleet.app.events.repairRequest.GetRepairRequests;
import de.telekom.detefleet.app.events.repairRequest.ResendRepairRequest;
import de.telekom.detefleet.app.events.repairRequest.SendRepairRequest;
import de.telekom.detefleet.app.events.repairRequest.TakeGarage;
import de.telekom.detefleet.app.events.security.SSLCertificateInvalid;
import de.telekom.detefleet.app.events.security.SSLCertificateValid;
import de.telekom.detefleet.app.events.view.RemoveViewContext;

import org.robotlegs.base.ContextEvent;
import org.robotlegs.core.ICommandMap;
import org.robotlegs.core.ISignalCommandMap;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class BootstrapCommands
{
	public function BootstrapCommands(commandMap:ICommandMap, signalCommandMap:ISignalCommandMap) {
		// init controller
		commandMap.mapEvent(Event.ACTIVATE, HandleAppActivationCMD, Event);
		commandMap.mapEvent(Event.DEACTIVATE, HandleAppDeactivationCMD, Event);

        commandMap.mapEvent(ContextEvent.STARTUP_COMPLETE, ForcePortraitModeCMD, ContextEvent, true);
        commandMap.mapEvent(ContextEvent.STARTUP_COMPLETE, InitScreenClassCMD, ContextEvent, true);
        commandMap.mapEvent(ContextEvent.STARTUP_COMPLETE, SetupViewContainerCMD, ContextEvent, true);
        commandMap.mapEvent(ContextEvent.STARTUP_COMPLETE, IsDeviceRooted, ContextEvent, true);

        signalCommandMap.mapSignalClass(JailbreakCheckComplete, InitViewsCMD, true);
        signalCommandMap.mapSignalClass(JailbreakCheckComplete, InitializeDatabaseCMD, true);
        signalCommandMap.mapSignalClass(JailbreakCheckComplete, InitNetworkViewsModelCMD, true);

		/*/
		 commandMap.mapEvent(ContextEvent.STARTUP_COMPLETE, ComponentTestCMD, ContextEvent, true);
		 commandMap.mapEvent(ContextEvent.STARTUP_COMPLETE, AddFPSMeterCMD, ContextEvent, true);
		 super.startup();
		 return
		 // */

		// commandMap.mapEvent(ContextEvent.STARTUP_COMPLETE, InitTheMinerCMD, ContextEvent, true);
		// commandMap.mapEvent(ContextEvent.STARTUP_COMPLETE, InitDeMonsterCMD, ContextEvent, true);


		// show view commands
		signalCommandMap.mapSignalClass(RenewSessionSuccess, RenewSessionDataCMD);
		signalCommandMap.mapSignalClass(RenewSessionFault, RemoveAuthTokenCMD);
		signalCommandMap.mapSignalClass(LoginSuccess, SaveAuthKeyCMD);
		signalCommandMap.mapSignalClass(LoginSuccess, RenewSessionDataCMD);
		signalCommandMap.mapSignalClass(LoginRequest, LoginCMD);

		signalCommandMap.mapSignalClass(GetFuelTypes, GetFuelTypesCMD);

		signalCommandMap.mapSignalClass(PersistMileage, PersistMileageCMD);
		signalCommandMap.mapSignalClass(LoadLastMileage, GetMileageCMD);

		signalCommandMap.mapSignalClass(SearchFillingStations, PersistFillingStationServiceRequestCMD);
		signalCommandMap.mapSignalClass(SearchFillingStations, SearchFillingStationsCMD);
		signalCommandMap.mapSignalClass(GetLastFillingStationSearchRequest, LoadSearchRequestCMD);
		signalCommandMap.mapSignalClass(RequestSelectBoxList, CreateSelectBoxListCMD);

		signalCommandMap.mapSignalClass(SearchGarages, SearchGaragesCMD);
		signalCommandMap.mapSignalClass(GetLastGarageSearchRequest, GetLastGarageSearchRequestCMD);
		signalCommandMap.mapSignalClass(GetVehicleServiceCases, GetVehicleServiceCasesCMD);

		signalCommandMap.mapSignalClass(GetRepairRequests, GetRepairRequestsCMD);

		// Logout
		signalCommandMap.mapSignalClass(Logout, LogoutCMD);
		signalCommandMap.mapSignalClass(LogoutSuccess, RemoveAuthTokenCMD);
		signalCommandMap.mapSignalClass(LogoutSuccess, CleanupAfterLogoutCMD);
		signalCommandMap.mapSignalClass(LogoutFailure, RemoveAuthTokenCMD);
		signalCommandMap.mapSignalClass(LogoutFailure, CleanupAfterLogoutCMD);

		// PhoneCall
		signalCommandMap.mapSignalClass(PhoneCallRequested, ShowCallPhoneDialogCMD);
		signalCommandMap.mapSignalClass(CallPhone, CallPhoneCMD);

		// Repair Request
		signalCommandMap.mapSignalClass(TakeGarage, TakeGarageCMD);
		signalCommandMap.mapSignalClass(SendRepairRequest, SendRepairRequestCMD);
		signalCommandMap.mapSignalClass(ResendRepairRequest, ResendRepairRequestCMD);

		// Garage
		signalCommandMap.mapSignalClass(CleanGarageList, CleanGarageListCMD);

		// view Context
		signalCommandMap.mapSignalClass(RemoveViewContext, RemoveViewContextCMD);

		//Loader
		signalCommandMap.mapSignalClass(ServiceFaultSignal, HideLoaderCMD);

		//Messages
		signalCommandMap.mapSignalClass(GetMessages, GetMessagesCMD);
		signalCommandMap.mapSignalClass(MessageSelected, SetMessageReadCMD);
        signalCommandMap.mapSignalClass(GetMessageIds, GetMessageIdsCMD);
		
		// security
		signalCommandMap.mapSignalClass(JailbreakCheckComplete, LoadCertificatesCMD);
		//signalCommandMap.mapSignalClass(SSLCertificateValid, HandleValidCertificateCMD);
		//signalCommandMap.mapSignalClass(SSLCertificateInvalid, HandleInvalidCertificateCMD);

	}
}
}
