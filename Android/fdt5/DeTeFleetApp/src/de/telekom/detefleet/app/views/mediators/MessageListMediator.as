package de.telekom.detefleet.app.views.mediators
{
import de.telekom.detefleet.app.events.GetMessages;
import de.telekom.detefleet.app.events.MessageSelected;
import de.telekom.detefleet.app.events.MessagesChanged;
import de.telekom.detefleet.app.events.loader.HideLoader;
import de.telekom.detefleet.app.events.loader.ShowLoader;
import de.telekom.detefleet.app.events.view.RequestView;
import de.telekom.detefleet.app.events.view.TransitionCompleted;
import de.telekom.detefleet.app.events.view.ViewReady;
import de.telekom.detefleet.app.models.vo.MessageVO;
import de.telekom.detefleet.app.models.vo.TransitionStateVO;
import de.telekom.detefleet.app.models.vo.ViewRequestVO;
import de.telekom.detefleet.app.views.components.views.MessageDetailView;
import de.telekom.detefleet.app.views.components.views.MessageListView;

import flash.system.System;

import org.robotlegs.mvcs.Mediator;

/**
 *
 */
public class MessageListMediator extends Mediator
{
	/**
	 * SIGNALS
	 */
	[Inject]
	public var messagesChanged:MessagesChanged;
	[Inject]
	public var messageSelected:MessageSelected;
	[Inject]
	public var viewReady:ViewReady;
	[Inject]
	public var transitionCompleted:TransitionCompleted;
	[Inject]
	public var gotoView:RequestView;
	[Inject]
	public var getMessages:GetMessages;
	[Inject]
	public var hideLoader:HideLoader;
	[Inject]
	public var showLoader:ShowLoader;
	/**
	 * VIEW
	 */
	[Inject]
	public var view:MessageListView;

	/**
	 *
	 *
	 */
	public override function onRegister():void {
		// Add Listener
		view.listItemClickedCallback = listItemClickedCallback;
		messagesChanged.add(populateMessages);
		transitionCompleted.add(loadMessages);

		viewReady.dispatch(view);
	}

	private function loadMessages(transitionState:TransitionStateVO):void {
		if(transitionState.viewToShow == view && !(transitionState.viewToHide is MessageDetailView)) {
			showLoader.dispatch();
			getMessages.dispatch();
		}
        if(transitionState.viewToHide == view) {
            view.unselectAll();
        }
	}

	/**
	 *
	 */
	protected function listItemClickedCallback(message:MessageVO):void {
		gotoView.dispatch(new ViewRequestVO(MessageDetailView));
		messageSelected.dispatch(message);
	}

	/**
	 *
	 * @param messages
	 *
	 */
	private function populateMessages(messages:Vector.<MessageVO>):void {
        System.gc();

		view.populateMessages(messages);
		hideLoader.dispatch();

	}

	override public function preRemove():void {
		super.preRemove();

        view.listItemClickedCallback = null;
        messagesChanged.remove(populateMessages);
        transitionCompleted.remove(loadMessages);
	}
}
}
