package de.telekom.detefleet.app.controller
{
import org.robotlegs.mvcs.SignalCommand;

import flash.net.URLRequest;
import flash.net.navigateToURL;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class CallPhoneCMD extends SignalCommand
{
	[Inject]
	public var phone:String;
	
	override public function execute():void{
		navigateToURL(new URLRequest("tel:"+ phone)); 
	}
}
}
