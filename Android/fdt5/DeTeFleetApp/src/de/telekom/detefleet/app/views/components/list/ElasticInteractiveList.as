package de.telekom.detefleet.app.views.components.list
{
import flash.events.Event;

/**
 *
 */
public class ElasticInteractiveList extends de.telekom.detefleet.app.views.components.list.List
{
	protected var isSpringForceActive:Boolean;

	/**
	 * ElasticInteractiveStampList constructor
	 */
	public function ElasticInteractiveList(viewWidth:Number, viewHeight:Number) {
		super(viewWidth, viewHeight);
	}

	final override protected function updateRenderRectangle(event:Event):void {

		//trace("updateRenderRectangle vY: " + vY);
		event.preventDefault();
		event.stopImmediatePropagation();

		const FRICTION:Number = .95;
		const LIST_MASS:Number = 200;
		const SPRING_AMOUNT:Number = 5;//.95;

		if(isDragging) {
			vY = mouseY - offset;

			offsetHistory.shift();
			offsetHistory.push(vY);

			offset = mouseY;
			targetY = _renderRect.y - vY;
			_renderRect.y = targetY;
		}
		else {
			_renderRect.y -= vY;
			vY *= FRICTION;
			//*/
			if(vY > -.01 && vY < .01) {
				vY = 0;
//				scrollBarNeedsRendering = false;
				if(!isSpringForceActive) {
					_scrollCanvas.removeEventListener(Event.ENTER_FRAME, updateRenderRectangle);
				}
			}
			//*/
		}

		//trace("_renderRectBoundY: " + _renderRectBoundY);
		//trace("_renderRect.y: " + _renderRect.y);

		var dy:Number = 0;
		var ay:Number = 0;

		if(_renderRect.y < 0) {
			isSpringForceActive = true;
			dy = _renderRect.y; // neg value
			ay = dy * SPRING_AMOUNT;
			vY += ay / LIST_MASS;
			if(_renderRect.y - vY >= 0) {
				vY = 0;
				_renderRect.y = 0; // make sure we hit the exact point
			}
			/*/
			 trace("dy: " + dy);
			 trace("ay: " + ay);
			 trace("vy: " + vY);
			 trace("--- ");
			 //*/
			//_renderRect.y += vY;
//			clearAndRender();
		}
		else if(_renderRect.y > _renderRectBoundY) {
			isSpringForceActive = true;
			dy = _renderRect.y - _renderRectBoundY; // pos value
			ay = dy * SPRING_AMOUNT;
			vY += ay / LIST_MASS;
			if(_renderRect.y - vY <= _renderRectBoundY) {
				vY = 0;
				_renderRect.y = _renderRectBoundY; // make sure we hit the exact point
			}
			/*/
			 trace("dy: " + dy);
			 trace("ay: " + ay);
			 trace("vy: " + vY);
			 trace("--- ");
			 //*/
			//_renderRect.y += vY;
//			clearAndRender();
		}
		else {
			isSpringForceActive = false;
//			render();
		}


		if(vY < 0 && _nextUpdateUpY >= _scrollCanvas.y && _draggingStartY >= _scrollCanvas.y) {
			trace("========================================");

			updateItems(0);

			_nextUpdateUpY = _scrollCanvas.y - _itemHeight;
			_nextUpdateDownY = _scrollCanvas.y;

			trace("_nextUpdateUpY: " + _nextUpdateUpY);
			trace("_nextUpdateDownY: " + _nextUpdateDownY);
		}

		if(vY > 0 && _nextUpdateDownY <= _scrollCanvas.y && _draggingStartY <= _scrollCanvas.y) {

			trace("========================================");
			updateItems(1);

			_nextUpdateUpY = _scrollCanvas.y;
			_nextUpdateDownY = _scrollCanvas.y + _itemHeight*2;

			trace("_nextUpdateUpY: " + _nextUpdateUpY);
			trace("_nextUpdateDownY: " + _nextUpdateDownY);
		}

		_scrollCanvas.y = -_renderRect.y;

		//trace("updateRenderRectangle | dy: " + dy + " | ay: " + ay + " | vy: " + vY);

		/*/
		 if ( _showScrollBar )
		 {
		 var scrollDist:Number = _renderRect.height - 2*scrollBarPaddingV - scrollRect.height;
		 var scrollPerc:Number = _renderRect.y / _renderRectBoundY;
		 scrollRect.y = scrollBarPaddingV + scrollDist * scrollPerc;
		 }
		 //*/
	}
}
}
