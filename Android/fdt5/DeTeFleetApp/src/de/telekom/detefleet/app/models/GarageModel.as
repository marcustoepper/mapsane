package de.telekom.detefleet.app.models
{
import de.telekom.detefleet.app.events.ChoosenVehicleServiceCaseChanged;
import de.telekom.detefleet.app.events.garage.SearchResultChanged;
import de.telekom.detefleet.app.events.garage.VehicleServiceCasesChanged;
import de.telekom.detefleet.app.events.repairRequest.TakenGarageChanged;
import de.telekom.detefleet.app.models.vo.GarageSearchRequestVO;
import de.telekom.detefleet.app.models.vo.GarageVO;
import de.telekom.detefleet.app.models.vo.VehicleServiceCaseVO;
import org.robotlegs.mvcs.Actor;


/**
 * Store some data related to Garages
 * 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class GarageModel extends Actor
{
	[Inject]
	public var searchResultChanged:SearchResultChanged;
	[Inject]
	public var vehicleServiceCasesChanged:VehicleServiceCasesChanged;
	[Inject]
	public var takenGarageChanged:TakenGarageChanged;
	[Inject]
	public var choosenVehicleServiceCaseChanged:ChoosenVehicleServiceCaseChanged;
	
	private var _choosenVehicleServiceCase:VehicleServiceCaseVO;
	
	/**
	 * Choosen Garage for RepairRequest
	 */
	private var _takenGarage:GarageVO;
	
	/**
	 * Includes the Last Search Request for Garages
	 */
	public var lastRequest:GarageSearchRequestVO;

	/**
	 * Includes the Garages of the Last Search
	 */
	private var _searchResult:Vector.<GarageVO>;


	/**
	 * Includes the Vehicle Service Cases of the Request
	 */
	private var _vehicleServiceCases:Vector.<VehicleServiceCaseVO>;

	/**
	 * remove all data from Memory
	 */
	public function cleanData():void{
		lastRequest = null;
		_searchResult = null;
	}

	public function get searchResult():Vector.<GarageVO> {
		return _searchResult;
	}

	public function set searchResult(searchResult:Vector.<GarageVO>):void {
		this._searchResult = searchResult;
		searchResultChanged.dispatch(searchResult);
	}

	public function get vehicleServiceCases():Vector.<VehicleServiceCaseVO> {
		return _vehicleServiceCases;
	}

	public function set vehicleServiceCases(vehicleServiceCases:Vector.<VehicleServiceCaseVO>):void {
		this._vehicleServiceCases = vehicleServiceCases;
		vehicleServiceCasesChanged.dispatch(vehicleServiceCases);
	}

	public function get takenGarage():GarageVO {
		return _takenGarage;
	}

	public function set takenGarage(takenGarage:GarageVO):void {
		this._takenGarage = takenGarage;
		takenGarageChanged.dispatch(takenGarage);
	}

	public function get choosenVehicleServiceCase():VehicleServiceCaseVO {
		return _choosenVehicleServiceCase;
	}

	public function set choosenVehicleServiceCase(choosenVehicleServiceCase:VehicleServiceCaseVO):void {
		this._choosenVehicleServiceCase = choosenVehicleServiceCase;
		choosenVehicleServiceCaseChanged.dispatch(choosenVehicleServiceCase);
	}
}
}