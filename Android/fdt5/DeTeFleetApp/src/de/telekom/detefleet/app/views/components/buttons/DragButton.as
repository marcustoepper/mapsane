package de.telekom.detefleet.app.views.components.buttons
{
	import flash.display.Sprite;
	
	import de.aoemedia.mobile.ui.components.buttons.AbstractButton;
	import de.aoemedia.mobile.ui.components.skins.ButtonBitmaps;
	import de.telekom.detefleet.app.assets.ButtonRepository;
	
	public class DragButton extends AbstractButton
	{
		protected var _hitArea:Sprite;
		protected var _width:Number = -1;
		protected var _height:Number = -1;
		
		public function DragButton()
		{
			var sBmps:ButtonRepository = ButtonRepository.getInstance();
			var bmps:ButtonBitmaps = new ButtonBitmaps(sBmps.dragButton, sBmps.dragButton, sBmps.dragButton, sBmps.dragButton);
			super(bmps);
			_width = width;
			_height = height;
		}
		
		override public function get width ( ):Number
		{
			if ( _width == -1 ) _width = super.width;
			return _width;
		}
		
		override public function get height ( ):Number
		{
			if ( _height == -1 ) _height = super.height;
			return _height;
		}
		
		public function defineHitArea ( w:Number, h:Number, showHitArea:Boolean = false ):void
		{
			if ( _hitArea == null )
			{
				_hitArea = new Sprite();
				addChild(_hitArea);
			}
			else if ( _hitArea.width == w && _hitArea.height == h )
			{
				return;
			}
			_hitArea.graphics.clear();
			_hitArea.graphics.beginFill(0xff0000, showHitArea ? .5 : 0);
			_hitArea.graphics.drawRect(0,0,w,h);
			_hitArea.graphics.endFill();
			removeChild(_hitArea);
			_hitArea.x = (width - _hitArea.width) >> 1;
			_hitArea.y = (height - _hitArea.height) >> 1;
			addChild(_hitArea);
		}
	}
}
