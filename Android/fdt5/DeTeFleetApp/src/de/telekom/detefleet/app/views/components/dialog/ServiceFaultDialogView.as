package de.telekom.detefleet.app.views.components.dialog {
import flash.geom.Rectangle;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class ServiceFaultDialogView extends ConfirmDialogView {
    public function ServiceFaultDialogView(viewport:Rectangle, message:String) {
        super(viewport, "Fehler", message);

    }
}
}
