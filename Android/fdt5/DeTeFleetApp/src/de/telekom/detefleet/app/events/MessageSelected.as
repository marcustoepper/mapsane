package de.telekom.detefleet.app.events
{
import de.telekom.detefleet.app.models.vo.MessageVO;

import org.osflash.signals.Signal;

public class MessageSelected extends Signal
{
	public function MessageSelected() {
		super(MessageVO);
	}
}
}
