package de.telekom.detefleet.app.models.vo
{
/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class MileageVO
{
	public var mileage:String;
	public var updated:Date;
}
}
