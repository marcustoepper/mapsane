package de.telekom.detefleet.app.views.components.form {
import de.aoemedia.mobile.ui.components.LabelComponent;
import de.aoemedia.mobile.ui.components.container.LayoutContainer;
import de.aoemedia.mobile.ui.components.container.LayoutOrientation;
import de.aoemedia.mobile.ui.components.forms.InputField;
import de.aoemedia.mobile.ui.components.skins.InputFieldBitmaps;
import de.telekom.detefleet.app.assets.Color;
import de.telekom.detefleet.app.assets.ScaleBitmaps;
import de.telekom.detefleet.app.assets.Size;
import de.telekom.detefleet.app.assets.TextFormatTemplate;

import flash.display.Sprite;

import org.bytearray.display.ScaleBitmap;

/**
 *
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 *
 */
public class InputFieldWrapper extends LayoutContainer {
    public static const POSITION_TOP:String = "top";
    public static const POSITION_LEFT:String = "left";

    protected var _label:LabelComponent;
    protected var _input:InputField;

    protected var _labelPosition:String;

    protected var _lines:Number = 1;

    /**
     *
     */
    public function InputFieldWrapper(view:Sprite) {
        super(view);

        _labelPosition = POSITION_TOP;

        createChildren();
        applySkin();
    }

    /**
     *
     */
    protected function createChildren():void {
        _label = new LabelComponent();
        _label.paddingLeft = -2;

        var sBmps:ScaleBitmaps = ScaleBitmaps.getInstance();
        var bg:ScaleBitmap = sBmps.input;
        var bmps:InputFieldBitmaps = new InputFieldBitmaps(bg, sBmps.inputTakingInput, bg, sBmps.inputError, sBmps.inputSuccess);

        _input = new InputField(bmps);
        _input.paddingHorizontal = scaleFactor * 10;
        _input.paddingVertical = scaleFactor * 6;

        addItem(_label);
        addItem(_input);


    }

    /**
     *
     */
    override public function invalidate(force:Boolean = false):void {
        super.invalidate(force);

        if (isNaN(percentalWidth)) {
            return;
        }

        var width:Number = innerWidth / scaleFactor;
        if (_labelPosition == POSITION_TOP) {
            _label.paddingLeft = -2;
            _input.right = NaN;

            if (width < 0) {
                return;
            }

            _input.setSize(width, (18 * _lines) + (_input.paddingVertical * 2));
        } else {
            _input.right = 0;

            _input.setSize(Size.FORM_FIELD_RIGHT, (18 * _lines) + (_input.paddingVertical * 2));
        }

    }

    /**
     *
     */
    protected function applySkin():void {
        _label.textFormat = TextFormatTemplate.getInstance().formLabel;
        _input.textFormat = TextFormatTemplate.getInstance().formInputField;
        _input.colorInvalid = Color.INVALID;
        _input.colorValid = Color.VALID;
    }

    /**
     *
     * @param width
     * @param height
     */
    public function setSize(width:Number, height:Number):void {
        _input.setSize(width, height);
    }

    public function get inputField():InputField {
        return _input;
    }

    public function get enabled():Boolean {
        return _input.enabled;
    }

    public function set enabled(b:Boolean):void {
        _input.enabled = b;
    }

    public function set label(value:String):void {
        _label.label = value;
    }

    public function get label():String {
        return _label.label;
    }

    public function get lines():Number {
        return _lines;
    }

    public function set lines(lines:Number):void {
        _lines = lines;
    }

    public function get labelPosition():String {
        return _labelPosition;
    }

    /**
     * Should the Label sit on top of the input field or on the left?
     *
     * @param labelPosition
     */
    public function set labelPosition(labelPosition:String):void {
        if (_labelPosition == labelPosition) {
            return;
        }
        _labelPosition = labelPosition;

        if (labelPosition == POSITION_TOP) {
            orientation = LayoutOrientation.VERTICAL;
        } else {
            orientation = LayoutOrientation.HORIZONTAL;
        }

        invalidate();
    }

}
}
