package de.telekom.detefleet.app.controller
{
import de.aoemedia.motion.TransitionType;
import de.telekom.detefleet.app.events.view.RequestView;
import de.telekom.detefleet.app.models.DatabaseModel;
import de.telekom.detefleet.app.models.FillingStationModel;
import de.telekom.detefleet.app.models.GarageModel;
import de.telekom.detefleet.app.models.GeolocationModel;
import de.telekom.detefleet.app.models.MileageModel;
import de.telekom.detefleet.app.models.RepairRequestModel;
import de.telekom.detefleet.app.models.UserModel;
import de.telekom.detefleet.app.models.ViewsModel;
import de.telekom.detefleet.app.models.vo.ViewRequestVO;
import de.telekom.detefleet.app.views.components.views.LoginView;

import org.robotlegs.mvcs.SignalCommand;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class CleanupAfterLogoutCMD extends SignalCommand
{

	[Inject]
	public var gotoView:RequestView;
	[Inject]
	public var databaseModel:DatabaseModel;
	[Inject]
	public var fillingStationModel:FillingStationModel;
	[Inject]
	public var mileageModel:MileageModel;
	[Inject]
	public var viewsModel:ViewsModel;
	[Inject]
	public var garageModel:GarageModel;
	[Inject]
	public var geolocationModel:GeolocationModel;
	[Inject]
	public var repairRequestModel:RepairRequestModel;
	[Inject]
	public var userModel:UserModel;

	override public function execute():void
	{
		// Clean DB
		fillingStationModel.truncateSearchRequest(databaseModel.connection);
		mileageModel.truncateMileage(databaseModel.connection);

		// clean Memory
		fillingStationModel.cleanData();
		// viewsModel.cleanData();
		mileageModel.cleanData();
		garageModel.cleanData();
		geolocationModel.cleanData();
		repairRequestModel.cleanData();
		userModel.cleanData();

		gotoView.dispatch(new ViewRequestVO(LoginView, TransitionType.SLIDE_RIGHT));

	}
}
}
