package de.telekom.detefleet.app.controller {
import de.telekom.detefleet.app.services.remote.IMessageService;

import org.robotlegs.mvcs.SignalCommand;

public class GetMessageIdsCMD extends SignalCommand {
    [Inject]
    public var service:IMessageService;

    override public function execute():void {
        service.getMessageIds();
    }
}
}
