package de.telekom.detefleet.app.views.components.buttons
{
import de.telekom.detefleet.app.assets.IconRepository;

import flash.display.Bitmap;

public class NewRepairRequestButton extends ListButton
{
	/**
	 * Construct
	 */
	public function NewRepairRequestButton(title:String)
	{
		super(title);

		var icon:Bitmap = IconRepository.plus;
		leftIcon = icon;
	}
}
}