package de.telekom.detefleet.app.views.mediators
{
import de.telekom.detefleet.app.events.view.ViewReady;
import de.telekom.detefleet.app.views.components.views.BreakdownChecklistView;

import org.robotlegs.mvcs.Mediator;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class BreakdownChecklistMeditator extends Mediator
{
	// Views
	[Inject]
	public var view:BreakdownChecklistView;
	
	[Inject]
	public var viewReady:ViewReady;
	
	public override function onRegister():void
	{
		viewReady.dispatch(view);
		
	}

}
}
