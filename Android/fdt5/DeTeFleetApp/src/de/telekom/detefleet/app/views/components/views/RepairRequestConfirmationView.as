package de.telekom.detefleet.app.views.components.views
{
import de.aoemedia.mobile.ui.MultiDpiHelper;
import de.telekom.detefleet.app.assets.IconRepository;
import de.telekom.detefleet.app.views.components.dialog.ConfirmDialogView;

import flash.geom.Rectangle;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class RepairRequestConfirmationView extends ConfirmDialogView
{
	public function RepairRequestConfirmationView(viewport:Rectangle)
	{
		var text:String = "Ihr Auftrag wurde von Telekom MobilitySolutions versandt und wird geprüft. Die von Ihnen ausgewählte Werkstatt wird zeitnah beauftragt. Sie werden schriftlich über die Beauftragung in Kenntnis gesetzt.";
		super(viewport, "Vielen Dank", text);
	}
	
	override protected function invalidate():void
	{
		super.invalidate();

		headlineTextField.x = icon.x + icon.width + scale(10);

        resetContainerPosition();
		invalidateBackground();
	}
	
	override protected function createChildren():void
	{
		super.createChildren();
		
		container.removeChild(icon);
		icon = IconRepository.check;
		container.addChild(icon);
	}
}
}
