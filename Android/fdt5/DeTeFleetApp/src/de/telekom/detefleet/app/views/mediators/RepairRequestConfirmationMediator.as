package de.telekom.detefleet.app.views.mediators
{
import de.aoemedia.motion.TransitionType;
import de.telekom.detefleet.app.events.CloseDialog;
import de.telekom.detefleet.app.events.DialogConfirmed;
import de.telekom.detefleet.app.events.view.BlockHistoryAdditionRequestView;
import de.telekom.detefleet.app.events.view.RemoveViewContext;
import de.telekom.detefleet.app.events.view.RequestView;
import de.telekom.detefleet.app.models.RepairRequestModel;
import de.telekom.detefleet.app.models.vo.ViewRequestVO;
import de.telekom.detefleet.app.views.components.views.RepairRequestConfirmationView;
import de.telekom.detefleet.app.views.components.views.RepairRequestHistoryView;

import org.robotlegs.mvcs.Mediator;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class RepairRequestConfirmationMediator extends Mediator
{
	[Inject]
	public var view:RepairRequestConfirmationView;

	[Inject]
	public var dialogConfirmed:DialogConfirmed;
	[Inject]
	public var closeDialog:CloseDialog;

	[Inject]
	public var requestView:RequestView;
	[Inject]
	public var removeViewContext:RemoveViewContext;

	[Inject]
	public var blockHistoryAdditionRequestView:BlockHistoryAdditionRequestView;

	public override function onRegister():void {
		view.okClicked.add(handleConfirmation);

	}

	private function handleConfirmation():void {
		dialogConfirmed.dispatch(view);
		closeDialog.dispatch(view);

		removeViewContext.dispatch(RepairRequestModel.CONTEXT);

		blockHistoryAdditionRequestView.dispatch(new ViewRequestVO(RepairRequestHistoryView, TransitionType.SLIDE_RIGHT));
	}

	public override function preRemove():void {
		view.okClicked.remove(handleConfirmation);
		super.preRemove();
	}
}
}
