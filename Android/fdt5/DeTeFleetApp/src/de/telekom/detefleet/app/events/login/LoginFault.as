package de.telekom.detefleet.app.events.login {
	import org.osflash.signals.Signal;
	
	
	/**
	 * 
	 */
	public class LoginFault extends Signal
	{
		public function LoginFault()
		{
			super(String);
		}
	}
}
