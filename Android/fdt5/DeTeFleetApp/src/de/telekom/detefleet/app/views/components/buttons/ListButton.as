package de.telekom.detefleet.app.views.components.buttons
{
import de.aoemedia.mobile.ui.components.buttons.AbstractLabelButton;
import de.aoemedia.mobile.ui.components.skins.ButtonBitmaps;
import de.telekom.detefleet.app.assets.Color;
import de.telekom.detefleet.app.assets.IconRepository;
import de.telekom.detefleet.app.assets.ScaleBitmaps;
import de.telekom.detefleet.app.assets.TextFormatTemplate;

import flash.display.Bitmap;

public class ListButton extends AbstractLabelButton
{
	protected var _leftIcon:Bitmap;
	protected var _rightIcon:Bitmap;

	protected var _paddingLeftInternal:Number = 11;
	protected var _paddingRightInternal:Number = 11;

	protected var _gap:Number = 3;

	/**
	 * Construct
	 */
	public function ListButton(title:String)
	{
		var sBmps:ScaleBitmaps = ScaleBitmaps.getInstance();
		var btn:Bitmap = sBmps.singleListButton;
		btn.alpha = .75;
		var bmps:ButtonBitmaps = new ButtonBitmaps(btn, sBmps.singleListButtonDown, sBmps.singleListButton, sBmps.singleListButtonDown);

		super(bmps);

		stopInvalidation = true;

		var arrIcon:Bitmap = IconRepository.iconArrow;
		rightIcon = arrIcon;

		label = title;
		labelColor =  Color.LIST_ITEM;
		labelColorDown = Color.DARK_GREY;

		setSize(320, 49);
		textFormat =  TextFormatTemplate.getInstance().listItemTitleHighlight;
		stopInvalidation = false;
	}

	/**
	 * Set Left Icon
	 */
	public function set leftIcon(icon:Bitmap):void {
		if (icon == _leftIcon) {
			// Do nothing if it is the same
			return;
		}

		if ( _leftIcon != null ) {
			removeChild(_leftIcon);
		}
		_leftIcon = icon;

		if (_leftIcon != null) {
			addChild(_leftIcon);
		}

		invalidate();
	}

	/**
	 * Set Right Icon
	 */
	protected function set rightIcon(icon:Bitmap):void {
		if ( _rightIcon != null ) removeChild(_rightIcon);
		_rightIcon = icon;
		addChild(_rightIcon);
		invalidate();
	}

	override public function setSize(width:Number = -1, height:Number = -1):void
	{
		super.setSize(width, height);

		if (_leftIcon) {
			_leftIcon.x = scale(_paddingLeftInternal);
			_leftIcon.y = componentHeight - _leftIcon.height >> 1;

			_label.x = _leftIcon.x + _leftIcon.width + scale(_gap);
		} else {
			_label.x = scale(_paddingLeftInternal);
		}
		_rightIcon.x = componentWidth - _rightIcon.width - scale(_paddingRightInternal);
		_rightIcon.y = componentHeight - _rightIcon.height >> 1;

	}

}
}
