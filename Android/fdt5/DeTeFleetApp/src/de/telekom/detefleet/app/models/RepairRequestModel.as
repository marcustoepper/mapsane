package de.telekom.detefleet.app.models
{
import de.telekom.detefleet.app.events.repairRequest.RepairRequestsChanged;
import de.telekom.detefleet.app.models.vo.RepairRequestVO;

import org.robotlegs.mvcs.Actor;

/**
 * Store some data related to RepairRequest
 * 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class RepairRequestModel extends Actor
{
	public static const CONTEXT:String = "repairRequest";
	
	[Inject]
	public var repairRequestsChanged:RepairRequestsChanged;
	
	public var sucessfulSendedRepairRequest:RepairRequestVO;
	
	/**
	 * Includes the RepairRequest of the Last Search
	 */
	private var _lastSearchResult:Vector.<RepairRequestVO>;

	/**
	 * remove all data from Memory
	 */
	public function cleanData():void{
		lastSearchResult = null;
	}

	public function get lastSearchResult():Vector.<RepairRequestVO> {
		return _lastSearchResult;
	}

	public function set lastSearchResult(lastSearchResult:Vector.<RepairRequestVO>):void {
		this._lastSearchResult = lastSearchResult;
		repairRequestsChanged.dispatch(lastSearchResult);
	}
}
}