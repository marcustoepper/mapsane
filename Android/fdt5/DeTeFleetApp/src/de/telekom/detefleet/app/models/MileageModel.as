package de.telekom.detefleet.app.models
{
import de.telekom.detefleet.app.events.mileage.LastMileageChanged;
import de.telekom.detefleet.app.models.vo.MileageVO;

import org.as3commons.logging.api.ILogger;
import org.as3commons.logging.api.getLogger;
import org.robotlegs.mvcs.Actor;

import flash.data.SQLConnection;
import flash.data.SQLResult;
import flash.data.SQLStatement;
import flash.errors.SQLError;

/**
 * Store some data related to Mileage
 * 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class MileageModel extends Actor
{
	private static const logger:ILogger = getLogger(MileageModel);

	[Inject]
	public var lastMileageChanged:LastMileageChanged;

	/**
	 * The last saved Mileage
	 */
	public var lastMileage:MileageVO;

	/**
	 * remove all data from Memory
	 */
	public function cleanData():void{
		lastMileage = null;
	}

	/**
	 * 
	 */
	public function persistMileage(mileage:String, sqlConnection:SQLConnection):void
	{
		var update:Date = new Date();

		// Then persist in Database
		sqlConnection.begin();
		try {
			var insert:SQLStatement = new SQLStatement();
			insert.sqlConnection = sqlConnection;
			insert.text = "INSERT INTO mileage (mileage, lastUpdate) " + "VALUES (:mileage, :lastUpdate)";
			insert.parameters[':mileage'] = mileage;
			insert.parameters[':lastUpdate'] = update;
			insert.execute();

			sqlConnection.commit();

			lastMileage = new MileageVO();
			lastMileage.mileage = mileage;
			lastMileage.updated = update;

			lastMileageChanged.dispatch(lastMileage);
		} catch (error:SQLError) {
			logger.error("DatabaseError: " + error.message);
			sqlConnection.rollback();
		}
	}

	/**
	 * Load Mileage from DB
	 */
	public function loadLastMileage(sqlConnection:SQLConnection):void
	{
		// First look in Memory
		if (lastMileage) {
			lastMileageChanged.dispatch(lastMileage);
			return;
		}

		// Then look in Database
		sqlConnection.begin();

		try {
			var select:SQLStatement = new SQLStatement();
			select.sqlConnection = sqlConnection;
			select.text = "SELECT * FROM mileage ORDER BY lastUpdate DESC LIMIT 1";
			select.execute();

			var result:SQLResult = select.getResult();
			if (result.data == null || result.data.length < 1) {
				lastMileageChanged.dispatch(null);				
				return;
			}
			lastMileage = new MileageVO();
			lastMileage.mileage = result.data[0].mileage;
			lastMileage.updated = result.data[0].lastUpdate;
			lastMileageChanged.dispatch(lastMileage);
		} catch (error:SQLError) {
			logger.error("DatabaseError: " + error.message);
		}
	}
	
	/**
	 * Remove all Data from table
	 */
	public function truncateMileage(sqlConnection:SQLConnection):void
	{
		var deleteStatement:SQLStatement = new SQLStatement();
		deleteStatement.text = "DELETE FROM mileage";
		deleteStatement.sqlConnection = sqlConnection;

		try {
			deleteStatement.execute();
		} catch(error:SQLError) {
			logger.error("DatabaseError: " + error.message);
		}
	}
}
}