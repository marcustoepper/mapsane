package de.telekom.detefleet.app.models
{
import de.telekom.detefleet.app.events.MessageIdsChanged;
import de.telekom.detefleet.app.events.MessagesChanged;
import de.telekom.detefleet.app.events.UnreadCountChanged;
import de.telekom.detefleet.app.models.vo.MessageVO;

import flash.data.SQLConnection;
import flash.data.SQLResult;
import flash.data.SQLStatement;
import flash.errors.SQLError;

import org.as3commons.logging.api.ILogger;
import org.as3commons.logging.api.getLogger;
import org.robotlegs.mvcs.Actor;

public class MessageModel extends Actor
{
	private static const logger:ILogger = getLogger(MessageModel);
	[Inject]
	public var messagesChanged:MessagesChanged;
    [Inject]
    public var unreadCountChanged:UnreadCountChanged;

	[Inject]
	public var databaseModel:DatabaseModel;

	private var _messages:Vector.<MessageVO>;
    private var _messageIds:Vector.<String>;

	public var selectedMessage:MessageVO;

	public function MessageModel() {
		super();

		_messages = new <MessageVO>[];
	}

	public function get messages():Vector.<MessageVO> {
		return _messages;
	}

	public function set messages(value:Vector.<MessageVO>):void {
		if(_messages == value) {
			return;
		}
		_messages = value;

        _messageIds = new <String>[];
		var alreadyRead:Vector.<String> = getAlreadyRead(databaseModel.connection); // TODO get connection from outside
		for (var i:int = 0; i < _messages.length; i++) {
            _messageIds[i] = _messages[i].id;
			for (var j:int = 0; j < alreadyRead.length; j++) {
				if(_messages[i].id == alreadyRead[j]) {
					_messages[i].read = true;
					break;
				}
			}
		}

        unreadCountChanged.dispatch(unreadCount);
		messagesChanged.dispatch(_messages);
	}

    public function get messageIds():Vector.<String> {
        return _messageIds;
    }

    public function set messageIds(value:Vector.<String>):void {
        _messageIds = value;

        unreadCountChanged.dispatch(unreadCount);
    }

	public function syncMessages():void {

	}

	public function getAlreadyRead(sqlConnection:SQLConnection):Vector.<String> {
		var alreadyReadMessageIds:Vector.<String> = new <String>[];

		try {
			var select:SQLStatement = new SQLStatement();
			select.sqlConnection = sqlConnection;
			select.text = "SELECT * FROM messages";
			select.execute();

			var result:SQLResult = select.getResult();
			if(result.data == null || result.data.length < 1) {
				return alreadyReadMessageIds;
			}

			var numResults:int = result.data.length;
			for (var i:int = 0; i < numResults; i++) {
				alreadyReadMessageIds[i] = result.data[i].id;
			}

		} catch (error:SQLError) {
			logger.error("DatabaseError: " + error.message);
		}

		return alreadyReadMessageIds;
	}

	/**
	 * Set Message to read
	 * @param message
	 * @param sqlConnection
	 */
	public function read(message:MessageVO, sqlConnection:SQLConnection):void {
		sqlConnection.begin();
		try {
			var insert:SQLStatement = new SQLStatement();
			insert.sqlConnection = sqlConnection;
			insert.text = "INSERT INTO messages (id, read) VALUES (:id, :read)";
			insert.parameters[':id'] = message.id;
			insert.parameters[':read'] = true;
			insert.execute();

			sqlConnection.commit();
		} catch (error:SQLError) {
			logger.error("DatabaseError: " + error.message);
			sqlConnection.rollback();
		}

        unreadCountChanged.dispatch(unreadCount);
	}

	/**
	 * How much Messages are unread
	 */
	public function get unreadCount():int {
        var count:int = _messageIds.length;
        var alreadyRead:Vector.<String> = getAlreadyRead(databaseModel.connection); // TODO get connection from outside

        for (var i:int = 0; i < _messageIds.length; i++) {
            for (var j:int = 0; j < alreadyRead.length; j++) {
                if(_messageIds[i] == alreadyRead[j]) {
                    count--;
                    break;
                }
            }
        }

		return count;
	}

}
}
