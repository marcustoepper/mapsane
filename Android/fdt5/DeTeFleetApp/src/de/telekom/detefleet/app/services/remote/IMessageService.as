package de.telekom.detefleet.app.services.remote
{
import de.telekom.detefleet.app.models.vo.ServiceAuthVO;

/**
 * Implement this for the Service to get Messages
 *
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public interface IMessageService
{
	/**
	 * Get Messages
	 *
	 * @param auth The Authentification for the Webservice
	 */
	function getMessages(auth:ServiceAuthVO):void;

    /**
     * Only returns the Message IDs
     */
    function getMessageIds():void;
}
}
