package de.telekom.detefleet.app.services.remote
{
import de.telekom.detefleet.app.events.ServiceFaultSignal;
import de.telekom.detefleet.app.events.garage.GarageLoadFault;
import de.telekom.detefleet.app.events.garage.VehicleServiceCaseLoadFault;
import de.telekom.detefleet.app.models.GarageModel;
import de.telekom.detefleet.app.models.vo.GarageSearchRequestVO;
import de.telekom.detefleet.app.models.vo.GarageVO;
import de.telekom.detefleet.app.models.vo.ServiceAuthVO;
import de.telekom.detefleet.app.models.vo.VehicleServiceCaseVO;

import org.as3commons.logging.api.ILogger;
import org.as3commons.logging.api.getLogger;
import org.robotlegs.mvcs.Actor;

import flash.events.Event;
import flash.events.HTTPStatusEvent;
import flash.events.IEventDispatcher;
import flash.events.IOErrorEvent;
import flash.events.SecurityErrorEvent;
import flash.net.URLLoader;
import flash.net.URLRequest;
import flash.net.URLRequestHeader;
import flash.net.URLRequestMethod;



/**
 * Implementation for an Json Service to get some Garages.
 * It uses the JSONServiceConfiguration for getting Configuration
 * 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class JSONGarageService extends Actor implements IGarageService
{
	private static const logger:ILogger = getLogger(JSONGarageService);

	// Model
	[Inject]
	public var model:GarageModel;

	// Signals
	[Inject]
	public var garageLoadFault:GarageLoadFault;
	[Inject]
	public var serviceFaultSignal:ServiceFaultSignal;

	[Inject]
	public var serviceCasesLoadFault:VehicleServiceCaseLoadFault;

	/**
	 * Search for Garages.
	 * 
	 * If successful it fills the GarageModel.lastSearchResult  with the Result an disptach an GarageLoaded Signal.
	 * If unseccessful it fills the GarageLoadFault with the error Message
	 * 
	 * @param auth The Authentification for the Webservice
	 * @param request The request for the Search
	 * 
	 */
	public function search(auth:ServiceAuthVO, request:GarageSearchRequestVO):void
	{
		var urlRequest:URLRequest = new URLRequest(JSONServiceConfiguration.endpointUrl + "/garage/search");
		// Content Type Header has to be send if the Request is JSON Type
		var header:URLRequestHeader = new URLRequestHeader("Content-Type", "application/json");
		var loader:URLLoader = new URLLoader();

		urlRequest.data = '{' + auth.toJSON() + ', "data":{' + request.toJSON() + '}}';
		logger.debug("search called: " + urlRequest.data);

		urlRequest.method = URLRequestMethod.POST;
		urlRequest.requestHeaders.push(header);

		addListeners(loader);
		loader.addEventListener(Event.COMPLETE, searchCompleteHandler);
		loader.load(urlRequest);
	}

	/**
	 * Handles the successful Request. 
	 * 
	 * @see JSONFillingStationService.search()
	 * @param event
	 * 
	 */
	protected function searchCompleteHandler(event:Event):void
	{
		var loader:URLLoader = URLLoader(event.target);
		logger.debug("Garages loaded: " + loader.data);
		try {
			var data:Object = JSON.parse(loader.data);
		} catch(e:Error) {
			garageLoadFault.dispatch("Couldn't parse JSON data.");
		}

		if (data.success) {
			var garages:Vector.<GarageVO> = new <GarageVO>[];

			for ( var i:uint = 0; i < data.data.length; i++ ) {
				garages[i] = new GarageVO(data.data[i]);
			}

			model.searchResult = garages;
		} else {
			garageLoadFault.dispatch(data.error);
		}

		loader.removeEventListener(Event.COMPLETE, searchCompleteHandler);
		removeListener(loader);
	}

	/**
	 * Ask the Service for existing Vehicle Service Cases
	 * 
	 * If successful it fills the GarageModel.serviceCases with the Result an disptach an ServiceCasesLoaded Signal.
	 * If unseccessful it fills the ServiceCasesLoadFault with the error Message
	 * 
	 * @param auth The Authentification for the Webservice
	 */
	public function getVehicleServiceCases(auth:ServiceAuthVO):void
	{
		var urlRequest:URLRequest = new URLRequest(JSONServiceConfiguration.endpointUrl + "/garage/getServiceCases");
		var header:URLRequestHeader = new URLRequestHeader("Content-Type", "application/json");
		var loader:URLLoader = new URLLoader();

		urlRequest.data = '{' + auth.toJSON() + '}';
		urlRequest.method = URLRequestMethod.POST;
		urlRequest.requestHeaders.push(header);

		addListeners(loader);
		loader.addEventListener(Event.COMPLETE, getServiceCasesCompleteHandler);
		loader.load(urlRequest);
	}

	/**
	 * Handles successful Request of asking Webservice for Vehicle Service Cases
	 * @param event
	 * 
	 */
	private function getServiceCasesCompleteHandler(event:Event):void
	{
		var loader:URLLoader = URLLoader(event.target);

		try {
			var data:Object = JSON.parse(loader.data);
		} catch(e:Error) {
			serviceCasesLoadFault.dispatch("Couldn't parse returned data.");
		}
		if (data.success) {
			// Convert JSON data into VOs
			var serviceCases:Vector.<VehicleServiceCaseVO> = new <VehicleServiceCaseVO>[];
			for (var i:int = 0; i < data.data.length; i++) {
				serviceCases[i] = new VehicleServiceCaseVO(data.data[i]);
			}
			model.vehicleServiceCases = serviceCases;
		} else {
			serviceCasesLoadFault.dispatch(data.error);
		}

		loader.removeEventListener(Event.COMPLETE, getServiceCasesCompleteHandler);
		removeListener(loader);
	}

	/**
	 * Handles an Security error of the Url Request.
	 * Should be a Problem in Production
	 */
	protected function securityErrorHandler(event:SecurityErrorEvent):void
	{
		removeListener(URLLoader(event.target));
		logger.error(event);
	}

	/**
	 * Handles HTTP Status Response. If its bigger/equal than 400 dispatch an Error
	 */
	protected function httpResponseStatusHandler(event:HTTPStatusEvent):void
	{
		if (event.status >= 400) {
			serviceFaultSignal.dispatch("Der Server ist nicht erreichbar");
			logger.error("httpResponseStatusHandler: " + event);
		}
	}

	/**
	 * Handles the io Errors
	 */
	protected function ioErrorHandler(event:IOErrorEvent):void
	{
		removeListener(URLLoader(event.target));
		serviceFaultSignal.dispatch("Der Server ist nicht erreichbar");
		logger.error("ioErrorHandler: " + event);
	}

	/**
	 * Adds some basic Listeners
	 */
	protected function addListeners(dispatcher:IEventDispatcher):void
	{
		dispatcher.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
		dispatcher.addEventListener(HTTPStatusEvent.HTTP_RESPONSE_STATUS, httpResponseStatusHandler);
		dispatcher.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
	}

	/**
	 * Remove some Basic Listeners
	 */
	protected function removeListener(dispatcher:IEventDispatcher):void
	{
		dispatcher.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
		dispatcher.removeEventListener(HTTPStatusEvent.HTTP_RESPONSE_STATUS, httpResponseStatusHandler);
		dispatcher.removeEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
	}
}
}
