package de.telekom.detefleet.app.context {
import de.telekom.detefleet.app.models.DatabaseModel;
import de.telekom.detefleet.app.models.DialogModel;
import de.telekom.detefleet.app.models.FillingStationModel;
import de.telekom.detefleet.app.models.GarageModel;
import de.telekom.detefleet.app.models.GeolocationModel;
import de.telekom.detefleet.app.models.JugglerModel;
import de.telekom.detefleet.app.models.LayoutData;
import de.telekom.detefleet.app.models.MessageModel;
import de.telekom.detefleet.app.models.MileageModel;
import de.telekom.detefleet.app.models.NetworkModel;
import de.telekom.detefleet.app.models.NetworkViewsModel;
import de.telekom.detefleet.app.models.RepairRequestModel;
import de.telekom.detefleet.app.models.UserModel;
import de.telekom.detefleet.app.models.ViewsModel;
import de.telekom.detefleet.app.services.webview.StageWebViewBridge;

import org.robotlegs.core.IInjector;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class BootstrapModels {
    public function BootstrapModels(injector:IInjector) {
        injector.mapSingleton(ViewsModel);
        injector.mapSingleton(UserModel);
        injector.mapSingleton(StageWebViewBridge);
        injector.mapSingleton(LayoutData);
        injector.mapSingleton(NetworkViewsModel);
        injector.mapSingleton(NetworkModel);
        injector.mapSingleton(DatabaseModel);
        injector.mapSingleton(FillingStationModel);
        injector.mapSingleton(MileageModel);
        injector.mapSingleton(GarageModel);
        injector.mapSingleton(JugglerModel);
        injector.mapSingleton(RepairRequestModel);
        injector.mapSingleton(GeolocationModel);
        injector.mapSingleton(DialogModel);
        injector.mapSingleton(MessageModel);

    }
}
}
