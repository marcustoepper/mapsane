package de.telekom.detefleet.app.views.components.list
{
import com.thanksmister.touchlist.renderers.ITouchListItemRenderer;

import de.telekom.detefleet.app.models.vo.RepairRequestVO;
import de.telekom.detefleet.app.views.components.listItem.RepairRequestListStamp;

import flash.display.DisplayObject;

import flash.utils.describeType;

import org.osflash.signals.Signal;

/**
 *
 */
public class RepairRequestList extends ListComponent
{
	public var resendClicked:Signal;

	/**
	 * Constructor
	 *
	 * @param viewWidth
	 * @param viewHeight
	 */
	public function RepairRequestList(viewWidth:Number, viewHeight:Number) {
		super(viewWidth, viewHeight);

		resendClicked = new Signal(RepairRequestVO);
	}

	/**
	 * Overwrite to be sure that a RepairRequestListStamp is set as itemRenderer
	 * @param value
	 */
	override public function set itemRenderer(value:Class):void {
		if(!describeType(value).factory.implementsInterface.(@type == "RepairRequestListStamp")) {
			throw new Error("itemRenderer must implement RepairRequestListStamp");
		}
		super.itemRenderer = value;
	}

	/**
	 * Add single item renderer to the list from the pool.
	 *
	 * @param index Number for the index of item in list
	 * @param addToTop Boolean value to add item to top of list or bottom
	 * */
	override protected function addListItem(index:Number, addToTop:Boolean = false):ITouchListItemRenderer {
		var listItem:ITouchListItemRenderer = super.addListItem(index, addToTop);

		if(listItem == null) {
			return null;
		}

		RepairRequestListStamp(listItem).resendClicked.add(resendClickedProxy);

		return listItem;
	}

	/**
	 *
	 * @param repairRequest
	 */
	private function resendClickedProxy(repairRequest:RepairRequestVO):void {
		resendClicked.dispatch(repairRequest);
	}

	/**
	 * Remove item from list and listeners, returning it to the pool.
	 *
	 * @param remoteFromTop Boolean value to remove item from top or bottom of list
	 * */
	override protected function removeListItem(removeFromTop:Boolean = true):ITouchListItemRenderer {
		var listItem:ITouchListItemRenderer = super.removeListItem(removeFromTop);
		if(listItem == null) {
			return null;
		}
		RepairRequestListStamp(listItem).resendClicked.remove(resendClickedProxy);

		return ITouchListItemRenderer(listItem);
	}

}
}
