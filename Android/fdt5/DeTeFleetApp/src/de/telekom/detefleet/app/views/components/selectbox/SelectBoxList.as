package de.telekom.detefleet.app.views.components.selectbox {
import com.kaizimmer.ui.Screen;

import de.aoemedia.mobile.ui.MultiDpiHelper;
import de.telekom.detefleet.app.assets.ScaleBitmaps;
import de.telekom.detefleet.app.views.components.list.ListComponent;

import flash.display.DisplayObject;
import flash.display.Sprite;

import org.bytearray.display.ScaleBitmap;

/**
 *
 * @author Kai Zimmer <kai.zimmer@aoemedia.de>
 */
public class SelectBoxList extends Sprite {
    protected var _labelField:String = "label";
    public var list:ListComponent;
    protected var listContainer:Sprite;

    public var onItemSelected:Function;
    public var onItemsAdded:Function;

    public function SelectBoxList():void {

        createChildren();
    }

    public function get labelField():String {
        return _labelField;
    }

    public function set labelField(labelField:String):void {
        _labelField = labelField;
        list.labelField = _labelField;
    }

    /**
     *
     */
    public function addItems(itemDescriptions:Vector.<Object>):void {
        var dataProvider:Array = [];
        for (var i:int = 0; i < itemDescriptions.length; i++) {
            dataProvider[i] = itemDescriptions[i];
        }
        list.dataProvider = dataProvider;
    }

    public function removeItems():void {
        list.removeAllListItems();
    }

    protected function createChildren():void {
        const CONTAINER_MARGIN_TOP:Number = MultiDpiHelper.scale(72);
        const CONTAINER_MARGIN_BOTTOM:uint = MultiDpiHelper.scale(4);
        const CONTAINER_MARGIN_HORIZONTAL:uint = MultiDpiHelper.scale(4);
        const CONTAINER_WIDTH:Number = Screen.stage.stageWidth - 2 * CONTAINER_MARGIN_HORIZONTAL;
        const CONTAINER_HEIGHT:Number = Screen.stage.stageHeight - CONTAINER_MARGIN_TOP - CONTAINER_MARGIN_BOTTOM;

        const LIST_MARGIN_TOP:uint = MultiDpiHelper.scale(10);
        const LIST_MARGIN_BOTTOM:uint = MultiDpiHelper.scale(20);
        const LIST_MARGIN_HORIZONTAL:uint = MultiDpiHelper.scale(8);

        const LIST_WIDTH:Number = CONTAINER_WIDTH - 2 * LIST_MARGIN_HORIZONTAL;
        const LIST_HEIGHT:Number = CONTAINER_HEIGHT - LIST_MARGIN_TOP - LIST_MARGIN_BOTTOM;

        var bg:ScaleBitmap = ScaleBitmaps.getInstance().stageBG;
        bg.width = CONTAINER_WIDTH;
        bg.height = CONTAINER_HEIGHT;

        list = new ListComponent(LIST_WIDTH, LIST_HEIGHT);
        list.itemRenderer = SelectBoxListItemButton;

        var object:SelectBoxListItemButton = new SelectBoxListItemButton();
        list.rowHeight = object.itemHeight;
        object = null;

        list.maxTapDelayTime = .5;

        list.x = LIST_MARGIN_HORIZONTAL;
        list.y = LIST_MARGIN_TOP;

        list.itemSelected.add(_onItemSelected);

        listContainer = new Sprite();
        listContainer.x = CONTAINER_MARGIN_HORIZONTAL;
        listContainer.y = CONTAINER_MARGIN_TOP;
        listContainer.addChild(DisplayObject(bg));
        listContainer.addChild(DisplayObject(list));
        addChild(listContainer);

        function _onItemSelected(item:*):void {
            if (onItemSelected != null) {
                onItemSelected(item);
            }
        }

        function _onListItemsAdded():void {
            if (onItemsAdded != null) {
                onItemsAdded();
            }
        }

    }



    public function unselectAll():void{
        list.unselectAll();
    }

}
}
