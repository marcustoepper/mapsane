package de.telekom.detefleet.app.events {
import org.osflash.signals.Signal;

public class UnreadCountChanged extends Signal {
    public function UnreadCountChanged() {
        super(int);
    }
}
}
