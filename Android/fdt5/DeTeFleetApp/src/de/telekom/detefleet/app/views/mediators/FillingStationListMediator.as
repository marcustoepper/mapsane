package de.telekom.detefleet.app.views.mediators
{
import de.aoemedia.motion.TransitionType;
import de.telekom.detefleet.app.events.GeolocationStatusUpdated;
import de.telekom.detefleet.app.events.LocationUpdated;
import de.telekom.detefleet.app.events.SearchResultIsEmpty;
import de.telekom.detefleet.app.events.fillingStation.FillingStationSelected;
import de.telekom.detefleet.app.events.fillingStation.FuelTypesChanged;
import de.telekom.detefleet.app.events.fillingStation.GetFuelTypes;
import de.telekom.detefleet.app.events.fillingStation.GetLastFillingStationSearchRequest;
import de.telekom.detefleet.app.events.fillingStation.SearchFillingStations;
import de.telekom.detefleet.app.events.fillingStation.SearchRequestLoaded;
import de.telekom.detefleet.app.events.fillingStation.SearchResultChanged;
import de.telekom.detefleet.app.events.loader.HideLoader;
import de.telekom.detefleet.app.events.loader.ShowLoader;
import de.telekom.detefleet.app.events.login.Logout;
import de.telekom.detefleet.app.events.view.BlockHistoryAdditionRequestView;
import de.telekom.detefleet.app.events.view.RequestView;
import de.telekom.detefleet.app.events.view.TransitionCompleted;
import de.telekom.detefleet.app.events.view.ViewReady;
import de.telekom.detefleet.app.models.FillingStationModel;
import de.telekom.detefleet.app.models.GeolocationModel;
import de.telekom.detefleet.app.models.vo.FillingStationRequestVO;
import de.telekom.detefleet.app.models.vo.FillingStationVO;
import de.telekom.detefleet.app.models.vo.FuelTypeVO;
import de.telekom.detefleet.app.models.vo.TransitionStateVO;
import de.telekom.detefleet.app.models.vo.ViewRequestVO;
import de.telekom.detefleet.app.views.components.views.FillingStationDetailView;
import de.telekom.detefleet.app.views.components.views.FillingStationListView;
import de.telekom.detefleet.app.views.components.views.FillingStationMapView;

import flash.sensors.Geolocation;
import flash.system.System;

import org.robotlegs.mvcs.Mediator;

/**
 *
 */
public class FillingStationListMediator extends Mediator
{
	/**
	 * SIGNALS
	 */
	[Inject]
	public var fuelTypesChanged:FuelTypesChanged;
	[Inject]
	public var searchResultChanged:SearchResultChanged;
	[Inject]
	public var getFuelTypes:GetFuelTypes;
	[Inject]
	public var getLastSearchRequest:GetLastFillingStationSearchRequest;
	[Inject]
	public var searchFillingStations:SearchFillingStations;
	[Inject]
	public var searchRequestLoaded:SearchRequestLoaded;
	[Inject]
	public var fillingStationSelected:FillingStationSelected;
	[Inject]
	public var locationUpdated:LocationUpdated;
	[Inject]
	public var geolocationStatusUpdated:GeolocationStatusUpdated;
	[Inject]
	public var transitionCompleted:TransitionCompleted;
	[Inject]
	public var viewReady:ViewReady;
	[Inject]
	public var logout:Logout;
	[Inject]
	public var gotoMapView:BlockHistoryAdditionRequestView;

	[Inject]
	public var searchResultIsEmpty:SearchResultIsEmpty;

	[Inject]
	public var signalShowLoader:ShowLoader;
	[Inject]
	public var signalHideLoader:HideLoader;
	/**
	 * Model
	 */
	[Inject]
	public var model:FillingStationModel;
	[Inject]
	public var geolocationModel:GeolocationModel;

	/**
	 * VIEW
	 */
	[Inject]
	public var view:FillingStationListView;

	[Inject]
	public var gotoView:RequestView;

	protected var latitude:Number;
	protected var longitude:Number;

	/**
	 *
	 *
	 */
	public override function onRegister():void {
		// Add Listener
		fuelTypesChanged.add(populateFuelTypes);
		searchResultChanged.add(populateFillingStationsList);
		transitionCompleted.add(showFilter);
		logout.add(handleLogout);
		view.signalToggleMapButtonClicked.add(toggleMap);

		view.searchButtonClickedCallback = sendSearchRequest;
		view.listItemClickedCallback = listItemClickedCallback;

		geolocationStatusUpdated.add(geoStatusHandler);

		geolocationModel.start();

		if(model.fuelTypes) {
			populateFuelTypes(model.fuelTypes);
		} else {
			getFuelTypes.dispatch();
		}

		// Find actual Location
		if(!Geolocation.isSupported || geolocationModel.geolocation.muted) {
			view.setDefaultLocationText("");
		}
	}

	private function handleLogout():void {
		view.resetForm();
	}

	protected function showFilter(transitionState:TransitionStateVO):void {
		if(transitionState.viewToShow == view) {
			if(model.searchResult == null || model.searchResult.length == 0) {
				view.openFilter();
			}
		}
        if(transitionState.viewToHide == view) {
            view.unselectAll();
        }
	}

	/**
	 *
	 */
	protected function listItemClickedCallback(fillingStation:FillingStationVO):void {
		gotoView.dispatch(new ViewRequestVO(FillingStationDetailView));
		fillingStationSelected.dispatch(fillingStation);
	}

	/**
	 *
	 */
	protected function toggleMap():void {
		//gotoMapView.dispatch(new ViewRequestVO(FillingStationMapView, TransitionType.FLIP_LEFT));
		gotoMapView.dispatch(new ViewRequestVO(FillingStationMapView, TransitionType.SLIDE_UP));
	}

	/**
	 *
	 *
	 */
	protected function geoStatusHandler():void {
		if(geolocationModel.geolocation.muted) {
			view.setDefaultLocationText("");
		} else {
			view.setDefaultLocationText(view.DEFAULT_LOCATION_TEXT);
		}
	}

	/**
	 *
	 *
	 */
	private function sendSearchRequest(request:FillingStationRequestVO):void {
		view.clear();
		if((request.location.address == null || request.location.address == "" ) && Geolocation.isSupported && !geolocationModel.geolocation.muted) {
			request.location.latitude = geolocationModel.latitude;
			request.location.longitude = geolocationModel.longitude;
		}
		signalShowLoader.dispatch();
		searchFillingStations.dispatch(request);
	}

	/**
	 * Handles the Last Request Loaded Signal
	 *
	 * @param request
	 *
	 */
	private function lastRequestLoaded(request:FillingStationRequestVO):void {
		if(request) {
			searchRequestLoaded.remove(lastRequestLoaded);
			view.fillSearchRequestForm(request);

			if(model.searchResult) {
				populateFillingStationsList(model.searchResult);
			} else {
				sendSearchRequest(request);
			}
		}

		viewReady.dispatch(view);
	}

	/**
	 *
	 * @param stations
	 *
	 */
	private function populateFillingStationsList(stations:Vector.<FillingStationVO>):void {
		System.gc();

		if(stations.length == 0) {
			searchResultIsEmpty.dispatch();
		}

		view.populateFillingStations(stations);

		signalHideLoader.dispatch();
	}

	/**
	 * Fill the SortBy DropDown with data
	 */
	private function populateSortByData():void {
		var sortByDataProvider:Vector.<Object> = new Vector.<Object>();
		sortByDataProvider.push({label:"Preis", data:FillingStationRequestVO.SORT_BY_PRICE});
		sortByDataProvider.push({label:"Entfernung", data:FillingStationRequestVO.SORT_BY_DISTANCE});

		view.sortBySelectBoxBtn.dataProvider = sortByDataProvider;
		view.sortBySelectBoxBtn.selectedItem = sortByDataProvider[0];
	}

	/**
	 * Fill the Fuel Types Drow Down with Data
	 *
	 * @param types
	 */
	private function populateFuelTypes(types:Vector.<FuelTypeVO>):void {
		view.setFuelTypeDataProvider(types);
		view.setSelectedFuelType(types[0]);

		populateSortByData();

		// First the fueltypes has to be filled before the last request can be loaded
		searchRequestLoaded.add(lastRequestLoaded);
		getLastSearchRequest.dispatch();

	}

	override public function preRemove():void {
		fuelTypesChanged.remove(populateFuelTypes);
		searchResultChanged.remove(populateFillingStationsList);

		view.signalToggleMapButtonClicked.remove(toggleMap);

		super.preRemove();
	}
}
}
