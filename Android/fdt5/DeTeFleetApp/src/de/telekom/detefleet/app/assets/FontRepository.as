package de.telekom.detefleet.app.assets {
import flash.text.Font;
import flash.text.TextFormat;

/**
 * Here you find the Fonts
 *
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class FontRepository {
    private var _textFormat:TextFormat;

    [Embed(source="../../../../../../assets/fonts/Tele-GroteskUlt.swf", symbol="Tele-GroteskUlt")]
    public static var TeleGroteskUlt:Class;
    [Embed(source="../../../../../../assets/fonts/Tele-GroteskFet.swf", symbol="Tele-GroteskFet")]
    public static var TeleGroteskFet:Class;
    [Embed(source="../../../../../../assets/fonts/Tele-GroteskHal.swf", symbol="Tele-GroteskHal")]
    public static var TeleGroteskHal:Class;
    [Embed(source="../../../../../../assets/fonts/Tele-GroteskNor.swf", symbol="Tele-GroteskNor")]
    public static var TeleGroteskNor:Class;
    /*/
     [Embed(source="../../../../../../../../../../../../../assets/fonts/t036018t-webfont.ttf", fontName="Tele-GroteskUlt", embedAsCFF=false)]
     public static var TeleGroteskUlt:Class;
     // public static var teleGroteskUltTF:TextFormat = new TextFormat("Tele-GroteskUlt");

     [Embed(source="../../../../../../../assets/fonts/t036016t-webfont.ttf", fontName="Tele-GroteskFet", embedAsCFF=false)]
     public static var TeleGroteskFet:Class;
     // public static var teleGroteskFetTF:TextFormat = new TextFormat("Tele-GroteskFet");

     [Embed(source="../../../../../../../assets/fonts/t036014t-webfont.ttf", fontName="Tele-GroteskHal", embedAsCFF=false)]
     public static var TeleGroteskHal:Class;
     // public static var teleGroteskHalTF:TextFormat = new TextFormat("Tele-GroteskHal");

     [Embed(source="../../../../../../../assets/fonts/t036013t-webfont.ttf", fontName="Tele-GroteskNor", embedAsCFF=false)]
     public static var TeleGroteskNor:Class;
     // public static var teleGroteskNorTF:TextFormat = new TextFormat("Tele-GroteskNor");
     // */
    protected static var _instance:FontRepository;

    public function FontRepository(sE:SingletonEnforcer) {
        Font.registerFont(TeleGroteskUlt);
        Font.registerFont(TeleGroteskFet);
        Font.registerFont(TeleGroteskHal);
        Font.registerFont(TeleGroteskNor);
    }

    public static function getInstance():FontRepository {
        if (_instance == null) {
            _instance = new FontRepository(new SingletonEnforcer());
            _instance._textFormat = new TextFormat();
        }
        return _instance;
    }

}
}

internal class SingletonEnforcer {
}