package de.telekom.detefleet.app.controller.debug
{
	import com.demonsters.debugger.MonsterDebugger;
	
	import org.robotlegs.mvcs.Command;
	
	public class InitDeMonsterCMD extends Command
	{
		public function InitDeMonsterCMD()
		{
			super();
		}
		
		override public function execute ( ):void
		{
			// Start the MonsterDebugger
			MonsterDebugger.initialize(contextView);
			MonsterDebugger.trace(this, "object?", "hello from monster");
		}
	}
}