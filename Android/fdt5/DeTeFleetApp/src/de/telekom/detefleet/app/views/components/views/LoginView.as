package de.telekom.detefleet.app.views.components.views {
import com.kaizimmer.events.ValidationEvent;
import com.kaizimmer.ui.Screen;
import com.kaizimmer.ui.form.FormFieldType;
import com.kaizimmer.ui.form.FormValidator;
import com.kaizimmer.ui.form.IFormObject;

import de.aoemedia.mobile.ui.components.LabelComponent;
import de.aoemedia.mobile.ui.components.container.LayoutContainer;
import de.aoemedia.mobile.ui.components.container.LayoutHorizontalAlign;
import de.aoemedia.mobile.ui.components.container.LayoutOrientation;
import de.aoemedia.mobile.ui.components.forms.FormField;
import de.aoemedia.mobile.ui.components.forms.StageInputField;
import de.telekom.detefleet.app.assets.Color;
import de.telekom.detefleet.app.assets.TextFormatTemplate;
import de.telekom.detefleet.app.models.vo.LoginFormData;
import de.telekom.detefleet.app.views.components.buttons.DefaultCheckBox;
import de.telekom.detefleet.app.views.components.buttons.DefaultLabelButton;
import de.telekom.detefleet.app.views.components.form.StageInputFieldWrapper;

import flash.events.Event;
import flash.events.FocusEvent;
import flash.events.MouseEvent;
import flash.text.SoftKeyboardType;
import flash.text.TextFormat;

import org.as3commons.logging.api.ILogger;
import org.as3commons.logging.api.getLogger;
import org.osflash.signals.Signal;

/**
 * ...
 * @author k.zimmer aka engimono
 */
public class LoginView extends NavigationBarView {
    private static const logger:ILogger = getLogger(LoginView);

    protected const DEFAULT_TEXT_MAIL_LOG:String = "Benutzername";
    protected const DEFAULT_TEXT_PASSWORD_LOG:String = "";

    protected var ffUser:FormField;
    protected var ffPassword:FormField;

    protected var loginFormViewValidator:FormValidator;

    protected var sendButton:DefaultLabelButton;

    public var signalSendLoginRequest:Signal;
    public var signalRequestPassword:Signal;

    protected var savePasswordCB:DefaultCheckBox;

    protected var usernameInputField:StageInputFieldWrapper;
    protected var passwordInputField:StageInputFieldWrapper;
    protected var container:LayoutContainer;
    protected var fieldsContainer:LayoutContainer;
    protected var sendButtonLayout:LayoutContainer;

    public function LoginView() {
        super("Login Mobility App", true);

        signalSendLoginRequest = new Signal(LoginFormData);
        signalRequestPassword = new Signal(String);
    }

    override protected function init():void {
        super.init();
    }

    override protected function createChildren():void {
        super.createChildren();

        container = new LayoutContainer(this);
        container.stopInvalidation = true;
        container.x = 0;
        container.y = navbar.height;
        container.setPadding(11, 11, 11, 11);
        container.gap = 25;
        container.horizontalAlign = LayoutHorizontalAlign.CENTER;
        container.explicitWidth = Screen.stage.stageWidth;

        fieldsContainer = new LayoutContainer(this);
        fieldsContainer.gap = 15;

        usernameInputField = new StageInputFieldWrapper(this);
        usernameInputField.label = "Benutzername";
        usernameInputField.setSize(272, 37);
        usernameInputField.softKeyboardType = SoftKeyboardType.EMAIL;
        fieldsContainer.addItem(usernameInputField);

        passwordInputField = new StageInputFieldWrapper(this);
        passwordInputField.label = "Passwort";
        passwordInputField.setSize(272, 37);
        passwordInputField.displayAsPassword = true;
        /*/
         StageInputField(passwordInputField.inputField)
         .stageTextField.stageText.addEventListener(FocusEvent.FOCUS_IN, resetPasswordField);
         //*/

        fieldsContainer.addItem(passwordInputField);

        var textformat:TextFormat = TextFormatTemplate.getInstance().formLabel;

        var storePasswordLayout:LayoutContainer = new LayoutContainer(this);
        storePasswordLayout.orientation = LayoutOrientation.HORIZONTAL;
        storePasswordLayout.gap = 10;

        savePasswordCB = new DefaultCheckBox(null, false);
        storePasswordLayout.addItem(savePasswordCB);

        var labelStorePassword:LabelComponent = new LabelComponent("Angemeldet bleiben", textformat);
        labelStorePassword.addEventListener(MouseEvent.MOUSE_UP, onSavePasswordLabelClicked);
        storePasswordLayout.addItem(labelStorePassword);

        fieldsContainer.addItem(storePasswordLayout);

        container.addItem(fieldsContainer);

        sendButtonLayout = new LayoutContainer(this);
        sendButtonLayout.paddingTop = 20;
        sendButtonLayout.percentalWidth = 100;
        sendButtonLayout.horizontalAlign = LayoutHorizontalAlign.CENTER;

        sendButton = new DefaultLabelButton("Anmelden");
        sendButton.setSize(198, 52);

        sendButton.textFormat = TextFormatTemplate.getInstance().buttonBig;
        sendButton.enabled = false;
        sendButton.labelColorDisabled = Color.MEDIUM_GREY;
        sendButton.labelColor = Color.DARK_GREY;
        sendButton.addEventListener(MouseEvent.MOUSE_UP, onSendBtnClicked);

        sendButtonLayout.addItem(sendButton);
        container.addItem(sendButtonLayout);

        container.stopInvalidation = false;
        container.invalidate();
        container.addItemsToView();

        _stageInputFields.push(usernameInputField);
        _stageInputFields.push(passwordInputField);

        initForms();
    }

    protected function initForms():void {
        loginFormViewValidator = new FormValidator();

        ffUser = new FormField(usernameInputField.inputField, "", true, FormFieldType.ALL);
        ffPassword = new FormField(passwordInputField.inputField, "", true, FormFieldType.PASSWORD, 1);

        ffUser.defaultText = DEFAULT_TEXT_MAIL_LOG;
        ffPassword.defaultText = DEFAULT_TEXT_PASSWORD_LOG;

        ffUser.liveChecking = true;
        ffUser.preventLiveCheckRendering = true;

        ffPassword.liveChecking = true;
        ffPassword.preventLiveCheckRendering = true;

        loginFormViewValidator.addFormObject(ffUser);
        loginFormViewValidator.addFormObject(ffPassword);

        loginFormViewValidator.addEventListener(ValidationEvent.CHANGE, onFormValidityChanged);
    }

    protected function onFormValidityChanged(e:ValidationEvent):void {
        trace("onFormValidityChanged - isValid: " + e.isValid);
        sendButton.enabled = e.isValid;
    }

    protected function resetPasswordField(event:FocusEvent = null):void {
        StageInputField(passwordInputField.inputField).stageTextField.text = "";
    }

    // *************************************************************************************************************
    // FORM HANDLERS START
    // *************************************************************************************************************

    protected function validateLoginFormView():void {
        /*/
         if ( LoginFormViewValidator.isFormValid )
         {
         hideLoginErrorNotifications();
         var fD:LoginFormData = new LoginFormData();
         fD.user = ffLogUser.textField.text;
         fD.password = ffPassword.textField.text;
         signalSendLoginRequest.dispatch(fD);
         }
         else highlightBadLoginFormViewObjects();
         // */
    }

    protected function highlightBadLoginFormViewObjects():void {
        var invalids:Array = loginFormViewValidator.invalidFormObjects;
        for (var i:uint = 0; i < invalids.length; i++) {
            highlightBadFormObject(IFormObject(invalids[i]));
        }
    }

    /**
     * Highlights the IFormObject's corresponding marker, not the IFormObject itself!
     */
    protected function highlightBadFormObject(o:IFormObject):void {
        // trace("highlightBadFormObject: " + o);
        switch (o) {
            case ffUser:
                // logMailBG.highlight();
                break;
            /*/
             case ffPassword:
             // logPasswordBG.highlight();
             break;
             // */
        }
    }

    // *************************************************************************************************************
    // UI HANDLERS START
    // *************************************************************************************************************

    protected function onSavePasswordLabelClicked(event:MouseEvent):void {
        savePasswordCB.isChecked = !savePasswordCB.isChecked;
    }

    protected function onSendBtnClicked(e:MouseEvent = null):void {
        if (!sendButton.enabled) return;
        // validateLoginFormView();
        var formData:LoginFormData = new LoginFormData();
        formData.user = ffUser.textField.text;
        formData.password = ffPassword.textField.text;
        // fD.password = passwordInput.stageTextField.text;
        formData.storePassword = savePasswordCB.isChecked;

        enableView(false);

        signalSendLoginRequest.dispatch(formData);
    }

    protected function onSendPasswordBtnClicked(e:MouseEvent):void {
        // signalCloseBtnClick.dispatch();
        if (!ffUser.validate()) {
            // logMailBG.highlight();
        } else {
            // logMailBG.normalize();
            signalRequestPassword.dispatch(ffUser.textField.text);
        }

    }

    // *************************************************************************************************************
    // PUBLIC API START
    // *************************************************************************************************************

    public function enableView(b:Boolean):void {
        usernameInputField.enabled = b;
        passwordInputField.enabled = b;
        savePasswordCB.enabled = b;
        if (b) passwordInputField.unfreeze();
        else passwordInputField.freeze();
        sendButton.enabled = b && loginFormViewValidator.isFormValid;
    }

    public function reset():void {
        ffUser.reset();
        //usernameInputField.applySkin();
        ffPassword.reset();

        usernameInputField.inputField.normalize();
        passwordInputField.inputField.normalize();

        enableView(true);

        // BUGFIX / HACK for #2804 (APP) Passwort sichtbar nach Ausloggen und neuerlicher Eingabe
        // When removing focus from password field and setting focus back again the field properly
        // displays as password again. That's why we set and the remove the fields focus programatically.
        StageInputField(passwordInputField.inputField).stageTextField.stageText.assignFocus();
        stage.focus = null;
    }

    public function renderSuccess(e:Event = null):void {
        usernameInputField.inputField.showSuccess();
        passwordInputField.inputField.showSuccess();
    }

    public function renderBadLogin(e:Event = null):void {
        enableView(true);
        //reset();
        usernameInputField.inputField.showError();
        passwordInputField.inputField.showError();
    }

    public function destroy():void {
        StageInputField(passwordInputField.inputField)
                .stageTextField.stageText.removeEventListener(FocusEvent.FOCUS_IN, resetPasswordField);
    }
}

}
