package de.telekom.detefleet.app.models
{
import de.telekom.detefleet.app.events.HideDialog;
import de.telekom.detefleet.app.events.ShowDialog;
import de.telekom.detefleet.app.events.view.OverlayHidden;
import de.telekom.detefleet.app.events.view.ShowingOverlay;
import de.telekom.detefleet.app.views.components.dialog.DialogView;

import org.robotlegs.mvcs.Actor;

/**
 * ...
 * @author dev.mode
 */
public class DialogModel extends Actor
{
	[Inject]
	public var hideDialog:HideDialog;
	[Inject]
	public var showDialog:ShowDialog;
	
	[Inject]
	public var signalShowingOverlay:ShowingOverlay;
	[Inject]
	public var signalOverlayHidden:OverlayHidden;
	

	private var _dialogs:Vector.<DialogView>;

	public function DialogModel() {
		_dialogs = new <DialogView>[];
	}

	public function addDialog(view:DialogView):void {
		_dialogs.push(view);
		// if this is the first (only) dialog -> show it immediately
		if(_dialogs.length == 1) {
			showDialog.dispatch(view);
		}
	}

	public function removeDialog():void {
		var view:DialogView = _dialogs.shift();
		hideDialog.dispatch(view);
		// if there are further dialogs waiting in the queue show the next one
		if(_dialogs.length > 0) {
			showDialog.dispatch(_dialogs[0]);
		}
	}
}
}
