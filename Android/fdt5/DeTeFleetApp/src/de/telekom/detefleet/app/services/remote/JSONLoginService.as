package de.telekom.detefleet.app.services.remote
{
import de.telekom.detefleet.app.models.vo.VehicleVO;
import de.telekom.detefleet.app.events.login.LogoutFailure;
import de.telekom.detefleet.app.events.login.LogoutSuccess;
import de.telekom.detefleet.app.models.UserModel;
import de.telekom.detefleet.app.events.ServiceFaultSignal;
import de.telekom.detefleet.app.events.login.LoginFault;
import de.telekom.detefleet.app.events.login.LoginSuccess;
import de.telekom.detefleet.app.events.RenewSessionFault;
import de.telekom.detefleet.app.events.RenewSessionSuccess;
import de.telekom.detefleet.app.models.vo.PersonVO;
import de.telekom.detefleet.app.models.vo.ServiceAuthVO;

import org.as3commons.logging.api.ILogger;
import org.as3commons.logging.api.getLogger;
import org.robotlegs.mvcs.Actor;

import flash.events.Event;
import flash.events.HTTPStatusEvent;
import flash.events.IEventDispatcher;
import flash.events.IOErrorEvent;
import flash.events.SecurityErrorEvent;
import flash.net.URLLoader;
import flash.net.URLRequest;
import flash.net.URLRequestHeader;
import flash.net.URLRequestMethod;

/**
 * Implementation for an Json Service to handle Login.
 * It uses the JSONServiceConfiguration for getting Configuration.
 * 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class JSONLoginService extends Actor implements ILoginService
{
	private static const logger:ILogger = getLogger(JSONLoginService);

	// Model
	[Inject]
	public var userModel:UserModel;

	// Signals
	[Inject]
	public var renewSessionSuccess:RenewSessionSuccess;
	[Inject]
	public var renewSessionFault:RenewSessionFault;
	[Inject]
	public var loginSuccess:LoginSuccess;
	[Inject]
	public var loginFault:LoginFault;
	[Inject]
	public var serviceFaultSignal:ServiceFaultSignal;
	[Inject]
	public var logoutSuccessful:LogoutSuccess;
	[Inject]
	public var logoutFailure:LogoutFailure;

	/**
	 * Log the User in. 
	 * 
	 * If sucessful dispatch a LoginSuccess Signal with the Logedin User. 
	 * Else dispatch a LoginFault with the Error Message. 
	 * 
	 * @param username The Username 
	 * @param password The Password of the User
	 * @param deviceKey The unique key of the Device
	 * 
	 */
	public function login(username:String, password:String, deviceKey:String):void
	{
		var loginRequest:URLRequest = new URLRequest(JSONServiceConfiguration.endpointUrl + "/login/login");
		var header:URLRequestHeader = new URLRequestHeader("Content-Type", "application/json");

		loginRequest.idleTimeout = 120000;

		var loader:URLLoader = new URLLoader();

		var requestObject:Object = {loginRequest:{userName:username, password:password}, deviceKey:deviceKey};
		var json:String = JSON.stringify(requestObject);

		logger.debug("Login Request: " + json);

		loginRequest.data = json;
		loginRequest.method = URLRequestMethod.POST;
		loginRequest.requestHeaders.push(header);

		loader.addEventListener(Event.COMPLETE, loginCompleteHandler);
		addListeners(loader);
		loader.load(loginRequest);
	}

	/**
	 * Handles successful Request of Login
	 * @param event
	 * 
	 */
	private function loginCompleteHandler(event:Event):void
	{
		var loader:URLLoader = URLLoader(event.target);
		logger.debug("Login Response: " + loader.data);

		try {
			var data:Object = JSON.parse(loader.data);
		} catch(e:Error) {
			loginFault.dispatch("Couldn't parse data.");
		}
		if (data.success) {
			
			var person:PersonVO = new PersonVO(data.data.person);
			person.vehicles.push(new VehicleVO(data.data.vehicleData.vehicle));
			
			var serviceAuth:ServiceAuthVO = new ServiceAuthVO();
			serviceAuth.authorizationToken = data.token;

			userModel.userData = person;
			loginSuccess.dispatch(person, serviceAuth);

		} else {
			loginFault.dispatch(data.error);
		}

		removeListener(loader);
		loader.removeEventListener(Event.COMPLETE, loginCompleteHandler);
	}

	/**
	 * Renews an existing User Session
	 *  
	 * If succesfull dispatch a RenewSessionSuccess.
	 * Else dispatch a RenewSessionFault with the Error Message. 
	 * 
	 * @param auth Authenification Data of User
	 */
	public function renewSession(auth:ServiceAuthVO):void
	{
		if(auth.authorizationToken == null){
			loginFault.dispatch("AuthToken ist nicht vorhanden.");
			return;
		}

		var loginRequest:URLRequest = new URLRequest(JSONServiceConfiguration.endpointUrl + "/login/renewsession");

		var header:URLRequestHeader = new URLRequestHeader("Content-Type", "application/json");
		var loader:URLLoader = new URLLoader();

		logger.debug("RenewSession Request: " + auth.toJSON());

		loginRequest.data = '{' + auth.toJSON() + '}';
		loginRequest.method = URLRequestMethod.POST;
		loginRequest.requestHeaders.push(header);

		loader.addEventListener(Event.COMPLETE, renewSessionCompleteHandler);
		addListeners(loader);
		loader.load(loginRequest);
	}

	/**
	 * Handles successful Request of renew the Session
	 * 
	 * @param event
	 * 
	 */
	protected function renewSessionCompleteHandler(event:Event):void
	{
		var loader:URLLoader = URLLoader(event.target);
		logger.debug("RenewSession Response: " + loader.data);

		try {
			var data:Object = JSON.parse(loader.data);
		} catch(e:Error) {
			loginFault.dispatch("Couldn't parse data.");
		}
		if (data.success) {
			var person:PersonVO = new PersonVO(data.data.person);
			person.vehicles.push(new VehicleVO(data.data.vehicleData.vehicle));
			
			userModel.userData = person;
			renewSessionSuccess.dispatch(person);
		} else {
			renewSessionFault.dispatch(data.error);
		}

		removeListener(loader);
		loader.removeEventListener(Event.COMPLETE, renewSessionCompleteHandler);
	}

	/**
	 * Logout a User
	 * 
	 * 
	 * If sucessful dispatch a LogoutSuccess Signal. 
	 * Else dispatch a LogoutFault with the Error Message. 
	 * 
	 * @param auth Authenification Data of User
	 */
	public function logout(auth:ServiceAuthVO):void
	{
		var request:URLRequest = new URLRequest(JSONServiceConfiguration.endpointUrl + "/login/logout");
		var header:URLRequestHeader = new URLRequestHeader("Content-Type", "application/json");
		var loader:URLLoader = new URLLoader();

		logger.debug("Logout Request: " + auth.toJSON());

		request.data = '{' + auth.toJSON() + '}';
		request.method = URLRequestMethod.POST;
		request.requestHeaders.push(header);

		loader.addEventListener(Event.COMPLETE, logoutCompleteHandler);
		addListeners(loader);
		loader.load(request);
	}

	/**
	 * Handles successful Request of Logout
	 * 
	 * @param event
	 * 
	 */
	protected function logoutCompleteHandler(event:Event):void
	{
		var loader:URLLoader = URLLoader(event.target);
		logger.debug("Logout Response: " + loader.data);

		try {
			var data:Object = JSON.parse(loader.data);
		} catch(e:Error) {
			loginFault.dispatch("Couldn't parse data.");
		}
		if (data.success) {
			logoutSuccessful.dispatch();
		} else {
			logoutFailure.dispatch();
		}

		removeListener(loader);
		loader.removeEventListener(Event.COMPLETE, renewSessionCompleteHandler);
	}

	/**
	 * Handles an Security error of the Url Request.
	 * Should be a Problem in Production
	 */
	protected function securityErrorHandler(event:SecurityErrorEvent):void
	{
		removeListener(URLLoader(event.target));
		logger.error(event);
	}

	/**
	 * Handles HTTP Status Response. If its bigger/equal than 400 dispatch an Error
	 */
	protected function httpResponseStatusHandler(event:HTTPStatusEvent):void
	{
		if (event.status >= 400) {
			serviceFaultSignal.dispatch("Der Server ist nicht erreichbar");

			logger.error("httpResponseStatusHandler: " + event);
		}
	}

	/**
	 * Handles the io Errors
	 */
	protected function ioErrorHandler(event:IOErrorEvent):void
	{
		removeListener(URLLoader(event.target));
		serviceFaultSignal.dispatch("Der Server ist nicht erreichbar");
		logger.error("ioErrorHandler: " + event);
	}

	/**
	 * Adds some basic Listeners
	 */
	protected function addListeners(dispatcher:IEventDispatcher):void
	{
		dispatcher.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
		dispatcher.addEventListener(HTTPStatusEvent.HTTP_RESPONSE_STATUS, httpResponseStatusHandler);
		dispatcher.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
	}

	/**
	 * Remove some Basic Listeners
	 */
	protected function removeListener(dispatcher:IEventDispatcher):void
	{
		dispatcher.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
		dispatcher.removeEventListener(HTTPStatusEvent.HTTP_RESPONSE_STATUS, httpResponseStatusHandler);
		dispatcher.removeEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
	}

}
}
