package de.telekom.detefleet.app.events
{
import de.telekom.detefleet.app.models.vo.RepairRequestVO;

import org.osflash.signals.Signal;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class RepairRequestSuccessfulSend extends Signal
{
	public function RepairRequestSuccessfulSend()
	{
		super(RepairRequestVO, Boolean);
	}
}
}
