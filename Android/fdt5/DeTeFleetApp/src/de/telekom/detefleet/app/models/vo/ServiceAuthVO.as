package de.telekom.detefleet.app.models.vo
{
import de.telekom.detefleet.app.models.JSONable;

/**
 * Stores Informationen needed for Service Requests for authentification.
 * 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class ServiceAuthVO implements JSONable
{
	/**
	 * The unique Device Key
	 */
	public var deviceKey:String;

	/**
	 * The Auth Token of Webservice 
	 */
	public var authorizationToken:String;

	/**
	 * Convert Object to JSON String
	 * 
	 * @return JSON string
	 */
	public function toJSON():String
	{
		return '"auth": {"token":"' + authorizationToken + '", "key":"' + deviceKey + '"}';
	}
}
}