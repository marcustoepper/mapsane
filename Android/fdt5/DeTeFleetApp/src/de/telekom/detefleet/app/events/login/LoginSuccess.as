package de.telekom.detefleet.app.events.login {
	import de.telekom.detefleet.app.models.vo.PersonVO;
	import de.telekom.detefleet.app.models.vo.ServiceAuthVO;

	import org.osflash.signals.Signal;
	
	
	public class LoginSuccess extends Signal
	{
		public function LoginSuccess()
		{
			super(PersonVO, ServiceAuthVO);
		}
	}
}
