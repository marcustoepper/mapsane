package de.telekom.detefleet.app.controller.fillingStation
{
import de.telekom.detefleet.app.models.DatabaseModel;
import de.telekom.detefleet.app.models.FillingStationModel;

import org.robotlegs.mvcs.SignalCommand;



/**
 * Reads Last Filling Station Search Request from Memory or from Database
 * 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class LoadSearchRequestCMD extends SignalCommand
{
	[Inject]
	public var databaseModel:DatabaseModel;
	[Inject]
	public var fillingStationModel:FillingStationModel;

	/**
	 * 
	 */
	public override function execute():void
	{
		fillingStationModel.loadSearchRequest(databaseModel.connection);
	}
}
}