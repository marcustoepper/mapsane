package de.telekom.detefleet.app.services.security
{
	public class CertificateVO
	{
		public var fingerprint:String;
		
		public function CertificateVO ( fingerprint:String )
		{
			this.fingerprint = fingerprint;
		}
	}
}