package de.telekom.detefleet.app.events.view
{
import org.osflash.signals.Signal;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class RemoveViewContext extends Signal
{
	public function RemoveViewContext()
	{
		super(String);
	}
}
}
