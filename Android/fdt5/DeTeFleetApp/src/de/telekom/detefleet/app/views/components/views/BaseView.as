package de.telekom.detefleet.app.views.components.views
{
	import flash.events.Event;
	import de.telekom.detefleet.app.views.components.form.StageInputFieldWrapper;

	public class BaseView extends AbstractView
	{
		protected var _stageInputFields:Vector.<StageInputFieldWrapper>;
		
		public function BaseView()
		{
			super();
		}
		
		override protected function init ( ):void
		{
			_stageInputFields = new <StageInputFieldWrapper>[];
		}
		
		public function showStageTexts(e:Event = null):void
		{
			for (var i:int = 0; i < _stageInputFields.length; i++)
			{
				_stageInputFields[i].showText(stage);
			}
		}
		
		public function hideStageTexts(e:Event = null):void
		{
			for (var i:int = 0; i < _stageInputFields.length; i++)
			{
				_stageInputFields[i].hideText();
			}
		}
		
		public function freezeStageTextFields():void
		{
			for (var i:int = 0; i < _stageInputFields.length; i++)
			{
				_stageInputFields[i].freeze();
			}
		}
		
		public function unFreezeStageTextFields():void
		{
			for (var i:int = 0; i < _stageInputFields.length; i++)
			{
				_stageInputFields[i].unfreeze();
			}
		}
		
		public function invalidateStageTextFieldsCoords():void
		{
			for (var i:int = 0; i < _stageInputFields.length; i++)
			{
				_stageInputFields[i].invalidateStageTextCoords();
			}
		}
		
	}
}