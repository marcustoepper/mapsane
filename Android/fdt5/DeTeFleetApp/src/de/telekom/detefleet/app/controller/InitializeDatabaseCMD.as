package de.telekom.detefleet.app.controller
{
import flash.system.Capabilities;

import de.telekom.detefleet.app.models.DatabaseModel;

import org.as3commons.logging.api.ILogger;
import org.as3commons.logging.api.getLogger;
import org.robotlegs.mvcs.Command;

import flash.data.SQLConnection;
import flash.data.SQLMode;
import flash.data.SQLStatement;
import flash.errors.SQLError;
import flash.events.SQLErrorEvent;
import flash.events.SQLEvent;
import flash.filesystem.File;

/**
 * Initialize Database. 
 * # Connection
 * # Create DB on Filesystem if not exist
 * # Create Tables
 * 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class InitializeDatabaseCMD extends Command
{
	[Inject]
	public var databaseModel:DatabaseModel;

	private static const logger:ILogger = getLogger(InitializeDatabaseCMD);

	/**
	 * 
	 */
	public override function execute():void
	{
		var connection:SQLConnection = new SQLConnection();

		var writeSessionDB:File;
		// win 7 hack. appdata folder is read only under win 7. You have to manually copy the DB File to your documentsDirectory
		if (Capabilities.os != "Windows 7") {
			writeSessionDB = File.applicationStorageDirectory.resolvePath(DatabaseModel.FILENAME);
			if (!writeSessionDB.exists) {
				var embededSessionDB:File = File.applicationDirectory.resolvePath(DatabaseModel.FILENAME);
				embededSessionDB.copyTo(writeSessionDB);
			}
		} else {
			writeSessionDB = File.documentsDirectory.resolvePath(DatabaseModel.FILENAME);
		}

		connection.addEventListener(SQLEvent.OPEN, openHandler);
		connection.addEventListener(SQLErrorEvent.ERROR, errorHandler);

		connection.open(writeSessionDB, SQLMode.UPDATE);

		databaseModel.connection = connection;

		createFillingStationRequestTable();
		createMileageTable();
		createGarageRequestTable();
		createMessagesTable();
	}

	/**
	 * 
	 */
	protected function createFillingStationRequestTable():void
	{
		try {
			var createStatment:SQLStatement = new SQLStatement();
			createStatment.sqlConnection = databaseModel.connection;

            createStatment.text = "CREATE TABLE IF NOT EXISTS fillingStationRequest (fueltype INTEGER, sortby TEXT, limitResult INTEGER, radius INTEGER, locationAddress TEXT, locationLatitude TEXT, locationLongitude TEXT)";

			createStatment.addEventListener(SQLEvent.RESULT, createResult);
			createStatment.addEventListener(SQLErrorEvent.ERROR, errorHandler);

			createStatment.execute();
		} catch(error:SQLError) {
			logger.error("Error message:" + error.message);
			logger.info("Details:" + error.details);
		}
	}
	
	/**
	 * 
	 */
	protected function createGarageRequestTable():void
	{
		try {
			var createStatment:SQLStatement = new SQLStatement();
			createStatment.sqlConnection = databaseModel.connection;

            createStatment.text = "CREATE TABLE IF NOT EXISTS garageSearchRequest (serviceCase INTEGER, sortby TEXT, limitResult INTEGER, radius INTEGER, locationAddress TEXT, locationLatitude TEXT, locationLongitude TEXT)";

			createStatment.addEventListener(SQLEvent.RESULT, createResult);
			createStatment.addEventListener(SQLErrorEvent.ERROR, errorHandler);

			createStatment.execute();
		} catch(error:SQLError) {
			logger.error("Error message:" + error.message);
			logger.info("Details:" + error.details);
		}
	}


	/**
	 * 
	 */
	protected function createMileageTable():void
	{
		try {
			var createStatment:SQLStatement = new SQLStatement();
			createStatment.sqlConnection = databaseModel.connection;

            createStatment.text = "CREATE TABLE IF NOT EXISTS mileage (mileage VARCHAR(7), lastUpdate DATETIME)";

			createStatment.addEventListener(SQLEvent.RESULT, createResult);
			createStatment.addEventListener(SQLErrorEvent.ERROR, errorHandler);

			createStatment.execute();
		} catch(error:SQLError) {
			logger.error("Error message:" + error.message);
			logger.info("Details:" + error.details);
		}
	}


	/**
	 *
	 */
	protected function createMessagesTable():void
	{
		try {
			var createStatment:SQLStatement = new SQLStatement();
			createStatment.sqlConnection = databaseModel.connection;

			createStatment.text = "CREATE TABLE IF NOT EXISTS messages (id VARCHAR, read INTEGER)";

			createStatment.addEventListener(SQLEvent.RESULT, createResult);
			createStatment.addEventListener(SQLErrorEvent.ERROR, errorHandler);

			createStatment.execute();
		} catch(error:SQLError) {
			logger.error("Error message:" + error.message);
			logger.info("Details:" + error.details);
		}
	}

	/**
	 * 
	 */
	protected function createResult(event:SQLEvent):void
	{
		logger.info("the table created successful");
	}

	/**
	 * 
	 */
	protected function openHandler(event:SQLEvent):void
	{
		logger.info("the database opened successfully");
	}

	/**
	 * 
	 */
	protected function errorHandler(event:SQLErrorEvent):void
	{
		logger.error("Error message:" + event.error.message);
		logger.info("Details:" + event.error.details);
	}
}
}
