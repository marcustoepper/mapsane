package de.telekom.detefleet.app.views.components.buttons
{
import de.aoemedia.mobile.ui.components.LabelComponent;
import de.telekom.detefleet.app.views.components.buttons.ListButton;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class ChooseGarageButton extends ListButton
{
	protected var _garageName:LabelComponent;


	public function ChooseGarageButton(title:String, garageName:String = "Bitte auswählen...")
	{
		super(title);
		_garageName.label = garageName;
	}

	override protected function createChildren():void
	{
		super.createChildren();

		_garageName = new LabelComponent();
		_garageName.shortenText = true;

		addChild(_garageName);
	}

	public function get garageName():LabelComponent {
		return _garageName;
	}

	public function set garageName(garageName:LabelComponent):void {
		_garageName = garageName;
	}

	override public function setSize(width:Number = -1, height:Number = -1):void
	{
		super.setSize(width, height);
		_garageName.y = _label.y;
		_garageName.x = _label.x + _label.width + scale(2);

		_garageName.textField.width = componentWidth - _garageName.x - (componentWidth - _rightIcon.x) - scale(2);
		_garageName.textField.height = _label.textField.height;

	}



}
}
