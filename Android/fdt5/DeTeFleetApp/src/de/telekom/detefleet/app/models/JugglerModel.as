package de.telekom.detefleet.app.models
{
import de.aoemedia.animation.Juggler;

import org.robotlegs.mvcs.Actor;

public class JugglerModel extends Actor
{
	private var _juggler:Juggler;

	public function JugglerModel()
	{
		super();
	}

	public function init(juggler:Juggler):void
	{
		_juggler = juggler;
	}

	public function startJuggler():void
	{
		_juggler.start();
	}

	public function stopJuggler():void
	{
		_juggler.stop();
	}

	public function get juggler():Juggler {
		return _juggler;
	}
}
}