package de.telekom.detefleet.app.models.vo
{
import com.adobe.utils.DateUtil;

public class MessageVO
{
	public var id:String;
	public var date:Date;
	public var highlight:Boolean;
	public var message:String;
	public var title:String;
	public var read:Boolean;

	/**
	 * Set values through anonymous object
	 *
	 * @param object
	 */
	public function MessageVO(object:Object) {
		if(object.date && object.date.length == 19){
			object.date += "+00:00";
		}
		date = DateUtil.parseW3CDTF(object.date);
		highlight = object.highlight;
		message = object.message;
		title = object.title;
		id = object.id;
	}
}
}
