package de.telekom.detefleet.app.controller.mileage
{
import de.telekom.detefleet.app.models.DatabaseModel;
import de.telekom.detefleet.app.models.MileageModel;

import org.robotlegs.mvcs.Command;



/**
 * Get the Mileage from DB
 * 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class GetMileageCMD extends Command
{
	[Inject]
	public var databaseModel:DatabaseModel;

	[Inject]
	public var model:MileageModel;

	/**
	 * 
	 * 
	 */
	public override function execute():void
	{
		model.loadLastMileage(databaseModel.connection);
	}
}
}
