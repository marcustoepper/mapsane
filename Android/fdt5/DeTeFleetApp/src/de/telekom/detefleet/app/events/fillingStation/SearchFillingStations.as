package de.telekom.detefleet.app.events.fillingStation {
	import de.telekom.detefleet.app.models.vo.FillingStationRequestVO;

	import org.osflash.signals.Signal;
	
	public class SearchFillingStations extends Signal
	{
		public function SearchFillingStations()
		{
			super(FillingStationRequestVO);
		}
	}
}