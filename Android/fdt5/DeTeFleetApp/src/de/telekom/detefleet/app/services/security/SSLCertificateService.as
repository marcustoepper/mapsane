package de.telekom.detefleet.app.services.security
{
	import de.aoemedia.ane.securityTools.SSLCertificateService;
	import de.aoemedia.ane.securityTools.event.SSLCertificateServiceEvent;
	import de.telekom.detefleet.app.events.security.SSLCertificateLoaded;
	import de.telekom.detefleet.app.events.security.SSLCertificateLoadingFailed;
	
	import org.robotlegs.mvcs.Actor;
	
	public class SSLCertificateService extends Actor implements ISSLCertificateService
	{
		protected var nativeSSLCertificateService:de.aoemedia.ane.securityTools.SSLCertificateService;
		protected var certificates:Vector.<CertificateVO>;
		protected var _hostDomain:String;
		
		[Inject]
		public var loaded:SSLCertificateLoaded;
		
		[Inject]
		public var loadingFailed:SSLCertificateLoadingFailed;
		
		
		public function SSLCertificateService ( )
		{
			//nativeSSLCertificateService = de.aoemedia.ane.securityTools.SSLCertificateService.getInstance();
			//nativeSSLCertificateService.addEventListener(SSLCertificateServiceEvent.CERTIFICATE_LOADED, onCertificatesLoaded);
			//nativeSSLCertificateService.addEventListener(SSLCertificateServiceEvent.CERTIFICATE_LOADING_FAILED, onLoadingFailed);
		}
		
		public function loadCertificate ( host:String ):void
		{
			trace("SSLCertificateService .loadCertificate: " + host);
			
			_hostDomain = host;
			certificates = new <CertificateVO>[];
			nativeSSLCertificateService.loadCertificates(host);
		}
		
		/*/
		public function getCertificates ( ):Vector.<String>
		{
			return fingerprintsSHA1;
		}
		//*/
		
		//*/
		public function getCertificates ( ):Vector.<CertificateVO>
		{
			return certificates;
		}
		//*/
		
		public function destroy ( ):void
		{
			nativeSSLCertificateService.removeEventListener(SSLCertificateServiceEvent.CERTIFICATE_LOADED, onCertificatesLoaded);
			nativeSSLCertificateService.removeEventListener(SSLCertificateServiceEvent.CERTIFICATE_LOADING_FAILED, onLoadingFailed);
		}
		
		protected function onCertificatesLoaded ( event:SSLCertificateServiceEvent ):void
		{
			trace("SSLCertificateService .onCertificatesLoaded: " + event);
			
			var fingerprintsSHA1:Vector.<String> = nativeSSLCertificateService.getFingerprints();
			
			for ( var i:uint = 0; i < fingerprintsSHA1.length; i++ )
			{
				certificates.push(new CertificateVO(fingerprintsSHA1[i]));
			}
			
			loaded.dispatch();
		}
		
		protected function onLoadingFailed ( event:SSLCertificateServiceEvent ):void
		{
			trace("SSLCertificateService .onLoadingFailed: " + event);
			loadingFailed.dispatch();
		}
		
	}
}