package de.telekom.detefleet.app.controller.debug
{
	import com.sociodox.theminer.TheMiner;
	
	import org.robotlegs.mvcs.Command;
	
	public class InitTheMinerCMD extends Command
	{
		public function InitTheMinerCMD()
		{
			super();
		}
		
		override public function execute ( ):void
		{
			contextView.addChild(new TheMiner());
		}
	}
}