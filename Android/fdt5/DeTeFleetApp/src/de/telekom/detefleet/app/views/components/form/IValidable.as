package de.telekom.detefleet.app.views.components.form {
	public interface IValidable {
		function showSuccess() : void;

		function showError() : void;
	}
}