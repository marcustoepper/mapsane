package de.telekom.detefleet.app.services.security
{
	public class HostVO
	{
		public var domain:String;
		public var certificates:Vector.<CertificateVO>;
		
		public function HostVO ( domain:String )
		{
			this.domain = domain;
			certificates = new <CertificateVO>[];
		}
		
		public function addCertificate ( certificate:CertificateVO ):void
		{
			certificates.push(certificate);
		}
	}
}