package de.telekom.detefleet.app.models
{
import de.telekom.detefleet.app.events.GeolocationStatusUpdated;
import de.telekom.detefleet.app.events.LocationUpdated;

import org.as3commons.logging.api.ILogger;
import org.as3commons.logging.api.getLogger;
import org.robotlegs.mvcs.Actor;

import flash.events.GeolocationEvent;
import flash.events.StatusEvent;
import flash.sensors.Geolocation;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class GeolocationModel extends Actor
{
	private static const logger:ILogger = getLogger(GeolocationModel);
	
	[Inject]
	public var locationUpdated:LocationUpdated;
	[Inject]
	public var geolocationStatusUpdated:GeolocationStatusUpdated;

	public var geolocation:Geolocation;

	public var latitude:Number;
	public var longitude:Number;

	/**
	 * remove all data from Memory
	 */
	public function cleanData():void
	{
		latitude = NaN;
		longitude = NaN;
	}

	/**
	 * Start watching the Geolocation
	 */
	public function start():void
	{
		if (geolocation == null && Geolocation.isSupported) {
			geolocation = new Geolocation();
			if (!geolocation.muted) {
				geolocation.addEventListener(GeolocationEvent.UPDATE, locationUpdate);
			}
			geolocation.addEventListener(StatusEvent.STATUS, geoStatusHandler);
			logger.debug('Geolocation supported');
		}
	}

	/**
	 * 
	 */
	private function geoStatusHandler(event:StatusEvent):void
	{
		if (geolocation.muted) {
			geolocation.removeEventListener(GeolocationEvent.UPDATE, locationUpdate);
			logger.debug('Geolocation muted');
		} else {
			geolocation.addEventListener(GeolocationEvent.UPDATE, locationUpdate);
			logger.debug('Geolocation unmuted');
		}
		geolocationStatusUpdated.dispatch();
		
	}

	/**
	 * 
	 */
	private function locationUpdate(event:GeolocationEvent):void
	{
		//trace("locationUpdate");
		latitude = event.latitude;
		longitude = event.longitude;
		
		locationUpdated.dispatch(latitude, longitude);
	}
}
}