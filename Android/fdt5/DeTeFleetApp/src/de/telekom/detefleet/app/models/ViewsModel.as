package de.telekom.detefleet.app.models
{
import de.telekom.detefleet.app.events.ViewContextChanged;
import de.telekom.detefleet.app.views.components.views.IView;

import flash.display.Sprite;
import flash.utils.Dictionary;

import org.as3commons.logging.api.ILogger;
import org.as3commons.logging.api.getLogger;
import org.robotlegs.mvcs.Actor;

/**
 * ...
 * @author dev.mode
 */
public class ViewsModel extends Actor
{
	[Inject]
	public var viewContextChanged:ViewContextChanged;

	private static const logger:ILogger = getLogger(ViewsModel);
	public var requestedViewClass:Class;
	public var requestedTransitionType:String;

	protected var _blockHistoryAddition:Boolean;
	protected var _currView:IView;
	protected var _views:Dictionary;
	protected var _lastViews:Vector.<IView>;
	protected var _readyViews:Vector.<IView>;
	protected var _viewContainer:Sprite;
	protected var _viewContainerOrigoX:Number = 0;

	protected var _context:String;

	public function ViewsModel() {
		init();
		_readyViews = new Vector.<IView>();
	}

	public function get requestedView():IView {
		return _views[requestedViewClass];
	}

	/**
	 * removes the View Context
	 */
	public function removeContext(context:String):void {
		if(_context == context) {
			_context = null;
		}
		viewContextChanged.dispatch(null);
	}

	/**
	 *
	 */
	public function addReadyView(view:IView):void {
		if(isViewReady(view)) {
			return;
		}
		_readyViews.push(view);
	}

	/**
	 *
	 */
	public function isViewReady(view:IView):Boolean {
		for each (var readyView:IView in _readyViews) {
			if(view == readyView) {
				return true;
			}
		}
		return false;
	}

	/**
	 *
	 */
	public function addView(viewClass:Class, view:IView):Boolean {
		logger.debug("addView: " + viewClass);
		if(_views[viewClass] != null) return false;
		_views[viewClass] = view;
		return true;
	}

	/**
	 *
	 */
	public function removeView(viewClass:Class):Boolean {
		if(getView(viewClass) == null) return false;
		delete _views[viewClass];
		return true;
	}

	public function getView(viewClass:Class):IView {
		return _views[viewClass] as IView;
	}

	public function hasLastView():Boolean {
		return _lastViews.length > 0;
	}

	/**
	 * Add a view to the History if _blockHistoryAddition is false
	 */
	public function addViewToHistory(view:IView):void {
		if(_blockHistoryAddition) {
			_blockHistoryAddition = false;
			return;
		}

		logger.debug("addViewToHistory: " + view);
		_lastViews.push(view);
		// BEWARE OF REFERENCES ON VIEW DESTRUCTION
	}

	/**
	 * Returns the Last View and remove it from List
	 */
	public function popLastView():IView {
		return _lastViews.pop();
	}

	/**
	 * Removes a number of Views from History
	 *
	 * @param number
	 */
	public function removeViewsFromHistory(number:int):void {
		var leftOver:int = _lastViews.length-number;
		if(number > _lastViews.length){
			leftOver = 0;
		}
		for (var i:int = _lastViews.length; i > leftOver; i--) {
			var view:IView = _lastViews.pop();
			logger.debug("removeViewsFromHistory: " + view);
		}
	}

	protected function init():void {
		_views = new Dictionary();
		_lastViews = new <IView>[];
	}

	protected function destroy():void {
		_views = null;
		_lastViews = null;
		_currView = null;
		_viewContainer = null;
	}

	// SETTER AND GETTER

	/**
	 *
	 */
	public function get blockHistoryAddition():Boolean {
		return _blockHistoryAddition;
	}

	public function set blockHistoryAddition(blockHistoryAddition:Boolean):void {
		_blockHistoryAddition = blockHistoryAddition;
	}

	/**
	 *
	 */
	public function get currentView():IView {
		return _currView;
	}

	public function set currentView(value:IView):void {
		// if ( _currView != null ) _lastViews.push(_currView);
		_currView = value;
	}

	/**
	 *
	 */
	public function get viewContainer():Sprite {
		return _viewContainer;
	}

	public function set viewContainer(value:Sprite):void {
		_viewContainer = value;
	}

	/**
	 *
	 */
	public function get viewContainerOrigoX():Number {
		return _viewContainerOrigoX;
	}

	public function set viewContainerOrigoX(x:Number):void {
		_viewContainerOrigoX = x;
	}

	public function get context():String {
		return _context;
	}

	public function set context(context:String):void {
		if(_context == context) {
			return;
		}
		_context = context;
		viewContextChanged.dispatch(context);

	}

}

}
