package de.telekom.detefleet.app.assets
{
/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class Color
{
	public static const COLOR_MAGENTA:Number = 0xe20174;
	public static const DARK_GREY:Number = 0x4b4b4b;
	public static const MEDIUM_GREY:Number = 0xa4a4a4;
	public static const LIGHT_GREY:Number = 0xdcdcdc;
    public static const SELECTED_BACKGROUND:Number = 0xd9d9d9;

	public static const ACTION_BUTTON_FONT:Number = 0xffffff;
	public static const HIGHLIGHT:Number = 0x0090c4;
	public static const CONTENT:Number = 0x8c8c8c;
	
	public static const LIST_ITEM:Number = 0x0090c4;

	public static const VALID:Number = 0x00ff00;
	public static const INVALID:Number = 0xff0000;
	
}
}