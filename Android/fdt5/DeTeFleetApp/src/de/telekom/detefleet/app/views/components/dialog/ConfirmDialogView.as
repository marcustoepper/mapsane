package de.telekom.detefleet.app.views.components.dialog {
import de.aoemedia.mobile.ui.MultiDpiHelper;
import de.aoemedia.mobile.ui.components.buttons.AbstractButton;
import de.telekom.detefleet.app.assets.Color;
import de.telekom.detefleet.app.assets.IconRepository;
import de.telekom.detefleet.app.assets.TextFormatTemplate;
import de.telekom.detefleet.app.views.components.buttons.DefaultLabelButton;

import flash.display.Bitmap;
import flash.display.DisplayObject;
import flash.geom.Rectangle;

import org.osflash.signals.Signal;

public class ConfirmDialogView extends DialogView {
    protected const BUTTON_PADDING_BOTTOM:uint = 11;

    protected var icon:Bitmap;
    protected var button:DefaultLabelButton;

    public var okClicked:Signal;

    public function ConfirmDialogView(viewport:Rectangle, headline:String, message:String) {
        super(viewport, headline, message);

        okClicked = new Signal();
    }

    override protected function invalidate():void {
        super.invalidate();

        button.setSize(126, 44);

        button.x = dialogWidth / 2 - button.width / 2;
        button.y = messageTextField.y + messageTextField.height + scale(BUTTON_PADDING_BOTTOM);

        icon.x = 0;
        icon.y = scale(4);
        icon.width = MultiDpiHelper.scale(22);
        icon.height = MultiDpiHelper.scale(22);

        headlineTextField.x = icon.x + icon.width + scale(10);

        resetContainerPosition();
        invalidateBackground();
    }

    override protected function createChildren():void {
        super.createChildren();

        button = new DefaultLabelButton("Ok");

        button.textFormat = TextFormatTemplate.getInstance().dialogButton;
        button.labelColor = Color.MEDIUM_GREY;
        button.labelColorDisabled = Color.DARK_GREY;
        button.signalClicked.add(signalBtnClick);

        container.addChild(DisplayObject(button));

        icon = IconRepository.warningIcon;
        container.addChild(icon);
    }

    protected function signalBtnClick(button:AbstractButton):void {
        okClicked.dispatch();
    }

}
}
