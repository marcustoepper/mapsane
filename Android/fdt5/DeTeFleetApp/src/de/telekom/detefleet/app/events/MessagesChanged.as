package de.telekom.detefleet.app.events
{
import de.telekom.detefleet.app.models.vo.MessageVO;

import org.osflash.signals.Signal;

public class MessagesChanged extends Signal
{
	public function MessagesChanged() {
		super(Vector.<MessageVO>);
	}
}
}
