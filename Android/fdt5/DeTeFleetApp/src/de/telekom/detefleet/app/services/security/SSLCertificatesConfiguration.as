package de.telekom.detefleet.app.services.security
{
	public class SSLCertificatesConfiguration
	{
		[Embed(source="../../../../../../../config/fingerprints.xml", mimeType="application/octet-stream")]
		protected static const CONFIG_XML:Class;
		
		protected static var _isParsed:Boolean = false;
		protected static var _hosts:Vector.<HostVO>;
	
		protected static function parseXML ( ):void
		{
			var config:XML = XML(new CONFIG_XML());
			var hosts:XMLList = config.hosts.host;
			_hosts = new <HostVO>[];
			trace("hosts.length(): " + hosts.length());
			for ( var i:uint = 0; i < hosts.length(); i++ )
			{
				var host:XML = hosts[i];
				var vo:HostVO = new HostVO(host.@domain as String);
				trace("domain: " + vo.domain);
				var certificates:XMLList = host.certificate;
				trace("certificates: " + certificates);
				for ( var j:uint = 0; j < certificates.length(); j++ )
				{
					var cert:XML = certificates[j];
					var certVO:CertificateVO = new CertificateVO(cert.@fingerprint as String);
					vo.addCertificate(certVO);
					trace("cert.@fingerprint: " + cert.@fingerprint);
				}
				trace("host: " + host);
				trace("vo: " + vo);
				_hosts.push(vo);
			}
			_isParsed = true;
		}
		
		
		/**
		 * 
		 */
		public static function get hosts ( ):Vector.<HostVO>
		{
			if ( !_isParsed ) parseXML();
			return _hosts;
		}
		
		/**
		 * 
		 */
		public static function getCertificatesByHost ( domain:String ):Vector.<CertificateVO>
		{
			if ( !_isParsed ) parseXML();
			
			for ( var i:uint = 0; i < _hosts.length; i++ )
			{
				if ( _hosts[i].domain == domain )
				{
					return _hosts[i].certificates.slice();
				}
			}
			
			return null;
		}
		
	}
}
