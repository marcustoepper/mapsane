package de.telekom.detefleet.app.controller.view
{
import de.aoemedia.motion.TransitionType;
import de.aoemedia.utils.ClassUtils;
import de.telekom.detefleet.app.events.view.BlockHistoryAdditionRequestView;
import de.telekom.detefleet.app.models.ViewsModel;
import de.telekom.detefleet.app.models.vo.ViewRequestVO;
import de.telekom.detefleet.app.views.components.views.IView;

import org.robotlegs.mvcs.Command;


/**
 * Show the Last view in History
 * 
 * @author Kai Zimmer <kai.zimmer@aoemedia.de>
 */
public class ShowLastViewCMD extends Command
{	
	[Inject]
	public var requestView:BlockHistoryAdditionRequestView;
	
	[Inject]
	public var viewsModel:ViewsModel;
	
	public override function execute():void
	{
		if ( !viewsModel.hasLastView() ) {
			return;
		}

		var lastView:IView = viewsModel.popLastView();
		
		requestView.dispatch(new ViewRequestVO(ClassUtils.getClass(lastView), TransitionType.SLIDE_RIGHT));
	}
}
}