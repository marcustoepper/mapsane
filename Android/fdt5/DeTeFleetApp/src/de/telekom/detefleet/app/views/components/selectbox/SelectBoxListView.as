package de.telekom.detefleet.app.views.components.selectbox {
import com.greensock.TweenLite;
import com.greensock.easing.Sine;
import com.thanksmister.touchlist.renderers.ITouchListItemRenderer;

import de.aoemedia.mobile.ui.components.buttons.AbstractButton;
import de.telekom.detefleet.app.views.components.buttons.actionBar.OkButton;
import de.telekom.detefleet.app.views.components.views.NavigationBarView;

import flash.display.DisplayObject;
import flash.system.System;

import org.osflash.signals.Signal;

/**
 *
 * @author Kai Zimmer <kai.zimmer@aoemedia.de>
 */
public class SelectBoxListView extends NavigationBarView {
    protected var confirmButton:OkButton;
    public var list:SelectBoxList;
    protected var selectedItem:Object;
    protected var _caller:SelectBoxButton;
    protected var _title:String;

    protected var _itemDescriptions:Vector.<Object>;

    public var signalOnHidden:Signal;
    public var signalSelectionConfirmed:Signal;
    public var signalSelectionCanceled:Signal;
    private var isSelectionDone:Boolean;

    public function SelectBoxListView() {
        super("SelectBoxListView", false);
        // prevent back button (mediator) from dispatching goBack signals
        _navbar.isHistoryBackDisabled = true;
        _navbar.signalBackBtnClicked.add(cancelSelection);

        signalOnHidden = new Signal();
        signalSelectionConfirmed = new Signal(Object);
        signalSelectionCanceled = new Signal();

        hide(0);
    }

    public function show(time:Number = .5):void {
        if (_title == null) {
            _navbar.label = _caller.prompt;
        }
        showNavbar(time);
        showList(time);

        System.gc();// User looks at the list so we can trigger Garbage Collection
    }

    public function hide(time:Number = .25):void {
        hideNavbar(time);
        hideList(time);
    }

    /**
     * If not set the callers prompt will be used as title
     */
    public function setTitle(title:String):void {
        _title = title;
        _navbar.label = title;
    }

    /**
     * Deletes the current title. The callers prompt will then be used as title.
     */
    public function resetTitle():void {
        _title = null;
        if (caller != null) {
            _navbar.label = caller.prompt;
        }
        else {
            _navbar.label = "";
        }
    }

    public function set caller(caller:SelectBoxButton):void {
        _caller = caller;
        labelField = caller.labelField;
    }

    public function get caller():SelectBoxButton {
        return _caller;
    }

    /**
     * @param itemDescriptions Array of object with at least an title property
     */
    public function setItems(itemDescriptions:Vector.<Object>):void {
        _itemDescriptions = itemDescriptions;
        list.addItems(itemDescriptions);
    }

    /**
     * @param itemDescriptions Array of object with at least an title property
     */
    public function getItems():Vector.<Object> {
        return _itemDescriptions;
    }

    public function removeItems():void {
        disableConfirmButton();
        list.removeItems();
        _itemDescriptions = null;
    }

    override protected function createChildren():void {
        super.createChildren();

        // add confirm button
        confirmButton = new OkButton();
        confirmButton.setSize(39, 39);
        confirmButton.signalClicked.add(confirmSelection);
        disableConfirmButton();
        _navbar.addActionButton(confirmButton);

        list = new SelectBoxList();
        list.onItemSelected = handleItemSelection;
        addChildAt(DisplayObject(list), 0);
    }

    protected function handleItemSelection(selectedItem:ITouchListItemRenderer):void {
        this.selectedItem = selectedItem.data;
        enableConfirmButton();
    }

    protected function enableConfirmButton():void {
        confirmButton.enabled = true;
        confirmButton.visible = true;
    }

    protected function disableConfirmButton():void {
        confirmButton.enabled = false;
        confirmButton.visible = false;
    }

    protected function showList(time:Number = .5):void {
        TweenLite.to(list, time, {y: 0, ease: Sine.easeOut});
    }

    protected function hideList(time:Number = .25):void {
        TweenLite.to(list, time, {y: screenHeight, ease: Sine.easeInOut, onComplete: onHidden});
    }

    protected function cancelSelection():void {
        isSelectionDone = false;
        signalSelectionCanceled.dispatch();
        hide();
    }

    protected function confirmSelection(button:AbstractButton):void {
        isSelectionDone = true;
        hide();
    }

    protected function onHidden():void {
        signalOnHidden.dispatch();
        if (isSelectionDone) {
            signalSelectionConfirmed.dispatch(selectedItem as Object);
        }
    }

    protected function destroy():void {
        list.removeItems();
        list = null;
        signalOnHidden.removeAll();
        signalSelectionCanceled.removeAll();
        signalSelectionConfirmed.removeAll();
    }

    public function get labelField():String {
        return list.labelField;
    }

    public function set labelField(labelField:String):void {
        list.labelField = labelField;
    }


}
}