package de.telekom.detefleet.app.services.local {
import de.telekom.detefleet.app.events.NetworkStatusChanged;
import de.telekom.detefleet.app.models.NetworkModel;

import flash.utils.setInterval;

import org.as3commons.logging.api.ILogger;
import org.as3commons.logging.api.getLogger;
import org.robotlegs.mvcs.Actor;

/**
 * Checks if the Webservice URL is reachable. It uses the domain in the JSONServiceConfiguration
 *
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class MockURLMonitorReachable extends Actor implements IReachable {
	private static const logger:ILogger = getLogger(MockURLMonitorReachable);

	[Inject]
	public var networkStatusChanged:NetworkStatusChanged;
	[Inject]
	public var networkModel:NetworkModel;

	/**
	 * Construct
	 */
	public function MockURLMonitorReachable():void {
	}

	/**
	 * Starts monitoring if Server URL is reachable
	 *
	 */
	public function start():void {
		announceStatus();
		setInterval(announceStatus, 20000);
		logger.info("Start URL Monitor");
	}

	/**
	 * Handles Event if the OnlineStatus Changed
	 *
	 */
	protected function announceStatus():void {
		networkModel.networkAvailable = true;
		networkStatusChanged.dispatch(NetworkStatusChanged.ONLINE);
	}

	/**
	 * Stops the monitoring
	 */
	public function stop():void {

	}
}
}