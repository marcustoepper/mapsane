package de.telekom.detefleet.app.controller.view
{
import de.telekom.detefleet.app.models.ViewsModel;
import de.telekom.detefleet.app.models.vo.TransitionStateVO;

import org.robotlegs.mvcs.SignalCommand;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class ChangeCurrentViewCMD extends SignalCommand
{
	[Inject]
	public var model:ViewsModel;

	[Inject]
	public var transitionState:TransitionStateVO;
	
	public override function execute():void
	{
		model.currentView = transitionState.viewToShow;
	}
}
}
