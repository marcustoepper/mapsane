package de.telekom.detefleet.app.context {
import de.telekom.detefleet.app.events.ChoosenVehicleServiceCaseChanged;
import de.telekom.detefleet.app.events.DialogConfirmed;
import de.telekom.detefleet.app.events.GeolocationStatusUpdated;
import de.telekom.detefleet.app.events.HideDialog;
import de.telekom.detefleet.app.events.LocationUpdated;
import de.telekom.detefleet.app.events.MessageIdsChanged;
import de.telekom.detefleet.app.events.MessagesChanged;
import de.telekom.detefleet.app.events.NetworkStatusChanged;
import de.telekom.detefleet.app.events.SelectionCanceled;
import de.telekom.detefleet.app.events.SelectionDone;
import de.telekom.detefleet.app.events.ShowDialog;
import de.telekom.detefleet.app.events.ShowSelectBoxList;
import de.telekom.detefleet.app.events.UnreadCountChanged;
import de.telekom.detefleet.app.events.ViewContextChanged;
import de.telekom.detefleet.app.events.fillingStation.FillingStationLoadFault;
import de.telekom.detefleet.app.events.fillingStation.FillingStationSelected;
import de.telekom.detefleet.app.events.fillingStation.FuelTypesChanged;
import de.telekom.detefleet.app.events.fillingStation.FuelTypesLoadFault;
import de.telekom.detefleet.app.events.fillingStation.SearchRequestLoaded;
import de.telekom.detefleet.app.events.fillingStation.SearchResultChanged;
import de.telekom.detefleet.app.events.garage.GarageLoadFault;
import de.telekom.detefleet.app.events.garage.GarageSelected;
import de.telekom.detefleet.app.events.garage.LastGarageRequestLoaded;
import de.telekom.detefleet.app.events.garage.VehicleServiceCaseLoadFault;
import de.telekom.detefleet.app.events.garage.VehicleServiceCasesChanged;
import de.telekom.detefleet.app.events.loader.HideLoader;
import de.telekom.detefleet.app.events.loader.ShowLoader;
import de.telekom.detefleet.app.events.mileage.LastMileageChanged;
import de.telekom.detefleet.app.events.repairRequest.RepairRequestsChanged;
import de.telekom.detefleet.app.events.repairRequest.RepairRequestsLoadFault;
import de.telekom.detefleet.app.events.repairRequest.TakenGarageChanged;
import de.telekom.detefleet.app.events.security.SSLCertificateInvalid;
import de.telekom.detefleet.app.events.security.SSLCertificateLoaded;
import de.telekom.detefleet.app.events.security.SSLCertificateLoadingFailed;
import de.telekom.detefleet.app.events.security.SSLCertificateValid;
import de.telekom.detefleet.app.events.view.OverlayHidden;
import de.telekom.detefleet.app.events.view.ShowView;
import de.telekom.detefleet.app.events.view.ShowingOverlay;
import de.telekom.detefleet.app.events.view.TransitionChanged;

import org.robotlegs.core.IInjector;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class BootstrapSignals {
    public function BootstrapSignals(injector:IInjector) {
        // Inject Signals
        injector.mapSingleton(NetworkStatusChanged);
        injector.mapSingleton(FuelTypesLoadFault);
        injector.mapSingleton(FillingStationLoadFault);
        injector.mapSingleton(FillingStationSelected);
        injector.mapSingleton(GarageSelected);
        injector.mapSingleton(GarageLoadFault);
        injector.mapSingleton(VehicleServiceCaseLoadFault);
        injector.mapSingleton(TransitionChanged);
        injector.mapSingleton(SelectionDone);
        injector.mapSingleton(ShowSelectBoxList);
        injector.mapSingleton(SelectionCanceled);
        injector.mapSingleton(LastMileageChanged);
        injector.mapSingleton(RepairRequestsChanged);
        injector.mapSingleton(RepairRequestsLoadFault);

        // Inject Loader Signals
        injector.mapSingleton(FuelTypesChanged);
        injector.mapSingleton(SearchResultChanged);
        injector.mapSingleton(SearchRequestLoaded);
        injector.mapSingleton(de.telekom.detefleet.app.events.garage.SearchResultChanged);
        injector.mapSingleton(VehicleServiceCasesChanged);
        injector.mapSingleton(LastGarageRequestLoaded);
        injector.mapSingleton(MessagesChanged);
        injector.mapSingleton(MessageIdsChanged);
        injector.mapSingleton(UnreadCountChanged);

        injector.mapSingleton(ShowView);
        injector.mapSingleton(ShowDialog);
        injector.mapSingleton(HideDialog);
        injector.mapSingleton(DialogConfirmed);
        injector.mapSingleton(ShowLoader);
        injector.mapSingleton(HideLoader);

        injector.mapSingleton(ShowingOverlay);
        injector.mapSingleton(OverlayHidden);

        injector.mapSingleton(TakenGarageChanged);
        injector.mapSingleton(ChoosenVehicleServiceCaseChanged);

        injector.mapSingleton(LocationUpdated);
        injector.mapSingleton(GeolocationStatusUpdated);

        injector.mapSingleton(ViewContextChanged);
		
        injector.mapSingleton(SSLCertificateLoaded);
        injector.mapSingleton(SSLCertificateLoadingFailed);
        injector.mapSingleton(SSLCertificateValid);
        injector.mapSingleton(SSLCertificateInvalid);
    }
}
}
