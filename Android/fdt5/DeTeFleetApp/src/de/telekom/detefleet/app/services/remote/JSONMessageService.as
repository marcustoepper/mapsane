package de.telekom.detefleet.app.services.remote {
import flash.events.Event;
import flash.events.HTTPStatusEvent;
import flash.events.IEventDispatcher;
import flash.events.IOErrorEvent;
import flash.events.SecurityErrorEvent;
import flash.net.URLLoader;
import flash.net.URLRequest;
import flash.net.URLRequestHeader;
import flash.net.URLRequestMethod;

import de.telekom.detefleet.app.events.ServiceFaultSignal;
import de.telekom.detefleet.app.models.MessageModel;
import de.telekom.detefleet.app.models.vo.MessageVO;
import de.telekom.detefleet.app.models.vo.ServiceAuthVO;

import org.as3commons.logging.api.ILogger;
import org.as3commons.logging.api.getLogger;
import org.robotlegs.mvcs.Actor;

public class JSONMessageService extends Actor implements IMessageService {
    private static const logger:ILogger = getLogger(JSONMessageService);

    [Inject]
    public var serviceFaultSignal:ServiceFaultSignal;

    [Inject]
    public var messageModel:MessageModel;

    public function getMessages(auth:ServiceAuthVO):void {
        var urlRequest:URLRequest = new URLRequest(JSONServiceConfiguration.endpointUrl + "/messages");
        var header:URLRequestHeader = new URLRequestHeader("Content-Type", "application/json");
        var loader:URLLoader = new URLLoader();

        urlRequest.data = '{' + auth.toJSON() + '}';
        urlRequest.method = URLRequestMethod.POST;
        urlRequest.requestHeaders.push(header);

        addListeners(loader);
        loader.addEventListener(Event.COMPLETE, getMessagesCompleteHandler);
        loader.load(urlRequest);
    }

    /**
     * Handles successful Request of asking Webservice for FuelTypes
     * @param event
     *
     */
    protected function getMessagesCompleteHandler(event:Event):void {
        var loader:URLLoader = URLLoader(event.target);

        try {
            var data:Object = JSON.parse(loader.data);
        } catch (e:Error) {
            serviceFaultSignal.dispatch("Daten sind nicht valide");
        }
        if (data.success) {
            // Convert JSON data into VOs
            var messages:Vector.<MessageVO> = new <MessageVO>[];
            for (var i:int = 0; i < data.data.length; i++) {
                messages[i] = new MessageVO(data.data[i]);
            }
            messageModel.messages = messages;
        } else {
            serviceFaultSignal.dispatch(data.error);
        }

        loader.removeEventListener(Event.COMPLETE, getMessagesCompleteHandler);
        removeListener(loader);
    }


    /**
     * TODO aktivieren sobald backend vorhanden
     */
    public function getMessageIds():void {
        var urlRequest:URLRequest = new URLRequest(JSONServiceConfiguration.endpointUrl + "/messages/getAvailableMessageIds");
        var header:URLRequestHeader = new URLRequestHeader("Content-Type", "application/json");
        var loader:URLLoader = new URLLoader();

        urlRequest.method = URLRequestMethod.POST;
        urlRequest.requestHeaders.push(header);

        addListeners(loader);
        loader.addEventListener(Event.COMPLETE, getMessageIdsCompleteHandler);
        loader.load(urlRequest);
    }

    /**
     *
     */
    protected function getMessageIdsCompleteHandler(event:Event):void {
        var loader:URLLoader = URLLoader(event.target);

        try {
            var data:Object = JSON.parse(loader.data);
        } catch (e:Error) {
            serviceFaultSignal.dispatch("Daten sind nicht valide");
        }
        if (data.success) {
            // Convert JSON data into VOs
			/*/
            var messageIds:Vector.<String> = new <String>[];
            for (var i:int = 0; i < data.data.length; i++) {
                messageIds[i] = data.data[i];
            }
            messageModel.messageIds = messageIds;
			//*/
			messageModel.messageIds = Vector.<String>(data.data);
			
        } else {
            serviceFaultSignal.dispatch(data.error);
        }

        loader.removeEventListener(Event.COMPLETE, getMessageIdsCompleteHandler);
        removeListener(loader);
    }

    /**
     * Handles an Security error of the Url Request.
     * Should be a Problem in Production
     */
    protected function securityErrorHandler(event:SecurityErrorEvent):void {
        removeListener(URLLoader(event.target));
        logger.error(event);
    }

    /**
     * Handles HTTP Status Response. If its bigger/equal than 400 dispatch an Error
     */
    protected function httpResponseStatusHandler(event:HTTPStatusEvent):void {
        if (event.status >= 400) {
            serviceFaultSignal.dispatch("Der Service ist nicht verfügbar.");
            logger.error("httpResponseStatusHandler: " + event);
        }
    }

    /**
     * Handles the io Errors
     */
    protected function ioErrorHandler(event:IOErrorEvent):void {
        removeListener(URLLoader(event.target));
        serviceFaultSignal.dispatch("Der Service ist nicht verfügbar (IOError).");
        logger.error("ioErrorHandler: " + event);
    }

    /**
     * Adds some basic Listeners
     */
    protected function addListeners(dispatcher:IEventDispatcher):void {
        dispatcher.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
        dispatcher.addEventListener(HTTPStatusEvent.HTTP_RESPONSE_STATUS, httpResponseStatusHandler);
        dispatcher.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
    }

    /**
     * Remove some Basic Listeners
     */
    protected function removeListener(dispatcher:IEventDispatcher):void {
        dispatcher.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
        dispatcher.removeEventListener(HTTPStatusEvent.HTTP_RESPONSE_STATUS, httpResponseStatusHandler);
        dispatcher.removeEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
    }

}
}
