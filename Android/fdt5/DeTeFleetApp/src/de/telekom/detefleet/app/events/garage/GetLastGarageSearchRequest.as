package de.telekom.detefleet.app.events.garage
{
import org.osflash.signals.Signal;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class GetLastGarageSearchRequest extends Signal
{
	public function GetLastGarageSearchRequest()
	{
		super();
	}
}
}
