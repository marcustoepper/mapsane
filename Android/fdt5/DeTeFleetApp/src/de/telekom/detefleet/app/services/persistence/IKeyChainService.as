package de.telekom.detefleet.app.services.persistence {
	/**
	 * Use this Interface to implement a secure Key Value Storage 
	 * 
	 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
	 * 
	 */
	public interface IKeyChainService {
		/**
		 * Persist the value for the given key. Overwrite existing values.
		 * Should dispatch an success or fault Event.
		 *  
		 * @param key The key under which the value is stored
		 * @param value The value which is stored
		 * 
		 */
		function setItem(key : String, value : String) : void;

		/**
		 * Get the stored value of a key
		 *   
		 * @param key The key to search for the value
		 * @return the value for the given key
		 * 
		 */
		function getItem(key : String) : String;

		/**
		 * Remove the stored value
		 * 
		 * @param key The key for the value which you want to remove
		 * 
		 */
		function removeItem(key : String) : void;
	}
}