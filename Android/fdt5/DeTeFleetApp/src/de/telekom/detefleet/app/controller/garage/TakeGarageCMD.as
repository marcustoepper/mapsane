package de.telekom.detefleet.app.controller.garage
{
import de.telekom.detefleet.app.models.vo.ViewRequestVO;
import de.aoemedia.motion.TransitionType;
import de.telekom.detefleet.app.views.components.views.RepairRequestFormView;
import de.telekom.detefleet.app.events.view.RequestView;
import de.telekom.detefleet.app.models.GarageModel;
import de.telekom.detefleet.app.models.vo.GarageVO;

import org.robotlegs.mvcs.SignalCommand;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class TakeGarageCMD extends SignalCommand
{
	// Payload
	[Inject]
	public var garage:GarageVO;
	
	[Inject]
	public var gotoView:RequestView;
	[Inject]
	public var model:GarageModel;

	public override function execute():void
	{
		model.takenGarage = garage;
		gotoView.dispatch(new ViewRequestVO(RepairRequestFormView, TransitionType.SLIDE_RIGHT));
	}
}
}