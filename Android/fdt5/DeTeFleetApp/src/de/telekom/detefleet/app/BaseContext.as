package de.telekom.detefleet.app
{
import de.telekom.detefleet.app.context.BootstrapViewCommands;
import de.telekom.detefleet.app.context.BootstrapCommands;
import de.telekom.detefleet.app.context.BootstrapModels;
import de.telekom.detefleet.app.context.BootstrapSignals;
import de.telekom.detefleet.app.context.BootstrapViewMediators;

import org.robotlegs.mvcs.SignalContext;

import flash.display.Sprite;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class BaseContext extends SignalContext
{
	public function BaseContext(contextView:Sprite)
	{
		super(contextView);
	}
	
	/**
	 * 
	 */
	public override function startup():void
	{
		new BootstrapModels(injector);
		new BootstrapCommands(commandMap, signalCommandMap);
		new BootstrapViewCommands(commandMap, signalCommandMap);
		new BootstrapViewMediators(mediatorMap);
		new BootstrapSignals(injector);

		// we're done
		super.startup();
	}
}
}
