package de.telekom.detefleet.app.events
{
	import de.telekom.detefleet.app.views.components.selectbox.SelectBoxButton;
	
	import org.osflash.signals.Signal;
	
	public class SelectionCanceled extends Signal
	{
		public function SelectionCanceled ( )
		{
			super(SelectBoxButton);
		}
	}
}