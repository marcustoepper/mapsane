package de.telekom.detefleet.app.views.components.buttons.actionBar
{
import de.telekom.detefleet.app.assets.IconRepository;

public class MapButton extends IconButton
{
	public function MapButton()
	{
		super();
	}

	override protected function createChildren():void
	{
		_icon = IconRepository.map;
		addChild(_icon);

		super.createChildren();
	}
}
}