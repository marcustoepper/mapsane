package de.telekom.detefleet.app.events
{
	import de.telekom.detefleet.app.views.components.selectbox.SelectBoxButton;
	
	import org.osflash.signals.Signal;
	
	public class ShowSelectBoxList extends Signal
	{	
		public function ShowSelectBoxList ( )
		{
			super(Vector.<Object>, SelectBoxButton);
		}
	}
}