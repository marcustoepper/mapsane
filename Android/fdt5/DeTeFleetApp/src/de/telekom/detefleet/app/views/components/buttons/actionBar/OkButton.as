package de.telekom.detefleet.app.views.components.buttons.actionBar
{
import de.telekom.detefleet.app.views.components.buttons.ActionButton;
	
	public class OkButton extends ActionButton
	{
		public function OkButton ( )
		{
			super("Ok");
		}
		
	}
}