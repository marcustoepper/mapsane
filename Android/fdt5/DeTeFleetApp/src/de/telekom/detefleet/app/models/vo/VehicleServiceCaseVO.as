package de.telekom.detefleet.app.models.vo
{

/**
 * Represents a Vehicle Service Case like Inspection, Tire Change
 * 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class VehicleServiceCaseVO
{
	public var id:int;
	public var serviceCase:String;
	public var mandatory:Array;

	/**
	 * Construct
	 * 
	 * @param object Anonymous Object which data will be set the properties of this object 
	 */
	public function VehicleServiceCaseVO(object:Object = null)
	{
		if (object == null) {
			return;
		}

		id = object.id as int;
		serviceCase = object.title;
		mandatory = object.mandatory;
	}

	/**
	 * Checks if a field is mandatory
	 */
	public function isMandatory(field:String):Boolean
	{
		for each (var item : String in mandatory) {
			if (item == field) {
				return true;
			}
		}
		return false;
	}
}
}