package de.telekom.detefleet.app.events {
	import org.osflash.signals.Signal;
	
	public class RenewSessionFault extends Signal
	{
		public function RenewSessionFault()
		{
			super(String);
		}
	}
}