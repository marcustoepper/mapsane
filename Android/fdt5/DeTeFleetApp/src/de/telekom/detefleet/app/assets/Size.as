package de.telekom.detefleet.app.assets
{
/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class Size
{
	public static const FORM_FIELD_RIGHT:Number = 200;
	public static const FORM_FIELD_BOTTOM:Number = 272;
}
}
