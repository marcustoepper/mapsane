package de.telekom.detefleet.app.services.local
{
/**
 * Classes which test if an URL or Network is reachable should implement this interface.
 * If the Networkstatus changed the implementation should dispatch an NetworkStatusChanged Signal 
 * 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public interface IReachable
{
	/**
	 * Starts the monitoring
	 */
	function start():void;

	/**
	 * Stops the monitoring
	 */
	function stop():void;
}
}