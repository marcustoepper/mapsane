package de.telekom.detefleet.app.views.components.buttons.actionBar {
import de.aoemedia.mobile.ui.components.buttons.AbstractButton;
import de.aoemedia.mobile.ui.components.skins.ButtonBitmaps;
import de.telekom.detefleet.app.assets.IconRepository;
import de.telekom.detefleet.app.assets.ScaleBitmaps;

import flash.display.Bitmap;

public class ListButton extends AbstractButton {

    protected var _icon:Bitmap;

    public function ListButton() {
        var sBmps:ScaleBitmaps = ScaleBitmaps.getInstance();
        var bmps:ButtonBitmaps
                = new ButtonBitmaps(sBmps.transparentButton,
                sBmps.transparentButtonDown,
                sBmps.transparentButton,
                sBmps.transparentButton);
        super(bmps);
    }

    override protected function createChildren():void {
        _icon = IconRepository.list;
        addChild(_icon);

        super.createChildren();
    }

    override public function setSize(w:Number = -1, h:Number = -1):void {
        super.setSize(w, h);

        _icon.x = (componentWidth - _icon.width) >> 1;
        _icon.y = (componentHeight - _icon.height) >> 1;
    }

}
}