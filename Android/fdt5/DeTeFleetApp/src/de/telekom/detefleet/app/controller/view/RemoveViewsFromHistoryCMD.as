package de.telekom.detefleet.app.controller.view
{
import de.telekom.detefleet.app.models.ViewsModel;

import org.robotlegs.mvcs.SignalCommand;

public class RemoveViewsFromHistoryCMD extends SignalCommand
{
	[Inject]
	public var model:ViewsModel;

	[Inject]
	public var number:int;

	override public function execute():void {
		model.removeViewsFromHistory(number);
	}
}
}
