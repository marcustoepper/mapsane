package de.telekom.detefleet.app.services.map {
	/**
	 * An Map API to communicate with the Map
	 */
	public interface IMap {
		/**
		 * Sets the Center of the Map
		 * @param latitude
		 * @param longitude
		 */
		function setCenter(latitude : Number, longitude : Number);

		/**
		 * Removes all Markers and Overlays from the map
		 */
		function clear();

		/**
		 * Sets the Current Position of the User (set little blue marker)
		 *
		 * @param latitude
		 * @param longitude
		 * @param center	Should the Map center to the Current Position
		 */
		function setCurrentPosition(latitude : Number, longitude : Number, center : Boolean);

		/**
		 *
		 * @param id 				An Id
		 * @param latitude
		 * @param longitude
		 * @param infoBoxContent	{head:"some Content", content:"some Content", callback:Function}
		 * 							callback is the Function which is called if the Button is clicked
		 * @param overlayContent	{head:"0.22 €", content:"vor 33 Min."}
		 */
		function addMarker(id : int, latitude : Number, longitude : Number, infoBoxContent : Object, overlayContent : Object);
	}
}
