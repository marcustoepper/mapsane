package de.telekom.detefleet.app.views.components.buttons
{
import de.aoemedia.mobile.ui.components.buttons.AbstractButton;
import de.aoemedia.mobile.ui.components.skins.ButtonBitmaps;
import de.telekom.detefleet.app.assets.IconRepository;
import de.telekom.detefleet.app.assets.ScaleBitmaps;

import flash.display.Bitmap;

public class CloseDialogButton extends AbstractButton
{
	protected var _icon:Bitmap;
	
	public function CloseDialogButton()
	{
		var sBmps:ScaleBitmaps = ScaleBitmaps.getInstance();
		var bmps:ButtonBitmaps = new ButtonBitmaps(sBmps.defaultButton, sBmps.defaultButtonDown, sBmps.defaultButtonDisabled, sBmps.defaultButtonDisabled);
		super(bmps);
	}

	override public function setSize(w:Number = -1, h:Number = -1):void
	{
		super.setSize(w, h);

		_icon.x = (componentWidth - _icon.width) >> 1;
		_icon.y = (componentHeight - _icon.height) >> 1;
	}
	
	override protected function createChildren():void
	{
		_icon = IconRepository.cross;
		addChild(_icon);

		super.createChildren();
	}
}
}