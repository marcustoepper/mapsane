package de.telekom.detefleet.app.services.local {
	/**
	 * Implement this if you want to read an unique device key.
	 * 
	 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
	 * 
	 */
	public interface IDeviceKey {
		/**
		 * Returns the unique Key of the Device.
		 * 
		 * @return The key of the Device
		 */
		function getKey() : String;
	}
}