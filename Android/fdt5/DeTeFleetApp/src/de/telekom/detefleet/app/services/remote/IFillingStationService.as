package de.telekom.detefleet.app.services.remote
{
import de.telekom.detefleet.app.models.vo.FillingStationRequestVO;
import de.telekom.detefleet.app.models.vo.ServiceAuthVO;

/**
 * Implement this for the Service to get Filling Stations
 * 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public interface IFillingStationService
{
	/**
	 * Search for Filling Stations
	 * 
	 * If successful it fills the FillingStationModel with the Result an disptach an FillingStationsLoaded Signal.
	 * If unseccessful it fills the FillingStationLoadFault with the error Message
	 * 
	 * @param auth The Authentification for the Webservice
	 * @param request The request for the Search
	 */
	function search(auth:ServiceAuthVO, request:FillingStationRequestVO):void;

	/**
	 * Ask the Service for the existing Fuel Types
	 * 
	 * If successful it fills the FillingStationModel.fuelTypes  with the Result an disptach an FuelTypesLoaded Signal.
	 * If unseccessful it fills the FuelTypesLoadFault with the error Message
	 * 
	 * @param auth The Authentification for the Webservice
	 */
	function getFuelTypes(auth:ServiceAuthVO):void;
}
}