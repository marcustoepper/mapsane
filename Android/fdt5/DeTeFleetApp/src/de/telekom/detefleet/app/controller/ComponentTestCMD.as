package de.telekom.detefleet.app.controller
{
import de.telekom.detefleet.app.views.components.selectbox.SelectBoxListView;

import com.kaizimmer.ui.Screen;

import org.robotlegs.mvcs.Command;

import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.utils.getTimer;
	
	/**
	 * ...
	 * @author dev.mode
	 */
	public class ComponentTestCMD extends Command
	{
		//[Inject]
		//public var model:Model;
		
		public override function execute ( ):void
		{
			//var btn:BaseButton = new BaseButton();
			
			/*/
			var tFormat:TextFormat = FontRepository.getInstance().getFormatTeleGroteskNor();
			tFormat.color = 0xff0000;
			tFormat.size = 56;
			
			var nav:NavigationBar = new NavigationBar();
			nav.label = "Hello World";
			nav.textFormat = tFormat;
			nav.x = 0;
			nav.y = 0;
			//nav.setSize(200, 50);
            contextView.addChild(nav);
			//*/
			
			/*/
			var labelButton:MyLabelButton = new MyLabelButton();
			labelButton.label = "my label button";
			labelButton.y = 200;
			labelButton.setSize(300, 100);
			
			var tFormat:TextFormat = FontRepository.getInstance().getFormatTeleGroteskNor();
			tFormat.color = 0xffffff;
			tFormat.size = 28;
			
			labelButton.textFormat = tFormat;
			labelButton.setSize(400,100);
			labelButton.label = "yet another label";
			labelButton.addEventListener(MouseEvent.MOUSE_UP, handleLabelButton);
			contextView.addChild(labelButton);
			//*/
			
			/*/
			var labelButton:IconLabelButton = new IconLabelButton();
			labelButton.label = "my label button";
			labelButton.y = 200;
			labelButton.setSize(300, 100);
			
			var tFormat:TextFormat = FontRepository.getInstance().getFormatTeleGroteskNor();
			tFormat.color = 0xffffff;
			tFormat.size = 28;
			
			labelButton.textFormat = tFormat;
			labelButton.setSize(400,100);
			labelButton.label = "yet another iconic btn";
			labelButton.leftIcon = MultiResolutionBitmaps.getInstance().iconRefueling;
			labelButton.rightIcon = MultiResolutionBitmaps.getInstance().iconArrow;
			labelButton.addEventListener(MouseEvent.MOUSE_UP, handleLabelButton);
			contextView.addChild(labelButton);
			//*/
			
			/*/
			var label:LabelComponent = new LabelComponent();
			label.label = "foobar";
			contextView.addChild(label);
			//*/
			
			
			//*/
			var itemsNum:Number = 50;
			var itemDescr:Object;
			var itemDescriptions:Vector.<Object> = new <Object>[];
			
			var t:Number = getTimer();
			
			for ( var i:uint=0; i < itemsNum; i++ )
			{
				itemDescr = {};
				itemDescr.label = "item_" + t + "_" + i;
				itemDescriptions.push(itemDescr);
			}
			
			var listView:SelectBoxListView = new SelectBoxListView();
			listView.setTitle("SelectBox Test");
			//listView.addItems(itemDescriptions);
			
			contextView.addChild(listView);
			listView.setTitle("SelectBoxList Test");
			listView.show();
			
			var testBG:Sprite = new Sprite();
			testBG.graphics.beginFill(0xff0000);
			testBG.graphics.drawRect(0,0,Screen.referenceWidth, Screen.referenceHeight);
			testBG.graphics.endFill();
			contextView.addChildAt(testBG, 0);
			
			//*/
		}

		private function handleLabelButton(event:MouseEvent):void
		{
			trace("handleLabelButton");
		}
		
	}
	
}