package de.telekom.detefleet.app.events.garage
{
import org.osflash.signals.Signal;

/**
 * Fired if you wnat to get Service Cases
 * 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class GetVehicleServiceCases extends Signal
{
	public function GetVehicleServiceCases()
	{
		super();
	}
}
}
