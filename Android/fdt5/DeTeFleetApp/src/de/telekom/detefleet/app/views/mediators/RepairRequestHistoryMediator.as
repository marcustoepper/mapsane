package de.telekom.detefleet.app.views.mediators
{
import de.telekom.detefleet.app.events.loader.HideLoader;
import de.telekom.detefleet.app.events.loader.ShowLoader;
import de.telekom.detefleet.app.events.repairRequest.GetRepairRequests;
import de.telekom.detefleet.app.events.repairRequest.RepairRequestsChanged;
import de.telekom.detefleet.app.events.view.RequestView;
import de.telekom.detefleet.app.events.view.TransitionCompleted;
import de.telekom.detefleet.app.events.view.ViewReady;
import de.telekom.detefleet.app.models.DialogModel;
import de.telekom.detefleet.app.models.LayoutData;
import de.telekom.detefleet.app.models.vo.RepairRequestVO;
import de.telekom.detefleet.app.models.vo.TransitionStateVO;
import de.telekom.detefleet.app.models.vo.ViewRequestVO;
import de.telekom.detefleet.app.views.components.dialog.RepairRequestResendConfirmationDialogView;
import de.telekom.detefleet.app.views.components.views.RepairRequestFormView;
import de.telekom.detefleet.app.views.components.views.RepairRequestHistoryView;

import org.robotlegs.mvcs.Mediator;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class RepairRequestHistoryMediator extends Mediator
{
	[Inject]
	public var view:RepairRequestHistoryView;
	[Inject]
	public var getRepairRequest:GetRepairRequests;
	[Inject]
	public var repairRequestsChanged:RepairRequestsChanged;
	[Inject]
	public var gotoView:RequestView;
	[Inject]
	public var transitionCompleted:TransitionCompleted;

	[Inject]
	public var viewReady:ViewReady;
	[Inject]
	public var showLoader:ShowLoader;

	[Inject]
	public var hideLoader:HideLoader;

	[Inject]
	public var model:DialogModel;

	[Inject]
	public var layout:LayoutData;

	/**
	 *
	 *
	 */
	public override function onRegister():void {
		view.resendClicked.add(resendClicked);
		repairRequestsChanged.add(populateRepairRequests);
		transitionCompleted.add(loadRepairRequests);

		view.newRepairRequestClickedCallback = gotoNewRepairSearch;

		viewReady.dispatch(view);
	}

	private function loadRepairRequests(transitionState:TransitionStateVO):void {
		if(transitionState.viewToShow == view) {
			showLoader.dispatch();
			getRepairRequest.dispatch();
		}
        if(transitionState.viewToHide == view) {
            view.unselectAll();
        }
	}

	private function resendClicked(repairRequest:RepairRequestVO):void {
		var dialog:RepairRequestResendConfirmationDialogView = new RepairRequestResendConfirmationDialogView(layout.viewport);
		dialog.data = repairRequest;
		model.addDialog(dialog);
	}

	protected function populateRepairRequests(requests:Vector.<RepairRequestVO>):void {
		view.populateRepairRequests(requests);
		hideLoader.dispatch();
	}

	protected function gotoNewRepairSearch():void {
		gotoView.dispatch(new ViewRequestVO(RepairRequestFormView));
	}

	/**
	 * Cleanup
	 */
	override public function preRemove():void {
		view.resendClicked.remove(resendClicked);
		repairRequestsChanged.remove(populateRepairRequests);
		view.newRepairRequestClickedCallback = null;
	}
}
}
