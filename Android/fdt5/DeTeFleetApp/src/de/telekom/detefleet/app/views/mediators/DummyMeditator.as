package de.telekom.detefleet.app.views.mediators
{
import de.telekom.detefleet.app.events.view.ViewReady;
import de.telekom.detefleet.app.views.components.views.DummyView;

import org.robotlegs.mvcs.Mediator;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class DummyMeditator extends Mediator
{
	[Inject]
	public var view:DummyView;
	
	[Inject]
	public var viewReady:ViewReady;
	
	public override function onRegister():void
	{
		viewReady.dispatch(view);
	}
}
}
