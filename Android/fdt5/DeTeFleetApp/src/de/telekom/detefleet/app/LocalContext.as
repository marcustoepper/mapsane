package de.telekom.detefleet.app
{
import flash.display.Sprite;
import de.telekom.detefleet.app.context.BootstrapLocalRemoteServices;

/**
 * This is the Context for use the App without Remote Server Connection
 * 
 * @author Daniel Kopp <danie.kopp@aoemedia.de>
 */
public class LocalContext extends BaseContext
{
	public function LocalContext(contextView:Sprite)
	{
		super(contextView);
	}
	/**
	 * 
	 */
	public override function startup():void
	{
		new BootstrapLocalRemoteServices(injector);
		
		super.startup();
	}

}

}