package de.telekom.detefleet.app.controller.dialog
{
import de.telekom.detefleet.app.models.DialogModel;
import de.telekom.detefleet.app.views.components.dialog.DialogView;

import org.robotlegs.mvcs.Command;

/**
 * ...
 * @author dev.mode
 */
public class RemoveDialogCMD extends Command
{

	[Inject]
	public var view:DialogView;

	[Inject]
	public var model:DialogModel;

	public override function execute():void {
		model.removeDialog();
	}
}
}
