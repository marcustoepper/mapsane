package de.telekom.detefleet.app.events.mileage
{
import org.osflash.signals.Signal;

/**
 * Save the Mileage
 * 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class PersistMileage extends Signal
{
	public function PersistMileage()
	{
		super(String);
	}
}
}
