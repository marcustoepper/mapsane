package de.telekom.detefleet.app.services.remote
{
import de.telekom.detefleet.app.models.vo.RepairRequestVO;
import de.telekom.detefleet.app.models.vo.ServiceAuthVO;

/**
 * Implement this for the Service to do some repair Stuff
 * 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public interface IRepairRequestService
{
	/**
	 * Return existing Repair requests
	 * 
	 * If successful it fills the RepairRequestModel with the Result an dispatch an RepairRequestsLoaded Signal.
	 * If unseccessful it fills the RepairRequestsLoadFault with the error Message
	 * 
	 * @param auth The Authentification for the Webservice
	 */
	function getRepairRequests(auth:ServiceAuthVO):void;

	/**
	 * Creates and send a Repair Request
	 * 
	 * If successful it dispatch an RepairRequestSuccessful Signal.
	 * If unseccessful it fills the RepairRequestFault with the error Message
	 * 
	 * @param auth The Authentification for the Webservice
	 * @param request The RepairRequest which should be send
	 */
	function createRepairRequest(auth:ServiceAuthVO, request:RepairRequestVO):void;

	/**
	 * Restarts an already sended Repair Request
	 * 
	 * If successful it dispatch an RepairRequestSuccessful Signal.
	 * If unseccessful it fills the RepairRequestFault with the error Message
	 * 
	 * @param auth The Authentification for the Webservice
	 * @param id The Id of the RepairRequest which should be send again
	 */
	function restartRepairRequest(auth:ServiceAuthVO, id:String):void;
}
}
