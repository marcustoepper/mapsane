package de.telekom.detefleet.app.views.components.views
{
import de.aoemedia.mobile.ui.components.buttons.AbstractButton;
import de.telekom.detefleet.app.assets.TextFormatTemplate;
import de.telekom.detefleet.app.views.components.buttons.actionBar.ListButton;

import org.osflash.signals.Signal;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class GarageMapView extends MapView
{
	public var signalToggleListButtonClicked:Signal;
	protected var toggleListButton:*;

	public function GarageMapView()
	{
		super("Werkstatt");

		signalToggleListButtonClicked = new Signal();
	}


	/**
	 * 
	 * 
	 */
	override protected function createChildren():void
	{
		super.createChildren();

		navbar.textFormat = TextFormatTemplate.getInstance().actionbarNormal;

		toggleListButton = new ListButton();
		toggleListButton.setSize(39, 39);
		toggleListButton.signalClicked.add(toggleList);
		_navbar.addActionButton(toggleListButton);

	}

	protected function toggleList(button:AbstractButton):void
	{
		signalToggleListButtonClicked.dispatch();
	}
}
}
