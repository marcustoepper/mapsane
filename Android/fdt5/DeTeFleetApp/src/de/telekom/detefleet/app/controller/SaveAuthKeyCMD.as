package de.telekom.detefleet.app.controller
{
import de.telekom.detefleet.app.models.UserModel;
import de.telekom.detefleet.app.models.vo.ServiceAuthVO;
import de.telekom.detefleet.app.services.persistence.IKeyChainService;

import org.robotlegs.mvcs.Command;

/**
 * Save the Authentification Key
 * 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class SaveAuthKeyCMD extends Command
{
	[Inject]
	public var keyChainService:IKeyChainService;

	[Inject]
	public var auth:ServiceAuthVO;

	[Inject]
	public var userModel:UserModel;

	public override function execute():void
	{
		if (userModel.storeAuthToken) {
			keyChainService.setItem("authToken", auth.authorizationToken);
		}
		userModel.authToken = auth.authorizationToken;
	}
}
}
