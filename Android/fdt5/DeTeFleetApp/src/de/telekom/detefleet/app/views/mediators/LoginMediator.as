package de.telekom.detefleet.app.views.mediators
{
import de.telekom.detefleet.app.events.loader.HideLoader;
import de.telekom.detefleet.app.events.loader.ShowLoader;
import de.telekom.detefleet.app.events.login.LoginFault;
import de.telekom.detefleet.app.events.login.LoginRequest;
import de.telekom.detefleet.app.events.login.LoginSuccess;
import de.telekom.detefleet.app.events.view.OverlayHidden;
import de.telekom.detefleet.app.events.view.ShowingOverlay;
import de.telekom.detefleet.app.events.view.TransitionChanged;
import de.telekom.detefleet.app.events.view.ViewReady;
import de.telekom.detefleet.app.models.vo.LoginFormData;
import de.telekom.detefleet.app.models.vo.PersonVO;
import de.telekom.detefleet.app.models.vo.ServiceAuthVO;
import de.telekom.detefleet.app.models.vo.TransitionStateVO;
import de.telekom.detefleet.app.views.components.dialog.DialogView;
import de.telekom.detefleet.app.views.components.views.IView;
import de.telekom.detefleet.app.views.components.views.LoginView;

import org.as3commons.logging.api.ILogger;
import org.as3commons.logging.api.getLogger;
import org.robotlegs.mvcs.Mediator;

/**
 * ...
 * @author dev.mode
 */
public class LoginMediator extends Mediator
{
	[Inject]
	public var view:LoginView;

	[Inject]
	public var loginSuccess:LoginSuccess;

	[Inject]
	public var loginFault:LoginFault;

	[Inject]
	public var loginRequest:LoginRequest;

	[Inject]
	public var transitionChanged:TransitionChanged;

	[Inject]
	public var viewReady:ViewReady;
	
	[Inject]
	public var showingOveraly:ShowingOverlay;

	[Inject]
	public var overlayHidden:OverlayHidden;

	[Inject]
	public var signalShowLoader:ShowLoader;
	[Inject]
	public var signalHideLoader:HideLoader;

	private static const logger:ILogger = getLogger(LoginMediator);

	/**
	 *
	 *
	 */
	public override function onRegister():void {
		view.signalSendLoginRequest.add(onSendBtnClick);
		transitionChanged.add(handleTransitionChanged);

		loginSuccess.add(onLoginSuccess);
		loginFault.add(onLoginFault);
		
		showingOveraly.add(onShowingOverlay);
		overlayHidden.add(onOverlayHidden);

		viewReady.dispatch(view);
	}
	
	protected function onOverlayHidden():void
	{
		//view.showStageTexts();
		view.unFreezeStageTextFields();
	}
	
	protected function onShowingOverlay():void
	{
		//view.hideStageTexts();
		view.freezeStageTextFields();
	}
	
	protected function handleTransitionChanged(state:TransitionStateVO):void {
		
		trace("handleTransitionChanged - state.viewToShow: " + state.viewToShow);
		
		switch (state.type) {
			case TransitionStateVO.IN:
				showStageTexts(state.viewToShow);
				break;
			case TransitionStateVO.TRANSITIONING:
				hideStageTexts(state.viewToHide);
				break;
			default:
		}
	}

	protected function showStageTexts(view:IView):void {
		if(this.view != view) return;
		this.view.showStageTexts();
		this.view.reset();
	}

	protected function hideStageTexts(view:IView):void {
		if(this.view == view) {
			this.view.hideStageTexts();
		}
	}

	protected function onSendBtnClick(loginData:LoginFormData):void {
		signalShowLoader.dispatch();
		loginRequest.dispatch(loginData);
	}

	/**fid
	 * Occures if the Login is successful
	 * @param e
	 *
	 */
	protected function onLoginSuccess(person:PersonVO, auth:ServiceAuthVO):void {
		view.reset();
		logger.info("Logged in as: " + person.firstName + " " + person.lastName + " (" + person.telephoneNumber + ")");

		signalHideLoader.dispatch();
	}

	/**
	 *
	 */
	protected function onLoginFault(faultMessage:String):void {
		// var result:String = "Login failed" + e.body;
		var result:String = "Login failed: " + faultMessage;
		signalHideLoader.dispatch();
		view.renderBadLogin();
		// view.reset();
	}

	override public function preRemove():void {
		view.signalSendLoginRequest.remove(onSendBtnClick);
		// since transitioned to signals LoginSuccess and LoginError this Mediator doesn't
		// receive the following events any longer
		// signalCommandMap.mapSignalClass(LoginSuccess, ShowHomeViewCMD);
		loginSuccess.remove(onLoginSuccess);
		loginFault.remove(onLoginFault);
		transitionChanged.remove(handleTransitionChanged);
		
		showingOveraly.remove(onShowingOverlay);
		overlayHidden.remove(onOverlayHidden);
		
		view.destroy();

		super.preRemove();
	}
}
}
