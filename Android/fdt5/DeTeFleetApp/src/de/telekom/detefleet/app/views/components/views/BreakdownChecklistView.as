package de.telekom.detefleet.app.views.components.views
{
import com.kaizimmer.ui.Screen;

import de.aoemedia.mobile.ui.components.RuleComponent;
import de.telekom.detefleet.app.assets.Color;
import de.telekom.detefleet.app.assets.TextFormatTemplate;

import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import flash.text.TextFormat;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class BreakdownChecklistView extends ScrollableNavigatorBarView
{
	private var content:Array = [{head:'Warnblinker einschalten'}, {head:'Fahrzeug abstellen', content:'Wenn möglich, fahren Sie so weit wie möglich rechts ran (Seitenstreifen oder Pannenbucht)'}, {head:'In Sicherheit bringen', content:'Steigen Sie und alle Insassen aus und verlassen die Straße. Achten Sie auf den rückwärtigen Verkehr und begeben sich auf der Autobahn sofort hinter die Leitplanke.'}, {head:'Warnweste anziehen'}, {head:'Warndreieck aufstellen', content:'Entgegen der Fahrtrichtung, Abstand weit genug vom Unfallort entfernt:\n\
- mind. 50m innerorts\n\
- mind. 100m außerorts\n\
- mind. 200m auf Autobahnen.\n\
Den nachfolgenden Verkehr vom Straßenrand aus warnen, am besten mit einer Warnleuchte.'}, {head:'Panne beheben', content:'Beheben Sie die Panne nur dann selbst, wenn Sie sich oder andere dabei nicht gefährden!'}];

	/**
	 * 
	 */
	public function BreakdownChecklistView()
	{
		super("Checkliste Panne");
		
		cacheAsBitmap = true;
	}

	/**
	 * 
	 */
	override protected function createChildren():void
	{
		super.createChildren();

		navbar.textFormat = TextFormatTemplate.getInstance().actionbarSmall;
		
		var paddingLeft:Number = 11;
		var paddingRight:Number = 11;
		var gap:Number = scale(16);
		var leading:Number = scale(8);
		var actualY:Number = navbar.height + gap;

		for (var i:int = 0; i < content.length; i++) {
			var head:TextField = new TextField();

			head.selectable = false;
			head.embedFonts = true;
			head.multiline = true;
			head.wordWrap = true;
			head.defaultTextFormat = TextFormatTemplate.getInstance().headline;
			head.mouseEnabled = false;
			head.text = (i + 1) + ". " + content[i].head;

			head.autoSize = TextFieldAutoSize.NONE;
			head.width = Screen.stage.stageWidth-scale(paddingLeft + paddingRight);
			head.height = head.textHeight + scale(4) - leading;

			head.y = actualY;
			head.x = scale(paddingLeft);
			
			addChild(head);
			actualY = head.y + head.height + gap;

			if(!content[i].content) {
			} else {
				var contentTextField:TextField = new TextField();

				contentTextField.selectable = false;
				contentTextField.embedFonts = true;
				contentTextField.multiline = true;
				contentTextField.wordWrap = true;
				contentTextField.mouseEnabled = false;
				contentTextField.defaultTextFormat = TextFormatTemplate.getInstance().content;

				contentTextField.text = content[i].content;

				contentTextField.autoSize = TextFieldAutoSize.NONE;
				contentTextField.width = Screen.stage.stageWidth - scale(paddingLeft + paddingRight);
				contentTextField.height = contentTextField.textHeight + scale(4);

				contentTextField.y = actualY - gap;
				contentTextField.x = scale(paddingLeft);

				addChild(contentTextField);

				actualY = contentTextField.y + contentTextField.height + gap;
			}

			var rule:RuleComponent = new RuleComponent(0.5, 320 - paddingLeft - paddingRight, Color.MEDIUM_GREY);
			rule.x = scale(paddingLeft);
			rule.y = actualY;
			addChild(rule);

			actualY = rule.y + rule.height + gap;
		}
	}
}
}
