package de.telekom.detefleet.app.events.repairRequest
{
import de.telekom.detefleet.app.models.vo.RepairRequestVO;
import org.osflash.signals.Signal;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class SendRepairRequest extends Signal
{
	public function SendRepairRequest()
	{
		super(RepairRequestVO);
	}
}
}
