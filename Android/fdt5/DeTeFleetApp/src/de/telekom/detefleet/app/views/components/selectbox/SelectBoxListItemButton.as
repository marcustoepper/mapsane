package de.telekom.detefleet.app.views.components.selectbox {
import com.kaizimmer.ui.Screen;
import com.thanksmister.touchlist.renderers.ITouchListItemRenderer;

import de.aoemedia.mobile.ui.MultiDpiHelper;
import de.telekom.detefleet.app.assets.Color;
import de.telekom.detefleet.app.assets.TextFormatTemplate;

import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;

public class SelectBoxListItemButton extends Sprite implements ITouchListItemRenderer {
    protected var _labelField:String = "label";

    protected var _leftPadding:uint;

    private var _label:TextField;

    protected var _index:Number;
    private var _itemWidth:Number;
    private var _itemHeight:Number;
    private var _itemSelectedCallback:Function;
    private var _itemPressedCallback:Function;

    protected var childsCreated:Boolean = false;

    private var _data:Object;

    public function SelectBoxListItemButton() {
        _leftPadding = MultiDpiHelper.scale(20);

        _itemHeight = MultiDpiHelper.scale(49);
        _itemWidth = Screen.stage.stageWidth - MultiDpiHelper.scale(2 * 8 + 2 * 4)
        addEventListener(MouseEvent.MOUSE_DOWN, pressHandler);

        createChildren();
        invalidate();
    }

    /**
     *
     */
    protected function createChildren():void {
        _label = new TextField();
        _label.defaultTextFormat = TextFormatTemplate.getInstance().selectBoxListItem;
        _label.text = "";
        _label.autoSize = TextFieldAutoSize.LEFT;
        _label.mouseEnabled = false;
        _label.embedFonts = true;
        addChild(_label);

        drawUnselectBackground();

        childsCreated = true;
    }

    /**
     *
     */
    protected function invalidate():void {
        if (!childsCreated) {
            return;
        }

        _label.x = _leftPadding;
        _label.y = itemHeight - _label.height >> 1;
    }

    public function set data(data:Object):void {
        if (_data == data) {
            return;
        }
        _data = data;

        _label.text = data[_labelField] as String;
        invalidate();
    }

    public function get data():Object {
        return _data;
    }

    public function get labelField():String {
        return _labelField;
    }

    public function set labelField(labelField:String):void {
        _labelField = labelField;
    }

    public function set index(value:Number):void {
        _index = value;
    }

    public function get index():Number {
        return _index;
    }

    public function set itemWidth(value:Number):void {
        _itemWidth = value;
    }

    public function get itemWidth():Number {
        return _itemWidth;
    }

    public function set itemHeight(value:Number):void {
        _itemHeight = value;
    }

    public function get itemHeight():Number {
        return _itemHeight;
    }

    public function selectable(value:Boolean):void {
        if (value) {
            addEventListener(MouseEvent.MOUSE_UP, selectHandler);
        } else {
            removeEventListener(MouseEvent.MOUSE_UP, selectHandler);
        }
    }

    public function destroy():void {
        graphics.clear();
        unselect();
    }

    public function get itemSelectedCallback():Function {
        return _itemSelectedCallback;
    }

    public function set itemSelectedCallback(value:Function):void {
        _itemSelectedCallback = value;
    }

    public function get itemPressedCallback():Function {
        return _itemPressedCallback;
    }

    public function set itemPressedCallback(value:Function):void {
        _itemPressedCallback = value;
    }

    /**
     * Dispatched when item is first pressed on tap or mouse down.
     * */
    protected function pressHandler(e:Event):void {
        _itemPressedCallback(this);
    }

    /**
     * Dispatched when item is selected, usually on touch end or mouse up.
     * */
    protected function selectHandler(e:Event):void {
        _itemSelectedCallback(this);
    }

    public function unselect():void {
        _label.textColor = Color.DARK_GREY;
        drawUnselectBackground();
    }

    public function select():void {
        _label.textColor = Color.DARK_GREY;
        drawSelectBackground();
    }

    protected function drawSelectBackground():void {
        graphics.clear();

        graphics.beginFill(Color.SELECTED_BACKGROUND);
        graphics.drawRect(0, 0, itemWidth, itemHeight);
        graphics.endFill();
    }

    protected function drawUnselectBackground():void {
        graphics.clear();

        graphics.beginFill(0xffffff);
        graphics.drawRect(0, 0, itemWidth, itemHeight);
        graphics.endFill();
    }
}
}
