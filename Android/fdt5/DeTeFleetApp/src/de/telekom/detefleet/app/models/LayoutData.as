package de.telekom.detefleet.app.models {
	import org.robotlegs.mvcs.Actor;

	import flash.geom.Rectangle;
	
	/**
	 * ...
	 * @author dev.mode
	 */
	public class LayoutData extends Actor
	{
		
		public var isLetterBoxed:Boolean = false;
		public var letterBoxBarWidth:Number = 0;
		public var viewport:Rectangle;
		
		public function LayoutData ( )
		{
			viewport = new Rectangle();
		}
		
	}
	
}