package de.telekom.detefleet.app.views.mediators
{
import de.telekom.detefleet.app.events.CloseDialog;
import de.telekom.detefleet.app.events.loader.ShowLoader;
import de.telekom.detefleet.app.events.repairRequest.ResendRepairRequest;
import de.telekom.detefleet.app.models.vo.RepairRequestVO;
import de.telekom.detefleet.app.views.components.dialog.RepairRequestResendConfirmationDialogView;

import org.robotlegs.mvcs.Mediator;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class RepairRequestResendConfirmationDialogMediator extends Mediator
{
	[Inject]
	public var view:RepairRequestResendConfirmationDialogView;

	[Inject]
	public var closeDialog:CloseDialog;

	[Inject]
	public var showLoader:ShowLoader;

	[Inject]
	public var resendRepairRequest:ResendRepairRequest;

	public override function onRegister():void {
		view.okClicked.add(handleOk);
		view.cancelClicked.add(handleCancel);
	}

	private function handleOk():void {
		closeDialog.dispatch(view);
		showLoader.dispatch();
		resendRepairRequest.dispatch(RepairRequestVO(view.data));
	}

	protected function handleCancel():void {
		closeDialog.dispatch(view);
	}

	public override function preRemove():void {
		view.okClicked.remove(handleOk);
		view.cancelClicked.remove(handleCancel);
		super.preRemove();
	}

}
}
