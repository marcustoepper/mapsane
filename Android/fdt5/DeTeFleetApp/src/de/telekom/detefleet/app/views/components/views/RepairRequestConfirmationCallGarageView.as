package de.telekom.detefleet.app.views.components.views {
import de.aoemedia.mobile.ui.MultiDpiHelper;
import de.aoemedia.mobile.ui.components.buttons.AbstractButton;
import de.telekom.detefleet.app.assets.Color;
import de.telekom.detefleet.app.assets.IconRepository;
import de.telekom.detefleet.app.assets.TextFormatTemplate;
import de.telekom.detefleet.app.views.components.buttons.CloseDialogButton;
import de.telekom.detefleet.app.views.components.buttons.DefaultLabelButton;
import de.telekom.detefleet.app.views.components.dialog.DialogView;

import flash.display.Bitmap;
import flash.display.DisplayObject;
import flash.geom.Rectangle;

import org.osflash.signals.Signal;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class RepairRequestConfirmationCallGarageView extends DialogView {
    protected const BUTTON_PADDING_BOTTOM:uint = 11;

    protected var icon:Bitmap;
    protected var closeButton:CloseDialogButton;

    public var callGarageClicked:Signal;
    public var closeClicked:Signal;

    private var callGarageButton:DefaultLabelButton;

    public function RepairRequestConfirmationCallGarageView(viewport:Rectangle) {
        var text:String = "Ihr Reparaturanstoß wurde an die Werkstatt gesendet. Möchten sie diese jetzt zur Terminvereinbarung anrufen?";
        super(viewport, "Vielen Dank", text);

        callGarageClicked = new Signal();
        closeClicked = new Signal();
    }

    /**
     *
     */
    override protected function createChildren():void {
        super.createChildren();

        callGarageButton = new DefaultLabelButton("Werkstatt anrufen");
        callGarageButton.icon = IconRepository.call;
        callGarageButton.signalClicked.add(proxyCallGarageClicked);
        callGarageButton.textFormat = TextFormatTemplate.getInstance().dialogButton;
        callGarageButton.labelColorDisabled = Color.MEDIUM_GREY;
        callGarageButton.labelColor = Color.DARK_GREY;

        container.addChild(DisplayObject(callGarageButton));

        icon = IconRepository.check;
        container.addChild(icon);

        closeButton = new CloseDialogButton();
        closeButton.setSize(39, 39);
        closeButton.signalClicked.add(handleCloseClick);
        container.addChild(DisplayObject(closeButton));
    }

    private function handleCloseClick(button:AbstractButton):void {
        closeClicked.dispatch();
    }

    /**
     *
     */
    override protected function invalidate():void {
        super.invalidate();

        callGarageButton.setSize(200, 44);
        callGarageButton.x = containerWidth / 2 - callGarageButton.width / 2;
        callGarageButton.y = messageTextField.y + messageTextField.height + scale(BUTTON_PADDING_BOTTOM);

        icon.x = 0;
        icon.y = scale(4);

        headlineTextField.x = icon.x + icon.width + scale(10);

        closeButton.x = container.width - closeButton.width;
        closeButton.y = 0;

        resetContainerPosition();
        invalidateBackground();
    }

    /**
     *
     * @param button
     */
    private function proxyCallGarageClicked(button:AbstractButton):void {
        callGarageClicked.dispatch();
    }
}
}
