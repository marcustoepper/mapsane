package de.telekom.detefleet.app.models
{
import org.robotlegs.mvcs.Actor;

/**
 * Stores data related to the Network Connection
 * 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class NetworkModel extends Actor
{
	public var _networkAvailable:Boolean;

	/**
	 * Construct. Sets defaults
	 */
	public function NetworkModel()
	{
		super();
		networkAvailable = false;
	}

	/**
	 * Stores if the Network is available
	 */	
	public function set networkAvailable(available:Boolean):void {
		_networkAvailable = available;
	}

	public function get networkAvailable():Boolean {
		return _networkAvailable;
	}
}
}