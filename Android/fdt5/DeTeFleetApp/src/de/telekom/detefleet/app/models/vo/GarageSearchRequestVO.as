package de.telekom.detefleet.app.models.vo
{
import de.telekom.detefleet.app.models.JSONable;

import flash.data.SQLStatement;

/**
 * The Request Data to search for Garage 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class GarageSearchRequestVO implements JSONable, SQLStatementable
{
	public var radius:Number = 30;
	public var serviceCase:int;
	public var location:LocationRequestVO;

	/**
	 * Convert Object to JSON String
	 * 
	 * @return JSON string
	 */
	public function toJSON():String
	{
		return '"request": {"radius":"' + radius + '", "serviceCase": "' + serviceCase + '", "location":{' + location.toJSON() + '}}';
	}

	public function updateStatement():String
	{
		return "";
	}

	public function insertStatement():String
	{
		return "INSERT INTO garageSearchRequest (serviceCase, radius, locationAddress, locationLatitude, locationLongitude) " + "VALUES (:serviceCase, :radius, :locationAddress, :locationLatitude, :locationLongitude)";
	}

	public function selectStatement():String
	{
		return "SELECT * FROM garageSearchRequest LIMIT 1";
	}

	public function deleteStatement():String
	{
		return "DELETE FROM garageSearchRequest";
	}

	public function fillStatementResult(row:Object):void
	{
		serviceCase = row.serviceCase;
		radius = row.radius;

		location = new LocationRequestVO();
		location.address = row.locationAddress;
		location.latitude = row.locationLatitude;
		location.longitude = row.locationLongitude;
	}

	public function setStatementParameters(statement:SQLStatement):void
	{
		statement.parameters[':serviceCase'] = serviceCase;
		statement.parameters[':radius'] = radius;
		statement.parameters[':locationAddress'] = location.address;
		statement.parameters[':locationLatitude'] = location.latitude;
		statement.parameters[':locationLongitude'] = location.longitude;
	}
}
}