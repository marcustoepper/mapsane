package de.telekom.detefleet.app.events.fillingStation {
	import org.osflash.signals.Signal;
	
	public class GetLastFillingStationSearchRequest extends Signal
	{
		public function GetLastFillingStationSearchRequest()
		{
			super();
		}
	}
}