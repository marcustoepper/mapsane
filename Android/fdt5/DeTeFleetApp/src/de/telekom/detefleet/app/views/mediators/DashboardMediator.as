package de.telekom.detefleet.app.views.mediators {
import de.telekom.detefleet.app.assets.IconRepository;
import de.telekom.detefleet.app.events.GetMessageIds;
import de.telekom.detefleet.app.events.NetworkStatusChanged;
import de.telekom.detefleet.app.events.UnreadCountChanged;
import de.telekom.detefleet.app.events.login.Logout;
import de.telekom.detefleet.app.events.view.RemoveViewsFromHistory;
import de.telekom.detefleet.app.events.view.RequestView;
import de.telekom.detefleet.app.events.view.TransitionCompleted;
import de.telekom.detefleet.app.events.view.ViewReady;
import de.telekom.detefleet.app.models.MessageModel;
import de.telekom.detefleet.app.models.NetworkModel;
import de.telekom.detefleet.app.models.vo.DashboardButtonVO;
import de.telekom.detefleet.app.models.vo.TransitionStateVO;
import de.telekom.detefleet.app.models.vo.ViewRequestVO;
import de.telekom.detefleet.app.views.components.views.BreakDownServiceView;
import de.telekom.detefleet.app.views.components.views.DashboardView;
import de.telekom.detefleet.app.views.components.views.FillingStationListView;
import de.telekom.detefleet.app.views.components.views.GarageListView;
import de.telekom.detefleet.app.views.components.views.ImprintView;
import de.telekom.detefleet.app.views.components.views.MessageListView;
import de.telekom.detefleet.app.views.components.views.MileageView;
import de.telekom.detefleet.app.views.components.views.RepairRequestHistoryView;

import flash.events.TimerEvent;
import flash.utils.Timer;

import org.robotlegs.mvcs.Mediator;

/**
 *
 *
 * @author dev.mode
 */
public class DashboardMediator extends Mediator {
    [Inject]
    public var networkStatusChanged:NetworkStatusChanged;

    [Inject]
    public var view:DashboardView;

    // Model
    [Inject]
    public var networkModel:NetworkModel;
    [Inject]
    public var messageModel:MessageModel;

    // Signals
    [Inject]
    public var gotoView:RequestView;
    [Inject]
    public var transitionCompleted:TransitionCompleted;
    [Inject]
    public var logout:Logout;
    [Inject]
    public var viewReady:ViewReady;
    [Inject]
    public var removeViewsFromHistory:RemoveViewsFromHistory;
    [Inject]
    public var unreadCountChanged:UnreadCountChanged;
    [Inject]
    public var getMessageIds:GetMessageIds;


    private var messagesData:DashboardButtonVO;

    private var messageTimer:Timer;

    public override function onRegister():void {
        view.buttonClicked.add(handleButtonClicked);


        view.signalImprintBtnClicked.add(requestViewImprint);

        // Listen to the context
        unreadCountChanged.add(handleUnreadCountChanged);
        transitionCompleted.add(onTransitionedOut);
        networkStatusChanged.add(handleNetworkStatusChanged);

        if (networkModel.networkAvailable) {
            view.setOnline();
            getMessageIds.dispatch();
        } else {
            view.setOffline();
        }

        var dataProvider:Array = [];
        dataProvider.push(new DashboardButtonVO(IconRepository.iconRefueling, "Tanken", FillingStationListView, null, false, true));
        dataProvider.push(new DashboardButtonVO(IconRepository.iconKilometerReading, "Kilometerstand", MileageView));
        dataProvider.push(new DashboardButtonVO(IconRepository.iconBreakdownService, "Pannendienst", BreakDownServiceView));
//		itemDescriptions.push(new DashboardButtonVO(IconRepository.iconDamageReport, "Schadensmeldung", FillingStationListView));
        dataProvider.push(new DashboardButtonVO(IconRepository.repairHistoryDashboard, "Reparaturanstoß", RepairRequestHistoryView, null, false, true));
        dataProvider.push(new DashboardButtonVO(IconRepository.iconGarage, "Werkstattsuche", GarageListView, null, false, true));
        messagesData = new DashboardButtonVO(IconRepository.message, "Meldungen", MessageListView, null, false, true);
        dataProvider.push(messagesData);
        dataProvider.push(new DashboardButtonVO(IconRepository.logoutDashboard, "Logout", null, handleLogout, true));

        view.buttonClicked.add(handleButtonClicked);
        view.dashboardList.dataProvider = dataProvider;

        messageTimer = new Timer(300000);

        viewReady.dispatch(view);
    }

    private function messageTimerTick(event:TimerEvent):void {
        getMessageIds.dispatch();
    }

    private function handleUnreadCountChanged(count:int):void {
        messagesData.badgeCount = count;
        view.dashboardList.resetBadgeCount();
    }

    private function handleLogout():void {
        logout.dispatch();
    }

    protected function handleNetworkStatusChanged(status:String):void {
        if (status == NetworkStatusChanged.ONLINE) {
            view.setOnline();
            getMessageIds.dispatch();

            messageTimer.start();
            messageTimer.addEventListener(TimerEvent.TIMER, messageTimerTick);
            return;
        }
        view.setOffline();

        messageTimer.stop();
    }

    private function handleButtonClicked(data:DashboardButtonVO):void {
        if (data.callback != null) {
            (data.callback)();
        }
        if (data.targetView != null) {
            gotoView.dispatch(new ViewRequestVO(data.targetView));
        }
    }

    protected function requestViewImprint():void {
        gotoView.dispatch(new ViewRequestVO(ImprintView));
    }

    protected function onTransitionedOut(state:TransitionStateVO):void {
        if (state.viewToHide != view) {
            return;
        }
        view.resetButtonColors();
        view.unselectAll();
    }

    override public function preRemove():void {
        view.buttonClicked.remove(handleButtonClicked);
        transitionCompleted.remove(onTransitionedOut);

        view.signalImprintBtnClicked.remove(requestViewImprint);

        networkStatusChanged.remove(handleNetworkStatusChanged);

        messageTimer.stop();

        super.preRemove();
    }
}
}
