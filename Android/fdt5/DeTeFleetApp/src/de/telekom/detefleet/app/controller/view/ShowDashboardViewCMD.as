package de.telekom.detefleet.app.controller.view
{
import de.aoemedia.motion.TransitionType;
import de.telekom.detefleet.app.events.view.RequestView;
import de.telekom.detefleet.app.models.ViewsModel;
import de.telekom.detefleet.app.models.vo.ViewRequestVO;
import de.telekom.detefleet.app.views.components.views.DashboardView;
import de.telekom.detefleet.app.views.components.views.LaunchImageView;

import org.robotlegs.mvcs.Command;


/**
 * Shows the Dashboard
 * 
 * @author Kai Zimmer <kai.zimmer@aoemedia.de>
 */
public class ShowDashboardViewCMD extends Command
{
	[Inject]
	public var requestView:RequestView;

	[Inject]
	public var viewsModel:ViewsModel;

	public override function execute():void
	{
		requestView.dispatch(new ViewRequestVO(DashboardView));
		viewsModel.removeView(LaunchImageView);
	}
}
}
