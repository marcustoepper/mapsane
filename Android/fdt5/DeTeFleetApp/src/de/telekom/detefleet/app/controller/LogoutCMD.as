package de.telekom.detefleet.app.controller
{
import de.telekom.detefleet.app.services.remote.ILoginService;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class LogoutCMD extends AuthCommand
{
	[Inject]
	public var loginService:ILoginService;

	override public function execute():void {
		loginService.logout(auth);
	}
}
}
