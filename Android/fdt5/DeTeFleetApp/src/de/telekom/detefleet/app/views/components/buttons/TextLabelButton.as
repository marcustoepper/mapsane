package de.telekom.detefleet.app.views.components.buttons
{
import de.aoemedia.mobile.ui.components.buttons.AbstractLabelButton;
import de.aoemedia.mobile.ui.components.skins.ButtonBitmaps;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class TextLabelButton extends AbstractLabelButton
{
	public function TextLabelButton(label:String = "")
	{
		var bmps:ButtonBitmaps = new ButtonBitmaps(null, null, null, null);
		super(bmps, label);
	}
}
}
