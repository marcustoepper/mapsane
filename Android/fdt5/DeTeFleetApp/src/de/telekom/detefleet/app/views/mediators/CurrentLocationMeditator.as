package de.telekom.detefleet.app.views.mediators {
import de.aoemedia.ane.maps.MapEvent;

import de.telekom.detefleet.app.events.LocationUpdated;
import de.telekom.detefleet.app.events.view.TransitionChanged;
import de.telekom.detefleet.app.events.view.TransitionCompleted;
import de.telekom.detefleet.app.events.view.ViewReady;
import de.telekom.detefleet.app.models.GeolocationModel;
import de.telekom.detefleet.app.models.vo.TransitionStateVO;
import de.telekom.detefleet.app.views.components.views.CurrentLocationView;

import org.robotlegs.mvcs.Mediator;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class CurrentLocationMeditator extends Mediator {
    // Views
    [Inject]
    public var view:CurrentLocationView;
    [Inject]
    public var locationUpdated:LocationUpdated;
    [Inject]
    public var geolocationModel:GeolocationModel;

    // Signal
    [Inject]
    public var viewReady:ViewReady;
    [Inject]
    public var transitionCompleted:TransitionCompleted;
    [Inject]
    public var transitionChanged:TransitionChanged;

    private var mapLoaded:Boolean = false;
    private var centered:Boolean = false;

    /**
     *
     */
    public override function onRegister():void {
        view.navbar.signalBackBtnClicked.add(backButtonClicked);

        transitionCompleted.add(handleTransitionCompleted);
        transitionChanged.add(handleTransitionChanged);

        geolocationModel.start();

        viewReady.dispatch(view);
    }

    private function handleLocationUpdate(longitude:Number, latitude:Number):void {
        centerMap();
    }

    private function backButtonClicked():void {
        view.hideMap();
        resetMap();
    }


    private function handleTransitionCompleted(state:TransitionStateVO):void {
        if (state.viewToShow != view) {
            return;
        }
        view.map.addEventListener(MapEvent.TILES_LOADED, handleMapLoaded);
        locationUpdated.add(handleLocationUpdate);
        view.clear();
        view.showMap();

        centerMap();
    }

    private function centerMap():void {
        if (mapLoaded && !centered && view.map.visible) {
            view.map.resetCenter(geolocationModel.latitude, geolocationModel.longitude);
            centered = true;
        }

    }

    private function handleMapLoaded(event:MapEvent):void {
        mapLoaded = true;
    }


    protected function handleTransitionChanged(state:TransitionStateVO):void {
        if (state.viewToHide == view && state.type == TransitionStateVO.TRANSITIONING) {
            view.hideMap();
            resetMap();
        }
    }

    private function resetMap():void {
        view.map.removeEventListener(MapEvent.TILES_LOADED, handleMapLoaded);
        locationUpdated.remove(handleLocationUpdate);
        centered = false;
    }

    /**
     *
     */
    override public function preRemove():void {
        transitionCompleted.remove(handleTransitionCompleted);
        super.preRemove();
    }
}
}
