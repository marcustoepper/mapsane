package de.telekom.detefleet.app.views.mediators
{
import de.aoemedia.motion.TransitionType;
import de.telekom.detefleet.app.events.RepairRequestSuccessfulSend;
import de.telekom.detefleet.app.events.ValidationError;
import de.telekom.detefleet.app.events.garage.CleanGarageList;
import de.telekom.detefleet.app.events.garage.GetVehicleServiceCases;
import de.telekom.detefleet.app.events.garage.VehicleServiceCasesChanged;
import de.telekom.detefleet.app.events.loader.HideLoader;
import de.telekom.detefleet.app.events.loader.ShowLoader;
import de.telekom.detefleet.app.events.repairRequest.SendRepairRequest;
import de.telekom.detefleet.app.events.repairRequest.TakenGarageChanged;
import de.telekom.detefleet.app.events.view.OverlayHidden;
import de.telekom.detefleet.app.events.view.RemoveViewContext;
import de.telekom.detefleet.app.events.view.RemoveViewsFromHistory;
import de.telekom.detefleet.app.events.view.RequestView;
import de.telekom.detefleet.app.events.view.ShowingOverlay;
import de.telekom.detefleet.app.events.view.TransitionChanged;
import de.telekom.detefleet.app.events.view.ViewReady;
import de.telekom.detefleet.app.models.GarageModel;
import de.telekom.detefleet.app.models.RepairRequestModel;
import de.telekom.detefleet.app.models.UserModel;
import de.telekom.detefleet.app.models.ViewsModel;
import de.telekom.detefleet.app.models.vo.GarageVO;
import de.telekom.detefleet.app.models.vo.RepairRequestVO;
import de.telekom.detefleet.app.models.vo.TransitionStateVO;
import de.telekom.detefleet.app.models.vo.VehicleServiceCaseVO;
import de.telekom.detefleet.app.models.vo.VehicleVO;
import de.telekom.detefleet.app.models.vo.ViewRequestVO;
import de.telekom.detefleet.app.views.components.views.GarageListView;
import de.telekom.detefleet.app.views.components.views.IView;
import de.telekom.detefleet.app.views.components.views.RepairRequestFormView;

import org.robotlegs.mvcs.Mediator;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class RepairRequestFormMediator extends Mediator
{
	// Signals
	[Inject]
	public var gotoView:RequestView;
	[Inject]
	public var viewReady:ViewReady;
	[Inject]
	public var removeViewContext:RemoveViewContext;
	[Inject]
	public var takenGarageChanged:TakenGarageChanged;
	[Inject]
	public var vehicleServiceCasesChanged:VehicleServiceCasesChanged;
	[Inject]
	public var getVehicleServiceCases:GetVehicleServiceCases;
	[Inject]
	public var sendRepairRequest:SendRepairRequest;
	[Inject]
	public var cleanGarageList:CleanGarageList;
	[Inject]
	public var removeViewsFromHistory:RemoveViewsFromHistory;

	[Inject]
	public var repairRequestSuccessfulSend:RepairRequestSuccessfulSend;

	[Inject]
	public var signalShowLoader:ShowLoader;
	[Inject]
	public var signalHideLoader:HideLoader;

	[Inject]
	public var validationError:ValidationError;
	
	[Inject]
	public var transitionChanged:TransitionChanged;
	
	[Inject]
	public var showingOveraly:ShowingOverlay;
	[Inject]
	public var overlayHidden:OverlayHidden;

	// View
	[Inject]
	public var view:RepairRequestFormView;

	// Model
	[Inject]
	public var model:GarageModel;
	[Inject]
	public var viewsModel:ViewsModel;
	[Inject]
	public var userModel:UserModel;

	public override function onRegister():void {
		transitionChanged.add(handleTransitionChanged);
		
		showingOveraly.add(onShowingOverlay);
		overlayHidden.add(onOverlayHidden);
		
		view.sendButtonClicked.add(handleSendButtonClicked);
		view.navbar.signalBackBtnClicked.add(handleBackButton);
		view.chooseGarage.add(handleChooseGarage);
		view.serviceCaseSelectBoxBtn.signalSelectionChanged.add(serviceCaseSelected);

		view.formErrorCallback = handleFormError;

		takenGarageChanged.add(handleTakenGarageChanged);
		vehicleServiceCasesChanged.add(populateVehicleServiceCases);
		repairRequestSuccessfulSend.add(handleSuccessfulSending);

		if(model.vehicleServiceCases) {
			populateVehicleServiceCases(model.vehicleServiceCases);
		} else {
			getVehicleServiceCases.dispatch();
		}

		populateVehicleData(userModel.userData.vehicles);
	}

	private function handleFormError(message:String):void {
		validationError.dispatch(message);
	}

	private function handleSuccessfulSending(request:RepairRequestVO, callGarage:Boolean):void {
		removeViewsFromHistory.dispatch(4);
		view.resetForm();
	}

	private function handleSendButtonClicked(request:RepairRequestVO):void {
		signalShowLoader.dispatch();
		sendRepairRequest.dispatch(request);

	}

	private function serviceCaseSelected(serviceCase:VehicleServiceCaseVO):void {
		model.choosenVehicleServiceCase = serviceCase;
		view.removeGarage();
	}

	private function populateVehicleData(vehicles:Vector.<VehicleVO>):void {
		view.myCarSelectBoxBtn.dataProvider = vehicles;
		view.myCarSelectBoxBtn.selectedItem = vehicles[0];
	}

	private function populateVehicleServiceCases(serviceCases:Vector.<VehicleServiceCaseVO>):void {
		viewReady.dispatch(view);
		view.serviceCaseSelectBoxBtn.dataProvider = serviceCases;
		view.serviceCaseSelectBoxBtn.selectedItem = serviceCases[0];
	}

	private function handleBackButton():void {
		removeViewContext.dispatch(RepairRequestModel.CONTEXT);
	}

	private function handleTakenGarageChanged(garage:GarageVO):void {
		view.setGarage(garage);
	}

	private function handleChooseGarage():void {
		cleanGarageList.dispatch();
		gotoView.dispatch(new ViewRequestVO(GarageListView, TransitionType.SLIDE_LEFT, RepairRequestModel.CONTEXT));
	}

	override public function preRemove():void {
		view.navbar.signalBackBtnClicked.remove(handleBackButton);
		view.chooseGarage.remove(handleChooseGarage);
		view.sendButtonClicked.remove(handleSendButtonClicked);
		view.serviceCaseSelectBoxBtn.signalSelectionChanged.remove(serviceCaseSelected);

		takenGarageChanged.remove(handleTakenGarageChanged);
		vehicleServiceCasesChanged.remove(populateVehicleServiceCases);
		
		showingOveraly.remove(onShowingOverlay);
		overlayHidden.remove(onOverlayHidden);
	}
	
	protected function handleTransitionChanged(state:TransitionStateVO):void {
		
		//trace("handleTransitionChanged - state.viewToShow: " + state.viewToShow);
		switch (state.type) {
			case TransitionStateVO.IN:
				showStageTexts(state.viewToShow);
				break;
			case TransitionStateVO.TRANSITIONING:
				hideStageTexts(state.viewToHide);
				break;
			default:
		}
	}
	
	protected function showStageTexts(view:IView):void {
		if(this.view != view) return;
		this.view.showStageTexts();
	}
	
	protected function hideStageTexts(view:IView):void {
		if(this.view == view) {
			this.view.hideStageTexts();
		}
	}
	
	protected function onOverlayHidden():void
	{
		//if ( viewsModel.currentView == view ) view.showStageTexts();
		if ( viewsModel.currentView == view )
		{
			trace("||||| unFreezeStageTextFields");
			
			view.unFreezeStageTextFields();
		}
	}
	
	protected function onShowingOverlay():void
	{
		//if ( viewsModel.currentView == view ) view.hideStageTexts();
		if ( viewsModel.currentView == view )
		{
			trace("||||| freezeStageTextFields");
			
			view.freezeStageTextFields();
		}
		
	}
}
}
