package de.telekom.detefleet.app.events {
import org.osflash.signals.Signal;

public class MessageIdsChanged extends Signal {
    public function MessageIdsChanged() {
        super(Vector.<int>);
    }
}
}
