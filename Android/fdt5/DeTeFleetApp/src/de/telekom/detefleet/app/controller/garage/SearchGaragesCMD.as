package de.telekom.detefleet.app.controller.garage
{
import de.telekom.detefleet.app.controller.AuthCommand;
import de.telekom.detefleet.app.models.vo.GarageSearchRequestVO;
import de.telekom.detefleet.app.services.remote.IGarageService;

public class SearchGaragesCMD extends AuthCommand
{
	[Inject]
	public var service:IGarageService;

	[Inject]
	public var request:GarageSearchRequestVO;

	public override function execute():void {
		service.search(auth, request);
	}
}
}
