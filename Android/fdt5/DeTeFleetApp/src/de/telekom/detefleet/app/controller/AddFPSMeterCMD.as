package de.telekom.detefleet.app.controller {
	import net.hires.debug.Stats;

	import org.robotlegs.mvcs.Command;

	import flash.display.Sprite;
	import flash.system.Capabilities;
	
	/**
	 * ...
	 * @author dev.mode
	 */
	public class AddFPSMeterCMD extends Command
	{
		//[Inject]
		//public var model:Model;
		
		public override function execute ( ):void
		{
			var statsHolder:Sprite = new Sprite();
			var stats:Stats = new Stats();
			if ( Capabilities.screenResolutionX <= 1024 )
			{
				stats.y = Capabilities.screenResolutionY - 200;
			}
			else {
				stats.y = contextView.height - 200;
			}
			statsHolder.addChild(stats);
			contextView.addChild(statsHolder);
		}
		
	}
	
}