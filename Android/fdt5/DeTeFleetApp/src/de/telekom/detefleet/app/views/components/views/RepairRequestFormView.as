package de.telekom.detefleet.app.views.components.views
{
import com.kaizimmer.events.ValidationEvent;
import com.kaizimmer.ui.Screen;
import com.kaizimmer.ui.form.FormFieldType;
import com.kaizimmer.ui.form.FormValidator;

import de.aoemedia.mobile.ui.components.forms.StageInputField;
import de.aoemedia.mobile.ui.components.forms.StageTextField;
import de.telekom.detefleet.app.assets.Color;

import flash.events.MouseEvent;
import flash.text.SoftKeyboardType;
import flash.text.TextFormat;

import de.aoemedia.mobile.ui.MultiDpiHelper;
import de.aoemedia.mobile.ui.components.buttons.AbstractButton;
import de.aoemedia.mobile.ui.components.container.LayoutContainer;
import de.aoemedia.mobile.ui.components.forms.FormField;
import de.telekom.detefleet.app.assets.FontRepository;
import de.telekom.detefleet.app.assets.IconRepository;
import de.telekom.detefleet.app.assets.TextFormatTemplate;
import de.telekom.detefleet.app.models.vo.GarageVO;
import de.telekom.detefleet.app.models.vo.RepairRequestVO;
import de.telekom.detefleet.app.models.vo.VehicleServiceCaseVO;
import de.telekom.detefleet.app.views.components.buttons.ActionButton;
import de.telekom.detefleet.app.views.components.buttons.ChooseGarageButton;
import de.telekom.detefleet.app.views.components.form.InputFieldWrapper;
import de.telekom.detefleet.app.views.components.form.StageInputFieldWrapper;
import de.telekom.detefleet.app.views.components.selectbox.SelectBoxButton;
import de.telekom.detefleet.app.views.components.selectbox.SelectBoxButtonWrapper;

import org.osflash.signals.Signal;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class RepairRequestFormView extends ScrollableNavigatorBarView
{
	public var chooseGarage:Signal;
	public var formErrorCallback:Function;

	private var formLayout:LayoutContainer;
	private var currentMileage:StageInputFieldWrapper;
	private var phoneNumber:StageInputFieldWrapper;
	private var notes:InputFieldWrapper;
	protected var sendButton:ActionButton;
	public var myCarSelectBoxBtn:SelectBoxButton;
	public var serviceCaseSelectBoxBtn:SelectBoxButton;
	private var formfieldNotes:FormField;
	private var formValidator:FormValidator;

	private var errorMessage:String = "";
	private var isValid:Boolean = true;

	protected var choosenGarage:GarageVO;

	public var sendButtonClicked:Signal;
	private var formfieldMileage:FormField;
	private var chooseGarageButton:ChooseGarageButton;
    private var formfieldPhone:FormField;

    private var _isSendButtonClicked:Boolean;

	public function RepairRequestFormView() {
		super("Reparaturanstoß");

		chooseGarage = new Signal();
		sendButtonClicked = new Signal(RepairRequestVO);
		paddingBottom = 11;
	}

	protected function setStyles():void {
		var textFormat:TextFormat = TextFormatTemplate.getInstance().formLabel;
		chooseGarageButton.textFormat = textFormat;
		chooseGarageButton.labelColor = Color.DARK_GREY;

		textFormat = TextFormatTemplate.getInstance().formLabelHighlight;
		chooseGarageButton.garageName.textFormat = textFormat;
	}

	override protected function createChildren():void {
		super.createChildren();

		navbar.textFormat = TextFormatTemplate.getInstance().actionbarSmall;


		createForm();
	}

	protected function createForm():void {
		_navbar.textFormat = TextFormatTemplate.getInstance().actionbarSmall;

		formLayout = new LayoutContainer(draggingView);
        formLayout.stopInvalidation = true;
		formLayout.y = navbar.height;
		formLayout.explicitWidth = Screen.stage.stageWidth;
		formLayout.paddingTop = 11;
		formLayout.paddingLeft = 11;
		formLayout.paddingRight = 11;
		formLayout.paddingBottom = 11;
		formLayout.gap = 15;

		var myCarWrapper:SelectBoxButtonWrapper = new SelectBoxButtonWrapper(draggingView, "Mein Fahrzeug");
		myCarSelectBoxBtn = myCarWrapper.selectBoxButton;
		myCarWrapper.percentalWidth = 100;
		myCarWrapper.gap = 2;
		myCarWrapper.label = "Mein Fahrzeug";
		myCarWrapper.labelPosition = SelectBoxButtonWrapper.POSITION_TOP;
		myCarSelectBoxBtn.enabled = false;
		myCarSelectBoxBtn.labelField = "plateNo";

		formLayout.addItem(myCarWrapper);

		var serviceCaseWrapper:SelectBoxButtonWrapper = new SelectBoxButtonWrapper(draggingView, "Servicefall");
		serviceCaseSelectBoxBtn = serviceCaseWrapper.selectBoxButton;
		serviceCaseWrapper.percentalWidth = 100;
		serviceCaseWrapper.gap = 2;
		serviceCaseWrapper.label = "Servicefall auswählen";
		serviceCaseWrapper.labelPosition = SelectBoxButtonWrapper.POSITION_TOP;
		serviceCaseSelectBoxBtn.labelField = 'serviceCase';
		serviceCaseSelectBoxBtn.textFieldWidth = 200;
		serviceCaseSelectBoxBtn.signalSelectionChanged.add(serviceCaseChanged);
		formLayout.addItem(serviceCaseWrapper);

		chooseGarageButton = new ChooseGarageButton("Werkstatt:");
		chooseGarageButton.setSize(320, 40);
		chooseGarageButton.paddingLeft = -11;
		chooseGarageButton.signalClicked.add(chooseGarageButtonClicked);
		formLayout.addItem(chooseGarageButton);

		/*/
		currentMileage = new InputFieldWrapper(draggingView);
		currentMileage.label = "Aktueller Kilometerstand";
		currentMileage.percentalWidth = 100;
		//*/
		
		currentMileage = new StageInputFieldWrapper(draggingView);
		currentMileage.label = "Aktueller Kilometerstand";
		currentMileage.percentalWidth = 100;
		/*/
		currentMileage.fontSize = MultiDpiHelper.scale(10);
		currentMileage.textPaddingVertical = MultiDpiHelper.scale(6);
		//*/
		currentMileage.softKeyboardType = SoftKeyboardType.NUMBER;
        StageInputField(currentMileage.inputField).stageTextField.stageText.maxChars = 7;

		/*/
		phoneNumber = new InputFieldWrapper(draggingView);
		phoneNumber.label = "Telefonnummer";
		phoneNumber.percentalWidth = 100;
		//*/
		
		phoneNumber = new StageInputFieldWrapper(draggingView);
		phoneNumber.label = "Telefonnummer:";
		//phoneNumber.setSize(272, 37);
		phoneNumber.percentalWidth = 100;
		phoneNumber.softKeyboardType = SoftKeyboardType.NUMBER;
		//phoneNumber.showText(stage);

		notes = new InputFieldWrapper(draggingView);
		notes.inputField.multiline = true;
		notes.label = "Beschreibung";
		notes.percentalWidth = 100;
		notes.lines = 5;
		
		_stageInputFields.push(currentMileage);
		_stageInputFields.push(phoneNumber);

		sendButton = new ActionButton("Senden");
		sendButton.setSize(320 - formLayout.paddingLeft - formLayout.paddingRight, 37);
		sendButton.icon = IconRepository.mail;
		sendButton.signalClicked.add(handleSendButtonClicked);

		formLayout.addItem(notes);
		formLayout.addItem(currentMileage);
		formLayout.addItem(phoneNumber);
		formLayout.addItem(sendButton);

        formLayout.stopInvalidation = false;
        formLayout.invalidate();
		formLayout.addItemsToView();

		initForms();
		setStyles();

		invalidate();
		
	}

	private function handleSendButtonClicked(button:AbstractButton):void {
		validateForm();
		if(errorMessage != "") {
            return;
        }
		
		_isSendButtonClicked = true;

		var requestData:RepairRequestVO = new RepairRequestVO();

		requestData.mileage = Number(StageInputField(currentMileage.inputField).stageTextField.text);
		requestData.vehicleServiceCase = VehicleServiceCaseVO(serviceCaseSelectBoxBtn.selectedItem);
		requestData.phone = phoneNumber.inputField.field.text;
		requestData.garage = choosenGarage;
		requestData.notes = notes.inputField.field.text;

		sendButtonClicked.dispatch(requestData);

		errorMessage = "";
		isValid = true;
	}

	/**
	 *
	 */
	protected function initForms():void {
		formValidator = new FormValidator();

		formfieldNotes = new FormField(notes.inputField, " ", true, FormFieldType.ALL, 1);
		formfieldNotes.liveChecking = true;
		formfieldNotes.preventLiveCheckRendering = true;

		formfieldMileage = new FormField(currentMileage.inputField, " ", true, FormFieldType.INTEGER, 0, 7);
		formfieldMileage.liveChecking = true;
		formfieldMileage.preventLiveCheckRendering = true;

        formfieldPhone = new FormField(phoneNumber.inputField, " ", true, FormFieldType.ALL, 1);
        formfieldPhone.liveChecking = true;
        formfieldPhone.preventLiveCheckRendering = true;

		formValidator.addFormObject(formfieldNotes);
		formValidator.addFormObject(formfieldMileage);

        formValidator.addFormObject(formfieldPhone);
    }

	private function validateForm():void{
		isValid = true;
		errorMessage = "";

		if(choosenGarage == null){
			errorMessage += "Bitte wählen Sie eine Werkstatt aus.\n";
			isValid = false;
		}
		if(!formfieldMileage.validate()){
			errorMessage += "Bitte geben Sie Ihren Kilometerstand ein.\n";
			isValid = false;
		}
		if(!formfieldNotes.validate()){
			errorMessage += "Bitte beschreiben Sie Ihren Servicefall.\n";
			isValid = false;
		}
        if(!formfieldPhone.validate()){
            errorMessage += "Bitte tragen Sie Ihre Telefonnummer ein.\n";
            isValid = false;
        }
		if(errorMessage != ""){
			formErrorCallback(errorMessage);
		}
	}

	private function serviceCaseChanged(serviceCase:Object):void {
		formfieldNotes.isMandatory = VehicleServiceCaseVO(serviceCase).isMandatory('notes');
	}

	private function chooseGarageButtonClicked(button:AbstractButton):void {
		chooseGarage.dispatch();
	}

	public function setGarage(garage:GarageVO):void {
		chooseGarageButton.garageName.label = garage.name;
		choosenGarage = garage;
	}

	public function removeGarage():void {
		chooseGarageButton.garageName.label = "Bitte auswählen...";
		choosenGarage = null;
	}

	/**
	 * Clear the Form
	 */
	public function resetForm():void {
		formfieldNotes.reset();
		formfieldMileage.reset();
        StageInputField(phoneNumber.inputField).stageTextField.text = "";
		removeGarage();
		serviceCaseSelectBoxBtn.selectedItem = serviceCaseSelectBoxBtn.dataProvider[0];
	}
	
	override protected function startDragging ( event:MouseEvent ):void
	{
		super.startDragging(event);
		freezeStageTextFields();
	}
	
	override protected function stopDragging ( event:MouseEvent ):void
	{
		super.stopDragging(event);
		invalidateStageTextFieldsCoords();
		
		// make sure that stopDragging invocations caused by the sendButton mouse up event don't unfreeze the StageTextFields! 
		if ( _isSendButtonClicked ) return;
		unFreezeStageTextFields();
	}
	
}
}
