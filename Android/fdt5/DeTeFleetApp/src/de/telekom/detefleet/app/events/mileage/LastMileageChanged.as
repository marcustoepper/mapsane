package de.telekom.detefleet.app.events.mileage
{
import de.telekom.detefleet.app.models.vo.MileageVO;

import org.osflash.signals.Signal;

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public class LastMileageChanged extends Signal
{
	public function LastMileageChanged()
	{
		super(MileageVO);
	}
}
}
