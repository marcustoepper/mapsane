package de.telekom.detefleet.app.services.remote
{
import de.telekom.detefleet.app.models.vo.ServiceAuthVO;


/**
 * Implement this for the handle the Remote Login
 * 
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 * 
 */
public interface ILoginService
{
	/**
	 * Log the User in. 
	 * 
	 * If sucessful dispatch a LoginSuccess Signal with the Logedin User. 
	 * Else dispatch a LoginFault with the Error Message. 
	 * 
	 * @param username The Username 
	 * @param password The Password of the User
	 * @param deviceKey The unique key of the Device
	 * 
	 */
	function login(username:String, password:String, deviceKey:String):void;

	/**
	 * Renews an existing User Session
	 * 
	 * If succesfull dispatch a RenewSessionSuccess.
	 * Else dispatch a RenewSessionFault with the Error Message. 
	 *  
	 * @param auth Authenification Data of User
	 * 
	 */
	function renewSession(auth:ServiceAuthVO):void;
	
	/**
	 * Logout a User
	 * 
	 * 
	 * If sucessful dispatch a LogoutSuccess Signal. 
	 * Else dispatch a LogoutFault with the Error Message. 
	 * 
	 * @param auth Authenification Data of User
	 */
	function logout(auth:ServiceAuthVO):void;
}
}