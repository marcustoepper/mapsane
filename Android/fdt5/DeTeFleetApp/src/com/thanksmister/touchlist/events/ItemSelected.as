
package com.thanksmister.touchlist.events
{
import com.thanksmister.touchlist.renderers.ITouchListItemRenderer;

import org.osflash.signals.Signal;

public class ItemSelected extends Signal
{
	public function ItemSelected():void{
		super(ITouchListItemRenderer);
	}


}
}
