package com.thanksmister.touchlist.events
{
import com.thanksmister.touchlist.renderers.ITouchListItemRenderer;

import org.osflash.signals.Signal;

public class ItemPressed extends Signal
{
	public function ItemPressed():void {
		super(ITouchListItemRenderer);
	}
}
}
