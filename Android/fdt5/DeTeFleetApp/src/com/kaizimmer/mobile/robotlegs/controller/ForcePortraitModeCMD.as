/*
* Copyright (c) 2012 Kai Zimmer [http://www.kaizimmer.com]
*
* Permission is hereby granted to use, modify, and distribute this file
* in accordance with the terms of the license agreement accompanying it.
*/
package com.kaizimmer.mobile.robotlegs.controller
{
	import flash.display.Stage;
	import flash.display.StageOrientation;
	import flash.events.StageOrientationEvent;
	
	import org.robotlegs.mvcs.Command;
	
	/**
	 * Prevents changing the orientation when device is rotated to landscape position.
	 * 
	 * @author k.zimmer aka engimono
	 */
	public class ForcePortraitModeCMD extends Command
	{
		//[Inject]
		//public var model:Model;
		
		protected var stage:Stage;
		
		/**
		 * 
		 * 
		 */
		public override function execute ( ):void
		{
			stage = contextView.stage;
			if ( stage.supportedOrientations )
			{
				stage.autoOrients = true;
				// StageOrientationEvent.ORIENTATION_CHANGING are not dispatched on Android Devices!
				stage.addEventListener(StageOrientationEvent.ORIENTATION_CHANGING, onOrientationChanging );
				//stage.addEventListener(StageOrientationEvent.ORIENTATION_CHANGE,onOrientationChange);
			}
			
		}
		
		/**
		 * Prevents changing the orientation when device is rotated to landscape position.
		 * 
		 */
		protected function onOrientationChanging ( event:StageOrientationEvent ):void
		{
			/*
			Make sure the application starts in portrait mode by setting 
			<aspectRatio>portrait</aspectRatio> in the app descriptor file
			*/
			switch( event.afterOrientation )
			{
				case StageOrientation.ROTATED_LEFT:
				case StageOrientation.ROTATED_RIGHT:
					event.preventDefault();
					break;
				
				case StageOrientation.DEFAULT:
				case StageOrientation.UPSIDE_DOWN:
				default:
					// sometimes (when the app starts and immediately rotates from default to upsidedown) 
					// the orientation change isn't performed so to be sure we set the orientation explicitly
					stage.setOrientation(event.afterOrientation);
			}
		}
		
	}
	
}