﻿/*
* The MIT License
* 
* Copyright (c) 2010 Kai Zimmer [http://www.kaizimmer.com]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
package com.kaizimmer.ui.form
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	/**
	 * ...
	 * @author kzimmer aka engimono aka mxskout - www.kaizimmer.com
	 */
	internal class AbstractRadioButton extends Sprite
	{
		protected var _isSelected:Boolean;
		protected var _data:Object;
		
		//-------------------------------------------------------------------------
		
		public function AbstractRadioButton ( data:Object )
		{
			_data = data;
			_isSelected = false;
			addEventListener(MouseEvent.CLICK, toggleState);
			updateDisplay();
		}
		
		//-------------------------------------------------------------------------
		
		internal function set isSelected ( selected:Boolean ):void
		{
			_isSelected = selected;
			updateDisplay();
		}
		
		//-------------------------------------------------------------------------
		
		internal function get isSelected ( ):Boolean
		{
			return _isSelected;
		}
		
		//-------------------------------------------------------------------------
		
		public function get data ( ):Object { return _data; }
		
		//-------------------------------------------------------------------------
		
		public function set data ( value:Object ):void 
		{
			_data = value;
		}
		
		//-------------------------------------------------------------------------
		
		protected function toggleState ( e:MouseEvent ):void 
		{
			_isSelected = !_isSelected;
			if ( _isSelected ) dispatchClickEvent();
		}
		
		//-------------------------------------------------------------------------
		
		protected function updateDisplay ( ):void
		{
			if ( _isSelected )
			{
				renderSelectedDisplay();
			}
			else {
				renderDeselectedDisplay();
			}
		}
		
		//-------------------------------------------------------------------------
		
		protected function renderSelectedDisplay ( ):void
		{
			throw(new Error("renderSelectedDisplay is an abstract method"));
		}
		
		//-------------------------------------------------------------------------
		
		protected function renderDeselectedDisplay ( ):void
		{
			throw(new Error("renderDeselectedDisplay is an abstract method"));
		}
		
		//-------------------------------------------------------------------------
		
		protected function dispatchClickEvent ( ):void
		{
			dispatchEvent(new MouseEvent(MouseEvent.CLICK, true));
		}
		
	}
	
}