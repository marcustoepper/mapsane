﻿/*
* The MIT License
* 
* Copyright (c) 2010 Kai Zimmer [http://www.kaizimmer.com]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
package com.kaizimmer.ui.form
{
	import com.kaizimmer.events.ValidationEvent;
	import com.kaizimmer.ui.form.ICheckBox;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	/**
	 * Simple CheckBox class.
	 * @author kzimmer aka engimono aka mxskout - www.kaizimmer.com
	 */
	public class CheckBox extends Sprite implements ICheckBox
	{
		
		protected var _isSelected:Boolean;
		protected var _selectedDisplay:DisplayObject;
		protected var _deselectedDisplay:DisplayObject;
		protected var _highlightDisplay:DisplayObject;
		protected var _isMandatory:Boolean;
		
		//-------------------------------------------------------------------------
		
		/**
		 * 
		 * @param	selectedDisplay:DisplayObject The displayobject used to render the selected state
		 * @param	deselectedDisplay:DisplayObject The displayobject used to render the inactive state
		 * @param	highlightDisplay:DisplayObject The displayobject used to render the error state
		 * @param	isMandatory:Boolean The checkbox's mandatory state
		 */
		public function CheckBox ( selectedDisplay:DisplayObject,
									deselectedDisplay:DisplayObject,
									highlightDisplay:DisplayObject,
									isMandatory:Boolean = false,
									selected:Boolean = false )
		{
			_selectedDisplay = selectedDisplay;
			_deselectedDisplay = deselectedDisplay;
			_highlightDisplay = highlightDisplay;
			_isMandatory = isMandatory;
			_isSelected = selected;
			// traverse displays to this sprite
			addChild(_selectedDisplay);
			addChild(_deselectedDisplay);
			addChild(_highlightDisplay);
			addEventListener(MouseEvent.CLICK, toggleState);
			updateDisplay();
		}
		
		//-------------------------------------------------------------------------
		
		public function validate ( ):Boolean
		{
			if ( !isMandatory ) return true;
			return _isSelected;
		}
		
		//-------------------------------------------------------------------------
		
		public function highlight ( ):void
		{
			// not implemented
			addChild(_highlightDisplay);
			
		}
		
		//-------------------------------------------------------------------------
		
		public function reset ( ):void
		{
			// not implemented yet
			if ( contains(_highlightDisplay) ) removeChild(_highlightDisplay);
		}
		
		//-------------------------------------------------------------------------
		
		/**
		 * Sets the checkbox's mandatory state.
		* 
		* @param	iM:Boolean	Value indicating wheter the
		* 			checkbox is mandatory or not.
		 */
		public function set isMandatory ( iM:Boolean ):void
		{
			if ( _isMandatory == iM ) return;
			if ( iM && !_isSelected )
			{
				dispatchValidationEvent();
			}
			_isMandatory = iM;
		}
		
		//-------------------------------------------------------------------------
		
		/**
		 * 
		 * @return Boolean indicating wheter the checkbox is mandatory.
		 */
		public function get isMandatory ( ):Boolean
		{
			return _isMandatory;
		}
		
		//-------------------------------------------------------------------------
		
		public function set isSelected ( selected:Boolean ):void
		{
			if ( _isSelected == selected ) return;
			_isSelected = selected;
			updateDisplay();
			if ( isMandatory ) dispatchValidationEvent();
		}
		
		//-------------------------------------------------------------------------
		
		public function get isSelected ( ):Boolean
		{
			return _isSelected;
		}
		
		//-------------------------------------------------------------------------
		
		protected function dispatchValidationEvent ( ):void
		{
			dispatchEvent(new ValidationEvent(ValidationEvent.CHANGE, true, false, _isSelected));
		}
		
		//-------------------------------------------------------------------------
		
		protected function toggleState ( e:MouseEvent ):void 
		{
			_isSelected = !_isSelected;
			updateDisplay();
			if ( isMandatory ) dispatchValidationEvent();
		}
		
		//-------------------------------------------------------------------------
		
		protected function updateDisplay ( ):void
		{
			if ( contains(_highlightDisplay) ) removeChild(_highlightDisplay);
			if ( _isSelected )
			{
				if ( contains(_deselectedDisplay) ) removeChild(_deselectedDisplay);
				addChild(_selectedDisplay);
			}
			else {
				if ( contains(_selectedDisplay) ) removeChild(_selectedDisplay);
				addChild(_deselectedDisplay);
			}
		}
		
	}
	
}