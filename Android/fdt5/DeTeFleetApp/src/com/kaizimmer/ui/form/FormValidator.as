﻿/*
* The MIT License
* 
* Copyright (c) 2010 Kai Zimmer [http://www.kaizimmer.com]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
package com.kaizimmer.ui.form
{
	import com.kaizimmer.events.ValidationEvent;
	import com.kaizimmer.ui.form.IFormObject;
	import flash.events.EventDispatcher;
	
	/**
	 * Dispatches com.kaizimmer.events.ValidationEvent(s) whenever the validity
	 * of the form changes.
	 * @author kzimmer aka engimono aka mxskout - www.kaizimmer.com
	 */
	public class FormValidator extends EventDispatcher
	{
		
		protected var _isFormValid:Boolean;
		protected var formObjects:Array;
		
		//-------------------------------------------------------------------------
		
		/**
		 * Constructor
		 */
		public function FormValidator ( )
		{
			_isFormValid = true;
			formObjects = new Array();
		}
		
		//-------------------------------------------------------------------------
		
		/**
		 * Registers IFormObject objects.
		 * @param	o:IFormObject The form object to register.
		 */
		public function addFormObject ( o:IFormObject ):void
		{
			formObjects.push(o);
			o.addEventListener(ValidationEvent.CHANGE, onValidityChange);
			_isFormValid = validateForm();
		}
		
		//-------------------------------------------------------------------------
		
		/**
		 * Unregisters IFormObject objects.
		 * @param	o:IFormObject The form object to unregister.
		 */
		public function removeFormObject ( o:IFormObject ):void
		{
			for ( var i:uint = 0; i < formObjects.length; i++ )
			{
				if ( formObjects[i] == 0 )
				{
					IFormObject(formObjects.splice(i, 1)[0]).
						removeEventListener(ValidationEvent.CHANGE, onValidityChange);
				}
			}
			_isFormValid = validateForm();
		}
		
		//-------------------------------------------------------------------------
		
		/**
		 * The form's current validity.
		 */
		public function get isFormValid ( ):Boolean
		{
			return validateForm();
		}
		
		//-------------------------------------------------------------------------
		
		/**
		 * Returns Array containing all invalid IFormObject instances
		 */
		public function get invalidFormObjects ( ):Array
		{
			var invalids:Array = new Array();
			for ( var i:uint = 0; i < formObjects.length; i++ )
			{
				if ( !IFormObject(formObjects[i]).validate() )
				{
					invalids.push(formObjects[i]);
				}
			}
			return invalids;
		}
		
		//-------------------------------------------------------------------------
		
		/**
		 * Prepares FormValidator object for garbage collection.
		 */
		public function destroy ( ):void
		{
			while ( formObjects.length > 0 )
			{
				removeFormObject(IFormObject(formObjects.shift()));
			}
		}
		
		//-------------------------------------------------------------------------
		
		protected function onValidityChange ( e:ValidationEvent ):void
		{
			if ( !e.isValid )
			{
				if ( _isFormValid )
				{
					dispatchEvent(new ValidationEvent(ValidationEvent.CHANGE, true, false, false));
					_isFormValid = false;
				}
			}
			else if ( validateForm() )
			{
				if ( !_isFormValid )
				{
					dispatchEvent(new ValidationEvent(ValidationEvent.CHANGE, true, false, true));
					_isFormValid = true;
				}
			}
		}
		
		//-------------------------------------------------------------------------
		
		protected function validateForm ( ):Boolean
		{
			for ( var i:uint = 0; i < formObjects.length; i++ )
			{
				if ( !IFormObject(formObjects[i]).validate() )
				{
					return false;
				}
			}
			return true;
		}
		
	}
	
}