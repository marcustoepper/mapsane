﻿/*
* The MIT License
* 
* Copyright (c) 2010 Kai Zimmer [http://www.kaizimmer.com]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
package com.kaizimmer.ui.form
{
	import com.kaizimmer.ui.form.IFormObject;
	import com.kaizimmer.ui.form.AbstractRadioButton;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	
	/**
	 * ...
	 * @author kzimmer aka engimono aka mxskout - www.kaizimmer.com
	 */
	public class RadioButtonGroup extends EventDispatcher implements IFormObject
	{
		protected var selectedRadioBtn:AbstractRadioButton;
		protected var radioButtons:Array;
		protected var _name:String;
		
		//-------------------------------------------------------------------------
		
		public function RadioButtonGroup ( groupName:String )
		{
			_name = groupName;
			radioButtons = new Array();
		}
		
		//-------------------------------------------------------------------------
		
		/**
		 * RadioButtonGroups are always valid
		 */
		public function validate ( ):Boolean
		{
			return true;
		}
		
		//-------------------------------------------------------------------------
		
		/**
		 * Does nothing
		 */
		public function highlight ( ):void
		{
			//
		}
		
		//-------------------------------------------------------------------------
		
		/**
		* Resets the RadioButtonGroup
		*/
		public function reset ( ):void
		{
			//
		}
		
		//-------------------------------------------------------------------------
		
		/**
		 * Registers AbstractRadioButton objects.
		 * @param	o:AbstractRadioButton The AbstractRadioButton object to register.
		 */
		public function addRadioButton ( o:AbstractRadioButton, isSelected:Boolean = false ):void
		{
			radioButtons.push(o);
			o.addEventListener(MouseEvent.CLICK, handleRadioBtnClick, true);
			if ( isSelected )
			{
				deselectAllButtons();
				selectRadioButton(o);
			}
		}
		
		//-------------------------------------------------------------------------
		
		/**
		 * Unregisters AbstractRadioButton objects.
		 * @param	o:AbstractRadioButton The AbstractRadioButton object to unregister.
		 */
		public function removeRadioButton ( o:AbstractRadioButton ):void
		{
			for ( var i:uint = 0; i < radioButtons.length; i++ )
			{
				if ( radioButtons[i] == o )
				{
					AbstractRadioButton(radioButtons.splice(i, 1)[0]).
						removeEventListener(MouseEvent.CLICK, handleRadioBtnClick);
					break;
				}
			}
			if ( radioButtons.length > 0 ) selectRadioButton(radioButtons[0]);
		}
		
		//-------------------------------------------------------------------------
		
		public function selectRadioButton ( btn:AbstractRadioButton ):void
		{
			if ( !isButtonRegistered(btn) ) return;
			deselectAllButtons();
			selectedRadioBtn = AbstractRadioButton(btn);
			btn.isSelected = true;
		}
		
		//-------------------------------------------------------------------------
		
		public function getSelectedRadioButton ( ):AbstractRadioButton
		{
			return selectedRadioBtn;
		}
		
		//-------------------------------------------------------------------------
		
		public function getSelectedData ( ):Object
		{
			return selectedRadioBtn.data;
		}
		
		//-------------------------------------------------------------------------
		
		public function get name ( ):String { return _name; }
		
		//-------------------------------------------------------------------------
		
		public function set name ( value:String ):void 
		{
			_name = value;
		}
		
		//-------------------------------------------------------------------------
		
		protected function isButtonRegistered ( btn:AbstractRadioButton ):Boolean
		{
			for ( var i:Number = 0; i < radioButtons.length; i++ )
			{
				if ( btn == radioButtons[i] ) return true;
			}
			return false;
		}
		
		//-------------------------------------------------------------------------
		
		protected function deselectAllButtons ( ):void
		{
			for ( var i:Number = 0; i < radioButtons.length; i++ )
			{
				AbstractRadioButton(radioButtons[i]).isSelected = false;
			}
		}
		
		//-------------------------------------------------------------------------
		
		protected function handleRadioBtnClick ( e:MouseEvent ):void 
		{
			deselectAllButtons();
			var btn:AbstractRadioButton = AbstractRadioButton(e.currentTarget);
			if ( btn != selectedRadioBtn ) 
			{
				dispatchEvent(new Event(Event.CHANGE));
			}
			selectedRadioBtn = btn;
			selectedRadioBtn.isSelected = true;
		}
		
	}
	
}