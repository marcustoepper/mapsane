﻿/*
* The MIT License
* 
* Copyright (c) 2010 Kai Zimmer [http://www.kaizimmer.com]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
package com.kaizimmer.ui.form
{
	import com.kaizimmer.ui.form.IFormObject;

	/**
	 * ...
	 * @author kzimmer aka engimono aka mxskout - www.kaizimmer.com
	 */
	public interface ICheckBox extends IFormObject
	{
		/**
		 * Sets the checkbox's mandatory state.
		* 
		* @param	iM:Boolean	Value indicating wheter the
		* 			checkbox is mandatory or not.
		 */
		function set isMandatory ( iM:Boolean ):void;
		/**
		 * 
		 * @return Boolean indicating wheter the checkbox is mandatory.
		 */
		function get isMandatory ( ):Boolean;
		/**
		 * Sets checkbox to active state
		 */
		function set isSelected ( selected:Boolean ):void;
		/**
		 * 
		 * @return Boolean indicating wheter the checkbox is active or not.
		 */
		function get isSelected ( ):Boolean;
	}
}
