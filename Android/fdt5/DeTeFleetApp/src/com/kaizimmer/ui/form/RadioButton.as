﻿/*
* The MIT License
* 
* Copyright (c) 2010 Kai Zimmer [http://www.kaizimmer.com]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
package com.kaizimmer.ui.form
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import com.kaizimmer.ui.form.AbstractRadioButton;
	
	/**
	 * ...
	 * @author kzimmer aka engimono aka mxskout - www.kaizimmer.com
	 */
	public class RadioButton extends AbstractRadioButton
	{
		
		protected var _selectedDisplay:DisplayObject;
		protected var _deselectedDisplay:DisplayObject;
		
		//-------------------------------------------------------------------------
		
		public function RadioButton ( selectedDisplay:DisplayObject,
										deselectedDisplay:DisplayObject,
										data:Object )
		{
			_selectedDisplay = selectedDisplay;
			_deselectedDisplay = deselectedDisplay;
			// traverse displays to this sprite
			addChild(_selectedDisplay);
			addChild(_deselectedDisplay);
			super(data);
		}
		
		//-------------------------------------------------------------------------
		
		override protected function renderSelectedDisplay ( ):void
		{
			if ( contains(_deselectedDisplay) ) removeChild(_deselectedDisplay);
			addChild(_selectedDisplay);
		}
		
		//-------------------------------------------------------------------------
		
		override protected function renderDeselectedDisplay ( ):void
		{
			if ( contains(_selectedDisplay) ) removeChild(_selectedDisplay);
			addChild(_deselectedDisplay);
		}
		
	}
	
}