/*
* The MIT License
* 
* Copyright (c) 2010 Kai Zimmer [http://www.kaizimmer.com]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
package com.kaizimmer.ui.form
{
	import com.kaizimmer.events.ValidationEvent;
	import com.kaizimmer.ui.form.FormFieldType;
	import com.kaizimmer.ui.form.IFormObject;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.FocusEvent;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	
	import de.aoemedia.mobile.ui.components.forms.ITextField;
	
	
	/**
	 * ...
	 * @author kzimmer aka engimono aka mxskout - www.kaizimmer.com
	 * 
	 * setTextFormat note:
	 * This method will not work if a style sheet is applied to the text field.
	 * 
	 */
	public class FormField extends EventDispatcher implements IFormObject
	{
		
		protected var _textField:ITextField;
		
		// The typeID of this inputfield.
		protected var contentType:uint;
		
		// The default text for the corresponding textfield.
		protected var _defaultText:String;
		
		// Flag indicating if this inputfield is mandatory.
		protected var _isMandatory:Boolean;
		
		// The min. number of tokens in this inputfield.
		protected var minLength:uint;
		// The max. number of tokens in this inputfield.
		protected var maxLength:Number;
		
		// The TextFormat of the corresponding textfield.
		protected var _defaultFormat:TextFormat;
		// The error TextFormat of the corresponding textfield
		protected var _errorFormat:TextFormat;
		
		// Tag indicating field as password field
		protected var isPassword:Boolean = false;
		
		// Flag indicating if the current form field state is valid.
		protected var _isValid:Boolean = true;
		
		// Flag indicating whether the formfield currently has focus
		protected var hasFocus:Boolean = false;
		
		// Flag indicating wheter formfield validates user input
		// on change and on focus_out
		protected var _liveChecking:Boolean = true;
		
		
		
		
		//-------------------------------------------------------------------------
		
		// Constructor
		
		//-------------------------------------------------------------------------
		
		/**
		 * 
		 */
		public function FormField ( textField:ITextField,
									defaultText:String,
									isMandatory:Boolean,
									contentType:uint = FormFieldType.ALL,
									minLen:uint = 0,
									maxLen:Number = Infinity )
		{
			_textField = textField;
			// Set the FormField instance's typeID to input.
			readOnly = false;
			// Set defaultText prop and default text.
			_defaultText = defaultText;
			_textField.text = defaultText;
			// Retrieve the corresponding default TextFormat.
			defaultFormat = _textField.getTextFormat();
			// Set the corresponding textfields error TextFormat.
			var ef:TextFormat = _textField.getTextFormat();
			//ef.color = 0xff0000;
			errorFormat = ef;
			// Set _isMandatory property.
			this._isMandatory = isMandatory;
			// Set contentType.
			setContentType(contentType);
			// Set this inputfields lengths.
			setLengths(minLen, maxLen);
			// 
			_textField.addEventListener(Event.CHANGE, onChange);
			_textField.addEventListener(FocusEvent.FOCUS_IN, onFocusIn);
			_textField.addEventListener(FocusEvent.FOCUS_OUT, onFocusOut);
		}
		
		
		
		
		//-------------------------------------------------------------------------
		
		// properties
		
		//-------------------------------------------------------------------------
		
		/**

		/**
		 * Set the formfield's liveChecking property. If set to true, liveChecking
		 * is performed (i.e. the formfield checks it's content's validity on
		 * change and focus out events).
		 * 
		 * @param	lc:Boolean	Bool. indicating wheter to use liveChecking or not.
		 */
		public function set liveChecking ( lc:Boolean ):void
		{
			_liveChecking = lc;
			if (lc) {
				update();
			}
			else {
				// ...liveChecking is off, so normalise text in order to prevent
				// user confusion with text still displayed as erroneous even when
				// corrected
				applyFormat(defaultFormat);
			}
		}
		
		/**
		 * Returns the formfield's default text.
		 */
		public function get liveChecking ( ):Boolean
		{
			return _liveChecking;
		}
		
		//-------------------------------------------------------------------------
		
		/**

		/**
		 * @param	txt:String	Sets the formfield's default text.
		 */
		public function set defaultText ( txt:String ):void
		{
			// if current text is old default text...
			if (_textField.text == _defaultText) {
				// ...update text
				_textField.text = txt;
			}
			// store new default text
			_defaultText = txt;
		}
		
		/**
		 * Returns the formfield's default text.
		 */
		public function get defaultText ( ):String
		{
			return _defaultText;
		}
		
		//-------------------------------------------------------------------------
		
		/**
		* Sets the inputfield's read only state.
		* 
		* @param	ro:Boolean	The value to set the formfield's read only state to.
		*/
		public function set readOnly ( ro:Boolean ):void
		{
			//
			_textField.selectable = !ro;
			_textField.type = ro == true ? TextFieldType.DYNAMIC : TextFieldType.INPUT;
		}
		
		/**
		* Returns the inputfield's read only state.
		* 
		* @return The inputfield's read only state.
		*/
		public function get readOnly ( ):Boolean
		{
			return _textField.type != TextFieldType.INPUT;
		}
		
		//-------------------------------------------------------------------------
		
		/**
		 * Sets the inputfield's mandatory state.
		* 
		* @param	iM:Boolean	Value indicating wheter the
		* 			formfield is mandatory or not.
		 */
		public function set isMandatory ( iM:Boolean ):void
		{
			_isMandatory = iM;
			// If text is the default or empty an string...
			if (_textField.text == _defaultText || _textField.text == "") {
				// ... don't do anything more here and quit.
				return;
			}
			// ...else check txt via invoking update
			update();
		}
		
		/**
		 * 
		 * @return Boolean indicating wheter the formfield is mandatory.
		 */
		public function get isMandatory ( ):Boolean
		{
			return _isMandatory;
		}

		//-------------------------------------------------------------------------
		
		/**
		* Stores the TextFormat. That format is required in order to be able to
		* normalise the textfield.
		* If the current input is valid the new format is applied directly.
		* 
		* @param tf:TextFormat  TextFormat for the formfield's default state.
		*/
		public function set defaultFormat ( tf:TextFormat ):void
		{
			_defaultFormat = tf;
			// if current input is valid...
			if (_isValid) {
				// ...apply format directly
				applyFormat(_defaultFormat);
			}
		}
		
		/**
		 * Returns the formfield's corresponding defaultFormat TextFormat
		 */
		public function get defaultFormat ( ):TextFormat
		{
			return _defaultFormat;
		}

		//-------------------------------------------------------------------------
		
		/**
		* Sets the error TextFormat. That format is required in order to be able to
		* highlight the textfield.
		* If current input is valid only the errorFormat value changes.
		* If current input is erroneous the new error format is applied directly.
		* 
		* @param tf:TextFormat  TextFormat for the formfield's error state.
		*/
		public function set errorFormat ( tf:TextFormat ):void
		{
			_errorFormat = tf;
			// if current input is erroneous...
			if (!_isValid) {
				// ...apply error format directly
				applyFormat(_errorFormat);
			}
		}
		
		/**
		 * Returns the formfield's corresponding errorFormat TextFormat
		 */
		public function get errorFormat ( ):TextFormat
		{
			return _errorFormat;
		}
		
		
		//-------------------------------------------------------------------------
		
		/**
		 * Returns corresponding TextField instance (Read only)
		 */
		public function get textField():ITextField { return _textField; }
		
		
		
		
		//-------------------------------------------------------------------------
		
		// publics
		
		//-------------------------------------------------------------------------
  		
		/**
		 * Returns boolean indicating wheter the formfield content is valid or not.
		 * @return	Boolean indicating wheter the formfield content is valid or not.
		 */
		public function validate ( ):Boolean
		{	
			// read only fields are always valid
			if (readOnly) {
				return true;
			}
			
			// If field is mandatory check validness
			if (_isMandatory) {
				var currLength:uint = _textField.length;
				if (currLength == 0 ||
					currLength < minLength ||
					currLength > maxLength ||
					_textField.text == _defaultText) {
					//trace("Error: wrong length or default text");
					return false;
				}
				else if (contentType == FormFieldType.EMAIL && !checkIfEMail()) {
					//trace("Error: bad email");
					return false;
				}
			}
			// If field is not mandatory return true. Furthermore there'll
			// be never a reason to modify the field format to visualise an error.
			return true;
		}

		//-------------------------------------------------------------------------
		
		/**
		 * Applies the formfield's corresponding errorFormat TextFormat
		 */
		public function highlight ( ):void
		{
			applyFormat(_errorFormat);
		}
		
		/**
		 * Applies the formfield's corresponding defaultFormat TextFormat
		 */
		public function normalize ( ):void
		{
			applyFormat(_defaultFormat);
		}
		
		//-------------------------------------------------------------------------
		
		/**
		* Sets the text to the formfield's default text and sets it's format to
		* default state.
		* Exception: If the formfield has currently focus, it's text gets deleted.
		*/
		public function reset ( ):void
		{
			applyFormat(_defaultFormat);
			
			// If formfield has focus...
			if (hasFocus) {
				// ...remove potential bad text...
				_textField.text = "";
				if ( isPassword ) _textField.displayAsPassword = true;
			}
			else {
				// ...else write default text
				_textField.text = _defaultText;
				if ( isPassword ) _textField.displayAsPassword = false;
			}
			
			// Resest _isValid
			_isValid = true;
			// update 
			if ( _isMandatory ) update();
		}
		
		//-------------------------------------------------------------------------
		
		/**
		 * Sets the formfield's contentType to the passed uint and restricts
		 * the user input.
		 * Note: If current text is not the default text the formfield is resetted.
		 * 
		 * @param	type:uint	uint indicating the contentType.
		 */
		public function setContentType ( type:uint ):void
		{
			// Set contentType to passed value. An invalid value will be corrected
			// in the succeeding switch statement's default case.
			contentType = type;
			
			switch (type) {
				case FormFieldType.SERIAL:
					_textField.restrict = "A-Za-z0-9\\-";
					break;
				//case FormFieldType.URL:
					//restrict = "A-Za-z0-9�������\\-";
					//break;
				case FormFieldType.EMAIL:
					_textField.restrict = "A-Z a-z 0-9 ������� @._\\- ^ ";
					break;
				case FormFieldType.PASSWORD:
					_textField.restrict = null;
					isPassword = true;
					break;
				case FormFieldType.LETTERS:
					_textField.restrict = "A-Z a-z �������";
					break;
				case FormFieldType.NUMBERS:
					_textField.restrict = "0-9,.";
					break;
				case FormFieldType.INTEGER:
					_textField.restrict = "0-9";
					break;
				case FormFieldType.MIXED:
					_textField.restrict = "A-Z a-z 0-9 �������";
					break;
				case FormFieldType.ALL:
				default:
					contentType = FormFieldType.ALL;
					_textField.restrict = null;
			}
			//*/
			// If text is not the default text reset it
			if (_textField.text != _defaultText) {
				reset();
			}
			//*/
		}
		
		//-------------------------------------------------------------------------
		
		/**
		 * Set's the formfield's min. and max length. Second parameter (max) is
		 * otpional. If omitted there's no max length restriction for the field.
		 */
		public function setLengths ( min:uint, max:Number = Infinity ):void
		{			
			maxLength = Math.abs(max);
			// Set minimal required length.
			minLength = (min < maxLength) ? min : maxLength;
			
			// Set textfield's allowed max length.
			if (maxLength == Infinity) {
				_textField.maxChars = 0;
			}
			else {
				_textField.maxChars = maxLength;
			}
			
			// Check text if not empty or default.
			if (_textField.text != _defaultText && _textField.text != "") {
				update();
			}
		}
		
		
		
		
		//-------------------------------------------------------------------------
		
		// protecteds
		
		//-------------------------------------------------------------------------
		
		// TODO: refactor this method!
		/**
		 * 
		 * @return	Boolean indicating wheter text is valid url or not.
		 */
		/*/
		protected function checkIfURL ( ):Boolean
		{
			var t:String = text;

			var allowedASCII:String =
				"&'*+-./0123456789=?ABCDEFGHIJKLMNOPQRSTUVWXYZ^"
				+ "_abcdefghijklmnopqrstuvwxyz{}~"
				+ "@";
			for (var i:Number=t.length; --i>=0;) {
				if (allowedASCII.indexOf(t.substr(i,1)) == -1) {
					return false;
				}
			}

			var subStrs:Array = t.split("@");
			var a:Array;
			
			if (subStrs.length != 2 ||
				subStrs[0].length < 1) {
				//trace("subStrs.length != 2 | subStrs: " + subStrs);
				return false;
			}
			a = subStrs[1].split(".");
			if (a.length < 2) {
				return false;
			}
			if (a.length == 2) {
				if (a[0].length < 3 ||
					a[1].length < 2 ||
					a[1].indexOf("-") != -1 ||
					a[1].indexOf("_") != -1 ) {
					return false;
				}
			}
			if (a.length > 2) {
				if (a[a.length-3].length < 3 ||
					a[a.length-2].length < 2 ||
					a[a.length-1].length < 2 ||
					a[a.length-1].indexOf("-") != -1 ||
					a[a.length-1].indexOf("_") != -1 ) {
					return false;
				}
			}
			return true;
		}
		//*/
		
		//-------------------------------------------------------------------------
		
		// TODO: refactor this method!
		/**
		 * 
		 * @return	Boolean indicating wheter text is valid email or not.
		 */
		protected function checkIfEMail ( ):Boolean
		{
			var t:String = _textField.text;

			var allowedASCII:String =
				"&'*+-./0123456789=?ABCDEFGHIJKLMNOPQRSTUVWXYZ^"
				+ "_aäöüßÄÖÜßbcdefghijklmnopqrstuvwxyz{}~"
				+ "@";
			for (var i:Number=t.length; --i>=0;) {
				if (allowedASCII.indexOf(t.substr(i,1)) == -1) {
					return false;
				}
			}

			var subStrs:Array = t.split("@");
			var a:Array;
			/*/
			var domain:String;
			var topLevel:String;
			//*/
			if (subStrs.length != 2 ||
				subStrs[0].length < 1) {
				//trace("subStrs.length != 2 | subStrs: " + subStrs);
				return false;
			}
			a = subStrs[1].split(".");
			if (a.length < 2) {
				return false;
			}
			if (a.length == 2) {
				//if (a[0].length < 3 ||
				if (a[0].length < 2 ||
					a[1].length < 2 ||
					a[1].indexOf("-") != -1 ||
					a[1].indexOf("_") != -1 ) {
					return false;
				}
			}
			if (a.length > 2) {
				//if (a[a.length-3].length < 3 ||
				if (a[a.length-3].length < 2 ||
					a[a.length-2].length < 2 ||
					a[a.length-1].length < 2 ||
					a[a.length-1].indexOf("-") != -1 ||
					a[a.length-1].indexOf("_") != -1 ) {
					return false;
				}
			}
			return true;
		}
		
		//-------------------------------------------------------------------------
		
		/**
		 * Checks if the formfield's validity state changed. (Via invoking the
		 * validate method). If so it changes the applied TextFormat.
		 * (i.e. invoking applyFormat, passing in the respective TextFormat object)
		 */
		protected function update ( ):void
		{
			// suspend this method as long as liveChecking is false
			if (!liveChecking) {
				return;
			}
			
			// Store previous textfield _isValid state
			var wasValid:Boolean = _isValid;
			_isValid = validate();
			// modify field format only if neccessary
			if (_isValid != wasValid)
			{
				// State has changed so update the field's format...
				updateFieldFormat();
				// ... and dispatch the corresponding event
				dispatchEvent(new ValidationEvent(ValidationEvent.CHANGE, true, false, _isValid));
			}
		}

		//-------------------------------------------------------------------------
		
		/**
		 * Changes the formfield's TextFormat according to it's current validity.
		 * (i.e. invoking applyFormat, passing in the respective TextFormat object)
		 */
		protected function updateFieldFormat ( ):void
		{
			// State has changed so...
			switch (_isValid)
			{
				case true:
					// ...normalise field
					applyFormat(_defaultFormat);
					break;
				case false:
					// ...highlight field
					applyFormat(_errorFormat);
					break;
			}
		}
		
		//-------------------------------------------------------------------------
		
		/**
		 * Applies the passed TextFormat object to the formfield.
		 * 
		 * @param	tf:TextFormat	TextFormat object to apply to the formfield.
		 */
		protected function applyFormat ( tf:TextFormat ):void
		{
			// set format for already existing text
			_textField.setTextFormat(tf);
			// set format for newly inserted text
			_textField.defaultTextFormat = tf;
		}
		
		
		
		
		//-------------------------------------------------------------------------
		
		// listeners
		
		//-------------------------------------------------------------------------

		/**
		* Validates the textfield's content when changing.
		* 
		* @param	e:Event  Reference to corresponding Event.CHANGE event.
		*/
		protected function onChange ( e:Event ):void
		{
			update();
		}

		//-------------------------------------------------------------------------

		/**
		* Removes default text from textfield when receiving focus in order to
		* enhance user convenience.
		* 
		* @param	e:FocusEvent Reference to corresponding
		* 			FocusEvent.FOCUS_IN event.
		*/
		//protected function onSetFocus ( oldFocus:Object ):void
		protected function onFocusIn ( e:FocusEvent ):void
		{
			hasFocus = true;
			
			// Don't manipulate read only fields
			if (readOnly) {
				return;
			}
			
			if (_textField.text == _defaultText) {
				_textField.text = "";
			}
			if (isPassword) {
				_textField.displayAsPassword = true;
			}
		}

		//-------------------------------------------------------------------------

		/**
		* Sets default text when focus is leaving an empty textfield.
		* 
		* @param	e:FocusEvent Reference to corresponding
		* 			FocusEvent.FOCUS_OUT event.
		*/
		//protected function onKillFocus ( newFocus:Object ):void
		protected function onFocusOut ( e:FocusEvent ):void
		{
			hasFocus = false;
			
			// Don't care about read only fields
			if (readOnly) {
				return;
			}
			
			switch (_textField.text) {
				case "":
					_textField.text = _defaultText;
				case _defaultText:
					if (isPassword) {
						// make password default text visible
						_textField.displayAsPassword = false;
					}
			}
			update();
		}
		
	}
	
}