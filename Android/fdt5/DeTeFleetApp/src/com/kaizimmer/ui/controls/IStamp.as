package com.kaizimmer.ui.controls
{

/**
 * @author Daniel Kopp <daniel.kopp@aoemedia.de>
 */
public interface IStamp
{
	function set data(data:Object):void;
	function get data():Object;

}
}
