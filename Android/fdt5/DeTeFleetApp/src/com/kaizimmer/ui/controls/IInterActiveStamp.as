package com.kaizimmer.ui.controls
{
	public interface IInterActiveStamp
	{
		function renderDefaultState ( ):void;
		function renderDownState ( ):void;
		function renderSelectedState ( ):void;
	}
}