/*
* The MIT License
* 
* Copyright (c) 2012 Kai Zimmer [http://www.kaizimmer.com]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
package com.kaizimmer.ui.controls
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	
	public class ElasticInteractiveStampList extends InteractiveStampList
	{
		protected var isSpringForceActive:Boolean;
		
		/**
		 * ElasticInteractiveStampList constructor
		 * @author kai.zimmer aka engimono
		 */
		public function ElasticInteractiveStampList ( viewWidth:Number, 
													  viewHeight:Number, 
													  maxStampsToRender:uint, 
													  stamp:DisplayObject, 
													  scaleFactor:Number = 1 )
		{
			super(viewWidth, viewHeight, maxStampsToRender, stamp, scaleFactor);
		}
		
		final override protected function updateRenderRectangle ( event:Event ):void
		{
			//trace("updateRenderRectangle vY: " + vY);
			event.preventDefault();
			event.stopImmediatePropagation();
			
			const FRICTION:Number = .95;
			const LIST_MASS:Number = 200;
			const SPRING_AMOUNT:Number = 5;//.95;
			
			if ( isDragging )
			{
				vY = mouseY - offset;
				
				offsetHistory.shift();
				offsetHistory.push(vY);
				
				offset = mouseY;
				targetY = _renderRect.y - vY;
				_renderRect.y = targetY;
			}
			else {
				_renderRect.y -= vY;
				vY *= FRICTION;
				//*/
				if ( vY > -.01 && vY < .01 )
				{
					vY = 0;
					scrollBarNeedsRendering = false;
					if ( !isSpringForceActive )
					{
						removeEventListener(Event.ENTER_FRAME, updateRenderRectangle);
					}
				}
				//*/
			}
			
			
			//trace("_renderRectBoundY: " + _renderRectBoundY);
			//trace("_renderRect.y: " + _renderRect.y);
			
			var dy:Number = 0;
			var ay:Number = 0;
			if ( _renderRect.y < 0 )
			{
				isSpringForceActive = true;
				dy = _renderRect.y; // neg value
				ay = dy * SPRING_AMOUNT;
				vY += ay / LIST_MASS;
				if ( _renderRect.y - vY >= 0 )
				{
					vY = 0;
					_renderRect.y = 0; // make sure we hit the exact point 
				}
				/*/
				trace("dy: " + dy);
				trace("ay: " + ay);
				trace("vy: " + vY);
				trace("--- ");
				//*/
				//_renderRect.y += vY;
				clearAndRender();
			}
			else if ( _renderRect.y > _renderRectBoundY )
			{
				isSpringForceActive = true;
				dy = _renderRect.y - _renderRectBoundY; // pos value
				ay = dy * SPRING_AMOUNT;
				vY += ay / LIST_MASS;
				if ( _renderRect.y - vY <= _renderRectBoundY )
				{
					vY = 0;
					_renderRect.y = _renderRectBoundY; // make sure we hit the exact point 
				}
				/*/
				trace("dy: " + dy);
				trace("ay: " + ay);
				trace("vy: " + vY);
				trace("--- ");
				//*/
				//_renderRect.y += vY;
				clearAndRender();
			}
			else {
				isSpringForceActive = false;
				render();
			}
			
			//trace("updateRenderRectangle | dy: " + dy + " | ay: " + ay + " | vy: " + vY);
			
			/*/
			if ( _showScrollBar )
			{
				var scrollDist:Number = _renderRect.height - 2*scrollBarPaddingV - scrollRect.height;
				var scrollPerc:Number = _renderRect.y / _renderRectBoundY;
				scrollRect.y = scrollBarPaddingV + scrollDist * scrollPerc;
			}
			//*/
		}
		
	}
}