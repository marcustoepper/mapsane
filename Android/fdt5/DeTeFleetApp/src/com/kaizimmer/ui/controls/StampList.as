/*
* The MIT License
* 
* Copyright (c) 2012 Kai Zimmer [http://www.kaizimmer.com]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
package com.kaizimmer.ui.controls
{
import org.as3commons.logging.api.ILogger;
import org.as3commons.logging.api.getLogger;
import org.osflash.signals.Signal;

import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.DisplayObject;
import flash.display.Sprite;
import flash.display.Stage;
import flash.display.StageQuality;
import flash.geom.Matrix;
import flash.geom.Point;
import flash.geom.Rectangle;
import flash.utils.setTimeout;
	
	public class StampList extends Sprite
	{
		
	private static const logger:ILogger = getLogger(StampList);
	
		protected static var _stage:Stage;
		protected static var _drawQuality:String;
		
		protected var scaleFactor:Number;
		protected var _stamp:DisplayObject;
		protected var _stampMatrix:Matrix;
		protected var _stampRect:Rectangle;
		protected var _renderRect:Rectangle;
		protected var _renderPoint:Point;
		protected var _renderRectBoundY:Number;
		protected var _itemDescriptions:Vector.<Object>;
		protected var _maxStamps:Number;
		protected var _viewCanvas:BitmapData;
		protected var _emptyCanvas:BitmapData;
		protected var _itemsCanvas:BitmapData;
		
		public var signalItemsAdded:Signal;
		
		/**
		 * StampList constructor
		 * @author k.zimmer aka engimono
		 */
		public function StampList ( viewWidth:Number, 
									viewHeight:Number, 
									maxStampsToRender:uint, 
									stamp:DisplayObject,
									scaleFactor:Number = 1 )
		{
			if(!(stamp is IStamp)){
				throw new Error("stamp has to be of Type IStamp");
			}
			
			signalItemsAdded = new Signal();
			
			this.scaleFactor = scaleFactor;
			_itemDescriptions = new <Object>[];
			_maxStamps = maxStampsToRender;
			_stamp = stamp;
			_stampMatrix = new Matrix(); // create identity matrix for translate operations
			
			// TODO: compare performance of succeeding 2 statements:
			//_stampRect = new Rectangle(0,0, _stamp.width, _stamp.height);
			_stampRect = _stamp.getBounds(_stamp);
			
			_renderRect = new Rectangle(0, 0, viewWidth, viewHeight);
			_renderPoint = new Point();
			
			_viewCanvas = new BitmapData(viewWidth, viewHeight, true, 0x0);
			_emptyCanvas = new BitmapData(viewWidth, viewHeight, true, 0x0);
			_itemsCanvas = new BitmapData(viewWidth, maxStampsToRender * _stampRect.height, true, 0x0);
			addChild(new Bitmap(_viewCanvas));
		}
		
		public function get stamp ( ):DisplayObject
		{
			return _stamp;
		}

		public static function setDrawQuality ( stageQuality:String, stage:Stage ):void
		{
			if ( stage == null )
			{
				throw new Error("StampList setDrawQuality() | stage argument is null");
			}
			_stage = stage;
			switch ( stageQuality )
			{
				case StageQuality.BEST:
				case StageQuality.HIGH:
				case StageQuality.HIGH_16X16:
				case StageQuality.HIGH_16X16_LINEAR:
				case StageQuality.HIGH_8X8:
				case StageQuality.HIGH_8X8_LINEAR:
				case StageQuality.MEDIUM:
				case StageQuality.LOW:
					_drawQuality = stageQuality;
					break;
				default:
					_drawQuality = null;
			}
		}
		
		// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		//TODO: COMPARE PERFORMANCE OF setting stage quality + draw vs drawWithQuality
		// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		public function addItems ( itemDescriptions:Vector.<Object>, renderWhenReady:Boolean = false ):void
		{
			//TODO: PERFORMANCE PROFILING
			if ( _itemDescriptions.length + itemDescriptions.length > _maxStamps )
			{
				throw new Error("StampList addItems() | Current items + items to add > max items");
			}
			
			// lock canvas
			_itemsCanvas.lock();
			_viewCanvas.lock();
			
			// PERFORMANCE TIP COULD BE REVERTED (the list would need to be constructed from bottom to top!?!)
			for ( var i:int=0; i < itemDescriptions.length; i++ )
			{
				_itemDescriptions.push(itemDescriptions[i]);
				IStamp(_stamp).data = itemDescriptions[i];
				
				//updateStamp(itemDescriptions[i]); // must be implemented by concrete subclasses
				if ( _itemDescriptions.length > 1 ) 
				{
					_stampMatrix.translate(_stampRect.x, _stampRect.height);
				}
				//_itemsCanvas.draw(_stamp, _stampMatrix);
				_itemsCanvas.drawWithQuality(_stamp, _stampMatrix, null, null, null, false, StageQuality.HIGH);
				//_itemsCanvas.drawWithQuality(_stamp, _stampMatrix, null, null, null, false, _drawQuality);
				
				var totalStampsHeight:Number = _itemDescriptions.length * _stampRect.height;
				if ( totalStampsHeight > _renderRect.height )
				{
					_renderRectBoundY = totalStampsHeight - _renderRect.height;
				}
				else {
					//_renderRectBoundY = _renderRect.height;
					_renderRectBoundY = 0;
				}
			}
			// unlock canvas
			_itemsCanvas.unlock();
			_viewCanvas.unlock();
			
			signalItemsAdded.dispatch();
			
			if ( renderWhenReady ) render();
		}
		
		// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		//TODO: COMPARE PERFORMANCE OF setting stage quality + draw vs drawWithQuality
		// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		public function addItems_off ( itemDescriptions:Array, renderWhenReady:Boolean = false ):void
		{
			//TODO: PERFORMANCE PROFILING
			const DELAY_FOR_QUALITY_CHANGE:Number = 100;
			
			var changedQuality:Boolean = false;
			var currQuality:String;
			
			if ( _itemDescriptions.length + itemDescriptions.length > _maxStamps )
			{
				throw new Error("StampList addItems() | Current items + items to add > max items");
			}
			
			if ( _drawQuality != null && _stage != null && _drawQuality != _stage.quality )
			{
				currQuality = _stage.quality;
				_stage.quality = _drawQuality;
				changedQuality = true;
				setTimeout(_addItems, DELAY_FOR_QUALITY_CHANGE);
			}
			else {
				_addItems();
				signalItemsAdded.dispatch();
				if ( renderWhenReady ) render();
			}
			
			
			function _addItems ( ):void
			{
				// lock canvas
				_itemsCanvas.lock();
				_viewCanvas.lock();
				/*/
				//TODO: TRY INLINING META TAG TO GAIN PERFORMANCE
				for ( var i:int=0; i < itemDescriptions.length; i++ )
				{
				addItem(itemDescriptions[i]);
				}
				//*/
				
				// PERFORMANCE TIP COULD BE REVERTED (the list would need to be constructed from bottom to top!?!)
				for ( var i:int=0; i < itemDescriptions.length; i++ )
				{
					_itemDescriptions.push(itemDescriptions[i]);
					
					IStamp(_stamp).data = itemDescriptions[i];
					//updateStamp(itemDescriptions[i]); // must be implemented by concrete subclasses
					if ( _itemDescriptions.length > 1 ) 
					{
						_stampMatrix.translate(_stampRect.x, _stampRect.height);
					}
					_itemsCanvas.draw(_stamp, _stampMatrix);
					//_itemsCanvas.drawWithQuality(_stamp, _stampMatrix, null, null, null, false, StageQuality.HIGH);
					/*/
					var totalStampsHeight:Number = _itemDescriptions.length * _stampRect.height;
					if ( totalStampsHeight > _renderRect.height )
					{
						_renderRectBoundY = totalStampsHeight - _renderRect.height;
					}
					else {
						_renderRectBoundY = _renderRect.height;
					}
					//*/
				}
				updateRenderRectBounds();
				// unlock canvas
				_itemsCanvas.unlock();
				_viewCanvas.unlock();
				
				if ( changedQuality )
				{
					setTimeout(resetQuality, DELAY_FOR_QUALITY_CHANGE);
				}
				
			}
			
			function resetQuality ( ):void
			{
				_stage.quality = currQuality;
				signalItemsAdded.dispatch();
				if ( renderWhenReady ) render();
			}
			
			/*/
			//TODO: PERFORMANCE PROFILING
			// lock canvas
			_itemsCanvas.lock();
			_viewCanvas.lock();
			//TODO: TRY INLINING TO GAIN PERFORMANCE
			for ( var i:int=0; i < itemDescriptions.length; i++ )
			{
			addItem(itemDescriptions[i]);
			}
			// unlock canvas
			_itemsCanvas.unlock();
			_viewCanvas.unlock();
			//*/
		}
		
		/*/
		public function addItem ( itemDescription:* ):void
		{
			//var currStageQuality:String = stage.quality;
			//StageQuality.LOW;
			//stage.quality = StageQuality.MEDIUM;
			_itemDescriptions.push(itemDescription);
			updateStamp(); // must be implemented by concrete subclasses
			if ( _itemDescriptions.length > 1 ) 
			{
				_stampMatrix.translate(_stampRect.x, _stampRect.height);
			}
			//_itemsCanvas.draw(_stamp, _stampMatrix);
			_itemsCanvas.drawWithQuality(_stamp, _stampMatrix, null, null, null, false, StageQuality.HIGH);
			//stage.quality = StageQuality.LOW;
			_renderRectBoundY = _itemDescriptions.length * _stampRect.height - _renderRect.height;
			
			//_viewCanvas.copyPixels(_itemsCanvas, _renderRect, _renderPoint); // done in render() now
		}
		//*/
		
		public function render ( ):void
		{
			_viewCanvas.copyPixels(_itemsCanvas, _renderRect, _renderPoint);
			//_viewCanvas.unlock();
			//_viewCanvas.lock();
		}
		
		public function clearAndRender ( ):void
		{
			// !!! IMPORTANT NOTE: USING THE CLEAR RECTANGLE DROPS THE MINIM FPS FROM 37 TO 31 !!!
			_viewCanvas.lock();
			_viewCanvas.copyPixels(_emptyCanvas, _emptyCanvas.rect, _renderPoint);
			//_viewCanvas.fillRect(_itemsCanvas.rect, 0x00FFFFFF); //TODO: COMPARE FILL RECT PERFORMANCE WITH COPYPIXELS (EMPTYCANVAS)
			_viewCanvas.copyPixels(_itemsCanvas, _renderRect, _renderPoint);
			//_viewCanvas.copyPixels(_itemsCanvas, _renderRect, _renderPoint,null,null,true);
			_viewCanvas.unlock();
		}
		
		public function scrollY ( newY:Number ):void
		{
			// TODO: RESTRICT Y TO MAX Y AND MIN Y (0)
			_renderRect.y = newY;
		}
		
		public function tweenToY ( targetY:Number, time:Number = 1 ):void
		{
			//TweenLite.killTweensOf(_renderRect);
			//var tween:TweenLite = new TweenLite(_renderRect, time, { y: targetY, onUpdate:render });
			//var tween:TweenLite = new TweenLite(_renderRect, time, { y: targetY });
		}
		
		public function setViewHeight ( h:Number, render:Boolean = false ):void
		{
			if ( _renderRect.height == h ) return;
			_renderRect.height = h > _viewCanvas.height ? _viewCanvas.height : h;
			updateRenderRectBounds();
			if ( render ) clearAndRender();
		}
		public function getViewHeight ( ):Number
		{
			return _renderRect.height;
		}
		
		public function lockCanvas ( ):void
		{
			_viewCanvas.lock();
		}
		
		public function unlockCanvas ( ):void
		{
			_viewCanvas.unlock();
		}
		
		public function removeItems ( render:Boolean = false ):void
		{
			_itemDescriptions = new <Object>[];
			_itemsCanvas.fillRect(_itemsCanvas.rect, 0x00FFFFFF); //TODO: COMPARE FILL RECT PERFORMANCE WITH COPYPIXELS (EMPTYCANVAS)
			_stampMatrix.identity();
			_renderRect.y = 0;
			if ( render )
			{
				clearAndRender();
			}
		}
		
		public function setSize ( w:Number, h:Number ):void
		{
			//TODO: implement me
		}
		
		public function destroy ( ):void
		{
			_itemsCanvas.dispose();
			_viewCanvas.dispose();
		}
		
//		protected function updateStamp ( currDescription:* ):void
//		{
//			throw(new Error("PerformanceList updateStamp needs to be overriden by concrete subclass"));
//		}
		
		protected function updateRenderRectBounds ( ):void
		{
			var totalStampsHeight:Number = _itemDescriptions.length * _stampRect.height;
			if ( totalStampsHeight > _renderRect.height )
			{
				_renderRectBoundY = totalStampsHeight - _renderRect.height;
			}
			else {
				//_renderRectBoundY = _renderRect.height;
				_renderRectBoundY = 0;
			}
		}
		
	}
}
