/*
* The MIT License
* 
* Copyright (c) 2012 Kai Zimmer [http://www.kaizimmer.com]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
package com.kaizimmer.ui.controls
{
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.StageQuality;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import flash.utils.clearTimeout;
	import flash.utils.setTimeout;
	
	import org.osflash.signals.Signal;
	
	public class InteractiveStampList extends StampList
	{
		protected const TAP_DELAY:Number = 325;
		
		protected var tapTimeout:uint;
		protected var _selectedItem:uint;
		
		protected var isDragging:Boolean = false;
		protected var offset:Number = 0;
		
		protected var OFFSETS_NUM:uint = 3;
		protected var offsetHistory:Vector.<Number>;
		
		protected var targetY:Number = 0;
		protected var vY:Number = 0;
		
		public var signalItemSelected:Signal;
		
		protected var _itemsCanvasCopy:BitmapData;
		
		protected var _selectedStampMatrix:Matrix;
		
		protected var _showScrollBar:Boolean = false;
		protected var scrollBarNeedsRendering:Boolean = false;
		protected var _scrollRect:Rectangle;
		protected var scrollBarColor:Number;
		protected var scrollBarPaddingV:Number;
		protected var maxScrollBarLength:int;
		

		/**
		 * InteractiveStampList constructor
		 * @author k.zimmer aka engimono
		 */
		public function InteractiveStampList ( viewWidth:Number, 
												viewHeight:Number, 
												maxStampsToRender:uint, 
												stamp:DisplayObject, 
												scaleFactor:Number = 1 )
		{
			super(viewWidth, viewHeight, maxStampsToRender, stamp, scaleFactor);
			
			if(!(stamp is IInterActiveStamp)){
				throw new Error("stamp has to be of Type IInterActiveStamp");
			}
			
			
			init();
		}
		
		override public function render ( ):void
		{
			// to gain performance we duplicate the super.render coder here instead of invoking it
			_viewCanvas.copyPixels(_itemsCanvas, _renderRect, _renderPoint);
			
			if ( _showScrollBar && scrollBarNeedsRendering )
			{
				var scrollDist:Number = _renderRect.height - 2*scrollBarPaddingV - _scrollRect.height;
				var scrollPerc:Number = _renderRect.y / _renderRectBoundY;
				_scrollRect.y = scrollBarPaddingV + scrollDist * scrollPerc;
				_viewCanvas.fillRect(_scrollRect, scrollBarColor);
			}
			//_viewCanvas.unlock();
			//_viewCanvas.lock();
		}
		
		public function setScrollBar ( scrollbarWidth:Number, 
									   horizontalPadding:Number = 0, 
									   verticalPadding:Number = 0, 
									   argbColor:uint = 0xFF000000 ):void
		{
			var xPos:Number = _itemsCanvas.width - horizontalPadding - scrollbarWidth;
			//maxScrollBarLength = int(_renderRect.height * _renderRect.height/_itemsCanvas.height);
			updateScrollBarLength();
			_scrollRect = new Rectangle(xPos, 0, scrollbarWidth, maxScrollBarLength);
			scrollBarPaddingV = verticalPadding;
			scrollBarColor = argbColor;
		}
		
		public function showScrollBar ( ):void
		{
			if ( _scrollRect == null ) return;
			_showScrollBar = true;
		}
		
		public function hideScrollBar ( ):void
		{
			_showScrollBar = false;
		}
		
		override public function addItems ( itemDescriptions:Vector.<Object>, renderWhenReady:Boolean = false ):void
		{
			super.addItems(itemDescriptions, renderWhenReady);
			_itemsCanvasCopy.copyPixels(_itemsCanvas, _itemsCanvas.rect, _renderPoint);
			
			updateScrollBarLength();
		}
		
		public function removeSelctedItemsFromRendering ( ):void
		{
			_itemsCanvas.copyPixels(_itemsCanvasCopy, _itemsCanvas.rect, _renderPoint);
			//_itemsCanvas.copyPixels(_itemsCanvasCopy, _itemsCanvas.rect, _renderPoint, null, null, true);
		}
		
		public function getSelectedItem ( ):*
		{
			return _itemDescriptions[_selectedItem];
		}
		
		override public function setViewHeight ( h:Number, render:Boolean=false ):void
		{
			if ( _renderRect.height == h ) return;
			
			super.setViewHeight(h,render);
			
			if ( h < _viewCanvas.height )
			{
				updateScrollBarLength();
			}
		}
		
		override public function removeItems ( render:Boolean = false ):void
		{
			vY = 0;
			if ( _stamp is IInterActiveStamp )
			{
				IInterActiveStamp(_stamp).renderDefaultState();
			}
			super.removeItems(render);
		}
		
		override public function destroy ( ):void
		{
			removeEventListener(MouseEvent.MOUSE_MOVE, handleMouseMove);
			removeEventListener(Event.ENTER_FRAME, updateRenderRectangle);
			
			
			removeEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
			removeEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
			removeEventListener(MouseEvent.MOUSE_OUT, stopDragging);
			
			_emptyCanvas.dispose();
			super.destroy();
		}
		
		protected function init ( ):void
		{
			_selectedStampMatrix = new Matrix(); // create identity matrix for drawing the highlight stamp
			_itemsCanvasCopy = new BitmapData(_itemsCanvas.width, _itemsCanvas.height, true, 0x0);
			
			offsetHistory = new <Number>[];
			for ( var i:uint=0; i < OFFSETS_NUM; i++ )
			{
				offsetHistory.push(0);
			}
			
			addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
			addEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
			addEventListener(MouseEvent.MOUSE_OUT, stopDragging);
			
			signalItemSelected = new Signal(Object);
		}
		
		protected function killTapTimeout ( ):void
		{
			if ( tapTimeout == 0 ) return;
			clearTimeout(tapTimeout);
			tapTimeout = 0;
		}
			
		protected function handleMouseDown ( event:MouseEvent ):void
		{
			//trace("handleMouseDown ");
			event.preventDefault();
			event.stopImmediatePropagation();
			
			// ignore mouse downs above or below the rendering area
			if ( mouseY > _renderRect.height )
			{
				trace("InterActiveStampList .handleMouseDown() | prevent interaction with non visible area of viewcanvas!");
				return;
			}
			
			killTapTimeout();
			// prevent taps while list is scrolling
			if ( vY == 0 )
			{
				tapTimeout = setTimeout(performTap, TAP_DELAY);
			}
			startDragging();
			addEventListener(MouseEvent.MOUSE_MOVE, handleMouseMove);
		}
		
		protected function handleMouseMove ( event:MouseEvent ):void
		{
			//trace("handleMouseMove ");
			//event.preventDefault();
			//event.stopImmediatePropagation();
			killTapTimeout();
			scrollBarNeedsRendering = true;
			event.updateAfterEvent();
		}
		
		protected function handleMouseUp ( event:MouseEvent ):void
		{
			//trace("handleMouseUp");
			event.preventDefault();
			event.stopImmediatePropagation();
			// tapTimeout != 0 means that the use tapped on the list while the list was standing still
			// we clearly consider this as an tap action
			// so we kill the tap timeout and perform the tap method immediately
			if ( tapTimeout != 0 )
			{
				// don't recognize mouse ups above or below the rendering area
				if ( mouseY >= 0 && mouseY <= _renderRect.height )
				{
					performTap();
				}
			}
			stopDragging();
		}
		
		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		//TODO: We should try to add a certain threshold to decide wheter an user interaction was a drag or a tap
		// At the moment tap with minimal translation are considered as dragging. That leads to the fact that there are
		// actions considred as a tap by the user that are considered as a drag by the list. This becomes especially 
		// noticeable when the list is not draggable. (When there are less items than the current rendering area is 
		// capable to render at once)
		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		protected function performTap ( ):void
		{
			killTapTimeout();
			
			_selectedItem = int((mouseY+_renderRect.y)/_stampRect.height);
			// prevent taps occurring in the current rendering area but being placed above or below the rendered items 
			// if this happens there are probably less items than the rendering area is capable to render at once (i.e. there is empty space)
			if ( _selectedItem < 0 || _selectedItem >= _itemDescriptions.length ) return;
			//trace("performTap - tappedItem: " + tappedItem);
			
			//highlightDown.y = _selectedItem * _stampRect.height - _renderRect.y;
			//highlightDown.visible = true;
			
			var selctedItemDescr:Object = getSelectedItem();
			
			if ( _stamp is IInterActiveStamp )
			{
				removeSelctedItemsFromRendering();
				
				/*/
				_stamp.y = _selectedItem * _stampRect.height - _renderRect.y;
				_stamp.visible = true;
				//*/
				IStamp(_stamp).data = selctedItemDescr;
				//updateStamp(selctedItemDescr);
				IInterActiveStamp(_stamp).renderSelectedState();
				
				_selectedStampMatrix.identity(); // reset matrix
				_selectedStampMatrix.translate(_stampRect.x, _selectedItem * _stampRect.height);
				
				//_itemsCanvas.draw(_stamp, _highlightStampMatrix);
				_itemsCanvas.drawWithQuality(_stamp, _selectedStampMatrix, null, null, null, false, StageQuality.HIGH);
				//_selectedItemsCanvas.drawWithQuality(_stamp, _selectedStampMatrix, null, null, null, false, StageQuality.HIGH);
				//_itemsCanvasCopy.drawWithQuality(_stamp, _selectedStampMatrix, null, BlendMode.OVERLAY, null, false, StageQuality.HIGH);
				
				render();
			}
			
			signalItemSelected.dispatch(selctedItemDescr);
			
		}
		
		protected function startDragging ( ):void
		{
			//trace("startDragging");
			
			// prevent dragging if there are less items than the rendering area is capable to render at once
			if ( _itemDescriptions.length * _stampRect.height <= _renderRect.height ) return;
			
			isDragging = true;
			
			// Reset the offset hsitory 
			vY = 0;
			for ( var i:uint = 0; i < OFFSETS_NUM; i++ )
			{
				offsetHistory[i] = 0;
			}
			
			offset = mouseY;
			removeEventListener(Event.ENTER_FRAME, updateRenderRectangle);
			addEventListener(Event.ENTER_FRAME, updateRenderRectangle);
		}
		
		protected function stopDragging ( event:MouseEvent = null ):void
		{
			if ( !isDragging ) return;
			//trace("stopDragging");
			if ( event != null )
			{
				event.preventDefault();
				event.stopImmediatePropagation();
			}
			
			// When runing on high frame rates (i.e. 60 fps for example) it may happen that there are two sequent frames
			// (before the mouse up event) where no change in mouseY is detected (even though the user moved his finger)
			// in that case vY would be 0 and the list stops instantly.
			// To make sure this won't happen we take the last OFFSETS_NUM frames into consideration: 
			vY = 0;
			for ( var i:uint = 0; i < OFFSETS_NUM; i++ )
			{
				vY += offsetHistory[i];
			}
			vY /= OFFSETS_NUM;
			
			trace("VY: " + vY);
			
			isDragging = false;
			removeEventListener(MouseEvent.MOUSE_MOVE, handleMouseMove);
		}
		
		protected function updateRenderRectangle ( event:Event ):void
		{
			//trace("updateRenderRectangle vY: " + vY);
			event.preventDefault();
			event.stopImmediatePropagation();
			
			const FRICTION:Number = .95;
			
			if ( isDragging )
			{
				vY = mouseY - offset;
				
				offsetHistory.shift();
				offsetHistory.push(vY);
				
				offset = mouseY;
				targetY = _renderRect.y - vY;
				_renderRect.y = targetY;
			}
			else {
				_renderRect.y -= vY;
				vY *= FRICTION;
				if ( vY > -.01 && vY < .01 )
				{
					vY = 0;
					scrollBarNeedsRendering = false;
					removeEventListener(Event.ENTER_FRAME, updateRenderRectangle);
				}
			}
			if ( _renderRect.y < 0 )
			{
				_renderRect.y = 0;
				vY = 0;
			}
			else if ( _renderRect.y > _renderRectBoundY )
			{
				_renderRect.y = _renderRectBoundY;
				vY = 0;
			}
			render();
			
			/*/
			if ( _showScrollBar )
			{
				var scrollDist:Number = _renderRect.height - 2*scrollBarPaddingV - scrollRect.height;
				var scrollPerc:Number = _renderRect.y / _renderRectBoundY;
				scrollRect.y = scrollBarPaddingV + scrollDist * scrollPerc;
			}
			//*/
		}
		
		protected function updateScrollBarLength ( ):void
		{
			maxScrollBarLength = int(_renderRect.height * _renderRect.height/_itemsCanvas.height);
			
			if ( _showScrollBar )
			{
				_scrollRect.height = maxScrollBarLength;
			}
		}
		
		
		
	}
}
