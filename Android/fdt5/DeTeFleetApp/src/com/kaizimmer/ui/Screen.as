package com.kaizimmer.ui {
import flash.display.Stage;
import flash.system.Capabilities;

public class Screen {
    // iphone
    public static const AT2X:uint = 6;
    public static const AT1X:uint = 5;
    // android
    public static const XHDPI:uint = 4;
    public static const HDPI:uint = 3;
    public static const MDPI:uint = 2;
    public static const LDPI:uint = 1;

    private static var _stage:Stage;

    protected static var _isInitialized:Boolean = false;
    protected static var _dpiClass:uint = 0;
    protected static var _dpi:uint = 0;
    protected static var _referenceDPI:uint = 0;
    private static var _referenceWidth:uint = 0;
    private static var _referenceHeight:uint = 0;
    protected static var _isIOS:Boolean = false;

    private static var _needsLetterBox:Boolean = false;

    private static var _scaleFactor:Number;

    protected static const INIT_ERROR_MESSAGE:String = "Screen class needs to be initialized before usage";

    /**
     *
     * @param referenceDPI
     * @param referenceScreenWidth
     * @param referenceScreenHeight
     *
     */
    public static function init(stage:Stage, referenceDPI:uint = 163, referenceScreenWidth:uint = 320, referenceScreenHeight:uint = 480):void {
        _isInitialized = true;
        _stage = stage;

        _isIOS = Capabilities.manufacturer.toLowerCase().search("iphone") != -1;
        _dpi = Capabilities.screenDPI;

        _referenceWidth = referenceScreenWidth;
        _referenceHeight = referenceScreenHeight;

        /*/
         //TODO:  The reference dpi should be design specific and not platform specific
         if ( _isAndroid ) _referenceDPI = 160;
         else _referenceDPI = 163;
         //*/
        _referenceDPI = referenceDPI;
        _scaleFactor = calcScaleFactor(referenceScreenWidth, referenceScreenHeight, _stage.fullScreenWidth, _stage.fullScreenHeight);
    }

    /**
     *
     * @return
     *
     */
    public static function get referenceWidth():uint {
        return _referenceWidth;
    }

    /**
     *
     * @return
     *
     */
    public static function get referenceHeight():uint {
        return _referenceHeight;
    }

    /**
     *
     * @return
     *
     */
    public static function get scaleFactor():Number {
        if (!_isInitialized) throw new Error(INIT_ERROR_MESSAGE);
        return _scaleFactor;
    }

    /**
     *
     * @return
     *
     */
    public static function get dpiClass():uint {
        if (!_isInitialized) throw new Error(INIT_ERROR_MESSAGE);
        if (_dpiClass != 0) return _dpiClass;

        var dpi:Number = Capabilities.screenDPI;
        trace("dpi: " + dpi);
        if (_isIOS) {
            switch (true) {
                case dpi < 326:
                    // 168
                    _dpiClass = AT1X;
                    break;
                default:
                    _dpiClass = AT2X;
            }
        }
        /**
         *  Density	ldpi	Resources for low-density (ldpi) screens (~120dpi).
         mdpi	Resources for medium-density (mdpi) screens (~160dpi). (This is the baseline density.)
         hdpi	Resources for high-density (hdpi) screens (~240dpi).
         xhdpi	Resources for extra high-density (xhdpi) screens (~320dpi).
         */
        else {
            switch (true) {
                case dpi < 120: //dpi < 160:
                    _dpiClass = LDPI;
                    break;
                case dpi < 200:
                    _dpiClass = MDPI;
                    break;
                case dpi < 280:
                    _dpiClass = HDPI;
                    break;
                default:
                    _dpiClass = XHDPI;
            }

        }
        trace("_dpiClass: " + _dpiClass);
        return _dpiClass;
    }

    /**
     *
     * @param densityIndependentPixels
     * @return
     *
     */
    public static function calcRealPixels(densityIndependentPixels:Number):Number {
        if (!_isInitialized) throw new Error(INIT_ERROR_MESSAGE);
        /*
         http://developer.android.com/guide/practices/screens_support.html

         Density-independent pixel (dp)

         The density-independent pixel is equivalent to one physical pixel on a 160 dpi screen,
         which is the baseline density assumed by the system for a "medium" density screen.
         At runtime, the system transparently handles any scaling of the dp units, as necessary,
         based on the actual density of the screen in use.
         The conversion of dp units to screen pixels is simple: px = dp * (dpi / 160).
         For example, on a 240 dpi screen, 1 dp equals 1.5 physical pixels.
         You should always use dp units when defining your application's UI, to ensure proper
         display of your UI on screens with different densities.
         */
        return densityIndependentPixels * _dpi / _referenceDPI;
    }

    /**
     *
     * @param realPixels
     * @return
     *
     */
    public static function calcVirtualPixels(realPixels:Number):Number {
        if (!_isInitialized) throw new Error(INIT_ERROR_MESSAGE);
        return realPixels * _referenceDPI / _dpi;
    }

    /**
     *
     * @return
     *
     */
    public static function calcDiagonalInInches():Number {
        var widthPixels:uint = Capabilities.screenResolutionX;
        var heightPixels:uint = Capabilities.screenResolutionY;
        var diagonalPixels:Number = Math.sqrt(widthPixels * widthPixels + heightPixels * heightPixels);
        var diagonalInches:Number = diagonalPixels / Capabilities.screenDPI;
        return diagonalInches;
    }

    /**
     *
     * @return
     *
     */
    public static function get needsLetterBox():Boolean {
        return _needsLetterBox;
    }

    /**
     *
     * @param referenceScreenWidth
     * @param referenceScreenHeight
     * @return
     *
     */
    public static function calcScaleFactor(referenceScreenWidth:uint, referenceScreenHeight:uint, screenWidth:Number, screenHeight:Number):Number {
        var referenceAspectRatio:Number = referenceScreenWidth / referenceScreenHeight;
        var deviceAspectRatio:Number = screenWidth / screenHeight;
        var scaleFactor:Number;

        _needsLetterBox = false;
        if (referenceAspectRatio >= deviceAspectRatio) {
            scaleFactor = screenWidth / referenceScreenWidth;
        }
        if (referenceAspectRatio < deviceAspectRatio) {
            scaleFactor = screenHeight / referenceScreenHeight;
            _needsLetterBox = true;
        }

        return scaleFactor;
    }

    public static function get stage():Stage {
        return _stage;
    }

    public static function set stage(value:Stage):void {
        _stage = value;
    }
}
}

/*/
 Google data:

 http://developer.android.com/guide/practices/screens_support.html

 Size	small	Resources for small size screens.
 normal	Resources for normal size screens. (This is the baseline size.)
 large	Resources for large size screens.
 xlarge	Resources for extra large size screens.
 Density	ldpi	Resources for low-density (ldpi) screens (~120dpi).
 mdpi	Resources for medium-density (mdpi) screens (~160dpi). (This is the baseline density.)
 hdpi	Resources for high-density (hdpi) screens (~240dpi).
 xhdpi	Resources for extra high-density (xhdpi) screens (~320dpi).
 nodpi	Resources for all densities. These are density-independent resources. The system does not scale resources tagged with this qualifier, regardless of the current screen's density.
 tvdpi	Resources for screens somewhere between mdpi and hdpi; approximately 213dpi. This is not considered a "primary" density group. It is mostly intended for televisions and most apps shouldn't need it—providing mdpi and hdpi resources is sufficient for most apps and the system will scale them as appropriate. If you find it necessary to provide tvdpi resources, you should size them at a factor of 1.33*mdpi. For example, a 100px x 100px image for mdpi screens should be 133px x 133px for tvdpi.
 //*/

/*/
 Starling data:

 http://wiki.starling-framework.org/manual/multi-resolution_development

 Screen size	 Asset type	 Factor	 Stage size
 240×320	 LD	 1	 240×320
 320×480	 LD	 1	 320×480
 480×640	 SD	 2	 240×320
 480×800	 SD	 2	 240×400
 640×960	 SD	 2	 320×480
 720×1280	 HD	 3	 240×426
 768×1024	 HD	 3	 256×341
 //*/
