/*
* The MIT License
* 
* Copyright (c) 2010 Kai Zimmer [http://www.kaizimmer.com]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
package com.kaizimmer.events
{
	import flash.events.Event;
	
	
	public class ValidationEvent extends Event
	{
		
		// Constant for the custom "validation" event type
		public static const CHANGE:String = "CHANGE";
		
		// Indicates the validation result
		public var isValid:Boolean;
		
		//-------------------------------------------------------------------------
		
		// Constructor
		public function ValidationEvent ( type:String,
											bubbles:Boolean = false,
											cancelable:Boolean = false,
											isValid:Boolean = false)
		{
			// Pass constructor parameters to the superclass constructor
			super(type, bubbles, cancelable);
			// Remember the validation result so it can be accessed within
			// ValidationEvent.ON_VALIDATION_CHANGE listners
			this.isValid = isValid;
		}
		
		//-------------------------------------------------------------------------
		
		/**
		 * 
		 */
		public override function clone ( ):Event
		{
			 return new ValidationEvent(type, bubbles, cancelable, isValid);
		}
		
		//-------------------------------------------------------------------------
		
		/**
		 * 
		 */
		public override function toString ( ):String
		{
			return formatToString("ValidationEvent", "type", "bubbles",
								  "cancelable", "eventPhase", "isValid");
		}
		
	}
	
}