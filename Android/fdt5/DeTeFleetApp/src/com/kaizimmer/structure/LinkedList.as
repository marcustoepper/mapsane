/**
 * Created with IntelliJ IDEA.
 * User: daniel.kopp
 * Date: 18.10.12
 * Time: 14:42
 * To change this template use File | Settings | File Templates.
 */
package com.kaizimmer.structure
{
public class LinkedList
{
	private var _first:ILinkedListItem;
	private var _last:ILinkedListItem;
	public var length:int = 0;

	public function clear():void{
		_first = null;
		_last = null;
		length = 0;
	}

	/**
	 * Add item at the end
	 *
	 * @param item
	 */
	public function push(item:ILinkedListItem):void {
		if(_first != null && _first == _last){
			_first.next = item;
		}

		if(_last != null) {
			_last.next = item;
			item.previous = _last;
		}

		_last = item;
		length++;

		if(_first == null){
			_first = _last;
		}
	}

	/**
	 * Return first Element and remove it from List
	 * @return
	 */
	public function shift():ILinkedListItem {
		if(_first == null) {
			return null;
		}
		var returnItem:ILinkedListItem = _first;
		if(_first.next != null) {
			_first = _first.next;
			_first.previous = null;
		} else {
			_first = null;
			_last = null;
		}
		length--;
		return returnItem;

	}

	/**
	 * Add item to the start
	 * @param item
	 */
	public function unshift(item:ILinkedListItem):void {
		if(_first == null){
			_first = item;
			_last = item;

			return;
		}

		var oldFirst:ILinkedListItem = _first;
		_first = item;
		length++;
		if(oldFirst == _last){
			_last.previous = _first;
			_first.next = _last;
		} else {
			oldFirst.previous = _first;
			_first.next = oldFirst;
		}
	}

	/**
	 * Get last Item and removed it from List
	 *
	 * @return
	 */
	public function pop():ILinkedListItem {
		if(_last == null) {
			return null;
		}

		var returnItem:ILinkedListItem = _last;
		if(_last.previous != null) {
			_last = _last.previous;
			_last.next = null;
		} else {
			_last = null;
			_first = null;
		}
		length--;
		return returnItem;
	}


	public function get first():ILinkedListItem {
		/**
		 * Add Item at the First Position
		 * @param item
		 */
		return _first;
	}

	public function set first(value:ILinkedListItem):void {
		_first = value;
	}

	public function get last():ILinkedListItem {
		return _last;
	}

	public function set last(value:ILinkedListItem):void {
		_last = value;
	}
}
}
