/**
 * Created with IntelliJ IDEA.
 * User: daniel.kopp
 * Date: 18.10.12
 * Time: 14:43
 * To change this template use File | Settings | File Templates.
 */
package com.kaizimmer.structure
{
public interface ILinkedListItem
{
	function get next():ILinkedListItem;
	function get previous():ILinkedListItem;
	function set next(value:ILinkedListItem):void;
	function set previous(value:ILinkedListItem):void;
}
}
