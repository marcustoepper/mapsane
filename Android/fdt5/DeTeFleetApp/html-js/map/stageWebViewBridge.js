/**
 * stageWebViewBridge
 * @author kai.zimmer@aoemedia.de
 */
window.stageWebViewBridge = (function ()
{
	var SEPERATOR_TOKEN = "?bridgedata=";
	// if several document location changes are triggered instantly one after another
	// only the last location change will be performed. that's why we use a stack
	// to store all changes and then process them with a small delay
	var messageQueue = [];
	var sendingTimeout = -1;
	var DELAY = 500;
	
	var bridge =
	{
		send: requestSendingJSONToFlash,
		log: sendLogToFlash
	};
	
	function requestSendingJSONToFlash ( data )
	{
		messageQueue.push(data);
		if ( sendingTimeout == -1 ) sendJSONToFlash();
		
		function sendJSONToFlash ( )
		{
			clearTimeout(sendingTimeout);
			sendingTimeout = -1;
			if ( messageQueue.length == 0 ) return;
			var data = messageQueue.shift();
			//alert("sending: " + SEPERATOR_TOKEN + JSON.stringify(data));
			document.location = SEPERATOR_TOKEN + JSON.stringify(data);
			sendingTimeout = setTimeout(sendJSONToFlash, DELAY);
		}
	}
	
	function sendLogToFlash ( msg )
	{
		requestSendingJSONToFlash( { jsLog: msg, logTimeStamp: new Date().getTime() + "ms" } );
	}

	return bridge;

})();