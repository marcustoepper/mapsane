/**
 * 
 * Examples: =========
 * 
 * Simple initialization --------------------- $map.init();
 * 
 * 
 * Extended inititalization (with options and initial markers)
 * ----------------------------------------------------------- $map.init( {
 * showCurrentPosition: false } [ { id: 'TEST_MARKER', position: new
 * google.maps.LatLng('50.062966', '8.322131'), options: {icon:
 * $map.markerIcons.pin} } ] );
 * 
 * 
 * Adding a marker --------------- $map.init();
 * $map.addMarker('CURRENT_POSITION', $map.currentPosition, {icon:
 * $map.markerIcons.myposition});
 * 
 * 
 * Adding a marker with some popups --------------------------------
 * 
 * var popups = [ { type: 'fuel-price-info', price: '1.71', time: 'vor 1 Std',
 * alwaysVisible: true, alignment: 'top-center' }, { type: 'info-content-box',
 * heading: 'Heading', content: 'content lorem ipusm bla' } ]; var popups =
 * this.addMarker('TEST_MARKER', new google.maps.LatLng('50.062966',
 * '8.322131'), {icon: this.markerIcons.pin}, {popups: popups});
 * 
 */

var $map = {

	gMap : null,
	currentPosition : null,
	options : {
		showCurrentPosition : true,
		hideAlwaysVisiblePopupsZoomLevel : 5,
		zoom: 15
	},
	areInfoWindowsVisible: false,
	markerStorage : {},
	markerIcons : {},

	// Settings for the different popup-types.
	'popupSettings' : {
		'info-content-box' : {
			'template' : '<div class="info-content-box-inner"><div><h2 class="heading"></h2><p class="content"></p><a href="javascript:void(0);" class="info-icon"></a></div></div>'
		},
		'fuel-price-info' : {
			'template' : '<div><span class="price"></span><br/><span class="time"></span></div>'
		}
	},

	/**
	 * JS initialization of the map component
	 * 
	 */
	init : function(options, markers) {
		var _this = this;
		options = options || {};
		markers = markers || [];

		/*/
		// default options
		this.options.zoom = 15;
		//*/
		this.options.mapTypeId = google.maps.MapTypeId.ROADMAP;

		// merge options
		$.extend(true, this.options, options);

		// init marker icons
		this.markerIcons.myposition = new google.maps.MarkerImage(
				'map-pin-here.png', new google.maps.Size(64, 64),
				new google.maps.Point(0, 0), new google.maps.Point(32, 32));

		this.markerIcons.pin = new google.maps.MarkerImage('map-pin.png',
				new google.maps.Size(40, 38), new google.maps.Point(0, 0),
				new google.maps.Point(5, 33));

		// inititalize google map
		this.gMap = new google.maps.Map(document.getElementById("map_canvas"),
				this.options);
		if (this.options.showCurrentPosition == true) {
			this.showCurrentPosition();
		} else { // if no current position is set map zooms out and centers
			// germany.
			this.centerGermany();
		}

		this.gMap.setZoom(this.options.zoom);

		google.maps.event.addListenerOnce(this.gMap, 'idle', function() {
			$('#loading-animation').hide();
		});

		// adds initial markers if exists
		if (markers.length > 0) {
			for (var i = 0; i < markers.length; i++) {
				this.addMarker(markers[i].id, markers[i].position, markers[i].options);
			}
		}

		google.maps.event.addListener(this.gMap, 'zoom_changed', function() {
			var zoomLevel = _this.gMap.getZoom();

			//*/
			window.stageWebViewBridge.log("gMap zoom_changed | zoomLevel: " + zoomLevel + 
											" | _this.options.hideAlwaysVisiblePopupsZoomLevel: " + 
											_this.options.hideAlwaysVisiblePopupsZoomLevel);
			//*/
			
			return;

			if (zoomLevel < _this.options.hideAlwaysVisiblePopupsZoomLevel) {
				window.stageWebViewBridge.log("zoom_changed > hideAlwaysVisiblePopups ");
				_this.hideAlwaysVisiblePopups();
			} else {
				window.stageWebViewBridge.log("zoom_changed > showAlwaysVisiblePopups ");
				_this.showAlwaysVisiblePopups();
			}

		});

	},

	/**
	 * Detects the current position and adds an marker to the map. Also centers
	 * the map to the current position.
	 * 
	 */
	showCurrentPosition : function(lat, lng, center) {
		//alert("lat: " + lat + " | lng: " + lng);
		if (!lat && !lng) {
			alert("!lat && !lng");
			this.centerGermany();
		} else {
			var marker = this.getMarkerById('CURRENT_POSITION');
			if (marker == undefined) {
				marker = this.addMarker('CURRENT_POSITION',
						new google.maps.LatLng(lat, lng), {
							icon : this.markerIcons.myposition
						});
			} else {
				marker.setPosition(new google.maps.LatLng(lat, lng));
			}
			
			if(center==true){
				//alert("center");
				this.gMap.setCenter(marker.getPosition());
			}
		}
	},

	/**
	 * Adds a marker to the map and storage.
	 * 
	 * Possible options:
	 * https://developers.google.com/maps/documentation/javascript/reference#MarkerOptions
	 * 
	 * 
	 * @param {String}
	 *            markerId An id for the marker which is used for the
	 *            markerStorage
	 * @param {google.maps.LatLng}
	 *            coords The coords for the marker
	 * @param {object}
	 *            options The options for marker
	 * 
	 */
	addMarker : function(markerId, coords, options, data) {
		//addMarker : function(markerId, lat, lng, options, data) {
		if (!markerId) {
			// @todo: error handling
			// console.log('Marker needs markerId!');
			return false;
		}

		var _options = {
			icon : this.markerIcons.pin,
			position : coords,
			//position :  = new google.maps.LatLng(lat, lng),
			map : this.gMap
		};

		$.extend(true, _options, options);

		// adding the markers to the list
		var _marker = new google.maps.Marker(_options);
		_marker.storageId = markerId;
		_marker.infowindows = [];
		this.markerStorage[markerId] = {
			'marker' : _marker
		};

		// adding the popups to the marker
		if (data && data.popups && data.popups.length > 0) {
			for (var i = 0; i < data.popups.length; i++) {
				this.addPopup(_marker, data.popups[i]);
			}
		}

		return _marker;
	},

	/**
	 * Removes a marker from the map and storage.
	 * 
	 * 
	 * @param {google.maps.Marker}
	 *            marker A google-maps-marker-object (including an extra-option:
	 *            "storageId").
	 * 
	 */
	removeMarker : function(marker) {
		marker.marker.setMap(null);
		for (var i = 0; i < marker.marker.infowindows.length; i++) {
			marker.marker.infowindows[i].close();
			marker.marker.infowindows[i].setMap(null);
		}

		delete this.markerStorage[marker.storageId];
	},

	/**
	 * Removes a marker from the map and storage by the given marker-id.
	 * 
	 * 
	 * @param {String}
	 *            id A marker-id which exists in the markerStorage
	 * 
	 */
	removeMarkerById : function(id) {
		var marker = this.markerStorage[id];
		if (marker) {
			this.removeMarker(marker);
		}
	},

	getMarkerById : function(id) {
		var marker = this.markerStorage[id];
		if (marker) {
			return marker.marker;
		}
		return marker;
	},

	/**
	 * Clears the map. - Removes all markers from the map and cleans internal
	 * storages of the mapComponent.
	 * 
	 */
	clear : function() {
		for ( var key in this.markerStorage) {
			if (key != 'CURRENT_POSITION') {
				this.removeMarker(this.markerStorage[key]);
			}
		}
	},

	/**
	 * Adds an popup to a marker.
	 * 
	 * @param {google.maps.Marker}
	 *            marker A google maps marker
	 * @param {Object}
	 *            data Data/settings for the popup
	 * 
	 * 
	 * Options samples (data): =======================
	 * 
	 * Fuel-Price-Popup: ----------------- type: 'fuel-price-info', price:
	 * '1.71', time: 'vor 1 Std', alwaysVisible: true
	 * 
	 * info-box-Popup: ----------------- type: 'info-content-box', heading:
	 * 'Heading', content: 'content lorem ipusm bla'
	 * 
	 */
	addPopup : function(marker, data) {
		var _this = this;

		// creates content from template
		var html = $(this.popupSettings[data.type].template);
		var alignment = '';
		switch (data.type) {
		case 'info-content-box':
			html.find('.heading').html(data.heading);
			html.find('.content').html(data.content);
			html.find('.info-icon').bind(
					'click',
					function() {
						var markerStorageId = $(this).parents('.infobox:first').data('storageid');
						window.stageWebViewBridge.send({
							id : markerStorageId
						});
					});
			alignment = 'top-center';
			break;
		case 'fuel-price-info':
			var price = html.find('.price');
			price.html(data.price);
			html.find('.time').html(data.time);
			alignment = 'top-right';
			break;
		}

		// creates the pop up
		var _popup = new InfoBox({
			map : this.gMap,
			marker : marker,
			content : html[0],
			cssClass : data.type,
			alignment : alignment,
			alwaysVisible : data.alwaysVisible
		});

		// pushes the pop up to the marker storage
		this.markerStorage[marker.storageId].marker.infowindows.push(_popup);

		// adds the event-listener(s) to the marker
		google.maps.event.addListener( marker,'click',function() 
			{
				_popup.toggle();
	
				for ( var key in _this.markerStorage) {
					for (var i = 0; i < _this.markerStorage[key].marker.infowindows.length; i++) {
						if (_this.markerStorage[key].marker.infowindows[i].isVisible === true
								&& _this.markerStorage[key].marker.storageId != _popup.marker.storageId) {
							_this.markerStorage[key].marker.infowindows[i].close();
						}
					}
				}
			});

		// adds the event-listener to the whole map
		google.maps.event.addListener(this.gMap, 'click', function() {
			for ( var key in _this.markerStorage) {
				var item = _this.markerStorage[key];
				for (var i = 0; i < item.marker.infowindows.length; i++) {
					if (item.marker.infowindows[i].isVisible === true) {
						item.marker.infowindows[i].close();
					}
				}
			}
		});
	},

	/**
	 * Centers map to germany with a respectable zoom.
	 * 
	 */
	centerGermany : function() {
		this.gMap.setCenter(new google.maps.LatLng(51.120765, 10.216873));
		this.gMap.setZoom(7);
	},

	hideAlwaysVisiblePopups : function() {
		
		for ( var key in this.markerStorage) {
			for (var i = 0; i < this.markerStorage[key].marker.infowindows.length; i++) {
				if (this.markerStorage[key].marker.infowindows[i].alwaysVisible === true) {
					//this.markerStorage[key].marker.infowindows[i].close(); // parameter true seems not to be defined in the api!?
				}
			}
		}
	},

	showAlwaysVisiblePopups : function() {
		
		for ( var key in this.markerStorage) {
			for (var i = 0; i < this.markerStorage[key].marker.infowindows.length; i++) {
				if (this.markerStorage[key].marker.infowindows[i].alwaysVisible === true) {
					//this.markerStorage[key].marker.infowindows[i].open(this.gMap);
				}
			}
		}
	},

	setCenter : function(lat, lng) {
		var _lat = lat || 0;
		var _lng = lng || 0;
		var pos = new google.maps.LatLng(_lat, _lng);
		this.gMap.setCenter(pos);
	}

}