function InfoBox(options) {
	options = options || {};

	google.maps.OverlayView.apply(this, arguments);
	
	this.gMap = options.map;
	this.marker = options.marker;
	this.content_ = options.content || '';
	this.position_ = new google.maps.LatLng(0, 0);
	this.cssClass = options.cssClass || '';
	this.alignment = options.alignment || 'top-center'; //top-center // bottom-right // bottom left
	this.alwaysVisible = options.alwaysVisible  || false;
	this.isVisible = (this.alwaysVisible === true) ? true : false;
	if (this.alwaysVisible === true) {
		this.open(this.gMap, this.marker);
	}
}


InfoBox.prototype = new google.maps.OverlayView();

InfoBox.prototype.draw = function () {
	this.createDiv_();
	this.div_.style.position = 'absolute';
	this.adjustPosition();
};

InfoBox.prototype.createDiv_ = function () {
	var cancelHandler = function (e) {
		e.cancelBubble = true;
		
		if (e.stopPropagation) {
			e.stopPropagation();
		}
	};

	if (!this.div_) {
	
		this.div_ = document.createElement("div");
		this.div_.className = 'infobox' + ' ' + this.cssClass;
		this.div_.setAttribute('data-storageid', this.marker.storageId);
		
		if (typeof this.content_.nodeType === "undefined") {
			this.div_.innerHTML = '' + this.content_;
		}
		else {
			this.div_.innerHTML = '';
			this.div_.appendChild(this.content_);
		}
		
		//$(this.div_).append('<div class="arrow" style="height: 17px; width: 100%; background: blue;"></div>');
		
		// Add the InfoBox DIV to the DOM
		this.getPanes()['floatPane'].appendChild(this.div_);
		
		google.maps.event.trigger(this, "domready");
	}
};

InfoBox.prototype.open = function (map, anchor) {
	if (anchor) {
		this.position_ = anchor.getPosition();
	}
	this.setMap(map);
	this.isVisible = true;
};

InfoBox.prototype.close = function (force) {
	if (force || this.alwaysVisible !== true) {
		this.setMap(null);
		this.isVisible = false;
	}
};

InfoBox.prototype.toggle = function () {
	if (this.isVisible === false) {			
		this.open(this.gMap, this.marker);
	} else {
		this.close();
	}
};


InfoBox.prototype.onRemove = function () {
	if (this.div_) {
		this.div_.parentNode.removeChild(this.div_);
		this.div_ = null;
	}
};

InfoBox.prototype.adjustPosition = function() {
	var pixPosition = this.getProjection().fromLatLngToDivPixel(this.position_);
	var xPos = 0;
	var yPos = 0;
	
	switch (this.alignment) {
	
		case 'top-right':
			xPos = pixPosition.x + 20;
			yPos = pixPosition.y - $(this.div_).height() - 20;
			break;
			
		case 'bottom-right':
			xPos = pixPosition.x + 20;
			yPos = pixPosition.y;
			break;
		
		case 'top-center':
			xPos = pixPosition.x + 25 - ($(this.div_).width() / 2);
			yPos = pixPosition.y - $(this.div_).height() - 50;
			break;
		
		case 'bottom-left':
		default:
			xPos = pixPosition.x;
			yPos = pixPosition.y;
	}
	
	this.div_.style.left = xPos + "px";
	this.div_.style.top = yPos + "px";	
}