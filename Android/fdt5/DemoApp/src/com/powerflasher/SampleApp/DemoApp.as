package com.powerflasher.SampleApp {

	import flash.display.Sprite;
	import flash.display.Shape;
	import de.aoemedia.ane.maps.Map;
	import flash.events.MouseEvent;
	
	import org.osflash.signals.Signal;
	import flash.geom.Rectangle;
	
	
	public class DemoApp extends Sprite {
 	
 		
		public var map:Map;
		public var button:Sprite;
		
		public function DemoApp() {
			
			/*var circle : Shape = new Shape();
			circle.graphics.beginFill(0x550000);
			circle.graphics.drawCircle(150, 150, 100);
			addChild(circle);
			*/
			
			button = new Sprite();
			button.graphics.beginFill(0xFFCC00);
			button.graphics.drawRect(0, 250, 200, 200);
			button.graphics.endFill();
			button.addEventListener(MouseEvent.CLICK, buttonClickHandler);
			this.addChild(button);
			
			map = new Map();
			map.calloutSelected.add(myCalloutSelected);
			//map.viewPort = new Rectangle(0,0,50,50);
		}
		
		public function myCalloutSelected(eventData:Object):void {
			trace("callout selected: " + eventData.markerId);
		}
		
		public function buttonClickHandler(event:MouseEvent):void {
			trace("Button clicked!");
			this.map.testFoo();
			
		}
	}
}
	
