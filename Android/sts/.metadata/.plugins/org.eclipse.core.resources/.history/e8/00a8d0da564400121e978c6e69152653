package de.aoemedia.ane.maps;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.adobe.fre.FREContext;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

import de.aoemedia.ane.maps.components.OverlayManager;
import de.aoemedia.ane.maps.components.AoeOverlay;

public class AoeMapActivity extends MapActivity implements OnTouchListener {

	public MapView mapView;
	public MapController mapController;
	public List<Overlay> mapOverlays;
	public OverlayManager overlayManager;

	public static Context context;
	public static FREContext freContext;

	public View view;

	public Handler mHandler = new Handler();

	private String lastPressedButtonTag = "";
	private int selectedCalloutId = -1;
	
	private LocationManager myLocationManager;
	private LocationListener myLocationListener;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// getWindow().addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
		// getWindow().addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);

		// Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		// Remove notification bar
		// this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		// WindowManager.LayoutParams.FLAG_FULLSCREEN);

		view = LayoutInflater.from(getBaseContext()).inflate(
				freContext.getResourceId("layout.activity_map"), null);
		setContentView(view);

		// TODO maybe, touchlistening can be removed later, as we only want touches
		// on either the map (tap-listener in OverlayManager) or buttons (clickhandlers).
		// currently, e.g. the space between the buttons is another inactive button,
		// so there's no touch, either
		view.setOnTouchListener(this);

		mapView = (MapView) view.findViewWithTag("mapview");
		
		// TODO do we want those?
		mapView.setBuiltInZoomControls(true);

		// TODO maybe animate to Bonn as default?
		GeoPoint point = new GeoPoint((int) (50.057122 * 1E6),
				(int) (8.337104 * 1E6));
		mapView.getController().animateTo(point);
		// TODO replace with dynamic updates:
		// initializeLocationManagment();
		
		// set clickhandler for the top buttonrow
		initializeButtons();
		
		// finally, set reference
		MapsExtension.aoeMapActivity = this;
		
		
		Drawable drawable = AoeMapActivity.this.getResources().getDrawable(freContext.getResourceId("drawable.androidmarker"));
		overlayManager = new OverlayManager(drawable, this, mapView);
		GeoPoint point2 = new GeoPoint((int)(50.057122*1E6),(int)(8.337104*1E6));
		AoeOverlay overlayitem = new AoeOverlay(getApplicationContext(), mapView, point2, "1,59 �", "vor 33 min.");
		overlayManager.addOverlay(overlayitem);
		mapOverlays = mapView.getOverlays();
		mapOverlays.add(overlayManager);
		mapView.invalidate();
		
	}

	@Override
	protected void onResume() {
		super.onResume();
		MapsExtension.aoeMapActivity = this;
	}

	@Override
	protected void onPause() {
		super.onPause();
		MapsExtension.aoeMapActivity = null;
	}

	//private void zoomToMyLocation() {
	//}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

	@Override
	public boolean onTouch(View arg0, MotionEvent arg1) {

		// mapview seems to capture touches, so this isn's used?
		// if (arg0 != mapView) {
		// finish();
		// }

		finish();

		return true;
	}

	public void setSelectedCalloutDataForBroadcast(int selectedCalloutId) {
		lastPressedButtonTag = MapsExtension.BUTTON_CALLOUT_INFO;
		this.selectedCalloutId = selectedCalloutId;
		finish();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();

		// Send the info back to the service
		// TODO send data back from here?
		Intent intent = new Intent();
		
		intent.putExtra("WhichButtonPressed", lastPressedButtonTag);
		
		if (lastPressedButtonTag == "button_callout_info") {
			intent.putExtra("SelectedCalloutId", String.valueOf(selectedCalloutId));
		}
		
		intent.setAction("de.aoemedia.ane.maps.MapViewClosed");
		sendBroadcast(intent);

		MapsExtension.aoeMapActivity = null;
	}
	
	public void initializeButtons() {

		// left button, BACK
		Button button1 = (Button) view.findViewWithTag("button1");
		button1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				
				lastPressedButtonTag = MapsExtension.BUTTON_BACK;
				finish();
			}
		});
		
		// right button, LIST
		Button button3 = (Button) view.findViewWithTag("button3");
		button3.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				
				lastPressedButtonTag = MapsExtension.BUTTON_LIST;
				finish();
			}
		});
	}

	public void initializeLocationManagment() {
		myLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

		myLocationListener = new AoeLocationListener();

		myLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, myLocationListener);
		
		GeoPoint initGeoPoint = new GeoPoint(
				(int) (myLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER).getLatitude() * 1000000), 
				(int) (myLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER).getLongitude() * 1000000));
		
		mapController.animateTo(initGeoPoint);
	}
	
	class AoeLocationListener implements LocationListener {

		public void onLocationChanged(Location argLocation) {
			GeoPoint myGeoPoint = new GeoPoint(
					(int) (argLocation.getLatitude() * 1000000),
					(int) (argLocation.getLongitude() * 1000000));

			mapController.animateTo(myGeoPoint);
		}

		public void onProviderDisabled(String provider) {
		}

		public void onProviderEnabled(String provider) {
		}

		public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		}
	}
	
}
