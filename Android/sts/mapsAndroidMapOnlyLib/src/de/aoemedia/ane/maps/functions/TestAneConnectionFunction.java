package de.aoemedia.ane.maps.functions;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import com.adobe.fre.FREObject;
import com.adobe.fre.FREWrongThreadException;

public class TestAneConnectionFunction implements FREFunction {

	public static final String TAG = "TestAneConnectionFunction";

	@Override
	public FREObject call(FREContext context, FREObject[] args) {

		// Context appContext = context.getActivity().getApplicationContext();

		// just double and return the given string
		String inputString = "String";
		try {
			inputString = args[0].getAsString();
		} catch (Exception e) {

		}

		inputString = inputString + inputString;

		FREObject value = null;
		try {
			value = FREObject.newObject(inputString);
		} catch (FREWrongThreadException e) {
			e.printStackTrace();
		}

		return value;
	}
}