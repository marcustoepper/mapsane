package de.aoemedia.ane.maps.functions;

import android.os.AsyncTask;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import com.adobe.fre.FREObject;

import de.aoemedia.ane.maps.MapsExtension;

public class SetZoomFunction implements FREFunction {
	
	public static final String TAG = "SetZoomFunction";
	
	@Override
	public FREObject call(FREContext context, FREObject[] args) {
		
		try {
			new WaitAndSetZoomTask().execute(new Integer(args[0].getAsInt()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
        return null;
	}
	
	class WaitAndSetZoomTask extends AsyncTask<Integer, Void, Integer> {

		protected Integer doInBackground(Integer... zoomlevel) {

			do {

			} while (MapsExtension.aoeMapActivity == null);

			return zoomlevel[0];
		}

		protected void onPostExecute(Integer zoomlevel) {
			
			try {
				MapsExtension.aoeMapActivity.mapView.getController().setZoom(Integer.valueOf(zoomlevel));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}