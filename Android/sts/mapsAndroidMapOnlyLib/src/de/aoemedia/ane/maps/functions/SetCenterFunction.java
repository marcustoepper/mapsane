package de.aoemedia.ane.maps.functions;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import com.adobe.fre.FREObject;
import com.google.android.maps.GeoPoint;

import de.aoemedia.ane.maps.MapsExtension;

public class SetCenterFunction implements FREFunction {

	public static final String TAG = "SetCenterFunction";

	@Override
	public FREObject call(FREContext context, FREObject[] args) {

		Double lat = null;
		Double lng = null;

		try {
			lat = args[0].getProperty("_latitude").getAsDouble();
			lng = args[0].getProperty("_longitude").getAsDouble();

			GeoPoint newCenter = new GeoPoint((int) (lat * 1000000),
					(int) (lng * 1000000));

			MapsExtension.aoeMapActivity.mapController.animateTo(newCenter);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
}