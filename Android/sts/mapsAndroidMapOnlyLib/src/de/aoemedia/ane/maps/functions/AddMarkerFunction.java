package de.aoemedia.ane.maps.functions;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import com.adobe.fre.FREObject;

public class AddMarkerFunction implements FREFunction {
	
	public static final String TAG = "AddMarkerFunction";
	
	@Override
	public FREObject call(FREContext context, FREObject[] args) {
		
		// this is empty, as the extension uses addOverlay()
		
		return null;
	}
}