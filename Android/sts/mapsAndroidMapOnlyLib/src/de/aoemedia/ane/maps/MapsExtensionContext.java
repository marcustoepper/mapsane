package de.aoemedia.ane.maps;

import java.util.HashMap;
import java.util.Map;

import android.util.Log;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;

import de.aoemedia.ane.maps.functions.CreateMapViewFunction;
import de.aoemedia.ane.maps.functions.GetViewPortFunction;
import de.aoemedia.ane.maps.functions.HideMapViewFunction;
import de.aoemedia.ane.maps.functions.InitFunction;
import de.aoemedia.ane.maps.functions.AddMarkerFunction;
import de.aoemedia.ane.maps.functions.ClearFunction;
import de.aoemedia.ane.maps.functions.ResetCenterFunction;
import de.aoemedia.ane.maps.functions.SetViewPortFunction;
import de.aoemedia.ane.maps.functions.ShowMapViewFunction;
import de.aoemedia.ane.maps.functions.ShowUserLocationFunction;
import de.aoemedia.ane.maps.functions.SetZoomFunction;
import de.aoemedia.ane.maps.functions.SetCenterFunction;
import de.aoemedia.ane.maps.functions.AddOverlayFunction;

public class MapsExtensionContext extends FREContext {

	public static final String TAG = "MapsExtensionContext";

	@Override
	public void dispose() {
		Log.d(TAG, "Context disposed.");
	}

	@Override
	public Map<String, FREFunction> getFunctions() {

		Map<String, FREFunction> functions = new HashMap<String, FREFunction>();

		functions.put("createMapView", new CreateMapViewFunction());
		functions.put("getViewPort", new GetViewPortFunction());
		functions.put("init", new InitFunction());
		functions.put("clear", new ClearFunction());
		functions.put("resetCenter", new ResetCenterFunction());
		functions.put("addMarker", new AddMarkerFunction());
		functions.put("showMapView", new ShowMapViewFunction());
		functions.put("hideMapView", new HideMapViewFunction());
		functions.put("showUserLocation", new ShowUserLocationFunction());
		functions.put("setViewPort", new SetViewPortFunction());
		functions.put("setZoom", new SetZoomFunction());
		functions.put("setCenter", new SetCenterFunction());
		functions.put("addOverlay", new AddOverlayFunction());
		
		return functions;
	}
}