package de.aoemedia.ane.maps.functions;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import com.adobe.fre.FREObject;

import de.aoemedia.ane.maps.MapsExtension;

public class SetViewPortFunction implements FREFunction {

	public static final String TAG = "SetViewPortFunction";
	public Context appContext;
	public FREContext frecontext;

	@Override
	public FREObject call(FREContext context, FREObject[] args) {

		appContext = context.getActivity().getApplicationContext();
		frecontext = context;
	
		try {
			
			Double y = args[0].getProperty("y").getAsDouble();
			y = args[0].getProperty("y").getAsDouble();
			
			new WaitAndResizeTask().execute(y);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	class WaitAndResizeTask extends AsyncTask<Double, Void, Double> {

		protected Double doInBackground(Double... y) {

			do {

			} while (MapsExtension.aoeMapActivity == null);

			return y[0];
		}

		protected void onPostExecute(Double y) {

			LinearLayout buttonlayout = (LinearLayout) MapsExtension.aoeMapActivity.view
					.findViewWithTag("buttonlayout");
			LinearLayout.LayoutParams params = (LayoutParams) buttonlayout
					.getLayoutParams();
			params.height = y.intValue();
			buttonlayout.setLayoutParams(params);
			MapsExtension.aoeMapActivity.mapView.invalidate();
			buttonlayout.requestLayout();
			MapsExtension.aoeMapActivity.view.requestLayout();
		}
	}
}