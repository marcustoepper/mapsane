package de.aoemedia.ane.maps.functions;

import android.app.Activity;
import android.content.Intent;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import com.adobe.fre.FREObject;

import de.aoemedia.ane.maps.AoeMapActivity;
import de.aoemedia.ane.maps.service.MapWrappingService;

public class CreateMapViewFunction implements FREFunction {

	public static final String TAG = "CreateMapFunction";

	@Override
	public FREObject call(FREContext context, FREObject[] args) {

		AoeMapActivity.freContext = context;

		// this is the actual startingpoint. we instantiate a MapWrappingService,
		// as it has no visual components an can therefore be run without interfering
		// with the FlashApp
		Activity a = context.getActivity();
		Intent intent = new Intent(a, MapWrappingService.class);
		a.startService(intent);

		return null;
	}
}