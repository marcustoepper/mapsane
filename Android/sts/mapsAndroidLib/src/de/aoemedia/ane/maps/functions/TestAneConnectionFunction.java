package de.aoemedia.ane.maps.functions;

import android.content.Intent;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import com.adobe.fre.FREObject;
import com.adobe.fre.FREWrongThreadException;

import de.aoemedia.ane.maps.AoeMapActivity;

public class TestAneConnectionFunction implements FREFunction {
	
	public static final String TAG = "TestAneConnectionFunction";
	
	@Override
	public FREObject call(FREContext context, FREObject[] args) {
		
		// works
		//Intent intent = new Intent( context.getActivity(), AoeMapActivity.class );
		//context.getActivity().startActivity( intent );
		
		//Context appContext = context.getActivity().getApplicationContext();
		
		String inputString = "String";
	     try {
	    	 inputString = args[0].getAsString();
	     } catch (Exception e) {
	 
	     }
		
	     inputString = inputString + inputString;
	     
		FREObject value =null;
		try {
			value = FREObject.newObject( inputString );
		} catch (FREWrongThreadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return value;
	}
}