package de.aoemedia.ane.maps.functions;

//import android.content.Context;

import android.content.Context;
import android.content.Intent;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import com.adobe.fre.FREObject;
import com.adobe.fre.FREWrongThreadException;

import de.aoemedia.ane.maps.AoeMapActivity;

public class ClearFunction implements FREFunction {
	
	public static final String TAG = "ClearFunction";
	
	@Override
	public FREObject call(FREContext context, FREObject[] args) {
		
		//Context appContext = context.getActivity().getApplicationContext();
		
		Intent intent = new Intent( context.getActivity(), de.aoemedia.ane.maps.AoeMapActivity.class );
		context.getActivity().startActivity( intent );
		
		FREObject result = null;
		try {
			result = FREObject.newObject("hallo activity");
		} catch (FREWrongThreadException e) {
			e.printStackTrace();
		}
		
		return result;
	}
}