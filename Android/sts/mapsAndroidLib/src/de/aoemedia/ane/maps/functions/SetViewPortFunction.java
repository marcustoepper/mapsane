package de.aoemedia.ane.maps.functions;

import android.view.ViewGroup;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import com.adobe.fre.FREObject;

import de.aoemedia.ane.maps.AoeMapActivity;

public class SetViewPortFunction implements FREFunction {
	
	public static final String TAG = "SetViewPortFunction";
	
	@Override
	public FREObject call(FREContext context, FREObject[] args) {
		
		//Context appContext = context.getActivity().getApplicationContext();
		
		AoeMapActivity currentActivity = (AoeMapActivity)context.getActivity();
		ViewGroup.LayoutParams params = currentActivity.mapView.getLayoutParams();
		params.width = 50;
		params.width = 50;
		currentActivity.mapView.setLayoutParams(params);
		currentActivity.mapView.invalidate();
		
		return null;
	}
}