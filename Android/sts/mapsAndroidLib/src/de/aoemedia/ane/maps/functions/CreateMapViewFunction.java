package de.aoemedia.ane.maps.functions;

import android.R;
import android.content.Intent;
import android.widget.TextView;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import com.adobe.fre.FREObject;

import de.aoemedia.ane.maps.AoeMapActivity;
import de.aoemedia.ane.maps.MapsExtension;

public class CreateMapViewFunction implements FREFunction {
	
	public static final String TAG = "CreateMapFunction";
	
	@Override
	public FREObject call(FREContext context, FREObject[] args) {
		
		//Context appContext = context.getActivity().getApplicationContext();
		
		AoeMapActivity.freContext = context;
		
		Intent intent = new Intent( context.getActivity(), AoeMapActivity.class );
		context.getActivity().startActivity( intent );
		
		//int layoutRecordId = context.getResourceId("layout.recordLayout") ;
		
		
        
        
        //context.getActivity().setContentView(context.getResourceId("layout.recordLayout"));
        //TextView textfield = (TextView) context.getActivity().findViewById(R.layout.textfield1);
        //textfield.setText("testtext");
        
		return null;
	}
}