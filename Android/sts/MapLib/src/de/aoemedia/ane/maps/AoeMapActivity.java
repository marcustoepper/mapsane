
package de.aoemedia.ane.maps;

import java.util.List;
import java.util.zip.Inflater;

import com.adobe.fre.FREContext;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;

///import de.aoemedia.ane.maps.mapcomponents.AoeOverlay;
//import de.aoemedia.ane.maps.mapcomponents.OverlayManager;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class AoeMapActivity extends MapActivity {

	public MapView mapView;
	
	public static Context context;

	public static FREContext freContext;
	
	private View view;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //Remove notification bar
        //this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
         
        //view = LayoutInflater.from(getBaseContext()).inflate(freContext.getResourceId("layout.activity_map"), null);
        
        
        setContentView(R.layout.activity_map);
        /*
        mapView = (MapView)view.findViewWithTag("mapview");
        mapView.setBuiltInZoomControls(true);
        
        
     	GeoPoint point = new GeoPoint((int)(50.057122*1E6),(int)(8.337104*1E6));
		mapView.getController().animateTo(point);
		mapView.getController().setZoom(10);*/
	
	}

	@Override
	protected void onResume() {
		super.onResume();
		// when our activity resumes, we want to register for location updates
	}

	@Override
	protected void onPause() {
		super.onPause();
		// when our activity pauses, we want to remove listening for location
		// updates
	}

	/**
	 * This method zooms to the user's location with a zoom level of 10.
	 */
	private void zoomToMyLocation() {

	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}
}
