package de.aoemedia.ane.maps;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;


public class AoeOverlay extends OverlayItem {

	private Context context;
	private MapView mapView;
	
	public AoeOverlay(Context context, MapView mapView, GeoPoint point, String title, String snippet) {

		super(point, title, snippet);
		
		this.context = context;
		this.mapView = mapView;
		
		this.setMarker();
	}

	public void setMarker() {
		
		// could this be uses to utilize an xml layout?
		// this.mMarker.inflate(r, parser, attrs)
		
		final int drawableId = context.getResources().getIdentifier("annotationoverlay", "drawable", "de.aoemedia.ane.maps");
		
		this.setMarker(this.writeOnDrawable(drawableId, this.mTitle, this.mSnippet ));
		this.mMarker.setBounds(-10, -this.mMarker.getIntrinsicHeight(), this.mMarker.getIntrinsicWidth()-10, 0);		
	}

    public BitmapDrawable writeOnDrawable(int drawableId, String text, String text2){
    	
    	context.getResources().getDrawable(R.drawable.annotationoverlay);
    	
        Bitmap bm = BitmapFactory.decodeResource(context.getResources(), drawableId).copy(Bitmap.Config.ARGB_8888, true);

        Canvas canvas = new Canvas(bm);
        
        Paint paint = new Paint(); 
        paint.setStyle(Style.FILL);  
        paint.setColor(Color.WHITE); 
        paint.setAntiAlias(true);    
        Typeface boldFont = Typeface.createFromAsset(context.getAssets(), "telegrotesk-bold.ttf");
        paint.setTypeface(boldFont);
        
        paint.setTextSize(28); 
        canvas.drawText(text, 80, 40, paint);
 
        Typeface halbFont = Typeface.createFromAsset(context.getAssets(), "telegrotesk-halb.ttf");
        paint.setTypeface(halbFont);
        
        paint.setTextSize(16);    
        canvas.drawText(text2, 80, 65, paint);
        
        return new BitmapDrawable(context.getResources(), bm);
    }
}
