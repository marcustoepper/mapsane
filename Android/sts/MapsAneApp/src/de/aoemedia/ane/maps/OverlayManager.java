package de.aoemedia.ane.maps;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;

public class OverlayManager extends ItemizedOverlay {

	private ArrayList<OverlayItem> mOverlays = new ArrayList<OverlayItem>();

	private Context mContext;
	private MapView mapView;

	private int currentlySelectedItem = -1;
	
	public OverlayManager(Drawable defaultMarker, Context context, MapView mapView) {
		
		super(boundCenterBottom(defaultMarker));
		//super(boundCenter (defaultMarker));
		mContext = context;
		this.mapView = mapView;
	}

	@Override
	protected OverlayItem createItem(int i) {
		return mOverlays.get(i);
	}

	@Override
	public int size() {
		return mOverlays.size();
	}

	public void addOverlay(OverlayItem overlay) {
		mOverlays.add(overlay);
		populate();
	}

	public void clearOverlays() {
		mOverlays.clear();
	}
	
	@Override
	protected boolean onTap(int index) {

		currentlySelectedItem = index;
		
		OverlayItem item = mOverlays.get(index);
		
		/*
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
 		// set title
		alertDialogBuilder.setTitle("Your Title");
 		// set dialog message
		alertDialogBuilder
				.setMessage(String.valueOf(index))
				.setCancelable(false)
				.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, close
						// current activity
						//MainActivity.this.finish();
					}
				  })
				.setNegativeButton("No",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});
 		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();	
		return true;
		*/
		
		
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		LinearLayout layout = new LinearLayout(mContext);
		//layout.setVisibility(VISIBLE);
		
		View v = inflater.inflate(R.layout.callout, layout);
		
		v.setTag("callout");
		
        Typeface boldFont = Typeface.createFromAsset(mContext.getAssets(), "telegrotesk-bold.ttf");
        TextView callout_title = (TextView) v.findViewById(R.id.callout_title);
        callout_title.setTypeface(boldFont);
        
        TextView callout_subtitle = (TextView) v.findViewById(R.id.callout_subtitle);
        callout_subtitle.setTypeface(boldFont);
		
		GeoPoint point = mOverlays.get(index).getPoint();
		
		MapView.LayoutParams params = new MapView.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, point,
				MapView.LayoutParams.BOTTOM);
		params.mode = MapView.LayoutParams.MODE_MAP;
		
		Button calloutButton = (Button) v.findViewById(R.id.callout_button);
		calloutButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				
				// TODO fire signal here
				
				for (int i = 0; i < mapView.getChildCount(); i++) {
					if (mapView.getChildAt(i).getTag() == "callout") {
						mapView.removeViewAt(i);
					}
				}
				//Toast.makeText(OverlayManager.this.mContext, String.valueOf(OverlayManager.this.currentlySelectedItem), Toast.LENGTH_SHORT).show();
			}
		});
		
		mapView.addView(v, params);
		
		return true;
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event, MapView mapView) {
	    
		//Toast.makeText(mContext, "touched", Toast.LENGTH_SHORT).show();
		
		for (int i = 0; i < mapView.getChildCount(); i++) {
			if (mapView.getChildAt(i).getTag() == "callout") {
				mapView.removeViewAt(i);
			}
		}
		
	    return false;
	}	
	
    @Override
    public void draw(Canvas canvas, MapView mapView, boolean shadow)
    {
        if(!shadow)
        {
            super.draw(canvas, mapView, false);
        }
    }
}
