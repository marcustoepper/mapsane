package de.aoemedia.ane.maps;

import java.util.List;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;

public class MainActivity extends MapActivity {

	private MapView mapView;
	private MapController mapController;
	private List<Overlay> mapOverlays;
	private OverlayManager overlayManager;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main);

		mapView = (MapView) findViewById(R.id.mapview);
		mapView.setBuiltInZoomControls(false);
		
		mapOverlays = mapView.getOverlays();
		
		
		Drawable drawable = this.getResources().getDrawable(R.drawable.androidmarker);
		overlayManager = new OverlayManager(drawable, this, mapView);
		GeoPoint point = new GeoPoint((int)(50.057122*1E6),(int)(8.337104*1E6));
		OverlayItem overlayitem = new OverlayItem(point, "wiesbaden", "borsigstrasse");
		//overlayManager.addOverlay(overlayitem);
		
		
		mapOverlays.add(overlayManager);
		
		mapView.getController().animateTo(point);
		mapView.getController().setZoom(10);
		
		Button buttonClear = (Button) findViewById(R.id.button_clear);
		buttonClear.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				mapOverlays = mapView.getOverlays();
				mapOverlays.clear();				
				mapView.removeAllViews();
				mapView.invalidate();
				
				overlayManager.clearOverlays();
			}
		});
		
		Button buttonAdd = (Button) findViewById(R.id.button_add);
		buttonAdd.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Drawable drawable = MainActivity.this.getResources().getDrawable(R.drawable.androidmarker);
				GeoPoint point = new GeoPoint((int)(50.057122*1E6),(int)(8.337104*1E6));
				AoeOverlay overlayitem = new AoeOverlay(getApplicationContext(), mapView, point, "1,59 �", "vor 33 min.");
				//OverlayItem overlayitem = new OverlayItem(point, "wiesbaden", "globusstrasse");
				overlayManager.addOverlay(overlayitem);
				mapOverlays.add(overlayManager);
				
				GeoPoint point2 = new GeoPoint((int)(51.515259*1E6),(int)(-0.086623*1E6));
				AoeOverlay overlayitem2 = new AoeOverlay(getApplicationContext(), mapView, point2, "1,59 �", "vor 33 min.");
				overlayManager.addOverlay(overlayitem2);
				
				mapOverlays.add(overlayManager);
				mapView.invalidate();
			}
		});
		
		  //Intent intent = new Intent(this, MapWrappingService.class );
		  //startService(intent);
	}

	@Override
	protected void onResume() {
		super.onResume();
		// when our activity resumes, we want to register for location updates
		//myLocationOverlay.enableMyLocation();
	}

	@Override
	protected void onPause() {
		super.onPause();
		// when our activity pauses, we want to remove listening for location
		// updates
		//myLocationOverlay.disableMyLocation();
	}

	/**
	 * This method zooms to the user's location with a zoom level of 10.
	 */
	private void zoomToMyLocation() {
		/*GeoPoint myLocationGeoPoint = myLocationOverlay.getMyLocation();
		if (myLocationGeoPoint != null) {
			mapView.getController().animateTo(myLocationGeoPoint);
			mapView.getController().setZoom(10);
		} else {
			Toast.makeText(this, "Cannot determine location",
					Toast.LENGTH_SHORT).show();
		}*/
	}

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}
}
