package de.aoemedia.ane.maps
{
	import flash.events.Event;
	
	public class MapEvent extends Event
	{
		public static var TILES_LOADED_PENDING:String="mapevent_tilesloadedpending";
		public static var TILES_LOADED:String="mapevent_tilesloaded";
		public static var MAP_LOAD_ERROR:String="mapevent_maploaderror";
		
		public static var MAP_CALLOUT_ACCESSORY_SELECTED:String="callout_accessory_selected";
		public var data:Object;
		
		public function MapEvent(type:String,feature:Object=null, bubbles:Boolean=false, cancelable:Boolean=false, data:Object=null)
		{
			super(type, bubbles, cancelable);
			
			if (data != null) {
				this.data = data;
			}
		}
	}
}