//
//  MapWrapper.m
//  MapsNE_Native
//
//  Created by Meet Shah on 4/12/12.
//  Extended by Marcus Toepper on 10/15/12.
//

#import "MapWrapper.h"
#import "UtilityClass.h"
#import "MyCustomAnnotation.h"
#import "MyCustomOverlay.h"

#import "FlashRuntimeExtensions.h"

extern FREObject getContext2();

@implementation MapWrapper
@synthesize mapView,parentView;

// AOE extension
@synthesize currentSelectedId;

NSMutableArray * annotationsArray=nil;
NSMutableArray * overlaysArray=nil;

-(void)initWithDefaultFrame {
    //default size
    CGRect frame=CGRectMake(50, 50, 300,300);
    //Create a MKMapView Object
	MKMapView *aView=[[MKMapView alloc] initWithFrame:frame];
    aView.delegate=self;
	mapView=aView;

    
    annotationsArray=[[NSMutableArray alloc] init];
    overlaysArray=[[NSMutableArray alloc] init];
    
    // shows the blue dot
    mapView.showsUserLocation = YES;
    currentSelectedId = -1;
    
    // center on startup doesn't work, as the signal has some delay until it's first processed
}

-(void)setParentView:(UIView *)pV {
    
    parentView=pV;
}

//Sets the viewPort i.e the bounds of the map View
-(void)setViewPort:(CGRect)frame {
    
    [mapView setFrame:frame];
}

//Returns the viewPort i.e the bounds of the map View
-(CGRect)getViewPort {
    
	return mapView.frame;
}

//Adds the mapView onto the main View root View Controller
-(void)showMap {

	if([mapView superview]==nil) {
		[parentView addSubview:mapView];
	}
}

//Removes the mapView from the main View root View Controller
-(void)hideMap {
    
	if([mapView superview]!=nil) {
		[mapView removeFromSuperview];
	}
}

-(void)showUserLocation:(BOOL)show {
    
	mapView.showsUserLocation=show;
}

-(void)panTo:(CLLocationCoordinate2D)newCenter {
	
    [mapView setCenterCoordinate:newCenter animated:YES];
}

-(void)setZoom:(MKCoordinateRegion)newRegion{
	
	//MKCoordinateRegion rtf=[mapView regionThatFits:newRegion];
	
	[mapView setRegion:newRegion animated:YES];
}

-(double)getZoom{

	double z=[UtilityClass zoomFromSpan:[mapView region].span forMap:mapView];
	return z;
}

-(void)setMapCenter:(CLLocationCoordinate2D)newCenter {
    
	[mapView setCenterCoordinate:newCenter animated:YES];
}

-(CLLocationCoordinate2D)getMapCenter {
    
	return [mapView centerCoordinate];
}

-(void)zoomToRect:(MKMapRect)newRect{
    
    mapView.visibleMapRect=newRect;
}

-(void)addOverlayControl:(MyCustomOverlay *)overlay {

    [mapView addOverlay:overlay.polyline];
    [overlaysArray addObject:overlay];
}

-(void)removeOverlayControlWithOverlayID:(int32_t)myAsId {
    
    MyCustomOverlay *abc=nil;
    for(int i=0;i<[overlaysArray count];i++)
    {
        abc=[overlaysArray objectAtIndex:i];
        if([abc myId]==myAsId)
        {
            [overlaysArray removeObject:abc];
            [mapView removeOverlay:abc.polyline];
            break;
        }
    }
}

-(void)addMarkerAnnotation:(MyCustomAnnotation *)tAnn {
    [mapView addAnnotation:tAnn];
    [annotationsArray addObject:tAnn];
}

-(void)removeMarkerAnnotationWithMarkerID:(int32_t)myAsId {
    
    MyCustomAnnotation *abc=NULL;
    for(int i=0;i<[annotationsArray count];i++)
    {
        abc=[annotationsArray objectAtIndex:i];
        if([abc myId]==myAsId)
        {
            [annotationsArray removeObject:abc];
            [mapView removeAnnotation:abc];
            break;
        }
    }
}

-(void)removeAllMarker {
    
    for (id<MKAnnotation> annotation in self.mapView.annotations) {
        [self.mapView removeAnnotation:annotation];
    }
    
    if(annotationsArray == nil) {
        return;
    }
    
    if( [annotationsArray count] == 0){
        return;
    }
    
    [mapView removeAnnotations:annotationsArray];
    [annotationsArray removeAllObjects];
}

-(void)openMarkerWithMarkerID:(int32_t)param {
    
    MyCustomAnnotation *abc=NULL;
    for(int i=0;i<[annotationsArray count];i++)
    {
        abc=[annotationsArray objectAtIndex:i];
        if([abc myId]==param)
        {
            id<MKAnnotation> myAnnotation = [self.mapView.annotations objectAtIndex:i];
            [self.mapView selectAnnotation:myAnnotation animated:YES];
            int32_t myInt = [abc myId];
            NSString *myOutStr = [NSString stringWithFormat:@"%d", myInt];
            FREDispatchStatusEventAsync(getContext2(), (const uint8_t *)"PIN_SELECTED", (const uint8_t *)myOutStr.UTF8String);
            break;
        }
    }
}

//closes the callout of the pin with the given id
-(void)closeMarkerWithMarkerID:(int32_t)param {
    
    MyCustomAnnotation *abc=NULL;
    for(int i=0;i<[annotationsArray count];i++)
    {
        abc=[annotationsArray objectAtIndex:i];
        if([abc myId]==param)
        {
            id<MKAnnotation> myAnnotation = [self.mapView.annotations objectAtIndex:i];
            [self.mapView deselectAnnotation:myAnnotation animated:YES];
            int32_t myInt = [abc myId];
            NSString *myOutStr = [NSString stringWithFormat:@"%d", myInt];
            FREDispatchStatusEventAsync(getContext2(), (const uint8_t *)"PIN_DESELECTED", (const uint8_t *)myOutStr.UTF8String);
            break;
        }
    }
}

//MKMapView Delegate Event Handlers:
-(void)mapViewWillStartLoadingMap:(MKMapView *)mapView {
    
	FREDispatchStatusEventAsync(getContext2(), (const uint8_t *)"WillStartLoadingMap", (const uint8_t*)"");
}

-(void)mapViewDidFinishLoadingMap:(MKMapView *)mapView {
    
    FREDispatchStatusEventAsync(getContext2(), (const uint8_t *)"DidFinishLoadingMap", (const uint8_t*)"");
}

-(void)mapViewDidFailLoadingMap:(MKMapView *)mapView withError:(NSError *)error {
    
	FREDispatchStatusEventAsync(getContext2(), (const uint8_t *)"DidFailLoadingMap", (const uint8_t*)"");
}

//Annotation Event handlers
-(MKAnnotationView *)mapView:(MKMapView *)mapViewLocal viewForAnnotation:(id<MKAnnotation>)annotation {
    
    MyCustomAnnotation *myAnnotation = annotation;
    
    if([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    MKAnnotationView* pinView=(MKAnnotationView*)[mapViewLocal dequeueReusableAnnotationViewWithIdentifier:[NSString stringWithFormat:@"ViewForId%i", myAnnotation.myId]];
    
    if(!pinView) {
        pinView=[[[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:[NSString stringWithFormat:@"ViewForId%i", myAnnotation.myId]] autorelease];
    } else {
        pinView.annotation=annotation;
    }
    
    
    UIButton *customDisclosureButton = [[[UIButton buttonWithType:UIButtonTypeCustom] retain] autorelease];
    [customDisclosureButton setImage:myAnnotation.getCalloutButtonImage forState:UIControlStateNormal];
    [customDisclosureButton setFrame:CGRectMake(0, 0, 30, 30)];
    pinView.rightCalloutAccessoryView = customDisclosureButton;
   
    if ([myAnnotation.getAnnotationTitle isEqualToString:@""]) {
        
        // This whole block could probably go, as the container's height is zero anyway?
        // TODO check, if components are referenced elsewhere, so the map would crash
        UIView *titleDescriptionPanel = [[[UIView alloc] initWithFrame:CGRectMake(50, 0, 64, 10)] autorelease];
        
        UILabel *titelLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0, 5, 10, 10)] autorelease];
        titelLabel.text = myAnnotation.getAnnotationTitle;
        titelLabel.backgroundColor = [UIColor clearColor];
        titelLabel.textColor = [UIColor whiteColor];
        titelLabel.adjustsFontSizeToFitWidth = NO;
        [titelLabel setFont:[UIFont boldSystemFontOfSize:20]];
        [titleDescriptionPanel addSubview:titelLabel];
        
        UILabel *descriptionLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0, 10, 10, 10)] autorelease];
        descriptionLabel.text = myAnnotation.getAnnotationSubtitle;
        descriptionLabel.backgroundColor = [UIColor clearColor];
        descriptionLabel.adjustsFontSizeToFitWidth = NO;
        descriptionLabel.textColor = [UIColor whiteColor];
        [descriptionLabel setFont:[UIFont systemFontOfSize:10]];
        [titleDescriptionPanel addSubview:descriptionLabel];
        
        [pinView addSubview:titleDescriptionPanel];
        
        pinView.image = myAnnotation.annotationImage;
        pinView.centerOffset = CGPointMake(40, 0);
        
    } else {
        
        UIView *titleDescriptionPanel = [[[UIView alloc] initWithFrame:CGRectMake(40, 0, 81, 64)] autorelease];
        
        UILabel *titelLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0, 5, 60, 25)] autorelease];
        titelLabel.text = myAnnotation.getAnnotationTitle;
        titelLabel.backgroundColor = [UIColor clearColor];
        titelLabel.textColor = [UIColor whiteColor];
        titelLabel.adjustsFontSizeToFitWidth = YES;
        //[titelLabel setFont:[UIFont fontWithName:@"Tele-GroteskNor" size:20.0]];
        [titleDescriptionPanel addSubview:titelLabel];
        
        UILabel *descriptionLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0, 25, 60, 20)] autorelease];
        descriptionLabel.text = myAnnotation.getAnnotationSubtitle;
        descriptionLabel.backgroundColor = [UIColor clearColor];
        descriptionLabel.adjustsFontSizeToFitWidth = NO;
        descriptionLabel.textColor = [UIColor whiteColor];
        [descriptionLabel setFont:[UIFont systemFontOfSize:10]];
        [titleDescriptionPanel addSubview:descriptionLabel];
        
        [pinView addSubview:titleDescriptionPanel];
        
        pinView.image = myAnnotation.annotationImage;
        pinView.centerOffset = CGPointMake(40, 0);
    }

    pinView.enabled = YES;
    pinView.canShowCallout = YES;

    return pinView;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {    
    
    NSString *myOutStr = [NSString stringWithFormat:@"%d", currentSelectedId];
    FREDispatchStatusEventAsync(getContext2(), (const uint8_t *)"ACCESSORY_SELECTED", (const uint8_t *)myOutStr.UTF8String);
}

//Overlay Event Handlers
-(MKOverlayView *)mapView:(MKMapView *)mapViewLocal viewForOverlay:(id<MKOverlay>)overlay {
    
    if([overlay isKindOfClass:[MKPolyline class]])
    {
        MKPolylineView* aView=[[[MKPolylineView alloc] initWithPolyline:overlay] autorelease];
        aView.strokeColor=[[UIColor blueColor] colorWithAlphaComponent:0.5];
        aView.lineWidth=4;
        return aView;
    }
    
    return nil;
}

//select and deselect
-(void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
    if ([view.annotation isKindOfClass:[MKUserLocation class]])
        return; 
    
    MyCustomAnnotation *selAnnotation = view.annotation;
    
    currentSelectedId = -1;
    
    int32_t myInt=[selAnnotation myId];
    NSString *myOutStr = [NSString stringWithFormat:@"%d", myInt];
    FREDispatchStatusEventAsync(getContext2(), (const uint8_t *)"PIN_DESELECTED", (const uint8_t*)myOutStr.UTF8String);
}

-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {

    if ([view.annotation isKindOfClass:[MKUserLocation class]]) {
        
    } else {
        
        MyCustomAnnotation *selAnnotation = view.annotation;
        
        currentSelectedId = [selAnnotation myId];
        
        int32_t myInt = [selAnnotation myId];
        NSString *myOutStr = [NSString stringWithFormat:@"%d", myInt];
        FREDispatchStatusEventAsync(getContext2(), (const uint8_t *)"PIN_SELECTED", (const uint8_t *)myOutStr.UTF8String);
    }
}

- (void)mapView:(MKMapView *)currentMapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    
    // be aware, this gets called several times on startup
    
    MKAnnotationView* annotationView = [currentMapView viewForAnnotation:userLocation];
    annotationView.canShowCallout = NO;
    
    if ([mapView showsUserLocation] == NO) {
        [mapView setShowsUserLocation:YES];
    }
    
    // center on userlocation here? -> don't do this, as the device is countinuously updated
    //self.mapView.centerCoordinate = self.mapView.userLocation.location.coordinate;
}

- (void)locationUpdate:(CLLocation *)location {
    
    [mapView setCenterCoordinate:location.coordinate];
    if ([mapView showsUserLocation] == NO) {
        [mapView setShowsUserLocation:YES];
    }
}

@end
