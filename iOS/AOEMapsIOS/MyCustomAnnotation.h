//
//  MyCustomAnnotation.h
//  MapExplore
//
//  Created by Meet Shah on 9/1/11.
//  Extended by Marcus Toepper on 10/15/12.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MyCustomAnnotation : NSObject<MKAnnotation> {
    
    CLLocationCoordinate2D coordinate;
    NSString * _title;
    NSString * _subtitle;
    int32_t  myId;
    MKPinAnnotationColor markerPinColor;
    
    UIImage *_annotationImage;
    UIImage *_calloutButtonImage;
    
    NSString *_annotationTitle;
    NSString *_annotationSubtitle;
    
}

@property(readonly,nonatomic) int32_t myId;
@property(readwrite,nonatomic) MKPinAnnotationColor markerPinColor;
@property(readwrite,nonatomic) CLLocationCoordinate2D coordinate;

@property(copy) UIImage *annotationImage;
@property(copy) UIImage *calloutButtonImage;
@property(copy) NSString *calloutTitle;
@property(copy) NSString *calloutSubtitle;

-(void)initWithId:(int32_t)anyId andTitle:(NSString *)anyTitle andSubtitle:(NSString *)anySubtitle;
-(NSString *)title;
-(NSString *)subtitle;

- (UIImage *)getAnnotationImage;
- (UIImage *)getCalloutButtonImage;
- (NSString *)getAnnotationTitle;
- (NSString *)getAnnotationSubtitle;

-(void)initWithId:(int32_t)anyId andTitle:(NSString *)anyTitle andSubtitle:(NSString *)anySubtitle andAnnotationImage:(UIImage *)annotationImage andAnnotationTitle:(NSString*)paramAnnotationTitle andAnnotationSubtitle:(NSString *)paramAnnotationSubtitle andCalloutButtonImage:(UIImage *)calloutButtonImage;

- (NSString *)decodeUnicode:(NSString *)unescapedString;

@end
