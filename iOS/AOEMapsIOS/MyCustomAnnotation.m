//
//  MyCustomAnnotation.m
//  MapExplore
//
//  Created by Meet Shah on 9/1/11.
//  Extended by Marcus Toepper on 10/15/12.
//

#import "MyCustomAnnotation.h"


@implementation MyCustomAnnotation
@synthesize myId,markerPinColor;
@synthesize coordinate;

@synthesize annotationImage = _annotationImage;
@synthesize calloutButtonImage = _calloutButtonImage;
@synthesize calloutTitle = _annotationTitle;
@synthesize calloutSubtitle = _annotationSubtitle;

-(void)initWithId:(int32_t)anyId andTitle:(NSString *)anyTitle andSubtitle:(NSString *)anySubtitle {
    
    myId=anyId;
    markerPinColor=MKPinAnnotationColorRed;
    _title=[anyTitle retain];
    _subtitle=[anySubtitle retain];
}

-(NSString *)title {
    
    return _title;
}

-(NSString *)subtitle {
    
    return _subtitle;
}

- (UIImage *)getAnnotationImage {
    
    return _annotationImage;
}

- (UIImage *)getCalloutButtonImage {
    
    return _calloutButtonImage;
}

- (NSString *)getAnnotationTitle {
    
    return _annotationTitle;
}

- (NSString *)getAnnotationSubtitle {
    
    return _annotationSubtitle;
}

-(void)initWithId:(int32_t)anyId andTitle:(NSString *)anyTitle andSubtitle:(NSString *)anySubtitle andAnnotationImage:(UIImage *)paramAnnotationImage andAnnotationTitle:(NSString*)paramAnnotationTitle andAnnotationSubtitle:(NSString *)paramAnnotationSubtitle andCalloutButtonImage:(UIImage *)paramCalloutButtonImage {
    
    myId=anyId;
    markerPinColor=MKPinAnnotationColorRed;
    _title=[anyTitle retain];
    _subtitle=[anySubtitle retain];
    
    _annotationImage = [paramAnnotationImage copy];
    
    _calloutButtonImage = [paramCalloutButtonImage copy];
    _annotationTitle = [paramAnnotationTitle copy];
    _annotationSubtitle = [paramAnnotationSubtitle copy];
}

- (NSString *)decodeUnicode:(NSString *)unescapedString {
    
    unescapedString  = [unescapedString stringByReplacingOccurrencesOfString:@"%" withString:@"\\"];
    
    NSString *convertedString = [unescapedString mutableCopy];
    
    CFStringRef transform = CFSTR("Any-Hex/Java");
    CFStringTransform(( CFMutableStringRef)convertedString, NULL, transform, YES);
    
    return convertedString;
}

@end
